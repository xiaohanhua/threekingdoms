from functools import partial
from settings import*
from sprites import*
from campaign_map import*
import datetime as dt
import pygame as pg
import os
import pickle
import sys

class Load_Game_Scene:
    def __init__(self, parent_scene):
        self.main_menu = parent_scene
        self.game_width, self.game_height = LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT
        if DEMO:
            self.screen = pg.display.set_mode((self.game_width, self.game_height))
        else:
            self.screen = pg.display.set_mode((self.game_width, self.game_height), pg.FULLSCREEN|pg.DOUBLEBUF)
        os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (50, 50)
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()

        pg.mixer.init()
        pg.font.init()
        self.hover_sound = pg.mixer.Sound("sfx_hover.ogg")
        self.click_sound = pg.mixer.Sound("sfx_click.ogg")
        self.deployment_fail_sound = pg.mixer.Sound("sfx_deployment_failed.ogg")


    def new(self):

        self.running = True
        self.playing = True
        pg.mouse.set_visible(True)

        # transitions
        self.transition_sprites = pg.sprite.Group()
        self.transition = None
        self.post_transition_function = None
        self.initiate_transition("in")
        self.selected_save_slot_index = 0
        self.selected_save_slot_index_offset = 0
        self.save_files = self.fetch_save_files()

        self.viewing_decision_interface = False
        self.decision_text = ""

        self.all_sprites = pg.sprite.Group()
        self.buttons = pg.sprite.Group()
        self.decision_sprites = pg.sprite.Group()
        self.save_slot_buttons = []

        self.background = Static(0, self.game_height - 650, "sprite_menu_bg.png")
        self.all_sprites.add(self.background)

        self.black_bg = Static(LANDSCAPE_WIDTH // 2, LANDSCAPE_HEIGHT // 2 - 15, "sprite_battle_preview_bg.png",
                               anchor="center")
        self.all_sprites.add(self.black_bg)

        self.ui_bg = Static(LANDSCAPE_WIDTH // 2, LANDSCAPE_HEIGHT // 2 - 30, "decision_interface_bg_small.png", anchor="center")
        self.all_sprites.add(self.ui_bg)


        for i in range(10):
            save_slot_button = Button(self.ui_bg.rect.left+11, self.ui_bg.rect.top+11+33*i,
                                  "sprite_save_slot_{}.png".format("dark" if i%2 == 0 else "light"),
                                  command=partial(self.update_selected_save_file, i), anchor="topleft")
            self.all_sprites.add(save_slot_button)
            self.buttons.add(save_slot_button)
            self.save_slot_buttons.append(save_slot_button)


        self.save_slot_highlight = Static(-3000, -3000, "sprite_save_slot_highlight.png", anchor="topleft")
        self.all_sprites.add(self.save_slot_highlight)

        self.toggle_country_left_button = Button(175, LANDSCAPE_HEIGHT//2, "menu_button_left.png",
                                                 command=partial(self.toggle_savefile_page, -1))
        self.buttons.add(self.toggle_country_left_button)
        self.all_sprites.add(self.toggle_country_left_button)

        self.toggle_country_right_button = Button(1025, LANDSCAPE_HEIGHT//2, "menu_button_right.png",
                                                  command=partial(self.toggle_savefile_page, 1))
        self.buttons.add(self.toggle_country_right_button)
        self.all_sprites.add(self.toggle_country_right_button)

        self.back_button = Button(LANDSCAPE_WIDTH//2-400, LANDSCAPE_HEIGHT-30, "menu_button_back.png",
                                  command=self.return_to_parent_scene, scale=(210,50))
        self.buttons.add(self.back_button)
        self.all_sprites.add(self.back_button)

        self.load_button = Button(self.save_slot_buttons[0].rect.right-55, self.save_slot_buttons[0].rect.top+4,
                                  "sprite_load_icon.png", command=self.load_game, scale=(25, 25))
        self.buttons.add(self.load_button)
        self.all_sprites.add(self.load_button)

        self.delete_button = Button(self.save_slot_buttons[0].rect.right-20, self.save_slot_buttons[0].rect.top+4,
                                    "sprite_close_button_large.png", command=self.delete_selected_savefile,
                                    scale=(25, 25))
        self.buttons.add(self.delete_button)
        self.all_sprites.add(self.delete_button)

        self.update_selected_save_file(0)
        self.run()


    def initiate_transition(self, direction):
        if direction == "in":
            self.transition = FadeBlackTransition(45, "in")
            self.transition_sprites.add(self.transition)
        else:
            self.transition = FadeBlackTransition(45, "out")
            self.transition_sprites.add(self.transition)


    def transition_clean_up(self):
        self.transition.kill()
        self.transition = None
        self.post_transition_function = None
        self.playing = False
        self.running = False
        for s in self.all_sprites:
            s.kill()


    def load_game(self):
        try:
            filename = list(self.save_files.keys())[self.selected_save_slot_index_offset*10+self.selected_save_slot_index]
            print("Loading saved game from:", filename)
            self.game_data = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, filename), "rb"))
        except Exception as e:
            print("Error loading from file...")
            print(e)
            return
        scene = Campaign_Map_Scene(self, self.game_data)
        scene.new(filename)

    def delete_selected_savefile(self):
        self.viewing_decision_interface = True
        self.decision_bg = Static(LANDSCAPE_WIDTH // 2, LANDSCAPE_HEIGHT // 2, "decision_interface_bg_small.png",
                                  anchor="center")
        self.all_sprites.add(self.decision_bg)
        self.decision_sprites.add(self.decision_bg)

        self.yes_button = Button(self.decision_bg.rect.left + 200, self.decision_bg.rect.bottom - 100,
                                 "menu_button_yes.png",
                                 command=partial(self.confirm_delete_selected_savefile, True))
        self.all_sprites.add(self.yes_button)
        self.buttons.add(self.yes_button)
        self.decision_sprites.add(self.yes_button)

        self.cancel_button = Button(self.decision_bg.rect.right - 200, self.decision_bg.rect.bottom - 100,
                                    "menu_button_cancel.png", command=partial(self.confirm_delete_selected_savefile, False))
        self.all_sprites.add(self.cancel_button)
        self.buttons.add(self.cancel_button)
        self.decision_sprites.add(self.cancel_button)

        self.decision_text = "Delete selected savefile? This cannot be undone."


    def confirm_delete_selected_savefile(self, confirm=False):
        if confirm:
            try:
                filename = list(self.save_files.keys())[self.selected_save_slot_index_offset*10+self.selected_save_slot_index]
                os.remove(os.path.join(SAVE_SLOTS_DIR, filename))
                print("Deleted file:", filename)
                self.save_files = self.fetch_save_files()
            except Exception as e:
                print("Cannot delete file...")
                print(e)

        for sp in self.decision_sprites:
            sp.kill()
        self.viewing_decision_interface = False
        self.decision_text = ""


    def update_selected_save_file(self, i):
        self.save_files = self.fetch_save_files()
        if self.selected_save_slot_index_offset*10 + i >= len(self.save_files):
            self.deployment_fail_sound = pg.mixer.Sound("sfx_deployment_failed.ogg")
            print("No save file selected...")
        else:
            self.selected_save_slot_index = i
            self.save_slot_highlight.rect.left = self.save_slot_buttons[self.selected_save_slot_index].rect.left
            self.save_slot_highlight.rect.top = self.save_slot_buttons[self.selected_save_slot_index].rect.top
            self.load_button.rect.top = self.save_slot_buttons[self.selected_save_slot_index].rect.top + 4
            self.delete_button.rect.top = self.save_slot_buttons[self.selected_save_slot_index].rect.top + 4
            #print(list(self.save_files.keys())[i+self.selected_save_slot_index_offset])


    def fetch_save_files(self, reverse=True, filter="autosave"):
        if not os.path.exists(SAVE_SLOTS_DIR):
            os.mkdir(SAVE_SLOTS_DIR)
        file_dicts = {}
        for filename in os.listdir(SAVE_SLOTS_DIR):
            if filter and filter not in filename.split(".p")[0]:
                if filename[-2::] == ".p":
                    file_dicts[filename] = os.path.getmtime(os.path.join(SAVE_SLOTS_DIR, filename))
        return dict(sorted(file_dicts.items(), key=lambda item: item[1], reverse=reverse))


    def toggle_savefile_page(self, direction):
        self.update_selected_save_file(0)
        self.selected_save_slot_index_offset += direction
        if self.selected_save_slot_index_offset > len(self.save_files)//10:
            self.selected_save_slot_index_offset = 0
        elif self.selected_save_slot_index_offset < 0:
            self.selected_save_slot_index_offset = len(self.save_files)//10


    def run(self):
        while self.playing:
            if self.running:
                self.clock.tick(FPS)
                self.events()
                self.update()
                self.draw()


    def events(self):
        pos = pg.mouse.get_pos()
        for button in self.buttons:
            if button.rect.collidepoint(pos):
                if pg.mouse.get_pressed()[0]:
                    button.pressed = True
                else:
                    button.pressed = False
                if not button.hovering and button not in self.save_slot_buttons:
                    button.hovering = True
                    self.hover_sound.play()
            else:
                button.pressed = False
                button.hovering = False

        for event in pg.event.get():
            if event.type == pg.QUIT:
                choice = pg.display.message_box("Quit",
                                                "Are you sure you wish to quit?",
                                                buttons=("Yes", "No"), return_button=1, escape_button=None
                                                )

                if choice == 0:
                    pg.quit()
                    sys.exit()

            if event.type == pg.MOUSEBUTTONUP:
                self.click_sound.play()
                for button in self.buttons:
                    if button.rect.collidepoint(pos) and (not self.viewing_decision_interface or button in self.decision_sprites):
                        button.pressed = False
                        button.command()
                        break
                if self.load_button.rect.collidepoint(pos):
                    self.load_button.command()
                elif self.delete_button.rect.collidepoint(pos):
                    self.delete_button.command()

            if event.type == pg.KEYUP:
                if event.key == pg.K_ESCAPE:
                    self.return_to_parent_scene()


    def return_to_parent_scene(self):
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = self.return_to_parent_scene
        elif not self.transition.fading:
            self.transition_clean_up()
            self.main_menu.new(start_music=False)


    def update(self):
        self.all_sprites.update()
        if self.transition:
            if self.transition.fading:
                self.transition.update()
            else:
                if self.post_transition_function:
                    self.post_transition_function()
                else:
                    self.transition = None


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)

            self.draw_text("Choose a file to load:", FONT_FUNCTION(DEFAULT_FONT, 24), WHITE, LANDSCAPE_WIDTH//2,
                           50, align="center")

            save_filenames = list(self.save_files.keys())
            for i in range(self.selected_save_slot_index_offset*10, self.selected_save_slot_index_offset*10+10):
                try:
                    filename = save_filenames[i]
                    corresponding_slot_button = self.save_slot_buttons[i%10]
                    self.draw_text(filename.replace(".p", ""), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                           corresponding_slot_button.rect.left+10, corresponding_slot_button.rect.top+5, align="topleft")
                    self.draw_text("{}".format(dt.datetime.fromtimestamp(self.save_files[filename]).strftime('%Y-%m-%d %H:%M:%S')),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                   corresponding_slot_button.rect.right-250, corresponding_slot_button.rect.top+5,
                                   align="topleft")
                except Exception as e:
                    pass


            self.decision_sprites.draw(self.screen)
            # DECISIONS CAPTION
            if self.decision_text:
                text_size = 600 // len(self.decision_text) + 15
                line_count = 0
                for line in self.decision_text.split("\n"):
                    self.draw_text(line, FONT_FUNCTION(DEFAULT_FONT, text_size), BLACK, self.game_width // 2,
                                   self.game_height // 3 + line_count * (text_size + 5))
                    line_count += 1

            self.transition_sprites.draw(self.screen)
            pg.display.flip()


    def draw_text(self, text, font, color, x, y, align="midtop"):
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        if align == "midleft":
            text_rect.midleft = (x, y)
        elif align == "topleft":
            text_rect.topleft = (x, y)
        else:
            text_rect.midtop = (x, y)
        self.screen.blit(text_surface, text_rect)


    def drawWrappedText(self, text, color, rect, font, aa=False, bkg=None):
        rect = pg.Rect(rect)
        y = rect.top
        lineSpacing = 10

        # get the height of the font
        fontHeight = font.size("Tg")[1]

        while text:
            i = 1

            # determine if the row of text will be outside our area
            if y + fontHeight > rect.bottom:
                break

            # determine maximum width of line
            while font.size(text[:i])[0] < rect.width and i < len(text):
                i += 1

            # if we've wrapped the text, then adjust the wrap to the last word
            if i < len(text):
                i = text.rfind(" ", 0, i) + 1

            # render the line and blit it to the surface
            if bkg:
                image = font.render(text[:i], 1, color, bkg)
                image.set_colorkey(bkg)
            else:
                image = font.render(text[:i], aa, color)

            self.screen.blit(image, (rect.left, y))
            y += fontHeight + lineSpacing

            # remove the text we just blitted
            text = text[i:]

        return text