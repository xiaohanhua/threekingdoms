from functools import partial
from settings import*
from attribution_text import ATTRIBUTION_STR
from sprites import*
import pygame as pg
import os

class Attribution_Scene:
    def __init__(self, parent_scene):
        self.main_menu = parent_scene
        self.game_width, self.game_height = LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT
        if DEMO:
            self.screen = pg.display.set_mode((self.game_width, self.game_height))
        else:
            self.screen = pg.display.set_mode((self.game_width, self.game_height), pg.FULLSCREEN|pg.DOUBLEBUF)
        os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (50, 50)
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()

        pg.mixer.init()
        pg.font.init()
        self.hover_sound = pg.mixer.Sound("sfx_hover.ogg")
        self.click_sound = pg.mixer.Sound("sfx_click.ogg")
        self.deployment_fail_sound = pg.mixer.Sound("sfx_deployment_failed.ogg")


    def new(self):

        self.running = True
        self.playing = True
        self.scroll_index = 0
        pg.mouse.set_visible(True)

        # transitions
        self.transition_sprites = pg.sprite.Group()
        self.transition = None
        self.post_transition_function = None
        self.initiate_transition("in")

        self.all_sprites = pg.sprite.Group()
        self.buttons = pg.sprite.Group()

        self.background = Static(0, self.game_height - 650, "sprite_menu_bg.png")
        self.all_sprites.add(self.background)

        self.back_button = Button(LANDSCAPE_WIDTH//2-450, LANDSCAPE_HEIGHT-30, "menu_button_back.png",
                                  command=self.return_to_parent_scene, scale=(210,50))
        self.buttons.add(self.back_button)
        self.all_sprites.add(self.back_button)

        lines = ATTRIBUTION_STR.split("\n")
        self.attribution_lines_length = len(lines)
        self.attribution_surface = pg.Surface((1200, self.attribution_lines_length*100))
        self.attribution_surface.set_alpha(200)
        line_number = 0
        font = FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE)
        for line in lines:
            if "<<<<<" in line:
                text_color = YELLOW
            else:
                text_color = WHITE

            if "***" in line:
                text_surface = FONT_FUNCTION(DEFAULT_FONT, 24).render(line, True, GREEN)
            else:
                text_surface = font.render(line, True, text_color)
            text_rect = text_surface.get_rect()
            text_rect.center = LANDSCAPE_WIDTH // 2, 650 + 30 * line_number
            self.attribution_surface.blit(text_surface, text_rect)
            line_number += 1

        self.run()


    def initiate_transition(self, direction):
        if direction == "in":
            self.transition = FadeBlackTransition(45, "in")
            self.transition_sprites.add(self.transition)
        else:
            self.transition = FadeBlackTransition(45, "out")
            self.transition_sprites.add(self.transition)


    def transition_clean_up(self):
        self.transition.kill()
        self.transition = None
        self.post_transition_function = None
        self.playing = False
        self.running = False
        for s in self.all_sprites:
            s.kill()


    def run(self):
        while self.playing:
            if self.running:
                self.clock.tick(FPS)
                self.events()
                self.update()
                self.draw()


    def events(self):
        pos = pg.mouse.get_pos()
        for button in self.buttons:
            if button.rect.collidepoint(pos):
                if pg.mouse.get_pressed()[0]:
                    button.pressed = True
                else:
                    button.pressed = False
                if not button.hovering:
                    button.hovering = True
                    self.hover_sound.play()
            else:
                button.pressed = False
                button.hovering = False

        for event in pg.event.get():
            if event.type == pg.MOUSEBUTTONUP:
                self.click_sound.play()
                for button in self.buttons:
                    if button.rect.collidepoint(pos):
                        button.pressed = False
                        button.command()
                        break

            if event.type == pg.KEYUP:
                if event.key == pg.K_ESCAPE:
                    self.return_to_parent_scene()

            if event.type == pg.QUIT:
                choice = pg.display.message_box("Quit",
                                                "Are you sure you wish to quit?",
                                                buttons=("Yes", "No"), return_button=1, escape_button=None
                                                )

                if choice == 0:
                    pg.quit()
                    sys.exit()

    def return_to_parent_scene(self):
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = self.return_to_parent_scene
        elif not self.transition.fading:
            self.transition_clean_up()
            self.main_menu.new(start_music=False)


    def update(self):
        self.all_sprites.update()
        self.scroll_index += 1
        if self.scroll_index >= self.attribution_lines_length*30 + 700:
            self.scroll_index = 0
        if self.transition:
            if self.transition.fading:
                self.transition.update()
            else:
                if self.post_transition_function:
                    self.post_transition_function()
                else:
                    self.transition = None


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)
            self.screen.blit(self.attribution_surface,
                             pg.Rect((0, -self.scroll_index), (1100, self.attribution_lines_length*100)))
            self.buttons.draw(self.screen)

            self.transition_sprites.draw(self.screen)
            pg.display.flip()


    def draw_text(self, text, font, color, x, y, align="midtop"):
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        if align == "midleft":
            text_rect.midleft = (x, y)
        elif align == "topleft":
            text_rect.topleft = (x, y)
        else:
            text_rect.midtop = (x, y)
        self.screen.blit(text_surface, text_rect)


    def drawWrappedText(self, text, color, rect, font, aa=False, bkg=None):
        rect = pg.Rect(rect)
        y = rect.top
        lineSpacing = 10

        # get the height of the font
        fontHeight = font.size("Tg")[1]

        while text:
            i = 1

            # determine if the row of text will be outside our area
            if y + fontHeight > rect.bottom:
                break

            # determine maximum width of line
            while font.size(text[:i])[0] < rect.width and i < len(text):
                i += 1

            # if we've wrapped the text, then adjust the wrap to the last word
            if i < len(text):
                i = text.rfind(" ", 0, i) + 1

            # render the line and blit it to the surface
            if bkg:
                image = font.render(text[:i], 1, color, bkg)
                image.set_colorkey(bkg)
            else:
                image = font.render(text[:i], aa, color)

            self.screen.blit(image, (rect.left, y))
            y += fontHeight + lineSpacing

            # remove the text we just blitted
            text = text[i:]

        return text