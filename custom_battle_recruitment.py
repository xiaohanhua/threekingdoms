from battle import*
from functools import partial

class Dummy:
    def __init__(self):
        self.enemy_unit_sprites = pg.sprite.Group()
        self.all_sprites = pg.sprite.Group()
        self.terrain = ""


class Custom_Battle_Recruitment_Scene:
    def __init__(self, parent_scene, player_general, enemy_general, terrain, army_size, difficulty):
        self.parent_scene = parent_scene
        self.player_general = player_general
        self.enemy_general = enemy_general
        self.terrain = terrain
        self.budget = army_size
        self.difficulty = difficulty
        self.enemy_budget = int(self.budget*(1+(self.difficulty*1.5-1)/10))
        self.game_width, self.game_height = LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT
        if DEMO:
            self.screen = pg.display.set_mode((self.game_width, self.game_height))
        else:
            self.screen = pg.display.set_mode((self.game_width, self.game_height), pg.FULLSCREEN|pg.DOUBLEBUF)
        os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (50, 50)
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()

        pg.mixer.init()
        pg.font.init()
        self.hover_sound = pg.mixer.Sound("sfx_hover.ogg")
        self.click_sound = pg.mixer.Sound("sfx_click.ogg")
        self.morale_boost_sound = pg.mixer.Sound("sfx_restore_status.ogg")


    def new(self):
        self.running = True
        self.playing = True
        pg.mouse.set_visible(True)
        self.cheat_code = ""

        # transitions
        self.transition_sprites = pg.sprite.Group()
        self.transition = None
        self.post_transition_function = None
        self.initiate_transition("in")

        self.all_sprites = pg.sprite.Group()
        self.recruitment_sprites = pg.sprite.Group()
        self.buttons = pg.sprite.Group()
        self.unit_stats_sprites = pg.sprite.Group()

        self.player_army_composition = {}
        self.enemy_army_composition = {}
        self.acquired_tech = {"Shu": [],
                              "Wu": [],
                              "Wei": [],
                              "Ma Teng": [],
                              "Liu Biao": [],
                              "Liu Zhang": [],
                              "Zhang Lu": [],
                              "Yuan Shao": [],
                              }
        self.city_sprites = []

        self.background = Static(0, self.game_height - 650, "sprite_menu_bg.png")
        self.all_sprites.add(self.background)

        self.recruitment_bg = Static(LANDSCAPE_WIDTH // 2, LANDSCAPE_HEIGHT // 2 - 30, "sprite_battle_preview_bg.png", anchor="center")
        self.all_sprites.add(self.recruitment_bg)

        self.gold_icon = Static(150, 20, "sprite_gold_tiny.png", anchor="midleft")
        self.all_sprites.add(self.gold_icon)


        self.recruit_unit_button = Button(self.recruitment_bg.rect.right - 350, self.recruitment_bg.rect.bottom - 50,
                                          "campaign_button_battle_preview_recruit.png",
                                          command=self.recruit_selected_unit, scale=(200, 50))
        self.all_sprites.add(self.recruit_unit_button)
        self.buttons.add(self.recruit_unit_button)

        self.disband_button = Button(LANDSCAPE_WIDTH - 225, self.recruitment_bg.rect.bottom - 50,
                                     "menu_button_disband.png", command=self.disband_selected_unit, scale=(200, 50))
        self.buttons.add(self.disband_button)
        self.all_sprites.add(self.disband_button)

        self.recruit_quantity_label = Static(self.recruitment_bg.rect.right - 600, self.recruitment_bg.rect.bottom - 53,
                                             "sprite_unit_quantity_label.png", anchor="center")
        self.all_sprites.add(self.recruit_quantity_label)

        self.plus_button = Button(self.recruit_quantity_label.rect.left + 120,
                                  self.recruit_quantity_label.rect.top + 25,
                                  "menu_button_plus.png", command=partial(self.toggle_recruitment_quantity, 10))
        self.all_sprites.add(self.plus_button)
        self.buttons.add(self.plus_button)
        self.recruitment_sprites.add(self.plus_button)

        self.minus_button = Button(self.recruit_quantity_label.rect.left + 17,
                                   self.recruit_quantity_label.rect.top + 25,
                                   "menu_button_minus.png", command=partial(self.toggle_recruitment_quantity, -10))
        self.all_sprites.add(self.minus_button)
        self.buttons.add(self.minus_button)
        self.recruitment_sprites.add(self.minus_button)


        self.back_button = Button(LANDSCAPE_WIDTH//2-300, LANDSCAPE_HEIGHT-30, "menu_button_back.png",
                                  command=self.return_to_parent_scene, scale=(210,50))
        self.buttons.add(self.back_button)
        self.all_sprites.add(self.back_button)

        self.next_button = Button(LANDSCAPE_WIDTH//2+300, LANDSCAPE_HEIGHT-30, "menu_button_next.png",
                                  command=self.load_battle, scale=(210,50))
        self.buttons.add(self.next_button)
        self.all_sprites.add(self.next_button)

        self.recruitment_limit = 1000
        self.page_number = 0 #in case > 20 recruitable units
        self.selected_unit_index = 0
        self.recruitable_units_icons = []
        self.cost_icons = []
        self.unit_recruitment_cost = UNIT_RECRUITMENT_COST.copy()
        self.recruitable_units = RECRUITABLE_UNIT_DICT[4].copy()

        self.recruitment_quantity = 10
        self.recruitment_message = ""
        self.game_message_color = GREEN
        self.last_displayed_message = 0

        self.recruitable_units += ["flaming_arrow_infantry", "crossbow_infantry", "catapult"]
        if self.player_general.country == "Shu":
            self.recruitable_units += ["barbarian_spear_woman", "barbarian_assassin", "barbarian_archer", "rattan_armor_infantry"]
        if self.player_general.country == "Wei":
            self.recruitable_units += ["tiger_knight_cavalry"]
        if self.player_general.name in ["Zhu Rong", "Meng Huo"]:
            self.recruitable_units.append("elephant_rider")
        if self.player_general.name in ["Ding Feng"]:
            self.recruitable_units.append("light_dagger_infantry")

        if len(self.recruitable_units) > 20:
            self.toggle_page_left_button = Button(150, LANDSCAPE_HEIGHT//2, "menu_button_left.png",
                                                  command=partial(self.toggle_page, -1))
            self.buttons.add(self.toggle_page_left_button)
            self.all_sprites.add(self.toggle_page_left_button)

            self.toggle_page_right_button = Button(700, LANDSCAPE_HEIGHT//2, "menu_button_right.png",
                                                   command=partial(self.toggle_page, 1))
            self.buttons.add(self.toggle_page_right_button)
            self.all_sprites.add(self.toggle_page_right_button)

        self.load_recruitable_units()
        self.update_recruit_button_status()
        self.load_unit_stats()
        self.run()


    def toggle_page(self, direction):
        self.page_number -= direction
        if self.page_number < 0:
            self.page_number = len(self.recruitable_units)//20
        elif self.page_number > len(self.recruitable_units)//20:
            self.page_number = 0
        self.selected_unit_index = 20*self.page_number
        self.load_recruitable_units()
        self.load_unit_stats()
        self.update_recruit_button_status()


    def load_recruitable_units(self):
        for sp in self.recruitable_units_icons:
            sp.kill()
        for sp in self.cost_icons:
            sp.kill()
        self.recruitable_units_icons = []
        self.cost_icons = []
        max_col = 5
        current_col = 0

        for i in range(self.page_number*20, self.page_number*20 + 20):
            if i < len(self.recruitable_units):
                row = i%20 // max_col
                unit_type = self.recruitable_units[i]
                unit_icon = UnitIcon(self, "left", current_col * 100 + 200, row * 120 + 60, unit_type,
                                     self.player_general.country.lower())
                if i == self.selected_unit_index:
                    unit_icon.selected = True
                self.recruitable_units_icons.append(unit_icon)
                self.all_sprites.add(unit_icon)

                cost_icon = Static(current_col * 100 + 200, row * 120 + 125, "sprite_gold_tiny.png")
                self.cost_icons.append(cost_icon)
                self.all_sprites.add(cost_icon)

                current_col += 1
                if current_col >= max_col:
                    current_col = 0


    def initiate_transition(self, direction):
        if direction == "in":
            self.transition = FadeBlackTransition(45, "in")
            self.transition_sprites.add(self.transition)
        else:
            self.transition = FadeBlackTransition(45, "out")
            self.transition_sprites.add(self.transition)


    def transition_clean_up(self):
        self.transition.kill()
        self.transition = None
        self.post_transition_function = None
        self.playing = False
        self.running = False
        for s in self.all_sprites:
            s.kill()


    def update_recruit_button_status(self):
        if self.recruitment_quantity*self.unit_recruitment_cost[self.recruitable_units[self.selected_unit_index]] > self.budget:
            self.recruit_unit_button.disabled = True
        else:
            self.recruit_unit_button.disabled = False

        selected_unit = self.recruitable_units[self.selected_unit_index]
        self.disband_button.disabled = True
        if selected_unit in self.player_army_composition:
            if self.player_army_composition[selected_unit] >= self.recruitment_quantity:
                self.disband_button.disabled = False


    def toggle_recruitment_quantity(self, delta):
        self.recruitment_quantity += delta
        if self.recruitment_quantity <= 10:
            self.recruitment_quantity = 10
        elif self.recruitment_quantity >= 999:
            self.recruitment_quantity = 999

        self.update_recruit_button_status()


    def recruit_selected_unit(self):
        selected_unit = self.recruitable_units[self.selected_unit_index]
        if selected_unit not in self.player_army_composition and len(self.player_army_composition) >= 8:
            self.recruitment_message = "Cannot have more than 8 different unit types."
            self.game_message_color = BRIGHT_ORANGE
            self.last_displayed_message = pg.time.get_ticks()
            return

        if selected_unit in self.player_army_composition:
            self.player_army_composition[selected_unit] += self.recruitment_quantity
        else:
            self.player_army_composition[selected_unit] = self.recruitment_quantity
        self.budget -= self.recruitment_quantity * self.unit_recruitment_cost[selected_unit]
        self.update_recruit_button_status()

        self.morale_boost_sound.play()
        self.recruitment_message = "Recruited {} x {}!".format(UNIT_FRIENDLY_NAMES[selected_unit], self.recruitment_quantity)
        self.game_message_color = GREEN
        self.last_displayed_message = pg.time.get_ticks()


    def load_unit_stats(self):
        for sp in self.unit_stats_sprites:
            sp.kill()
        selected_unit = self.recruitable_units[self.selected_unit_index]
        sample_unit = self.generate_sample_unit(selected_unit)
        avatar_filename = "sprite_{}_icon_{}.png".format(selected_unit, self.player_general.country.lower())
        tooltip_filename = "sprite_{}_tooltip.png".format(selected_unit)

        tooltip = Static(725, 60, tooltip_filename)
        self.unit_stats_sprites.add(tooltip)
        self.recruitment_sprites.add(tooltip)

        tooltip_avatar = Static(tooltip.rect.left + 16, tooltip.rect.top + 16, avatar_filename, anchor="topleft")
        self.unit_stats_sprites.add(tooltip_avatar)
        self.recruitment_sprites.add(tooltip_avatar)

        cell_filename = "sprite_stat_single_cell.png"
        if sample_unit.unit_class == "ranged":

            health_cell_length = int(sample_unit.health/20)
            for i in range(health_cell_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 110, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            attacking_range_cell_length = sample_unit.attacking_range // 100
            for i in range(attacking_range_cell_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 146, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            firing_rate_cell_length = int((sample_unit.firing_rate - .5) * 10) + 1
            for i in range(firing_rate_cell_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 182, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            firing_accuracy_cell_length = int(sample_unit.firing_accuracy * 10)
            for i in range(firing_accuracy_cell_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 218, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            damage_cell_length = int(sample_unit.firing_damage)
            for i in range(damage_cell_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 254, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            fatal_strike_rate_length = int(sample_unit.fatal_strike_rate * 100)
            for i in range(fatal_strike_rate_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 290, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            defense_length = int(sample_unit.defense)
            for i in range(defense_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 326, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            speed_length = int(sample_unit.walking_acc)
            for i in range(speed_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 362, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            unit_cost_length = int(UNIT_COMMAND_COST[selected_unit] // 5)
            for i in range(unit_cost_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 398, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

        elif sample_unit.unit_class == "cavalry":
            health_cell_length = int(sample_unit.health/20)
            for i in range(health_cell_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 110, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            attacking_range_cell_length = int(sample_unit.attacking_range / 10)
            for i in range(attacking_range_cell_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 146, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            damage_cell_length = int(sample_unit.attack)
            for i in range(damage_cell_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 182, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            fatal_strike_rate_length = int(sample_unit.fatal_strike_rate * 100)
            for i in range(fatal_strike_rate_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 218, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            defense_length = int(sample_unit.defense)
            for i in range(defense_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 254, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            speed_length = int(sample_unit.walking_acc)
            for i in range(speed_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 290, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            charge_range_length = int(sample_unit.max_charge_range)//50
            for i in range(charge_range_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 326, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            unit_cost_length = int(UNIT_COMMAND_COST[selected_unit] // 5)
            for i in range(unit_cost_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 362, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

        else:
            health_cell_length = int(sample_unit.health/20)
            for i in range(health_cell_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 110, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            attacking_range_cell_length = int(sample_unit.attacking_range/10)
            for i in range(attacking_range_cell_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 146, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            damage_cell_length = int(sample_unit.attack)
            for i in range(damage_cell_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 182, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            fatal_strike_rate_length = int(sample_unit.fatal_strike_rate * 100)
            for i in range(fatal_strike_rate_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 218, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            defense_length = int(sample_unit.defense)
            for i in range(defense_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 254, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            speed_length = int(sample_unit.walking_acc)
            for i in range(speed_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 290, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            unit_cost_length = int(UNIT_COMMAND_COST[selected_unit] // 5)
            for i in range(unit_cost_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 326, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)


    def generate_sample_unit(self, unit_type):
        dummy = Dummy()
        if unit_type == "spear_militia":
            return Spear_Militia(dummy, 0, 0, unit_type,
                                 self.player_general.country.lower(), "left", 0)

        elif unit_type == "sword_militia":
            return Sword_Militia(dummy, 0, 0, unit_type,
                                 self.player_general.country.lower(), "left", 0)

        elif unit_type == "axe_militia":
            return Axe_Militia(dummy, 0, 0, unit_type,
                                 self.player_general.country.lower(), "left", 0)

        elif unit_type == "bow_militia":
            return Bow_Militia(dummy, 0, 0, unit_type,
                               self.player_general.country.lower(), "left", 0)

        elif unit_type == "barbarian_spear_woman":
            return Barbarian_Spear_Woman(dummy, 0, 0, unit_type,
                                 self.player_general.country.lower(), "left", 0)

        elif unit_type == "barbarian_assassin":
            return Barbarian_Assassin(dummy, 0, 0, unit_type,
                                 self.player_general.country.lower(), "left", 0)

        elif unit_type == "barbarian_archer":
            return Barbarian_Archer(dummy, 0, 0, unit_type,
                                 self.player_general.country.lower(), "left", 0)

        elif unit_type == "rattan_armor_infantry":
            return Rattan_Armor_Infantry(dummy, 0, 0, unit_type,
                                 self.player_general.country.lower(), "left", 0)

        elif unit_type == "spear_infantry":
            return Spear_Infantry(dummy, 0, 0, unit_type,
                                 self.player_general.country.lower(), "left", 0)

        elif unit_type == "longspear_infantry":
            return Longspear_Infantry(dummy, 0, 0, unit_type,
                                 self.player_general.country.lower(), "left", 0)

        elif unit_type == "elite_spear_infantry":
            return Elite_Spear_Infantry(dummy, 0, 0, unit_type,
                                 self.player_general.country.lower(), "left", 0)

        elif unit_type == "light_dagger_infantry":
            return Light_Dagger_Infantry(dummy, 0, 0, unit_type,
                                 self.player_general.country.lower(), "left", 0)

        elif unit_type == "sword_infantry":
            return Sword_Infantry(dummy, 0, 0, unit_type,
                                 self.player_general.country.lower(), "left", 0)

        elif unit_type == "bow_infantry":
            return Bow_Infantry(dummy, 0, 0, unit_type,
                                 self.player_general.country.lower(), "left", 0)

        elif unit_type == "flaming_arrow_infantry":
            return Flaming_Arrow_Infantry(dummy, 0, 0, unit_type,
                                 self.player_general.country.lower(), "left", 0)

        elif unit_type == "crossbow_infantry":
            return Crossbow_Infantry(dummy, 0, 0, unit_type,
                                 self.player_general.country.lower(), "left", 0)

        elif unit_type == "catapult":
            return Catapult(dummy, 0, 0, unit_type,
                                 self.player_general.country.lower(), "left", 0)

        elif unit_type == "shielded_sword_infantry":
            return Shielded_Sword_Infantry(dummy, 0, 0, unit_type,
                                 self.player_general.country.lower(), "left", 0)

        elif unit_type == "elite_sword_infantry":
            return Elite_Sword_Infantry(dummy, 0, 0, unit_type,
                                 self.player_general.country.lower(), "left", 0)

        elif unit_type == "mace_infantry":
            return Mace_Infantry(dummy, 0, 0, unit_type,
                                 self.player_general.country.lower(), "left", 0)

        elif unit_type == "battleaxe_infantry":
            return Battleaxe_Infantry(dummy, 0, 0, unit_type,
                                 self.player_general.country.lower(), "left", 0)

        elif unit_type == "elephant_rider":
            return Elephant_Rider(dummy, 0, 0, unit_type,
                                 self.player_general.country.lower(), "left", 0)

        elif unit_type == "halberd_cavalry":
            return Halberd_Cavalry(dummy, 0, 0, unit_type,
                                  self.player_general.country.lower(), "left", 0)

        elif unit_type == "light_cavalry":
            return Light_Cavalry(dummy, 0, 0, unit_type,
                                  self.player_general.country.lower(), "left", 0)

        elif unit_type == "tiger_knight_cavalry":
            return Tiger_Knight_Cavalry(dummy, 0, 0, unit_type,
                                  self.player_general.country.lower(), "left", 0)



    def disband_selected_unit(self):
        selected_unit_name = self.recruitable_units[self.selected_unit_index]
        cost_per_unit = self.unit_recruitment_cost[selected_unit_name]
        if self.recruitment_quantity > self.player_army_composition[selected_unit_name]:
            quantity = self.player_army_composition[selected_unit_name]
        else:
            quantity = self.recruitment_quantity

        self.player_army_composition[selected_unit_name] -= quantity
        if self.player_army_composition[selected_unit_name] == 0:
            del self.player_army_composition[selected_unit_name]
        self.budget += quantity*cost_per_unit
        self.update_recruit_button_status()


    def load_battle(self):
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = self.load_battle
        elif not self.transition.fading:
            self.transition_clean_up()
            self.enemy_recruit_units()
            battle = Battle(self, self.player_general, self.enemy_general, player_army_object=self.player_army_composition,
                            enemy_army_object=self.enemy_army_composition, terrain=self.terrain)
            battle.new()


    def enemy_recruit_units(self):
        recruitable_units = RECRUITABLE_UNIT_DICT[4].copy()
        if self.enemy_general.country == "Shu":
            recruitable_units += ["barbarian_spear_woman", "barbarian_assassin", "barbarian_archer", "rattan_armor_infantry"]
        if self.enemy_general.country == "Wei":
            recruitable_units += ["tiger_knight_cavalry"]
        if self.enemy_general.name in ["Zhu Rong", "Meng Huo"]:
            recruitable_units.append("elephant_rider")
        if self.enemy_general.name in ["Ding Feng"]:
            self.recruitable_units.append("light_dagger_infantry")

        temp_unit_recruitment_cost = UNIT_RECRUITMENT_COST.copy()
        failures = 0
        while self.enemy_budget >= 170 and failures <= 30:
            random_unit = choice([unit for unit in recruitable_units if temp_unit_recruitment_cost[unit]*10 <= self.enemy_budget])
            cost = temp_unit_recruitment_cost[random_unit]*10
            recruit_random_unit = False
            if len(self.enemy_army_composition) >= 8 and random_unit not in self.enemy_army_composition:
                failures += 1
            elif random_unit in RANGED_UNITS and random() <= .4 and cost <= self.enemy_budget and cost/100 >= random():
                recruit_random_unit = True
            elif random_unit not in RANGED_UNITS and random() <= .8 and cost <= self.enemy_budget and cost/100 >= random():
                recruit_random_unit = True

            if recruit_random_unit:
                self.enemy_budget -= cost
                if random_unit in self.enemy_army_composition:
                    self.enemy_army_composition[random_unit] += 10
                else:
                    self.enemy_army_composition[random_unit] = 10



    def run(self):
        while self.playing:
            if self.running:
                self.clock.tick(FPS)
                self.events()
                self.update()
                self.draw()


    def events(self):
        pos = pg.mouse.get_pos()
        for button in self.buttons:
            if button.rect.collidepoint(pos):
                if pg.mouse.get_pressed()[0]:
                    button.pressed = True
                else:
                    button.pressed = False
                if not button.hovering:
                    button.hovering = True
                    self.hover_sound.play()
            else:
                button.pressed = False
                button.hovering = False

        for event in pg.event.get():
            if event.type == pg.QUIT:
                choice = pg.display.message_box("Quit",
                                                "Are you sure you wish to quit?",
                                                buttons=("Yes", "No"), return_button=1, escape_button=None
                                                )

                if choice == 0:
                    pg.quit()
                    sys.exit()

            if event.type == pg.MOUSEBUTTONUP:
                self.click_sound.play()
                for button in self.buttons:
                    if button.rect.collidepoint(pos) and not button.disabled:
                        button.pressed = False
                        button.command()
                        break
                for sp in self.recruitable_units_icons:
                    if sp.rect.collidepoint(pos):
                        self.recruitable_units_icons[self.selected_unit_index%20].selected = False
                        self.selected_unit_index = self.recruitable_units_icons.index(sp) + self.page_number*20
                        sp.selected = True
                        self.load_unit_stats()
                        self.update_recruit_button_status()

            if event.type == pg.KEYUP:
                if event.key == pg.K_ESCAPE:
                    self.return_to_parent_scene()

                elif event.key == pg.K_c and self.cheat_code == "":
                    self.cheat_code += "c"

                elif event.key == pg.K_o and self.cheat_code == "c":
                    self.cheat_code += "o"

                elif event.key == pg.K_i and self.cheat_code == "co":
                    self.cheat_code += "i"

                elif event.key == pg.K_n and self.cheat_code == "coi":
                    self.cheat_code = ""
                    self.budget += 100000

                else:
                    self.cheat_code = ""


    def return_to_parent_scene(self):
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = self.return_to_parent_scene
        elif not self.transition.fading:
            self.transition_clean_up()
            self.parent_scene.new()


    def update(self):
        self.all_sprites.update()
        if self.transition:
            if self.transition.fading:
                self.transition.update()
            else:
                if self.post_transition_function:
                    self.post_transition_function()
                else:
                    self.transition = None


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)
            self.recruitment_sprites.draw(self.screen)

            self.draw_text("Recruit units to your army:", FONT_FUNCTION(DEFAULT_FONT, 20), WHITE, LANDSCAPE_WIDTH//2,
                           10, align="center")

            self.draw_text("{}".format(self.budget),
                           FONT_FUNCTION(DEFAULT_FONT, 22), WHITE, self.recruitment_bg.rect.left + 75,
                           20, align="midleft")

            self.draw_text("Quantity:", FONT_FUNCTION(DEFAULT_FONT, 22), WHITE,
                           self.recruitment_bg.rect.left + 200, self.recruitment_bg.rect.bottom - 55,
                           align="midleft")

            self.draw_text("{}".format(self.recruitment_quantity),
                           FONT_FUNCTION(DEFAULT_FONT, 20), WHITE, self.recruit_quantity_label.rect.centerx,
                           self.recruit_quantity_label.rect.centery - 15, align="center")

            max_col = 5
            current_col = 0
            for i in range(self.page_number * 20, self.page_number * 20 + 20):
                if i < len(self.recruitable_units):
                    row = i%20 // max_col
                    unit_type = self.recruitable_units[i]
                    self.draw_text("{}".format(self.unit_recruitment_cost[unit_type]), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                                   YELLOW, current_col * 100 + 240, row * 120 + 125)

                    # display how much you have of this unit in garrison already
                    garrisoned_quantity = 0
                    if unit_type in self.player_army_composition:
                        garrisoned_quantity = self.player_army_composition[unit_type]
                    self.draw_text("{}".format(garrisoned_quantity), FONT_FUNCTION(DEFAULT_FONT, 14),
                                   WHITE, current_col * 100 + 255, row * 120 + 105)

                    current_col += 1
                    if current_col >= max_col:
                        current_col = 0

            if pg.time.get_ticks() - self.last_displayed_message <= 2000:
                self.draw_text(self.recruitment_message, FONT_FUNCTION(DEFAULT_FONT, 24), self.game_message_color,
                               LANDSCAPE_WIDTH // 2, LANDSCAPE_HEIGHT // 2)

            self.transition_sprites.draw(self.screen)
            pg.display.flip()


    def draw_text(self, text, font, color, x, y, align="midtop"):
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        if align == "midleft":
            text_rect.midleft = (x, y)
        elif align == "topleft":
            text_rect.topleft = (x, y)
        else:
            text_rect.midtop = (x, y)
        self.screen.blit(text_surface, text_rect)


    def drawWrappedText(self, text, color, rect, font, aa=False, bkg=None):
        rect = pg.Rect(rect)
        y = rect.top
        lineSpacing = 10

        # get the height of the font
        fontHeight = font.size("Tg")[1]

        while text:
            i = 1

            # determine if the row of text will be outside our area
            if y + fontHeight > rect.bottom:
                break

            # determine maximum width of line
            while font.size(text[:i])[0] < rect.width and i < len(text):
                i += 1

            # if we've wrapped the text, then adjust the wrap to the last word
            if i < len(text):
                i = text.rfind(" ", 0, i) + 1

            # render the line and blit it to the surface
            if bkg:
                image = font.render(text[:i], 1, color, bkg)
                image.set_colorkey(bkg)
            else:
                image = font.render(text[:i], aa, color)

            self.screen.blit(image, (rect.left, y))
            y += fontHeight + lineSpacing

            # remove the text we just blitted
            text = text[i:]

        return text