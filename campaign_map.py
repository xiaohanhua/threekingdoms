from battle import *
from army_management import Army_Management_Scene
from city_management import City_Management_Scene
from diplomacy import Diplomacy_Scene
from world_map import World_Map_Scene
from tech_tree import Tech_Tree_Scene
from save_game_scene import Save_Game_Scene
from game_classes import*
from helper_functions import*
import pickle
import heapq
import logging
import os
import sys

vec = pg.math.Vector2

def calculate_manhattan_distance(obj_1, obj_2):
    return abs(obj_1.grid_position_x - obj_2.grid_position_x) + abs(obj_1.grid_position_y - obj_2.grid_position_y)

def a_star_search(graph, start, end):
    frontier = PriorityQueue()
    frontier.put(vec2int(start), 0)
    path = {}
    cost = {}
    path[vec2int(start)] = None
    cost[vec2int(start)] = 0

    while not frontier.empty():
        current = frontier.get()
        if current == end:
            break
        for next in graph.find_neighbors(vec(current)):
            next = vec2int(next)
            next_cost = cost[current] + graph.cost(current, next)
            if next not in cost or next_cost < cost[next]:
                cost[next] = next_cost
                priority = next_cost + heuristic(end, vec(next))
                frontier.put(next, priority)
                path[next] = vec(current) - vec(next)
    return path

def heuristic(a, b):
    return (abs(a.x - b.x) + abs(a.y - b.y))

def generate_grid_from_map():
    grid = []
    #Base
    f = open(os.path.join(RESOURCES_DIR, "3k_map_Base.csv"), "r")
    lines = f.readlines()
    f.close()
    for line in lines:
        new_line = []
        ls = line.split(",")
        for char in ls:
            if int(char) != -1:
                new_line.append(1)
            else:
                new_line.append(0)
        grid.append(new_line)

    #Water
    f = open(os.path.join(RESOURCES_DIR, "3k_map_WaterLayer.csv"), "r")
    lines = f.readlines()
    f.close()
    for row in range(len(lines)):
        ls = lines[row].split(",")
        for col in range(len(ls)):
            cell = int(ls[col])
            if cell != -1:
                grid[row][col] = 2

    #Forest Layer
    f = open(os.path.join(RESOURCES_DIR, "3k_map_ForestLayer.csv"), "r")
    lines = f.readlines()
    f.close()
    for row in range(len(lines)):
        ls = lines[row].split(",")
        for col in range(len(ls)):
            cell = int(ls[col])
            if cell != -1:
                grid[row][col] = 3

    # Rock Layer
    f = open(os.path.join(RESOURCES_DIR, "3k_map_RockLayer.csv"), "r")
    lines = f.readlines()
    f.close()
    for row in range(len(lines)):
        ls = lines[row].split(",")
        for col in range(len(ls)):
            cell = int(ls[col])
            if cell != -1:
                grid[row][col] = 4

    #Mountain Layer (Walls)
    f = open(os.path.join(RESOURCES_DIR, "3k_map_MountainLayer.csv"), "r")
    lines = f.readlines()
    f.close()
    for row in range(len(lines)):
        ls = lines[row].split(",")
        for col in range(len(ls)):
            cell = int(ls[col])
            if cell != -1:
                grid[row][col] = 0


    return grid


class SquareGrid:
    def __init__(self, grid):
        self.grid = grid
        self.width = len(grid[0])
        self.height = len(grid)
        self.walls = []
        self.connections = [vec(1, 0), vec(-1, 0), vec(0, 1), vec(0, -1)]
        # comment/uncomment this for diagonals:
        #self.connections += [vec(1, 1), vec(-1, 1), vec(1, -1), vec(-1, -1)]

    def in_bounds(self, node):
        return 0 <= node.x < self.width and 0 <= node.y < self.height

    def passable(self, node):
        return node not in self.walls

    def find_neighbors(self, node):
        neighbors = [node + connection for connection in self.connections]
        neighbors = filter(self.in_bounds, neighbors)
        neighbors = filter(self.passable, neighbors)
        return neighbors


class WeightedGrid(SquareGrid):
    def __init__(self, grid):
        super().__init__(grid)
        self.weights = {}

    def cost(self, from_node, to_node):
        if (vec(to_node) - vec(from_node)).length_squared() == 1:
            return self.weights.get(to_node, 0)


class PriorityQueue:
    def __init__(self):
        self.nodes = []

    def put(self, node, cost):
        heapq.heappush(self.nodes, (cost, node))

    def get(self):
        return heapq.heappop(self.nodes)[1]

    def empty(self):
        return len(self.nodes) == 0

def vec2int(v):
    return (int(v.x), int(v.y))

def dijkstra_search(graph, start, end):
    frontier = PriorityQueue()
    frontier.put(vec2int(start), 0)
    path = {}
    cost = {}
    path[vec2int(start)] = None
    cost[vec2int(start)] = 0

    while not frontier.empty():
        current = frontier.get()
        if current == end:
            break
        for next in graph.find_neighbors(vec(current)):
            next = vec2int(next)
            next_cost = cost[current] + graph.cost(current, next)
            if next not in cost or next_cost < cost[next]:
                cost[next] = next_cost
                priority = next_cost
                frontier.put(next, priority)
                path[next] = vec(current) - vec(next)
    return path



class Campaign_Map_Scene:
    def __init__(self, main_game, loaded_game_data):
        self.main_game = main_game
        self.loaded_game_data = loaded_game_data
        self.game_width, self.game_height = LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT
        if DEMO:
            self.screen = pg.display.set_mode((self.game_width, self.game_height), pg.DOUBLEBUF)
        else:
            self.screen = pg.display.set_mode((self.game_width, self.game_height), pg.FULLSCREEN|pg.DOUBLEBUF)

        os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (20, 20)
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()

        pg.mixer.init()
        pg.font.init()
        self.hover_sound = pg.mixer.Sound("sfx_hover.ogg")
        self.click_sound = pg.mixer.Sound("sfx_click.ogg")
        self.save_game_sound = pg.mixer.Sound("sfx_save_game.ogg")


        self.persistent_game_message = "" #always displayed
        self.game_message = ""
        self.game_message_size = 16
        self.game_message_color = WHITE
        self.last_action = 0


        self.camera_zoom_target_army_index = 0
        self.camera_zoom_target_city_index = 0
        self.selected_player_army = None
        self.selected_enemy_army = None
        self.selected_player_city = None

        self.player_army_movement_animation = False
        self.enemy_army_movement_animation = False
        self.player_army_retreat_animation = 0
        self.enemy_army_retreat_animation = 0
        self.army_movement_last_animated = 0
        self.last_player_movement_direction = vec(0, 0)
        self.last_enemy_movement_direction = vec(0, 0)
        self.animation_delay = self.loaded_game_data["animation_delay"]
        self.army_movement_grid_coords = []
        self.enemy_moving = False
        self.resume_enemy_moving = False

        self.previewing_battle = False
        self.left_preview_army = None
        self.right_preview_army = None
        self.viewing_battle_summary = False
        self.viewing_army_merge_interface = False
        self.viewing_decision_window = False
        self.viewing_quick_guide = False
        self.viewing_list = None
        self.viewing_list_offset = 0
        self.viewing_army_info = None
        self.viewing_city_info = None
        self.current_advice = None
        self.last_advice_given_time = 0
        self.current_decision = None
        self.current_decision_text = ""
        self.dynamic_decision_text = {}
        self.upcoming_decisions = []
        self.current_turn_dilemmas = {}
        self.battle_won = False
        self.sp_atk_icons_list = []
        self.skill_icons_list = []
        self.special_attack_tooltip_description = ""
        self.special_attack_tooltip_x = 0
        self.special_attack_tooltip_y = 0
        self.game_tooltip_description = ""

        self.no_general_armies = {"Wu": Commander(name="No General", simple_name="none", age=30, loyalty=0,
                                    country="Wu", combat_prowess=1, intellect=1, command=50, charisma=1, action_points=0,
                                    level=1, experience=0, upgrade_points=0),
                                  "Wei": Commander(name="No General", simple_name="none", age=30, loyalty=0,
                                    country="Wei", combat_prowess=1, intellect=1, command=50, charisma=1, action_points=0,
                                    level=1, experience=0, upgrade_points=0),
                                  "Shu": Commander(name="No General", simple_name="none", age=0, loyalty=0,
                                    country="Shu", combat_prowess=1, intellect=1, command=50, charisma=1, action_points=0,
                                    level=1, experience=0, upgrade_points=0),
                                  "Ma Teng": Commander(name="No General", simple_name="none", age=0, loyalty=0,
                                    country="Ma Teng", combat_prowess=1, intellect=1, command=50, charisma=1, action_points=0,
                                    level=1, experience=0, upgrade_points=0),
                                  "Liu Biao": Commander(name="No General", simple_name="none", age=0, loyalty=0,
                                    country="Liu Biao", combat_prowess=1, intellect=1, command=50, charisma=1, action_points=0,
                                    level=1, experience=0, upgrade_points=0),
                                  "Liu Zhang": Commander(name="No General", simple_name="none", age=0, loyalty=0,
                                    country="Liu Zhang", combat_prowess=1, intellect=1, command=50, charisma=1, action_points=0,
                                    level=1, experience=0, upgrade_points=0),
                                  "Zhang Lu": Commander(name="No General", simple_name="none", age=0, loyalty=0,
                                    country="Zhang Lu", combat_prowess=1, intellect=1, command=50, charisma=1, action_points=0,
                                    level=1, experience=0, upgrade_points=0),
                                  "Yuan Shao": Commander(name="No General", simple_name="none", age=0, loyalty=0,
                                    country="Yuan Shao", combat_prowess=1, intellect=1, command=50, charisma=1, action_points=0,
                                    level=1, experience=0, upgrade_points=0),
                                  "Southern Barbarians": Commander(name="No General", simple_name="none", age=0, loyalty=0,
                                    country="Southern Barbarians", combat_prowess=1, intellect=1, command=50, charisma=1, action_points=0,
                                    level=1, experience=0, upgrade_points=0)}

        self.rebel_commanders = {}
        for rebel_commander_name in ["Shen Dan", "Tian Yu", "Song Qian", "Jiang Bin", "Jia Hua",
                                     "Han De", "Fu Tong", "Cheng Man", "Chen Lan", "Bei Yan", "Lv Qian"]:
            commander = Commander(name=rebel_commander_name, simple_name=rebel_commander_name.replace(" ","_").lower(),
                                  age=randrange(30,51), loyalty=randrange(30,51),
                                  country="Shu", combat_prowess=randrange(50,61), intellect=randrange(30,61),
                                  command=randrange(50,56), charisma=randrange(70,91), action_points=randrange(14,19),
                                  level=1, experience=0, upgrade_points=0)
            self.rebel_commanders[rebel_commander_name] = commander


    def new(self, load_from=None, post_battle_cmd=None, preset_camera_offset=None):

        self.running = True
        self.playing = True
        self.is_mute = False
        self.toolbar_visible = True
        self.current_bgm = "battleThemeA.mp3"
        pg.mouse.set_visible(True)

        try:
            for sp in self.all_sprites:
                sp.kill()
        except:
            pass

        #Check for memory leak
        #import os, psutil
        #process = psutil.Process(os.getpid())
        #print("\n\n\n===============================\nMem usage:", process.memory_info().rss)

        self.all_sprites = pg.sprite.Group()
        self.fixed_sprites = pg.sprite.Group()
        self.list_sprites = pg.sprite.Group()
        self.alert_sprites = pg.sprite.Group()
        self.priority_sprites = pg.sprite.Group()
        self.battle_preview_sprites = pg.sprite.Group()
        self.tooltip_sprites = pg.sprite.Group()
        self.info_sprites = pg.sprite.Group()
        self.decision_window_sprites = pg.sprite.Group()
        self.buttons = pg.sprite.Group()
        self.icons = pg.sprite.Group()

        self.army_sprites = pg.sprite.Group()
        self.enemy_army_sprites = pg.sprite.Group()
        self.player_army_sprites = pg.sprite.Group()
        self.available_countries = []

        self.city_sprites = pg.sprite.Group()
        self.walkable_grid_sprites = pg.sprite.Group()

        self.zoom_scale = 1
        self.map_width, self.map_height = 368*32, 288*32
        self.map_grid_width = self.map_width//int(self.zoom_scale*32)
        self.map_grid_height = self.map_height//int(self.zoom_scale*32)
        self.map_grid = generate_grid_from_map()


        self.camera_current_x, self.camera_current_y = self.loaded_game_data["camera_position"].copy()  # starting pt on entire map
        self.x_offset, self.y_offset = 0, 0
        if self.camera_current_x <= LANDSCAPE_WIDTH // 2:
            self.x_offset = 0
        elif self.camera_current_x >= self.map_width - LANDSCAPE_WIDTH:
            self.x_offset = self.map_width - LANDSCAPE_WIDTH
        else:
            self.x_offset = self.camera_current_x - LANDSCAPE_WIDTH // 2

        if self.camera_current_y <= LANDSCAPE_HEIGHT // 2:
            self.y_offset = 0
        elif self.camera_current_y >= self.map_height - LANDSCAPE_HEIGHT:
            self.y_offset = self.map_height - LANDSCAPE_HEIGHT
        else:
            self.y_offset = self.camera_current_y - LANDSCAPE_HEIGHT // 2

        if self.camera_current_x <= 0:
            self.camera_current_x = 0
        if self.camera_current_y <= 0:
            self.camera_current_y = 0

        #print(self.loaded_game_data["camera_position"])

        self.map = CampaignMap(self.main_game, -self.x_offset, -self.y_offset, anchor="topleft")
        self.all_sprites.add(self.map)

        # transitions
        self.transition_sprites = pg.sprite.Group()
        self.transition = None
        self.post_transition_function = None

        if load_from:
            self.loaded_game_data = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, load_from), "rb"))
            self.loaded_game_data = self.reconcile_loaded_game_data(self.loaded_game_data)
            if not self.loaded_game_data["player_turn"]:
                self.resume_enemy_moving = True
        else:
            self.initiate_transition("in")


        self.player_country = self.loaded_game_data["country"]
        self.difficulty = self.loaded_game_data["difficulty"]
        self.realm_divide = self.loaded_game_data["realm_divide"]
        self.show_advice = self.loaded_game_data["show_advice"]
        self.advice_given = self.loaded_game_data["advice_given"]
        self.gold = self.loaded_game_data["gold"]
        self.food = self.loaded_game_data["food"]
        self.tax = self.loaded_game_data["tax"]
        self.turn_number = self.loaded_game_data["turn_number"]
        self.hostile_relationships = self.loaded_game_data["hostile_relationships"]
        self.peace_relationships = self.loaded_game_data["peace_relationships"]
        self.trade_relationships = self.loaded_game_data["trade_relationships"]
        self.alliance_relationships = self.loaded_game_data["alliance_relationships"]
        self.diplomatic_relationships = self.loaded_game_data["diplomatic_relationships"]
        self.past_conflicts = self.loaded_game_data["past_conflicts"]
        self.diplomacy_penalty = self.loaded_game_data["diplomacy_penalty"]
        self.general_kill_release = self.loaded_game_data["general_kill_release"]
        

        self.game_alerts = self.loaded_game_data["game_alerts"]
        self.task_progress = self.loaded_game_data["task_progress"]
        self.dilemma_effects = self.loaded_game_data["dilemma_effects"]
        self.tech_tree = self.loaded_game_data["tech_tree"]
        self.current_tech = self.loaded_game_data["current_tech"]
        self.acquired_tech = self.loaded_game_data["acquired_tech"]
        self.character_pool = self.loaded_game_data["character_pool"]
        self.recruitable_characters = self.loaded_game_data["recruitable_characters"]
        self.recruited_characters = self.loaded_game_data["recruited_characters"]
        self.dead_characters = self.loaded_game_data["dead_characters"]
        self.ongoing_achievements = self.loaded_game_data["ongoing_achievements"]


        for city in self.loaded_game_data["city"]:
            city_sprite = self.city_obj_conversion(city, "to_sprite")
            city_sprite.position_x -= self.x_offset
            city_sprite.position_y -= self.y_offset
            if not load_from:
                city_sprite.update_happiness()
            self.all_sprites.add(city_sprite)
            self.city_sprites.add(city_sprite)
            self.fixed_sprites.add(city_sprite)


        for army in self.loaded_game_data["army"]:
            self.add_army_to_map(army)

        self.toolbar_bg = CampaignMapSprite(self, 0, 0, "sprite_campaign_map_toolbar_bg.png", anchor="topleft")
        self.all_sprites.add(self.toolbar_bg)
        self.icons.add(self.toolbar_bg)

        self.attack_icon_sprite = CampaignMapSprite(self, -30000, -30000, "sprite_campaign_map_attack_icon.png", anchor="center")
        self.all_sprites.add(self.attack_icon_sprite)
        self.priority_sprites.add(self.attack_icon_sprite)
        self.fixed_sprites.add(self.attack_icon_sprite)

        self.defend_icon_sprite = CampaignMapSprite(self, -30000, -30000, "sprite_campaign_map_defend_icon.png", anchor="center")
        self.all_sprites.add(self.defend_icon_sprite)
        self.priority_sprites.add(self.defend_icon_sprite)
        self.fixed_sprites.add(self.defend_icon_sprite)

        self.army_merge_icon_sprite = CampaignMapSprite(self, -30000, -30000, "sprite_campaign_map_army_merge_icon.png",
                                                    anchor="center")
        self.all_sprites.add(self.army_merge_icon_sprite)
        self.priority_sprites.add(self.army_merge_icon_sprite)
        self.fixed_sprites.add(self.army_merge_icon_sprite)

        self.gold_icon = CampaignMapSprite(self, 0, 0, "campaign_icon_gold.png", anchor="topleft")
        self.all_sprites.add(self.gold_icon)
        self.icons.add(self.gold_icon)

        self.food_icon = CampaignMapSprite(self, 6.5, 0, "campaign_icon_food.png", anchor="topleft")
        self.all_sprites.add(self.food_icon)
        self.icons.add(self.food_icon)


        self.world_map_icon = Button(int(13*self.zoom_scale*32), 3, "sprite_world_map_icon.png",
                                      command=self.load_world_map, tooltip = "Click to view large map.",
                                       anchor="topleft", master=self, shortened_tooltip=True)
        self.all_sprites.add(self.world_map_icon)
        self.icons.add(self.world_map_icon)

        self.diplomacy_icon = Button(int(16.5*self.zoom_scale*32), 1, "sprite_diplomacy_icon_{}.png".format(self.player_country),
                                      command=self.load_diplomacy, tooltip = "Click to view options for diplomacy.",
                                     anchor="topleft", scale=(50, 50), master=self, shortened_tooltip=True)
        self.all_sprites.add(self.diplomacy_icon)
        self.icons.add(self.diplomacy_icon)

        self.toolbar_general_icon = Button(int(19*self.zoom_scale*32), 0, "sprite_toolbar_general_icon.png",
                                      command=self.list_generals, tooltip = "Click to view a list of all generals under your command. Click any general to zoom to their location on the map.",
                                       anchor="topleft", scale=(55, 55), master=self, shortened_tooltip=True)
        self.all_sprites.add(self.toolbar_general_icon)
        self.icons.add(self.toolbar_general_icon)

        self.toolbar_cities_icon = Button(int(21.5 * self.zoom_scale * 32), 2, "sprite_toolbar_cities_icon.png",
                                           command=self.list_cities,
                                           tooltip="Click to view a list of cities under your command. Click any city to zoom to their location on the map.",
                                           anchor="topleft", scale=(51, 51), master=self, shortened_tooltip=True)
        self.all_sprites.add(self.toolbar_cities_icon)
        self.icons.add(self.toolbar_cities_icon)

        self.toolbar_tech_tree_icon = Button(int(24*self.zoom_scale*32), 0, "sprite_toolbar_tech_tree_icon.png",
                                      command=self.load_tech_tree, tooltip = "Click to view the tech tree and manage your faction's development.",
                                       anchor="topleft", scale=(55, 55), master=self, shortened_tooltip=True)
        self.all_sprites.add(self.toolbar_tech_tree_icon)
        self.icons.add(self.toolbar_tech_tree_icon)

        self.toolbar_alerts_icon = Button(int(26.5 * self.zoom_scale * 32), 2, "sprite_toolbar_alerts_icon.png",
                                             command=self.list_alerts,
                                             tooltip="View important messages. Click an individual message to zoom to the relevant location, if applicable.",
                                             anchor="topleft", scale=(51, 51), master=self, shortened_tooltip=True)
        self.all_sprites.add(self.toolbar_alerts_icon)
        self.icons.add(self.toolbar_alerts_icon)

        self.toolbar_end_turn_icon = Button(int(29*self.zoom_scale*32), 0, "sprite_toolbar_end_turn_icon.png",
                                      command=self.end_turn, tooltip = "Click to end your turn. \n\nCurrent turn: {}{} \n\nYear {} ({})".format(self.turn_number, "                    ", 200 + self.turn_number//2, "Spring" if self.turn_number%2==1 else "Fall"),
                                       anchor="topleft", scale=(55, 55), master=self, shortened_tooltip=True)
        self.all_sprites.add(self.toolbar_end_turn_icon)
        self.icons.add(self.toolbar_end_turn_icon)

        self.toolbar_guide_icon = Button(int(31.5 * self.zoom_scale * 32), 2, "sprite_toolbar_guide_icon.png",
                                        command=partial(self.toggle_quick_guide, False),
                                        tooltip="Click to view quick guide.",
                                        anchor="topleft", scale=(51, 51), master=self, shortened_tooltip=True)
        self.all_sprites.add(self.toolbar_guide_icon)
        self.icons.add(self.toolbar_guide_icon)

        self.toolbar_save_icon = Button(int(34*self.zoom_scale*32), 0, "sprite_toolbar_save_icon.png",
                                      command=self.custom_save_game, tooltip = "Click to save the game. Alternatively, press CTRL + S to quick save.",
                                       anchor="topleft", scale=(55, 55), master=self, shortened_tooltip=True)
        self.all_sprites.add(self.toolbar_save_icon)
        self.icons.add(self.toolbar_save_icon)

        # create arrows for hover over scrolling if applicable
        self.list_arrow_down = Static(-30000, -30000, "sprite_yellow_arrow_down_1.png",
                       [pg.image.load("sprite_yellow_arrow_down_{}.png".format(i)) for i in [2, 3, 2, 1]],
                       frame_delta=150)
        self.icons.add(self.list_arrow_down)
        self.all_sprites.add(self.list_arrow_down)
        self.priority_sprites.add(self.list_arrow_down)

        self.list_arrow_up = Static(-30000, -30000, "sprite_yellow_arrow_up_1.png",
                                      [pg.image.load("sprite_yellow_arrow_up_{}.png".format(i)) for i in [2, 3, 2, 1]],
                                      frame_delta=150)
        self.icons.add(self.list_arrow_up)
        self.all_sprites.add(self.list_arrow_up)
        self.priority_sprites.add(self.list_arrow_up)


        self.set_espionage_variables()
        self.set_diplomacy_variables()
        self.update_recruitable_characters_from_pool()
        if self.resume_enemy_moving:
            self.hide_toolbar_icons()
        if post_battle_cmd:
            post_battle_cmd()
        if not load_from:
            xu_chang_target = [city_sprite for city_sprite in self.city_sprites if city_sprite.name == "Xu Chang"][0]
            self.current_turn_dilemmas = {"objective": {"short_summary": DECISION_TEXT["objective"],
                                                        "target": xu_chang_target},
                                          "view_military_report": {"short_summary": DECISION_TEXT["view_military_report"],
                                                        "target": self.view_military_report}}
            self.zoom_to_next_army()
        else:
            for dilemma in self.current_turn_dilemmas:
                dilemma_attrs = self.current_turn_dilemmas[dilemma]
                if dilemma_attrs["target"]:
                    if type(dilemma_attrs["target"]) == CampaignMapCitySprite:
                        matching_target_obj = [city_sprite for city_sprite in self.city_sprites if city_sprite.name == dilemma_attrs["target"].name][0]
                        dilemma_attrs["target"] = matching_target_obj
                    elif type(dilemma_attrs["target"]) == CampaignMapArmySprite:
                        try: #use try/except in case army no longer exists
                            matching_target_obj = [army_sprite for army_sprite in self.army_sprites if army_sprite.commander.name == dilemma_attrs["target"].commander.name][0]
                            dilemma_attrs["target"] = matching_target_obj
                        except:
                            pass

        if preset_camera_offset:
            self.camera_zoom(obj=None, preset_offsets=preset_camera_offset)

        self.run()


    def reconcile_loaded_game_data(self, data):
        #fix save files if game was saved prior to some important update
        if "general_kill_release" not in data:
            data["general_kill_release"] = {"Shu": 0, "Wu": 0, "Wei": 0, "Ma Teng": 0, "Liu Biao": 0,
                                            "Liu Zhang": 0, "Zhang Lu": 0, "Yuan Shao": 0}
        if "enemy_troops_killed" not in data:
            data["enemy_troops_killed"] = 0
        if "player_troops_killed" not in data:
            data["player_troops_killed"] = 0
        if "enemy_generals_killed" not in data:
            data["enemy_generals_killed"] = 0
        if "player_generals_killed" not in data:
            data["player_generals_killed"] = 0
        if "num_generals_recruited" not in data:
            data["num_generals_recruited"] = 0
        if "num_generals_released" not in data:
            data["num_generals_released"] = 0
        if "battles_won" not in data:
            data["battles_won"] = 0
        if "battles_lost" not in data:
            data["battles_lost"] = 0
        if "gold_produced" not in data:
            data["gold_produced"] = 0
        if "Southern Barbarians" not in data["recruitable_characters"]:
            data["recruitable_characters"]["Southern Barbarians"] = []
        if "ongoing_achievements" not in data:
            data["ongoing_achievements"] = {"ranged_units_only": True, "no_ranged_units": True,
                                     "cavalry_units_only": True}

        for army in data["army"]:
            try:
                print(army.commander.name, "---", army.attack_target, army.defend_target)
            except:
                army.attack_target = None
                army.defend_target = None

            try:
                print(army.commander.idle_turns)
            except:
                army.commander.idle_turns = 0

        return data


    def initiate_transition(self, direction):
        if direction == "in":
            self.transition = FadeBlackTransition(80, "in")
            self.transition_sprites.add(self.transition)
        else:
            self.transition = FadeBlackTransition(80, "out")
            self.transition_sprites.add(self.transition)


    def transition_clean_up(self):
        self.transition.kill()
        self.transition = None
        self.post_transition_function = None
        self.playing = False
        self.running = False


    def give_advice(self, advice):
        if not self.current_advice and advice not in self.loaded_game_data["advice_given"] and self.show_advice:
            self.last_advice_given_time = pg.time.get_ticks()
            self.current_advice = advice
            self.loaded_game_data["advice_given"].append(advice)
            sfx_advice = pg.mixer.Sound(advice+".ogg")
            sfx_advice.play()
            pg.mixer.music.set_volume(.12)


    def generate_military_report(self):
        military_report = """Turn number (Year): {} ({} {})\n            
Battles won (Total battles): {} ({})            Gold produced: {}\n
Total enemy troops killed: {}            Total troops lost: {}\n
Generals recruited: {}            Enemy generals released: {}\n
Enemy generals slain: {}            Generals lost: {}\n""".format(self.turn_number,
                   200 + self.turn_number//2, "Spring" if self.turn_number%2==1 else "Fall",
                   self.loaded_game_data["battles_won"], self.loaded_game_data["battles_won"] + self.loaded_game_data["battles_lost"],
                   self.loaded_game_data["gold_produced"],
                   self.loaded_game_data["enemy_troops_killed"], self.loaded_game_data["player_troops_killed"],
                   self.loaded_game_data["num_generals_recruited"], self.loaded_game_data["num_generals_released"],
                   self.loaded_game_data["enemy_generals_killed"], self.loaded_game_data["player_generals_killed"],
                   )

        return military_report


    def add_army_to_map(self, army, target_city=None, offset_x=0, offset_y=0, embed_in_city=False):
        #delete army obj if one with same commander already exists
        for existing_army_sprite in self.army_sprites:
            if existing_army_sprite.commander.name == army.commander.name:
                self.kill_army(existing_army_sprite)
                break

        army_sprite = self.army_obj_conversion(army, "to_sprite")
        self.all_sprites.add(army_sprite)
        self.army_sprites.add(army_sprite)
        self.fixed_sprites.add(army_sprite)
        if army_sprite.commander.country != self.player_country:  # self.hostile_relationships[self.player_country]:
            self.enemy_army_sprites.add(army_sprite)
        elif army_sprite.commander.country == self.player_country:
            self.player_army_sprites.add(army_sprite)

        if target_city:
            for city_sprite in self.city_sprites:
                if city_sprite.name == target_city:
                    army_sprite.grid_position_x = city_sprite.grid_position_x + offset_x
                    army_sprite.grid_position_y = city_sprite.grid_position_y + offset_y
                    army_sprite.position_x = city_sprite.position_x + offset_x*int(self.zoom_scale*32)
                    army_sprite.position_y = city_sprite.position_y + offset_y*int(self.zoom_scale*32)
                    if embed_in_city:
                        self.embed_army_in_city(army_sprite, city_sprite)
                        print(army_sprite.commander.name, "Embedded in", city_sprite.name)
                    #print(army_sprite.grid_position_x, army_sprite.grid_position_y)
                    #print(army_sprite.position_x, army_sprite.position_y)
                    break
        else:
            army_sprite.position_x -= self.x_offset
            army_sprite.position_y -= self.y_offset


    def custom_save_game(self):
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = self.custom_save_game
        elif not self.transition.fading:
            self.transition_clean_up()
            self.toggle_selected_army_info(None)
            self.save_game(save_to="custom_save_autosave.p")
            save_game_scene = Save_Game_Scene(self)
            save_game_scene.new()


    def save_game(self, save_to=None):
        army = []
        city = []
        for army_sprite in self.army_sprites:
            army.append(self.army_obj_conversion(army_sprite, "from_sprite"))

        for city_sprite in self.city_sprites:
            city.append(self.city_obj_conversion(city_sprite, "from_sprite"))


        self.loaded_game_data["army"] = army
        self.loaded_game_data["city"] = city
        self.loaded_game_data["camera_position"] = [self.camera_current_x, self.camera_current_y]
        self.loaded_game_data["animation_delay"] = self.animation_delay
        self.loaded_game_data["player_turn"] = not self.resume_enemy_moving
        self.loaded_game_data["gold"] = self.gold
        self.loaded_game_data["food"] = self.food
        self.loaded_game_data["tax"] = self.tax
        self.loaded_game_data["difficulty"] = self.difficulty
        self.loaded_game_data["realm_divide"] = self.realm_divide
        self.loaded_game_data["show_advice"] = self.show_advice
        self.loaded_game_data["advice_given"] = self.advice_given
        self.loaded_game_data["turn_number"] = self.turn_number
        self.loaded_game_data["game_alerts"] = self.game_alerts
        self.loaded_game_data["hostile_relationships"] = self.hostile_relationships
        self.loaded_game_data["peace_relationships"] = self.peace_relationships
        self.loaded_game_data["trade_relationships"] = self.trade_relationships
        self.loaded_game_data["alliance_relationships"] = self.alliance_relationships
        self.loaded_game_data["diplomatic_relationships"] = self.diplomatic_relationships
        self.loaded_game_data["past_conflicts"] = self.past_conflicts
        self.loaded_game_data["diplomacy_penalty"] = self.diplomacy_penalty
        self.loaded_game_data["general_kill_release"] = self.general_kill_release
        self.loaded_game_data["dilemma_effects"] = self.dilemma_effects
        self.loaded_game_data["task_progress"] = self.task_progress
        self.loaded_game_data["tech_tree"] = self.tech_tree
        self.loaded_game_data["current_tech"] = self.current_tech
        self.loaded_game_data["acquired_tech"] = self.acquired_tech
        self.loaded_game_data["character_pool"] = self.character_pool
        self.loaded_game_data["recruitable_characters"] = self.recruitable_characters
        self.loaded_game_data["recruited_characters"] = self.recruited_characters
        self.loaded_game_data["dead_characters"] = self.dead_characters
        self.loaded_game_data["ongoing_achievements"] = self.ongoing_achievements

        #print("Camera position:", self.loaded_game_data["camera_position"])
        if save_to:
            pickle.dump(self.loaded_game_data, open(os.path.join(SAVE_SLOTS_DIR, save_to), "wb"))
        else:
            pickle.dump(self.loaded_game_data, open(os.path.join(SAVE_SLOTS_DIR, "data.p"), "wb"))

        self.play_sound_effect(self.save_game_sound)


    def play_sound_effect(self, sfx):
        if not self.is_mute:
            sfx.play()


    def generate_no_general_army(self, city):
        return Army(city.grid_position_x, city.grid_position_y, commander=self.no_general_armies[city.country],
                    army_composition=city.garrison, action_points=0, retreat_penalty=0, anchor="midleft")


    def army_obj_conversion(self, from_obj, conversion="from_sprite"):
        if conversion == "from_sprite":
            attack_target, defend_target = None, None
            if type(from_obj.attack_target) == CampaignMapArmySprite:
                attack_target = from_obj.attack_target.commander.name
            elif type(from_obj.attack_target) == CampaignMapCitySprite:
                attack_target = from_obj.attack_target.name
            if from_obj.defend_target:
                defend_target = from_obj.defend_target.name

            return Army(from_obj.grid_position_x, from_obj.grid_position_y,
                         from_obj.commander, from_obj.army_composition, from_obj.action_points,
                         from_obj.retreat_penalty, from_obj.embedded_in_city, attack_target,
                         defend_target, from_obj.anchor)
        else:
            attack_target, defend_target = None, None
            if type(from_obj.attack_target) == CampaignMapArmySprite:
                for army in self.army_sprites:
                    if army.commander.name == from_obj.attack_target:
                        attack_target = army
                        break
            elif type(from_obj.attack_target) == CampaignMapCitySprite:
                for city in self.city_sprites:
                    if city.name == from_obj.attack_target:
                        attack_target = city
                        break
            if from_obj.defend_target:
                for city in self.city_sprites:
                    if city.name == from_obj.defend_target:
                        defend_target = city
                        break

            return CampaignMapArmySprite(self, from_obj.grid_position_x, from_obj.grid_position_y,
                                         from_obj.commander, from_obj.army_composition, from_obj.action_points,
                                         from_obj.retreat_penalty, from_obj.embedded_in_city,
                                         attack_target, defend_target, anchor="midleft")


    def city_obj_conversion(self, from_obj, conversion="from_sprite"):
        if conversion == "from_sprite":
            return City(grid_position_x=from_obj.grid_position_x, grid_position_y=from_obj.grid_position_y,
                             name=from_obj.name, fortress_level=from_obj.fortress_level,
                             country=from_obj.country, population=from_obj.population,
                             population_growth=from_obj.population_growth, happiness=from_obj.happiness,
                             defender=from_obj.defender, anchor=from_obj.anchor, raw_buildings=from_obj.raw_buildings,
                             buildings=from_obj.buildings, garrison=from_obj.garrison,
                             units_recruited_this_turn=from_obj.units_recruited_this_turn, effects=from_obj.effects)
        else:
            return CampaignMapCitySprite(self, grid_position_x=from_obj.grid_position_x, grid_position_y=from_obj.grid_position_y,
                                                name=from_obj.name, fortress_level=from_obj.fortress_level,
                                                country=from_obj.country, population=from_obj.population, population_growth=from_obj.population_growth,
                                                happiness=from_obj.happiness, defender=from_obj.defender, anchor=from_obj.anchor,
                                                raw_buildings=from_obj.raw_buildings, buildings=from_obj.buildings,
                                                garrison=from_obj.garrison, units_recruited_this_turn=from_obj.units_recruited_this_turn,
                                                effects=from_obj.effects)


    def is_cell_walkable(self, x, y):
        try:
            return self.map_grid[y][x] != 0 and y > 0 and y < len(self.map_grid) and x > 0 and x < len(self.map_grid[0])
        except:
            return False


    def show_walkable_grids(self):
        x1,y1 = self.selected_player_army.grid_position_x, self.selected_player_army.grid_position_y
        self.walkable_range = self.selected_player_army.action_points

        self.adj_min_x = max([0, x1-self.walkable_range])
        self.adj_max_x = min([self.map_grid_width, x1+self.walkable_range+1])
        self.adj_min_y = max([0, y1-self.walkable_range])
        self.adj_max_y = min([self.map_grid_height, y1+self.walkable_range+1])
        self.adj_grid = [sub[self.adj_min_y:self.adj_max_y] for sub in self.map_grid[self.adj_min_x:self.adj_max_x]]

        walkable_grids = []

        g = WeightedGrid(self.adj_grid)
        for row in range(len(self.adj_grid)):
            for col in range(len(self.adj_grid[0])):
                cell = self.adj_grid[row][col]
                if cell == 0:
                    g.walls.append(vec(col, row))
                else:
                    g.weights[vec2int(vec(col, row))] = cell
        #print(self.adj_min_x, self.adj_max_x,self.adj_min_y, self.adj_max_y)
        for x2 in range(self.adj_min_x, self.adj_max_x):
            for y2 in range(self.adj_min_y, self.adj_max_y):
                if x1==x2 and y1==y2:
                    pass
                else:
                    try:
                        shortest_dist = self.calculate_shortest_path(g, x1, y1, x2, y2, self.selected_player_army)["cost"]
                        if shortest_dist <= self.walkable_range:
                            walkable_grids.append((x2, y2))

                    except Exception as e: #not walkable
                        logging.exception(e)


        for coords in walkable_grids:
            grid = CampaignMapSprite(self, coords[0], coords[1],
                                     "sprite_walkable_grid.png", anchor="topleft")
            grid.position_x = self.selected_player_army.position_x + int(self.zoom_scale*32)*(grid.grid_position_x - self.selected_player_army.grid_position_x)
            grid.position_y = self.selected_player_army.position_y + int(self.zoom_scale*32)*(grid.grid_position_y - self.selected_player_army.grid_position_y)
            self.walkable_grid_sprites.add(grid)
            self.fixed_sprites.add(grid)
            self.all_sprites.add(grid)



    def calculate_shortest_path(self, grid, start_x, start_y, goal_x, goal_y, army_obj):
        start = vec(start_x - self.grid_offset_x, start_y - self.grid_offset_y)
        goal = vec(goal_x - self.grid_offset_x, goal_y - self.grid_offset_y)
        #path = dijkstra_search(grid, goal, start)
        if start == goal:
            return {"path":[],"cost":0}
        else:
            path = a_star_search(grid, goal, start)

        try:
            result_path = {}
            current = start + path[vec2int(start)]
            result_path[vec2int(current)] = path[vec2int(current)]
            while current != goal:
                current = current + path[vec2int(current)]
                result_path[vec2int(current)] = path[vec2int(current)]

            cost = 0

            adjusted_result_path = {}
            for coord in result_path:
                adjusted_coord = tuple(map(sum,zip(coord,(self.grid_offset_x, self.grid_offset_y))))
                adjusted_result_path[adjusted_coord] = result_path[coord]
                standard_cost = grid.weights[coord]
                cost += self.calculate_adjusted_grid_movement_cost(standard_cost, army_obj)
            return {"path": adjusted_result_path, "cost": cost}

        except:
            return {"path": {}, "cost": 999999999}


    def show_shortest_path(self):
        for grid_sprite in self.walkable_grid_sprites:
            grid_sprite.kill()

        x1, y1 = self.selected_player_army.grid_position_x, self.selected_player_army.grid_position_y
        self.walkable_range = self.selected_player_army.action_points
        self.adj_min_x = int(max([0, x1 - self.walkable_range]))
        self.adj_max_x = int(min([self.map_grid_width, x1 + self.walkable_range + 1]))
        self.adj_min_y = int(max([0, y1 - self.walkable_range]))
        self.adj_max_y = int(min([self.map_grid_height, y1 + self.walkable_range + 1]))
        self.adj_grid = [sub[self.adj_min_x:self.adj_max_x] for sub in self.map_grid[self.adj_min_y:self.adj_max_y]]

        self.grid_offset_x = max([0, x1 - self.walkable_range])
        self.grid_offset_y = max([0, y1 - self.walkable_range])


        self.weighted_grid = WeightedGrid(self.adj_grid)
        for row in range(len(self.adj_grid)):
            for col in range(len(self.adj_grid[0])):
                cell = self.adj_grid[row][col]
                if cell == 0:
                    self.weighted_grid.walls.append(vec(col, row))
                else:
                    self.weighted_grid.weights[vec2int(vec(col, row))] = cell

        mpx, mpy = pg.mouse.get_pos()
        x2 = self.selected_player_army.grid_position_x + (mpx-self.selected_player_army.position_x)//int(self.zoom_scale*32)
        y2 = self.selected_player_army.grid_position_y + (mpy-self.selected_player_army.position_y)//int(self.zoom_scale*32)


        target_grid_filename = "sprite_unwalkable_grid.png"
        if abs(x2-x1) + abs(y2-y1) <= self.walkable_range:
            try:
                self.grid_offset_x = max([0, x1-self.walkable_range])
                self.grid_offset_y = max([0, y1-self.walkable_range])
                #shortest_path_results = calculate_shortest_path(g, x1-self.self.grid_offset_x, y1-self.self.grid_offset_y,
                #                                                x2-self.self.grid_offset_x, y2-self.self.grid_offset_y)
                shortest_path_results = self.calculate_shortest_path(self.weighted_grid, x1, y1, x2, y2, self.selected_player_army)

                if shortest_path_results["cost"] <= self.walkable_range:
                    for coords in shortest_path_results["path"]:
                        grid = CampaignMapSprite(self, self.selected_player_army.grid_position_x + coords[0] - x1,
                                                 self.selected_player_army.grid_position_y + coords[1] - y1,
                                                 "sprite_walkable_grid.png", anchor="topleft")
                        grid.position_x = self.selected_player_army.position_x + (coords[0]-x1)*int(self.zoom_scale*32)
                        grid.position_y = self.selected_player_army.position_y + (coords[1]-y1)*int(self.zoom_scale*32)
                        self.walkable_grid_sprites.add(grid)
                        self.fixed_sprites.add(grid)
                        self.all_sprites.add(grid)

                    target_grid_filename = "sprite_walkable_grid.png"
                    self.game_message = "Projected action point cost: {}".format(shortest_path_results["cost"])
                    self.game_message_color = WHITE
                    self.game_message_size = 20
                    self.last_action = pg.time.get_ticks()

                else:
                    self.game_message = "Destination out of range"
                    self.game_message_color = ORANGE
                    self.game_message_size = 20
                    self.last_action = pg.time.get_ticks()

            except Exception as e:
                self.game_message = "Destination unreachable"
                self.game_message_color = ORANGE
                self.game_message_size = 20
                self.last_action = pg.time.get_ticks()

        else:
            self.game_message = "Destination out of range"
            self.game_message_color = ORANGE
            self.game_message_size = 20
            self.last_action = pg.time.get_ticks()

        target_grid = CampaignMapSprite(self, x2, y2, target_grid_filename, anchor="topleft")
        target_grid.hovering = True
        target_grid.position_x = self.selected_player_army.position_x + (x2 - x1) * int(self.zoom_scale*32)
        target_grid.position_y = self.selected_player_army.position_y + (y2 - y1) * int(self.zoom_scale*32)
        self.walkable_grid_sprites.add(target_grid)
        self.fixed_sprites.add(target_grid)
        self.all_sprites.add(target_grid)


    def get_grid_terrain(self, row, col, expand_radius=0):
        if expand_radius == 0:
            cost = self.map_grid[row][col]
            if cost == 2:
                return "water"
            elif cost == 3:
                return "forest"
            elif cost == 4:
                return "rock"
            else:
                return "sand"

        else:
            surrounding_terrain_dic = {}
            for r in range(row-expand_radius, row+expand_radius+1):
                for c in range(col-expand_radius, col+expand_radius+1):
                    if r > 0 and c > 0 and r < self.map_grid_height and c < self.map_grid_width:
                        cost = self.map_grid[r][c]
                        if cost in surrounding_terrain_dic:
                            surrounding_terrain_dic[cost] += 1
                        else:
                            surrounding_terrain_dic[cost] = 1

            terrain_keys = list(surrounding_terrain_dic.keys())
            terrain_keys.sort(reverse=True)
            for k in terrain_keys:
                if k != 1:
                    v = surrounding_terrain_dic[k]
                    if v/((expand_radius*2)**2) >= .3:
                        if k == 2:
                            return "water"
                        elif k == 3:
                            return "forest"
                        elif k == 4:
                            return "rock"
                        elif k == 0:
                            return "rock"

            return "sand"


    def calculate_adjusted_grid_movement_cost(self, cost, army_obj):
        if cost == 2:
            if army_obj.commander.country == "Wu":
                cost = 1
        elif cost == 3:
            if "topographer" in army_obj.commander.skills:
                cost -= 1
        if cost <= 1:
            cost = 1
        return cost


    def calculate_grid_movement_cost(self, army_obj):
        cost = self.map_grid[army_obj.grid_position_y][army_obj.grid_position_x]
        return self.calculate_adjusted_grid_movement_cost(cost, army_obj)


    def battle_preview(self, player_army, enemy_army, siege_defense=None, siege_city_object=None):
        if self.selected_player_army:
            self.selected_player_army.selected = False
            self.selected_player_army = None
        if self.player_army_movement_animation:
            self.player_army_movement_animation = False
        self.attack_icon_sprite.position_x, self.attack_icon_sprite.position_y = -30000,-30000

        self.previewing_battle = True
        self.left_preview_army = player_army
        self.right_preview_army = enemy_army

        self.sp_atk_icons_list = []
        self.skill_icons_list = []

        self.battle_preview_bg = Static(LANDSCAPE_WIDTH//2, LANDSCAPE_HEIGHT//2, "sprite_battle_preview_bg.png",
                                        anchor="center")
        self.all_sprites.add(self.battle_preview_bg)
        self.battle_preview_sprites.add(self.battle_preview_bg)

        #self.close_battle_preview_button = Static(self.battle_preview_bg.rect.right, self.battle_preview_bg.rect.top,
        #                                      "sprite_close_button.png", anchor="center")
        #self.all_sprites.add(self.close_battle_preview_button)
        #self.battle_preview_sprites.add(self.close_battle_preview_button)

        #player side preview
        if self.left_preview_army.commander.simple_name != "none":
            self.battle_preview_player_general_avatar = Static(150, 50, "general_avatar_{}.png".format(self.left_preview_army.commander.simple_name),
                                                               anchor="topleft", scale=(70, 100))
            self.all_sprites.add(self.battle_preview_player_general_avatar)
            self.battle_preview_sprites.add(self.battle_preview_player_general_avatar)
            self.left_preview_army.commander.idle_turns = 0

        counter = 0
        for sp_atk in self.left_preview_army.commander.sp_atks:
            sp_atk_icon = SpecialAttackIcon(self, 150 + 55 * counter, 200, sp_atk, anchor="topleft")
            self.all_sprites.add(sp_atk_icon)
            self.battle_preview_sprites.add(sp_atk_icon)
            self.sp_atk_icons_list.append(sp_atk_icon)
            counter += 1

        counter = 0
        for skill in self.left_preview_army.commander.skills:
            sp_atk_icon = SpecialAttackIcon(self, 150 + 55 * counter, 300, skill, anchor="topleft", type="skill")
            self.all_sprites.add(sp_atk_icon)
            self.battle_preview_sprites.add(sp_atk_icon)
            self.skill_icons_list.append(sp_atk_icon)
            counter += 1

        self.left_army_unit_types = list(self.left_preview_army.army_composition.keys())
        count = 0
        for row in range(2):
            for column in range(4):
                if count < len(self.left_army_unit_types):
                    unit_type = self.left_army_unit_types[count]
                    unit_icon = UnitIcon(self, "left", column * 65 + 150, row * 62 + 400, unit_type, self.left_preview_army.commander.country)

                else:
                    unit_icon = UnitIcon(self, "left", column * 65 + 150, row * 62 + 400, None, None)
                self.battle_preview_sprites.add(unit_icon)
                self.all_sprites.add(unit_icon)
                count += 1


        #show terrain
        if siege_defense:
            terrain = self.get_grid_terrain(self.left_preview_army.grid_position_y,
                                            self.left_preview_army.grid_position_x, expand_radius=5)
            if terrain == "water":
                terrain = "sand"
        else:
            terrain = self.get_grid_terrain(self.left_preview_army.grid_position_y,
                                            self.left_preview_army.grid_position_x, expand_radius=2)


        terrain_icon = TerrainIcon(self, LANDSCAPE_WIDTH//2, 450, terrain, anchor="center", scale=(81,81))
        self.battle_preview_sprites.add(terrain_icon)
        self.icons.add(terrain_icon)
        self.all_sprites.add(terrain_icon)


        #option buttons
        auto_button = Button(LANDSCAPE_WIDTH//2, 150, "campaign_button_battle_preview_auto.png",
                              command=partial(self.auto_resolve_battle, self.left_preview_army, self.right_preview_army, siege_defense, siege_city_object))
        self.battle_preview_sprites.add(auto_button)
        self.buttons.add(auto_button)
        self.all_sprites.add(auto_button)
        fight_button = Button(LANDSCAPE_WIDTH//2, 250, "campaign_button_battle_preview_fight.png",
                              command=partial(self.enter_battle, siege_defense, siege_city_object, terrain))
        self.battle_preview_sprites.add(fight_button)
        self.buttons.add(fight_button)
        self.all_sprites.add(fight_button)
        retreat_button = Button(LANDSCAPE_WIDTH//2, 350, "campaign_button_battle_preview_retreat.png", command=self.retreat)
        self.battle_preview_sprites.add(retreat_button)
        self.buttons.add(retreat_button)
        self.all_sprites.add(retreat_button)

        if self.left_preview_army.retreat_penalty + 8 >= self.left_preview_army.max_action_points or siege_defense == "left":
            retreat_button.disabled = True


        #enemy side preview
        if self.right_preview_army.commander.simple_name != "none":
            self.battle_preview_enemy_general_avatar = Static(LANDSCAPE_WIDTH//2+180, 50, "general_avatar_{}.png".format(self.right_preview_army.commander.simple_name),
                                                               anchor="topleft", scale=(70, 100))
            self.all_sprites.add(self.battle_preview_enemy_general_avatar)
            self.battle_preview_sprites.add(self.battle_preview_enemy_general_avatar)
            self.right_preview_army.commander.idle_turns = 0

        counter = 0
        for sp_atk in self.right_preview_army.commander.sp_atks:
            sp_atk_icon = SpecialAttackIcon(self, LANDSCAPE_WIDTH//2+180 + 55 * counter, 200, sp_atk, anchor="topleft")
            self.all_sprites.add(sp_atk_icon)
            self.battle_preview_sprites.add(sp_atk_icon)
            self.sp_atk_icons_list.append(sp_atk_icon)
            counter += 1

        counter = 0
        for sp_atk in self.right_preview_army.commander.skills:
            sp_atk_icon = SpecialAttackIcon(self, LANDSCAPE_WIDTH//2 + 180 + 55 * counter, 300, sp_atk, anchor="topleft", type="skill")
            self.all_sprites.add(sp_atk_icon)
            self.battle_preview_sprites.add(sp_atk_icon)
            self.sp_atk_icons_list.append(sp_atk_icon)
            counter += 1

        self.right_army_unit_types = list(self.right_preview_army.army_composition.keys())
        count = 0
        for row in range(2):
            for column in range(4):
                if count < len(self.right_army_unit_types):
                    unit_type = self.right_army_unit_types[count]
                    unit_icon = UnitIcon(self, "left", column * 65 + 180 + LANDSCAPE_WIDTH//2, row * 62 + 400, unit_type,
                                         self.right_preview_army.commander.country)

                else:
                    unit_icon = UnitIcon(self, "left", column * 65 + 180 + LANDSCAPE_WIDTH//2, row * 62 + 400, None, None)
                self.battle_preview_sprites.add(unit_icon)
                self.all_sprites.add(unit_icon)
                count += 1


    def retreat(self):
        for sp in self.battle_preview_sprites:
            sp.kill()
        self.previewing_battle = False
        self.player_army_retreat_animation = 1

        if "orderly_retreat" in self.left_preview_army.commander.skills:
            self.left_preview_army.action_points -= 4
            self.left_preview_army.retreat_penalty += 4
        elif "cease_fire" in self.left_preview_army.commander.skills:
            pass
        else:
            self.left_preview_army.action_points -= 8
            self.left_preview_army.retreat_penalty += 8

        if self.left_preview_army.action_points < 0:
            self.left_preview_army.action_points = 0


        #remove retainers from defender army if battle did not take place
        if self.right_preview_army.embedded_in_city:
            for city_sprite in self.city_sprites:
                if city_sprite.name == self.right_preview_army.embedded_in_city:
                    walls = [building for building in city_sprite.buildings if building.name == "Walls"][0]
                    retainers = FORTRESS_WALL_RETAINERS[walls.level - 1]
                    for retainer, amount in retainers.items():
                        if retainer in self.right_preview_army.army_composition:
                            self.right_preview_army.army_composition[retainer] -= amount

                    city_sprite.garrison = self.right_preview_army.army_composition
                    break

        #self.left_preview_army.grid_position_x -= self.last_player_movement_direction[0]
        #self.left_preview_army.grid_position_y -= self.last_player_movement_direction[1]
        #self.left_preview_army.position_x -= self.last_player_movement_direction[0] * int(self.zoom_scale*32)
        #self.left_preview_army.position_y -= self.last_player_movement_direction[1] * int(self.zoom_scale*32)


    def auto_resolve_battle(self, left_army, right_army, siege_defense=None, siege_city_object=None):

        print("autoresolving...")
        self.previewing_battle = False
        for sp in self.battle_preview_sprites:
            sp.kill()
        # calculate ranged power on both sides
        left_ranged_power = 0
        for unit_type, amount in left_army.army_composition.items():
            if unit_type in RANGED_UNITS:
                left_ranged_power += UNIT_RECRUITMENT_COST[unit_type]*amount
        right_ranged_power = 0
        for unit_type, amount in right_army.army_composition.items():
            if unit_type in RANGED_UNITS:
                right_ranged_power += UNIT_RECRUITMENT_COST[unit_type] * amount


        left_troops_killed = 0
        right_troops_killed = 0
        # reduce by ranged power ratio first
        for unit_type, amount in left_army.army_composition.items():
            left_troops_killed += int(amount*right_ranged_power/(left_ranged_power+right_ranged_power+1)/10)
            left_army.army_composition[unit_type] -= int(amount*right_ranged_power/(left_ranged_power+right_ranged_power+1)/10)
        for unit_type, amount in right_army.army_composition.items():
            right_troops_killed += int(amount*left_ranged_power/(left_ranged_power+right_ranged_power+1)/10)
            right_army.army_composition[unit_type] -= int(amount*left_ranged_power/(left_ranged_power+right_ranged_power+1)/10)

        # calculate remaining power on both sides
        if siege_defense == "left":
            left_power = self.calculate_army_power(left_army)
            right_power = self.calculate_army_power(right_army)
        elif siege_defense == "right":
            left_power = self.calculate_army_power(left_army)
            right_power = self.calculate_army_power(right_army)
        else:
            left_power = self.calculate_army_power(left_army)
            right_power = self.calculate_army_power(right_army)

        # determine winner and reduce by power ratio
        for unit_type, amount in left_army.army_composition.items():
            left_troops_killed += int(amount*right_power/(left_power+right_power))
            left_army.army_composition[unit_type] -= int(amount*right_power/(left_power+right_power))
        for unit_type, amount in right_army.army_composition.items():
            right_troops_killed += int(amount*left_power/(left_power+right_power))
            right_army.army_composition[unit_type] -= int(amount*left_power/(left_power+right_power))

        if left_army.commander.country == self.player_country:
            self.loaded_game_data["enemy_troops_killed"] += right_troops_killed
            self.loaded_game_data["player_troops_killed"] += left_troops_killed


        captured_enemy_commander = 0
        if left_power > right_power:
            winner = "left"
            post_battle_left_number = sum(left_army.army_composition.values())
            post_battle_right_number = sum(right_army.army_composition.values())
            post_battle_army_size_ratio = post_battle_left_number / (post_battle_right_number + 1)
            post_battle_kill_bonus = .7 * post_battle_army_size_ratio ** .25
            left_army.commander.gain_exp(int(.5 * 300 + right_army.commander.level * 50))
            right_army.commander.gain_exp(50)
            if right_army.commander.simple_name != "none":
                if right_army.commander.loyalty >= 5:
                    right_army.commander.loyalty -= 5
            if "orderly_retreat" in right_army.commander.skills:
                post_battle_kill_bonus *= .5
            for unit_type, amount in right_army.army_composition.items():
                right_army.army_composition[unit_type] = int(max([0, amount * (1 - post_battle_kill_bonus)]))

            if random() >= right_army.commander.combat_prowess / 100 - (sum(right_army.army_composition.values()) == 0) * 1 and right_army.commander.simple_name != "none" and siege_defense != "left":
                captured_enemy_commander = 1

        else:
            winner = "right"
            post_battle_left_number = sum(left_army.army_composition.values())
            post_battle_right_number = sum(right_army.army_composition.values())
            post_battle_army_size_ratio = post_battle_right_number / (post_battle_left_number + 1)
            post_battle_kill_bonus = .7 * post_battle_army_size_ratio ** .25
            right_army.commander.gain_exp(int(.5 * 300 + left_army.commander.level * 50))
            left_army.commander.gain_exp(50)
            if left_army.commander.simple_name != "none":
                if left_army.commander.loyalty >= 5:
                    left_army.commander.loyalty -= 5
            if "orderly_retreat" in left_army.commander.skills:
                post_battle_kill_bonus *= .5
            for unit_type, amount in left_army.army_composition.items():
                left_army.army_composition[unit_type] = int(max([0, amount * (1 - post_battle_kill_bonus)]))

            if random() >= left_army.commander.combat_prowess / 100 - (sum(left_army.army_composition.values()) == 0) * 1 and left_army.commander.simple_name != "none" and siege_defense != "right":
                captured_enemy_commander = -1

        #check if player is involved in this battle
        if left_army.commander.country == self.player_country:
            if winner == "left":
                self.loaded_game_data["battles_won"] += 1
            else:
                self.loaded_game_data["battles_lost"] += 1
            self.post_battle_summary(winner=="left", captured_enemy_commander, siege_defense, siege_city_object)

        #if not... resolve between the AI --> no need to show post_battle_summary
        else:
            if winner == "left" and siege_defense == "right":
                for city_sprite in self.city_sprites:
                    if city_sprite.name == siege_city_object.name:
                        city_sprite.captured(left_army)
            elif winner == "right" and siege_defense == "left":
                for city_sprite in self.city_sprites:
                    if city_sprite.name == siege_city_object.name:
                        city_sprite.captured(right_army)

            for city_sprite in self.city_sprites:
                if city_sprite.name == left_army.embedded_in_city:
                    city_sprite.garrison = left_army.army_composition
                    break
            for city_sprite in self.city_sprites:
                if city_sprite.name == right_army.embedded_in_city:
                    city_sprite.garrison = right_army.army_composition
                    break

            if winner == "left":

                if right_army.commander.simple_name != "none":
                    if captured_enemy_commander == 1:
                        if right_army.commander.loyalty < 80 and random() >= right_army.commander.loyalty / 100:  # see if defeated commander defected
                            right_army.switch_country(left_army.commander.country)
                        else:
                            if right_army.embedded_in_city:
                                embedded_city = [city for city in self.city_sprites if city.name == right_army.embedded_in_city][0]
                                embedded_city.defender = ""
                                right_army.embedded_in_city = ""
                            self.kill_army(right_army)

                    else:
                        if sum(right_army.army_composition.values()) == 0:
                            closest_city, shortest_path_to_target = self.find_closest_ally_city(right_army, skip_path_calculation=True)
                            if closest_city:
                                self.send_to_closest_city(closest_city, right_army)
                            else:
                                if right_army.embedded_in_city:
                                    embedded_city = [city for city in self.city_sprites if city.name == right_army.embedded_in_city][0]
                                    embedded_city.defender = ""
                                    right_army.embedded_in_city = ""
                                self.kill_army(right_army)

                        else:
                            self.selected_enemy_army = right_army
                            self.right_preview_army = right_army
                            self.victorious_army = left_army
                            self.enemy_army_retreat_animation = 1

            else:

                if left_army.commander.simple_name != "none":
                    if captured_enemy_commander == -1:
                        if left_army.commander.loyalty < 80 and random() >= left_army.commander.loyalty / 100:  # see if defeated commander defected
                            left_army.switch_country(right_army.commander.country)
                        else:
                            if left_army.embedded_in_city:
                                embedded_city = [city for city in self.city_sprites if city.name == left_army.embedded_in_city][0]
                                embedded_city.defender = ""
                                left_army.embedded_in_city = ""
                            self.kill_army(left_army)

                    else:
                        if sum(left_army.army_composition.values()) == 0:
                            closest_city, shortest_path_to_target = self.find_closest_ally_city(left_army, skip_path_calculation=True)
                            if closest_city:
                                self.send_to_closest_city(closest_city, left_army)
                            else:
                                if left_army.embedded_in_city:
                                    embedded_city = [city for city in self.city_sprites if city.name == left_army.embedded_in_city][0]
                                    embedded_city.defender = ""
                                    left_army.embedded_in_city = ""
                                self.kill_army(left_army)

                        else:
                            self.selected_enemy_army = left_army
                            self.right_preview_army = left_army
                            self.victorious_army = right_army
                            self.enemy_army_retreat_animation = 1

            if self.resume_enemy_moving:
                self.enemy_moving = True


    def enter_battle(self, siege_defense=None, siege_city_object=None, terrain="sand"):

        if sum(self.right_preview_army.army_composition.values()) == 0:
            self.post_battle_summary(True, 1, siege_defense, siege_city_object)
        elif sum(self.left_preview_army.army_composition.values()) == 0:
            self.post_battle_summary(False, -1, siege_defense, siege_city_object)
        else:
            self.save_game(save_to="prebattle_autosave.p")
            self.previewing_battle = False
            battle = Battle(self, self.left_preview_army.commander, self.right_preview_army.commander,
                            self.left_preview_army, self.right_preview_army, custom=False,
                            siege_defense=siege_defense, siege_city_object=siege_city_object, terrain=terrain)
            battle.new()


    def post_battle_summary(self, won, captured_enemy_commander, siege_defense, siege_city_object):

        self.viewing_battle_summary = True
        self.battle_won = won
        if self.battle_won and siege_defense == "right":
            for city_sprite in self.city_sprites:
                if city_sprite.name == siege_city_object.name:
                    city_sprite.captured(self.left_preview_army)
                    break
        elif not self.battle_won and siege_defense == "left":
            for city_sprite in self.city_sprites:
                if city_sprite.name == siege_city_object.name:
                    city_sprite.captured(self.right_preview_army)
                    break

        elif (siege_defense == "left" and won) or self.left_preview_army.embedded_in_city:
            for city_sprite in self.city_sprites:
                if city_sprite.name == self.left_preview_army.embedded_in_city:
                    city_sprite.garrison = self.left_preview_army.army_composition
                    break
        elif (siege_defense == "right" and not won) or self.right_preview_army.embedded_in_city:
            for city_sprite in self.city_sprites:
                if city_sprite.name == self.right_preview_army.embedded_in_city:
                    city_sprite.garrison = self.right_preview_army.army_composition
                    break


        # Oath of Vengeance effects
        if won and "oath_of_vengeance" in self.left_preview_army.commander.skills:
            actual_left_army = [army_sprite for army_sprite in self.army_sprites if
                                army_sprite.commander.name == self.left_preview_army.commander.name][0]
            actual_left_army.action_points += 10
        elif "oath_of_vengeance" in self.right_preview_army.commander.skills:
            actual_right_army = [army_sprite for army_sprite in self.army_sprites if
                                army_sprite.commander.name == self.right_preview_army.commander.name][0]
            actual_right_army.action_points += 10

        # Burning Bridges effects
        if won and "burning_bridges" in self.right_preview_army.commander.skills:
            actual_left_army = [army_sprite for army_sprite in self.army_sprites if
                                army_sprite.commander.name == self.left_preview_army.commander.name][0]
            actual_left_army.action_points = actual_left_army.action_points // 2
        elif not won and "burning_bridges" in self.left_preview_army.commander.skills:
            actual_right_army = [army_sprite for army_sprite in self.army_sprites if
                                army_sprite.commander.name == self.right_preview_army.commander.name][0]
            actual_right_army.action_points = actual_right_army.action_points // 2


        self.battle_preview_bg = Static(LANDSCAPE_WIDTH//2, LANDSCAPE_HEIGHT//2, "sprite_battle_preview_bg.png",
                                        anchor="center")
        self.all_sprites.add(self.battle_preview_bg)
        self.battle_preview_sprites.add(self.battle_preview_bg)

        self.close_battle_preview_button = Button(self.battle_preview_bg.rect.right-35,
                                                  self.battle_preview_bg.rect.top+30,
                                                  "sprite_close_button_large.png",
                                                  command=partial(self.exit_battle_summary, captured_enemy_commander))
        self.all_sprites.add(self.close_battle_preview_button)
        self.buttons.add(self.close_battle_preview_button)
        self.battle_preview_sprites.add(self.close_battle_preview_button)

        #player side preview
        if self.left_preview_army.commander.simple_name != "none":
            self.battle_preview_player_general_avatar = Static(150, 50, "general_avatar_{}.png".format(self.left_preview_army.commander.simple_name),
                                                               anchor="topleft", scale=(70, 100))
            self.all_sprites.add(self.battle_preview_player_general_avatar)
            self.battle_preview_sprites.add(self.battle_preview_player_general_avatar)
        counter = 0
        for sp_atk in self.left_preview_army.commander.sp_atks:
            sp_atk_icon = SpecialAttackIcon(self, 150 + 55 * counter, 200, sp_atk, anchor="topleft")
            self.all_sprites.add(sp_atk_icon)
            self.battle_preview_sprites.add(sp_atk_icon)
            self.sp_atk_icons_list.append(sp_atk_icon)
            counter += 1

        counter = 0
        for skill in self.left_preview_army.commander.skills:
            sp_atk_icon = SpecialAttackIcon(self, 150 + 55 * counter, 300, skill, anchor="topleft", type="skill")
            self.all_sprites.add(sp_atk_icon)
            self.battle_preview_sprites.add(sp_atk_icon)
            self.skill_icons_list.append(sp_atk_icon)
            counter += 1

        self.left_army_unit_types = list(self.left_preview_army.army_composition.keys())
        count = 0
        for row in range(2):
            for column in range(4):
                if count < len(self.left_army_unit_types):
                    unit_type = self.left_army_unit_types[count]
                    unit_icon = UnitIcon(self, "left", column * 65 + 150, row * 62 + 400, unit_type, self.left_preview_army.commander.country)

                else:
                    unit_icon = UnitIcon(self, "left", column * 65 + 150, row * 62 + 400, None, None)
                self.battle_preview_sprites.add(unit_icon)
                self.all_sprites.add(unit_icon)
                count += 1

        #enemy side preview
        if self.right_preview_army.commander.simple_name != "none":
            self.battle_preview_enemy_general_avatar = Static(LANDSCAPE_WIDTH//2+180, 50, "general_avatar_{}.png".format(self.right_preview_army.commander.simple_name),
                                                               anchor="topleft", scale=(70, 100))
            self.all_sprites.add(self.battle_preview_enemy_general_avatar)
            self.battle_preview_sprites.add(self.battle_preview_enemy_general_avatar)

            # make enemy army info fully visible for rest of turn
            self.unit_espionage_variables[self.right_preview_army.commander.name] = [True]*8
            self.unit_size_espionage_variables[self.right_preview_army.commander.name] = [True]*8

        counter = 0
        for sp_atk in self.right_preview_army.commander.sp_atks:
            sp_atk_icon = SpecialAttackIcon(self, LANDSCAPE_WIDTH//2+180 + 55 * counter, 200, sp_atk, anchor="topleft")
            self.all_sprites.add(sp_atk_icon)
            self.battle_preview_sprites.add(sp_atk_icon)
            self.sp_atk_icons_list.append(sp_atk_icon)
            counter += 1

        counter = 0
        for sp_atk in self.right_preview_army.commander.skills:
            sp_atk_icon = SpecialAttackIcon(self, LANDSCAPE_WIDTH//2 + 180 + 55 * counter, 300, sp_atk, anchor="topleft",
                                            type="skill")
            self.all_sprites.add(sp_atk_icon)
            self.battle_preview_sprites.add(sp_atk_icon)
            self.sp_atk_icons_list.append(sp_atk_icon)
            counter += 1

        self.right_army_unit_types = list(self.right_preview_army.army_composition.keys())
        count = 0
        for row in range(2):
            for column in range(4):
                if count < len(self.right_army_unit_types):
                    unit_type = self.right_army_unit_types[count]
                    unit_icon = UnitIcon(self, "left", column * 65 + 180 + LANDSCAPE_WIDTH//2, row * 62 + 400, unit_type,
                                         self.right_preview_army.commander.country)

                else:
                    unit_icon = UnitIcon(self, "left", column * 65 + 180 + LANDSCAPE_WIDTH//2, row * 62 + 400, None, None)
                self.battle_preview_sprites.add(unit_icon)
                self.all_sprites.add(unit_icon)
                count += 1


    def exit_battle_summary(self, captured_enemy_commander=0):
        for army_sprite in self.army_sprites:
            if army_sprite.commander.name == self.left_preview_army.commander.name:
                self.left_preview_army = army_sprite
            elif army_sprite.commander.name == self.right_preview_army.commander.name:
                self.right_preview_army = army_sprite
        for sp in self.battle_preview_sprites:
            sp.kill()

        self.previewing_battle = False
        self.viewing_battle_summary = False

        if self.task_progress["southern_barbarians"] in [0,1,2,3] and self.battle_won and self.right_preview_army.commander.country == "Southern Barbarians":
            self.task_progress["southern_barbarians"] += 1
            if self.task_progress["southern_barbarians"] == 4:
                self.task_progress["southern_barbarians"] += 1
                self.queue_dilemma("southern_barbarians_suppressed")
                self.present_next_dilemma()

                if self.player_country == "Shu":
                    zhu_rong = Commander(name="Zhu Rong", simple_name="zhu_rong", age=31, loyalty=80,
                                         country="Shu", combat_prowess=72, intellect=60, command=80,
                                         charisma=69, action_points=27, level=5, experience=1200, upgrade_points=0)
                    meng_huo = Commander(name="Meng Huo", simple_name="meng_huo", age=40, loyalty=80,
                                         country="Shu", combat_prowess=70, intellect=10, command=115,
                                         charisma=25, action_points=24, level=5, experience=1200, upgrade_points=0)
                    king_duosi = Commander(name="King Duosi", simple_name="king_duosi", age=38, loyalty=80,
                                     country="Shu", combat_prowess=60, intellect=80, command=85,
                                     charisma=45, action_points=28, level=5, experience=1200, upgrade_points=0)
                    wutugu = Commander(name="Wutugu", simple_name="wutugu", age=46, loyalty=80,
                                       country="Shu", combat_prowess=80, intellect=5, command=111,
                                       charisma=10, action_points=25, level=5, experience=1200, upgrade_points=0)
                    zhu_rong_army = Army(8, 90, commander=zhu_rong,
                                         army_composition={"barbarian_spear_woman": 60, "barbarian_assassin": 30,
                                                           "barbarian_archer": 30},
                                         action_points=-1, retreat_penalty=0, anchor="midleft")
                    meng_huo_army = Army(6, 92, commander=meng_huo,
                                         army_composition={"barbarian_spear_woman": 100, "barbarian_assassin": 50,
                                                           "barbarian_archer": 50},
                                         action_points=-1, retreat_penalty=0, anchor="midleft")
                    wutugu_army = Army(5, 90, commander=wutugu,
                                         army_composition={"rattan_armor_infantry": 50,
                                                           "barbarian_archer": 50},
                                         action_points=-1, retreat_penalty=0, anchor="midleft")
                    king_duosi_army = Army(5, 90, commander=king_duosi,
                                       army_composition={"barbarian_assassin": 60,
                                                         "barbarian_archer": 80},
                                       action_points=-1, retreat_penalty=0, anchor="midleft")

                    self.add_army_to_map(zhu_rong_army, target_city="Jiang Zhou", offset_x=-28, offset_y=9)
                    self.add_army_to_map(meng_huo_army, target_city="Jiang Zhou", offset_x=-21, offset_y=-8)
                    self.add_army_to_map(king_duosi_army, target_city="Jiang Zhou", offset_x=-24, offset_y=12)
                    self.add_army_to_map(wutugu_army, target_city="Jiang Zhou", offset_x=-26, offset_y=10)

                else:
                    zhu_rong = Commander(name="Zhu Rong", simple_name="zhu_rong", age=31, loyalty=80,
                                         country="Southern Barbarians", combat_prowess=72, intellect=60, command=80,
                                         charisma=69, action_points=27, level=5, experience=1200, upgrade_points=0)
                    meng_huo = Commander(name="Meng Huo", simple_name="meng_huo", age=40, loyalty=80,
                                         country="Southern Barbarians", combat_prowess=70, intellect=10, command=115,
                                         charisma=25, action_points=24, level=5, experience=1200, upgrade_points=0)
                    king_duosi = Commander(name="King Duosi", simple_name="king_duosi", age=38, loyalty=80,
                                           country="Southern Barbarians", combat_prowess=60, intellect=80, command=85,
                                           charisma=45, action_points=28, level=5, experience=1200, upgrade_points=0)
                    wutugu = Commander(name="Wutugu", simple_name="wutugu", age=46, loyalty=80,
                                       country="Southern Barbarians", combat_prowess=80, intellect=5, command=111,
                                       charisma=10, action_points=25, level=5, experience=1200, upgrade_points=0)

                    num_player_cities = len([city_sprite for city_sprite in self.city_sprites if
                                             city_sprite.country == self.player_country])

                    zhu_rong_army = Army(25, 104, commander=zhu_rong,
                                         army_composition={"barbarian_spear_woman": 200 + num_player_cities * 5,
                                                           "barbarian_assassin": 200 + num_player_cities * 5,
                                                           "barbarian_archer": 50 + num_player_cities * 5},
                                         action_points=-1, retreat_penalty=0, anchor="midleft")
                    meng_huo_army = Army(30, 103, commander=meng_huo,
                                         army_composition={"barbarian_spear_woman": 200 + num_player_cities * 5,
                                                           "barbarian_assassin": 100 + num_player_cities * 5,
                                                           "barbarian_archer": 50 + num_player_cities * 5,
                                                           "elephant_rider": 50 + num_player_cities * 2},
                                         action_points=-1, retreat_penalty=0, anchor="midleft")
                    wutugu_army = Army(23, 105, commander=wutugu,
                                       army_composition={"rattan_armor_infantry": 200 + num_player_cities * 5,
                                                         "barbarian_archer": 200 + num_player_cities * 5},
                                       action_points=-1, retreat_penalty=0, anchor="midleft")
                    king_duosi_army = Army(27, 108, commander=king_duosi,
                                           army_composition={"barbarian_assassin": 100 + num_player_cities * 5,
                                                             "barbarian_archer": 100 + num_player_cities * 5},
                                           action_points=-1, retreat_penalty=0, anchor="midleft")

                    self.add_army_to_map(zhu_rong_army, target_city="Jiang Zhou", offset_x=-28, offset_y=9)
                    self.add_army_to_map(meng_huo_army, target_city="Jiang Zhou", offset_x=-21, offset_y=-8)
                    self.add_army_to_map(king_duosi_army, target_city="Jiang Zhou", offset_x=-24, offset_y=12)
                    self.add_army_to_map(wutugu_army, target_city="Jiang Zhou", offset_x=-26, offset_y=10)
                    self.peace_agreement("Southern Barbarians")
                    self.begin_relationship("alliance", "Southern Barbarians", self.player_country)
                    self.begin_relationship("trade", "Southern Barbarians", self.player_country)

                    # Southern Barbarians declare war against every enemy faction
                    for enemy_country in self.hostile_relationships[self.player_country]:
                        self.terminate_relationship("alliance", "Southern Barbarians", enemy_country)
                        self.terminate_relationship("trade", "Southern Barbarians", enemy_country)
                        self.begin_relationship("hostile", "Southern Barbarians", enemy_country)


            elif self.right_preview_army.commander.name == "Zhu Rong":
                self.queue_dilemma("zhu_rong_captured", target_object=None)
                self.present_next_dilemma()
            elif self.right_preview_army.commander.name == "Meng Huo":
                self.queue_dilemma("meng_huo_captured", target_object=None)
                self.present_next_dilemma()
            elif self.right_preview_army.commander.name == "King Duosi":
                self.queue_dilemma("king_duosi_captured", target_object=None)
                self.present_next_dilemma()
            elif self.right_preview_army.commander.name == "Wutugu":
                self.queue_dilemma("wutugu_captured", target_object=None)
                self.present_next_dilemma()

            self.kill_army(self.right_preview_army)
            self.right_preview_army = None
            self.selected_enemy_army = None

        elif captured_enemy_commander == 1:
            self.queue_dilemma("capture_enemy_commander", add_to_alerts=False)
            self.present_next_dilemma()
        else:
            if self.battle_won:
                if self.right_preview_army.commander.simple_name != "none":
                    if sum(self.right_preview_army.army_composition.values()) == 0:
                        closest_city, shortest_path_to_target = self.find_closest_ally_city(self.right_preview_army, skip_path_calculation=True)
                        if closest_city:
                            self.send_to_closest_city(closest_city, self.right_preview_army)
                        else:
                            self.victorious_army = self.left_preview_army
                            self.enemy_army_retreat_animation = 1
                    else:
                        self.victorious_army = self.left_preview_army
                        self.enemy_army_retreat_animation = 1
            else:
                if self.left_preview_army.commander.simple_name != "none":
                    if captured_enemy_commander == -1:
                        recruit_prob_multiplier = self.right_preview_army.commander.charisma / 50
                        if "forgery" in self.right_preview_army.commander.skills:
                            recruit_prob_multiplier *= 1.25
                        if "oath_of_vengeance" in self.right_preview_army.commander.skills:
                            recruit_prob_multiplier = 0
                        if self.left_preview_army.commander.loyalty < 80/recruit_prob_multiplier and random() >= self.left_preview_army.commander.loyalty/100/recruit_prob_multiplier: #see if player commander defected
                            self.left_preview_army.switch_country(self.right_preview_army.commander.country)
                            self.player_army_sprites.remove(self.left_preview_army)
                            self.enemy_army_sprites.add(self.left_preview_army)
                        else:
                            self.kill_army(self.left_preview_army)
                    else:
                        self.player_army_retreat_animation = 1

            if self.resume_enemy_moving:
                self.enemy_moving = True


    def recruit_enemy_commander(self):
        for sp in self.decision_window_sprites:
            sp.kill()
            if sp in self.buttons:
                self.buttons.remove(sp)
        self.upcoming_decisions.remove("capture_enemy_commander")
        self.viewing_decision_window = False
        self.current_decision = None
        self.right_preview_army.switch_country(self.player_country)
        self.enemy_army_sprites.remove(self.right_preview_army)
        self.player_army_sprites.add(self.right_preview_army)
        self.loaded_game_data["recruited_characters"].append(self.right_preview_army.commander.simple_name)
        self.loaded_game_data["num_generals_recruited"] += 1
        if sum(self.right_preview_army.army_composition.values()) == 0:
            self.right_preview_army.army_composition = {"spear_militia":30}
        if self.resume_enemy_moving:
            self.enemy_moving = True


    def kill_enemy_commander(self):
        for sp in self.decision_window_sprites:
            sp.kill()
            if sp in self.buttons:
                self.buttons.remove(sp)
        self.upcoming_decisions.remove("capture_enemy_commander")
        self.viewing_decision_window = False
        self.current_decision = None
        for city_sprite in self.city_sprites:
            if city_sprite.defender == self.right_preview_army.commander.name:
                city_sprite.defender = None
        self.general_kill_release[self.right_preview_army.commander.country] -= 15
        self.kill_army(self.right_preview_army)
        self.loaded_game_data["enemy_generals_killed"] += 1
        self.right_preview_army = None
        if self.resume_enemy_moving:
            self.enemy_moving = True


    def toggle_quick_guide(self, off=True):
        if off:
            self.buttons.remove(self.close_quick_guide_button)
            self.quick_guide_sprite.kill()
            self.close_quick_guide_button.kill()
            self.restore_toolbar_icons()
            self.viewing_quick_guide = False
        else:
            self.hide_toolbar_icons()
            self.viewing_quick_guide = True
            self.quick_guide_sprite = Static(LANDSCAPE_WIDTH // 2, LANDSCAPE_HEIGHT // 2,
                                             "sprite_campaign_quick_guide.png", anchor="center")
            self.all_sprites.add(self.quick_guide_sprite)
            self.icons.add(self.quick_guide_sprite)

            self.close_quick_guide_button = Button(self.quick_guide_sprite.rect.right, self.quick_guide_sprite.rect.top,
                                                      "sprite_close_button_large.png", command=self.toggle_quick_guide)
            self.all_sprites.add(self.close_quick_guide_button)
            self.buttons.add(self.close_quick_guide_button)
            self.icons.add(self.close_quick_guide_button)


    def view_military_report(self):
        self.persistent_game_message = self.generate_military_report()
        self.game_message_color = BLACK
        self.game_message_size = 18
        self.queue_dilemma("view_military_report", target_object=self.view_military_report)
        self.present_next_dilemma()


    def quit_game_decision(self, confirm=False):
        try:
            self.upcoming_decisions.remove("quit_game")
            del self.current_turn_dilemmas["quit_game"]
        except:
            print("Could not delete quit_game from upcoming decisions...")
        self.viewing_decision_window = False
        self.current_decision = None
        if confirm:
            self.return_to_menu()
        else:
            for sp in self.decision_window_sprites:
                try:
                    sp.kill()
                except:
                    pass
                if sp in self.buttons:
                    self.buttons.remove(sp)


    def release_enemy_commander(self):
        for sp in self.decision_window_sprites:
            sp.kill()
            if sp in self.buttons:
                self.buttons.remove(sp)
        self.right_preview_army.commander.loyalty = int(self.right_preview_army.commander.loyalty*.8)
        self.general_kill_release[self.right_preview_army.commander.country] += 15
        if sum(self.right_preview_army.army_composition.values()) == 0:
            self.right_preview_army.army_composition = {"spear_militia": 30}
        self.upcoming_decisions.remove("capture_enemy_commander")
        self.right_preview_army.action_points = 0
        self.viewing_decision_window = False
        self.current_decision = None
        self.loaded_game_data["num_generals_released"] += 1

        print("Released enemy commander...")
        if self.right_preview_army.embedded_in_city:
            embedded_city = [city for city in self.city_sprites if city.name == self.right_preview_army.embedded_in_city][0]
            embedded_city.defender = ""
            self.right_preview_army.embedded_in_city = ""

        closest_city, shortest_path_to_target = self.find_closest_ally_city(self.right_preview_army, min_distance=15, skip_path_calculation=True)
        if closest_city:
            self.send_to_closest_city(closest_city, self.right_preview_army)
        else:
            self.dead_characters.append(self.right_preview_army.commander.simple_name)
            self.kill_army(self.right_preview_army)
            self.right_preview_army = None

        if self.resume_enemy_moving:
            self.enemy_moving = True


    def kill_army(self, army_obj):
        if not army_obj:
            return
        if army_obj.commander.simple_name != "none":
            self.dead_characters.append(army_obj.commander.simple_name)
            if army_obj.commander.country == self.player_country:
                self.loaded_game_data["player_generals_killed"] += 1
            if "bequeathed_strategy" in army_obj.commander.skills:
                for army_sprite in self.army_sprites:
                    if army_sprite.commander.country == army_obj.commander.country and army_sprite.commander.name != army_obj.commander.name:
                        army_sprite.commander.upgrade_points += 50
                        break
            if army_obj.embedded_in_city: #remove commander as city's defender
                for city_sprite in self.city_sprites:
                    if city_sprite.name == army_obj.embedded_in_city:
                        city_sprite.defender = None
        try:
            army_obj.kill_self()
        except:
            pass


    def send_to_closest_city(self, city_obj, army_obj):
        if city_obj.defender:
            random_offset = choice([(-2, 2), (-2, -2), (2, 2), (2, -2)])
            delta_x = city_obj.grid_position_x - army_obj.grid_position_x + random_offset[0]
            delta_y = city_obj.grid_position_y - army_obj.grid_position_y + random_offset[1]
            army_obj.grid_position_x += delta_x
            army_obj.grid_position_y += delta_y
            army_obj.position_x += delta_x * int(self.zoom_scale*32)
            army_obj.position_y += delta_y * int(self.zoom_scale*32)
        else:
            delta_x = city_obj.grid_position_x - army_obj.grid_position_x
            delta_y = city_obj.grid_position_y - army_obj.grid_position_y
            army_obj.grid_position_x = city_obj.grid_position_x
            army_obj.grid_position_y = city_obj.grid_position_y
            army_obj.position_x += delta_x * int(self.zoom_scale*32)
            army_obj.position_y += delta_y * int(self.zoom_scale*32)
            print("Embedding...", army_obj.army_composition, city_obj.garrison)
            self.embed_army_in_city(army_obj, city_obj)


    def find_closest_ally_city(self, army_obj, manhattan=True, max_distance=999999999, min_distance=0, direction=None,
                               skip_path_calculation=False):
        closest_distance = max_distance
        closest_city = None
        shortest_path_to_target = None
        for city in self.city_sprites:
            correct_direction = True
            if direction == "NE":
                if city.grid_position_x <= army_obj.grid_position_x or city.grid_position_y >= army_obj.grid_position_y:
                    correct_direction = False
            elif direction == "SE":
                if city.grid_position_x <= army_obj.grid_position_x or city.grid_position_y <= army_obj.grid_position_y:
                    correct_direction = False
            elif direction == "NW":
                if city.grid_position_x >= army_obj.grid_position_x or city.grid_position_y >= army_obj.grid_position_y:
                    correct_direction = False
            elif direction == "SW":
                if city.grid_position_x >= army_obj.grid_position_x or city.grid_position_y <= army_obj.grid_position_y:
                    correct_direction = False

            if correct_direction:
                mh = calculate_manhattan_distance(city, army_obj)
                if city.country.lower() == army_obj.commander.country.lower() and mh <= max_distance:
                    if not skip_path_calculation:
                        shortest_path_to_target = self.calculate_shortest_path(self.weighted_grid, army_obj.grid_position_x,
                                                                               army_obj.grid_position_y, city.grid_position_x,
                                                                               city.grid_position_y, army_obj)
                    if manhattan:
                        dist = mh
                    else:
                        dist = shortest_path_to_target["cost"]
                    if dist < closest_distance and dist>= min_distance:
                        closest_distance = dist
                        closest_city = city

        if closest_city:
            print("Closest city in direction {}:".format(direction), closest_city.name)
        return closest_city, shortest_path_to_target


    def camera_zoom(self, obj, preset_offsets=None, select_obj=False):
        if preset_offsets:
            self.x_offset = preset_offsets[0]
            self.y_offset = preset_offsets[1]
        else:
            self.x_offset, self.y_offset = obj.position_x-LANDSCAPE_WIDTH//2, obj.position_y-LANDSCAPE_HEIGHT//2
        #print("offsets", self.x_offset, self.y_offset)
        if self.camera_current_x + self.x_offset <= LANDSCAPE_WIDTH//2:
            self.x_offset = -self.camera_current_x + LANDSCAPE_WIDTH//2
            self.camera_current_x = LANDSCAPE_WIDTH//2
        elif self.camera_current_x + self.x_offset >= self.map_width - LANDSCAPE_WIDTH:
            self.x_offset = self.map_width - LANDSCAPE_WIDTH//2 - self.camera_current_x
            self.camera_current_x += self.x_offset
        else:
            self.camera_current_x += self.x_offset

        if self.camera_current_y + self.y_offset <= LANDSCAPE_HEIGHT//2:
            self.y_offset = -self.camera_current_y + LANDSCAPE_HEIGHT//2
            self.camera_current_y = LANDSCAPE_HEIGHT//2
        elif self.camera_current_y + self.y_offset >= self.map_height - LANDSCAPE_HEIGHT:
            self.y_offset = self.map_height - LANDSCAPE_HEIGHT//2 - self.camera_current_y
            self.camera_current_y += self.y_offset
        else:
            self.camera_current_y += self.y_offset

        for sp in self.all_sprites:
            if sp not in self.icons and sp not in self.list_sprites and sp not in self.info_sprites:
                try:
                    sp.position_x -= self.x_offset
                    sp.position_y -= self.y_offset
                except Exception as e:
                    print(e)
                    logging.exception(e)
                    logging.info(sp)


        if obj and select_obj:
            if type(obj) == CampaignMapArmySprite:
                self.toggle_selected_army_info(None)
                self.toggle_selected_army_info(obj)
            elif type(obj) == CampaignMapCitySprite:
                self.toggle_selected_city_info(None)
                self.toggle_selected_city_info(obj)


    def calculate_city_power(self, city_obj, commander):
        army_composition = city_obj.garrison
        power = 1
        for unit_type, amount in army_composition.items():
            if unit_type in RANGED_UNITS:
                army_power = UNIT_RECRUITMENT_COST[unit_type] * amount * 2
            else:
                army_power = UNIT_RECRUITMENT_COST[unit_type] * amount
            power += army_power

        walls = [building for building in city_obj.buildings if building.name == "Walls"][0]
        power += walls.level ** 2 * 1000

        commander_bonus = commander.combat_prowess + commander.intellect + commander.command
        power *= (commander_bonus / 100) ** .5
        # print("Calculated army power for", commander.name, power)
        return power


    def calculate_army_power(self, army_obj):
        army_composition = army_obj.army_composition
        commander = army_obj.commander
        embedded_in_city = army_obj.embedded_in_city
        power = 1
        for unit_type, amount in army_composition.items():
            if unit_type in RANGED_UNITS:
                army_power = UNIT_RECRUITMENT_COST[unit_type]*amount*2
            else:
                army_power = UNIT_RECRUITMENT_COST[unit_type]*amount
            power += army_power
        if embedded_in_city:
            for city in self.city_sprites:
                if city.name == embedded_in_city:
                    walls = [building for building in city.buildings if building.name == "Walls"][0]
                    power += walls.level**2*1000
                    break

        commander_bonus = commander.combat_prowess + 1.5*commander.intellect + 2*commander.command
        power *= (commander_bonus/100)**.5
        #print("Calculated army power for", commander.name, power)
        return power


    def enemy_make_move(self):
        pg.event.pump()
        self.draw()
        self.set_diplomacy_variables()
        # enemy recruit first
        for city_sprite in self.city_sprites:
            if city_sprite.defender and city_sprite.country != self.player_country:
                self.enemy_recruit_units(city_sprite)

        if self.enemy_army_retreat_animation > 0:
            print("Enemy retreating...")
            return True

        no_more_movable_enemies = True
        if self.resume_enemy_moving and self.right_preview_army:
            self.selected_enemy_army = self.right_preview_army
            if self.right_preview_army.action_points > 0:
                no_more_movable_enemies = False

        if no_more_movable_enemies:
            for enemy_sprite in self.enemy_army_sprites:
                if enemy_sprite.action_points > 0:
                    self.selected_enemy_army = enemy_sprite
                    no_more_movable_enemies = False
                    break

        if no_more_movable_enemies:
            # Recruit units in other cities that are in danger if applicable
            for city_sprite in self.city_sprites:
                if city_sprite.country != self.player_country:
                    city_in_danger = False
                    for army_sprite in self.army_sprites:
                        if army_sprite.commander.country in self.hostile_relationships[city_sprite.country]:
                            if calculate_manhattan_distance(city_sprite, army_sprite) <= 50:
                                city_in_danger = True
                                print(city_sprite.name, "in danger")
                                break
                    if city_in_danger:
                        self.enemy_recruit_units(city_sprite)
            return False

        else:
            print(self.selected_enemy_army.commander.name)
            if self.selected_enemy_army.commander.name == "Zhang Zhao" or self.selected_enemy_army.commander.name == "Meng Da":
                print("Test")
            if self.selected_enemy_army.commander.upgrade_points > 0:
                self.selected_enemy_army.commander.auto_use_upgrade_points()

            x1, y1 = self.selected_enemy_army.grid_position_x, self.selected_enemy_army.grid_position_y
            if (x1 >= 368 - 30 and y1 <= 30) or (x1 <= 30 and y1 <= 30):
                self.searchable_range = 150
            elif self.selected_enemy_army.max_action_points * 5 >= 100:
                self.searchable_range = 100
            else:
                self.searchable_range = self.selected_enemy_army.max_action_points * 5

            self.adj_min_x = int(max([0, x1 - self.searchable_range]))
            self.adj_max_x = int(min([self.map_grid_width, x1 + self.searchable_range + 1]))
            self.adj_min_y = int(max([0, y1 - self.searchable_range]))
            self.adj_max_y = int(min([self.map_grid_height, y1 + self.searchable_range + 1]))
            self.adj_grid = [sub[self.adj_min_x:self.adj_max_x] for sub in self.map_grid[self.adj_min_y:self.adj_max_y]]

            self.weighted_grid = WeightedGrid(self.adj_grid)
            for row in range(len(self.adj_grid)):
                for col in range(len(self.adj_grid[0])):
                    cell = self.adj_grid[row][col]
                    if cell == 0:
                        self.weighted_grid.walls.append(vec(col, row))
                    else:
                        self.weighted_grid.weights[vec2int(vec(col, row))] = cell

            self.grid_offset_x = max([0, x1 - self.searchable_range])
            self.grid_offset_y = max([0, y1 - self.searchable_range])

            #Check if army already has a target from previous calculation
            search_range = sum(self.selected_enemy_army.army_composition.values())//200 + 10
            closest_target, shortest_path_to_target = self.find_closest_target(x1, y1, search_range, True)
            if closest_target:
                previous_target = None
            elif type(self.selected_enemy_army.attack_target) == CampaignMapCitySprite:
                previous_target = self.selected_enemy_army.attack_target
                if self.selected_enemy_army.attack_target.country not in self.loaded_game_data["hostile_relationships"][self.selected_enemy_army.commander.country]:
                    self.selected_enemy_army.attack_target = None
                    previous_target = None
            elif type(self.selected_enemy_army.attack_target) == CampaignMapArmySprite:
                previous_target = self.selected_enemy_army.attack_target
                if self.selected_enemy_army.attack_target.commander.country not in self.loaded_game_data["hostile_relationships"][self.selected_enemy_army.commander.country]:
                    self.selected_enemy_army.attack_target = None
                    previous_target = None
            elif self.selected_enemy_army.defend_target:
                previous_target = self.selected_enemy_army.defend_target
                if self.selected_enemy_army.defend_target.country != self.selected_enemy_army.commander.country:
                    self.selected_enemy_army.defend_target = None
                    previous_target = None
                else: #if defend target is already defended by own troops, ignore
                    for city in self.city_sprites:
                        if city.defender and city.name == self.selected_enemy_army.defend_target.name:
                            self.selected_enemy_army.defend_target = None
                            previous_target = None
                            self.selected_enemy_army.action_points = 0
                            break
            else:
                previous_target = None


            if previous_target:
                print("Using previous target...")
                x2, y2 = previous_target.grid_position_x, previous_target.grid_position_y
                if x1 == x2 and y1 == y2:
                    if random() <= .5:
                        x2 += choice([1, -1])
                    else:
                        y2 += choice([1, -1])
                shortest_path_to_target = self.calculate_shortest_path(self.weighted_grid, x1, y1, x2, y2, self.selected_enemy_army)

                if not shortest_path_to_target["path"]: #if unreachable, just stay in place
                    self.selected_enemy_army.action_points = 0
                else:
                    self.army_movement_grid_coords = []
                    for coord in shortest_path_to_target["path"]:
                        self.army_movement_grid_coords.append(coord)
                    self.army_movement_grid_coords.reverse()

            else:
                print(self.selected_enemy_army.commander.name, "checking for danger")
                #see if self in danger; if so, retreat to closest own city
                in_danger = self.check_if_in_danger(self.selected_enemy_army)
                passive_commander = "Scholar" in self.selected_enemy_army.commander.skills or "Intellectual" in self.selected_enemy_army.commander.skills or "wealthy_merchant" in self.selected_enemy_army.commander.skills
                defensive_commander = self.selected_enemy_army.commander.combat_prowess + self.selected_enemy_army.commander.command <= 120

                search_range = sum(self.selected_enemy_army.army_composition.values())//200 + 10
                easy_target_in_sight = self.find_closest_target(x1, y1, easy_targets_range=search_range, quick_search=True)[0]

                if (in_danger or ((defensive_commander or passive_commander) and (self.selected_enemy_army.commander.level-4)/5 <= random())) and not easy_target_in_sight:
                    print(self.selected_enemy_army.commander.name, "defensive")
                    closest_ally_city = self.retreat_to_closest_ally_city(self.selected_enemy_army)
                    if closest_ally_city:
                        self.enemy_recruit_units(closest_ally_city, variety_limit=self.selected_enemy_army.army_composition.copy())
                        if sum(self.selected_enemy_army.army_composition.values()) <= 150: #recruit more units if needed
                            self.enemy_recruit_units(closest_ally_city, variety_limit=self.selected_enemy_army.army_composition.copy())

                else: #find closest target to attack or travel to
                    print(self.selected_enemy_army.commander.name, "looking for target")
                    closest_target, shortest_path_to_target = self.find_closest_target(x1, y1)
                    reachable_target = False
                    if closest_target:
                        x2, y2 = closest_target.grid_position_x, closest_target.grid_position_y
                        if x1 == x2 and y1 == y2:
                            if random() <= .5:
                                x2 += choice([1, -1])
                            else:
                                y2 += choice([1, -1])

                        if abs(x1-x2) < self.searchable_range and abs(y1-y2) < self.searchable_range:
                            reachable_target = True
                            shortest_path_to_target = self.calculate_shortest_path(self.weighted_grid, x1, y1, x2, y2, self.selected_enemy_army)

                            if shortest_path_to_target["cost"] <= self.selected_enemy_army.max_action_points or (random() >= shortest_path_to_target["cost"]/(self.selected_enemy_army.max_action_points*5) - .15*self.difficulty or not self.selected_enemy_army.embedded_in_city):
                                self.army_movement_grid_coords = []
                                for coord in shortest_path_to_target["path"]:
                                    self.army_movement_grid_coords.append(coord)
                                self.army_movement_grid_coords.reverse()
                                self.selected_enemy_army.attack_target = closest_target
                                self.selected_enemy_army.defend_target = None
                            else:
                                #print(shortest_path_to_target["cost"], print(self.selected_enemy_army.max_action_points))
                                reachable_target = False

                    if not reachable_target: #if no suitable target; go defend closest city, if not defended, and recruit
                        print("Closest target not reachable...")
                        if self.selected_enemy_army.embedded_in_city:
                            for city_sprite in self.city_sprites:
                                if city_sprite.name == self.selected_enemy_army.embedded_in_city:
                                    self.enemy_recruit_units(city_sprite, variety_limit=self.selected_enemy_army.army_composition.copy())

                            self.selected_enemy_army.action_points = 0

                        else:
                            if self.selected_enemy_army.grid_position_x <= 50 and self.selected_enemy_army.grid_position_y <= 50:  # check if in corner city
                                closest_ally_city = self.retreat_to_closest_ally_city(self.selected_enemy_army,
                                                                                      max_distance=180,
                                                                                      min_distance=60,
                                                                                      direction="SE")
                            elif self.selected_enemy_army.grid_position_x >= 368 - 50 and self.selected_enemy_army.grid_position_y <= 50:  # check if in corner city
                                closest_ally_city = self.retreat_to_closest_ally_city(self.selected_enemy_army,
                                                                                      max_distance=180,
                                                                                      min_distance=60,
                                                                                      direction="SW")
                            elif self.selected_enemy_army.grid_position_x <= 50 and self.selected_enemy_army.grid_position_y >= 288 - 50:  # check if in corner city
                                closest_ally_city = self.retreat_to_closest_ally_city(self.selected_enemy_army,
                                                                                      max_distance=180,
                                                                                      min_distance=60,
                                                                                      direction="NE")
                            elif self.selected_enemy_army.grid_position_x >= 368 - 50 and self.selected_enemy_army.grid_position_y >= 288 - 50:  # check if in corner city
                                closest_ally_city = self.retreat_to_closest_ally_city(self.selected_enemy_army,
                                                                                      max_distance=180,
                                                                                      min_distance=60,
                                                                                      direction="NW")
                            else:
                                closest_ally_city = self.retreat_to_closest_ally_city(self.selected_enemy_army)

                            if closest_ally_city:
                                print("Recruiting more units and sending army to defend", closest_ally_city.name, self.selected_enemy_army.commander.name)
                                self.enemy_recruit_units(closest_ally_city, variety_limit=self.selected_enemy_army.army_composition.copy())
                            else:
                                self.selected_enemy_army.action_points = 0


            # Recruit units in other cities that are in danger if applicable
            for city_sprite in self.city_sprites:
                if city_sprite.country == self.selected_enemy_army.commander.country:
                    city_in_danger = False
                    for army_sprite in self.army_sprites:
                        if army_sprite.commander.country in self.hostile_relationships[city_sprite.country]:
                            if calculate_manhattan_distance(city_sprite, army_sprite) <= 50:
                                city_in_danger = True
                                break

                    if city_in_danger or (self.loaded_game_data["gold"][city_sprite.country]/20000 >= random()):
                        self.enemy_recruit_units(city_sprite)

            return True


    def retreat_to_closest_ally_city(self, army_obj, max_distance=999999999, min_distance=0, direction=None):
        if army_obj.embedded_in_city:
            self.selected_enemy_army.action_points = 0
            return [city_sprite for city_sprite in self.city_sprites if city_sprite.name == army_obj.embedded_in_city][0]

        x1 = army_obj.grid_position_x
        y1 = army_obj.grid_position_y
        closest_ally_city, shortest_path_to_target = self.find_closest_ally_city(self.selected_enemy_army, manhattan=False,
                                                        max_distance=self.selected_enemy_army.max_action_points*3,
                                                        min_distance=min_distance, direction=direction)
        if not closest_ally_city:
            closest_ally_city, shortest_path_to_target = self.find_closest_ally_city(self.selected_enemy_army, min_distance=min_distance,
                                                            direction=direction)
        if not closest_ally_city:
            army_obj.action_points = 0
            print(army_obj.commander.name, "can't find closest ally city")
            return

        x2, y2 = closest_ally_city.grid_position_x, closest_ally_city.grid_position_y
        if closest_ally_city.defender and closest_ally_city.defender != self.selected_enemy_army.commander.name:
            random_offset = choice([(-1, 1), (1, 1), (-1, -1), (1, -1)])
            x2 += random_offset[0]
            y2 += random_offset[1]
        if x1 == x2 and y1 == y2:
            self.selected_enemy_army.action_points = 0  # if already in closest city; don't move
        else:
            #shortest_path_to_target = self.calculate_shortest_path(self.weighted_grid, x1, y1, x2, y2, self.selected_enemy_army)
            self.army_movement_grid_coords = []
            for coord in shortest_path_to_target["path"]:
                self.army_movement_grid_coords.append(coord)
            self.army_movement_grid_coords.reverse()

            army_obj.defend_target = closest_ally_city
            army_obj.attack_target = None

        return closest_ally_city



    def check_if_in_danger(self, army_obj):
        if sum(army_obj.army_composition.values()) <= 50:
            return True
        for army_sprite in self.army_sprites:
            if army_sprite.commander.country in self.hostile_relationships[army_obj.commander.country]:

                if army_obj.embedded_in_city: #if embedded in city, check if city would be in danger if army leaves
                    for city_sprite in self.city_sprites:
                        if city_sprite.name == army_obj.embedded_in_city:
                            walls = [building for building in city_sprite.buildings if building.name == "Walls"][0]
                            danger = self.calculate_army_power(army_sprite) >= (walls.level**2*1000 + 1) * 1.5
                            close = calculate_manhattan_distance(army_sprite, city_sprite) <= 30
                            if danger and close:
                                print(army_obj.embedded_in_city, "in danger... Staying to defend city.")
                                return True
                            else:
                                break
                else:
                    danger = self.calculate_army_power(army_sprite) >= self.calculate_army_power(army_obj)*1.25
                    close = calculate_manhattan_distance(army_sprite, army_obj) <= 30
                    if danger and close:
                        print(army_obj.commander.name, "in danger... Taking defensive position in city.")
                        return True


    def enemy_recruit_units(self, city_obj, variety_limit={}):
        #if city_obj.name == "Gui Lin":
        #    print("Attemping to recruit units in", city_obj.name)
        barracks = [building for building in city_obj.buildings if building.name == "Barracks"][0]
        try:
            trade_port_level = [building for building in city_obj.buildings if building.name == "Trade Port"][0].level
        except:
            trade_port_level = 0
        country = city_obj.country
        if country not in self.available_countries:
            return
        recruitment_limit = barracks.level*50+(self.difficulty-1)*200-city_obj.units_recruited_this_turn
        c1 = self.calculate_projected_food_delta(country) * 10 + self.loaded_game_data["food"][country] > 0
        c2 = self.calculate_projected_gold_delta(country) * 10 + self.loaded_game_data["gold"][country] > 0
        if not (c1 and c2):
            return

        if len(variety_limit)>=6:
            recruitable_units = list(variety_limit.keys())
        elif len(city_obj.garrison) == 8:
            recruitable_units = list(city_obj.garrison.keys())
        else:
            recruitable_units = RECRUITABLE_UNIT_DICT[barracks.level].copy()
            if country == "Shu" and self.turn_number >= 25:
                recruitable_units += ["barbarian_spear_woman", "barbarian_assassin", "barbarian_archer",]
            elif country == "Wei" and barracks.level >= 4:
                recruitable_units += ["tiger_knight_cavalry"]

            if barracks.level >=2 and trade_port_level >= 1:
                recruitable_units += ["light_dagger_infantry"]

            if city_obj.defender:
                if city_obj.defender == "Ding Feng" and "light_dagger_infantry" not in recruitable_units:
                    recruitable_units += ["light_dagger_infantry"]

            if "fire_arrows" in self.acquired_tech[country] and barracks.level >=2:
                recruitable_units.append("flaming_arrow_infantry")
            if "crossbows" in self.acquired_tech[country] and barracks.level >=3:
                recruitable_units.append("crossbow_infantry")
            if "siege_weaponry" in self.acquired_tech[country] and barracks.level >=4:
                recruitable_units.append("catapult")


        if self.difficulty*25/100 + .1 >= random() and barracks.level >= 2: #smarter AI will tend to recruit better units instead of random
            for ru in recruitable_units:
                if "militia" in ru:
                    recruitable_units.remove(ru)

        recruited_units = {}
        for unit_type in city_obj.garrison:
            recruited_units[unit_type] = 0

        ranged_discount = 0
        melee_discount = 0
        temp_unit_recruitment_cost = UNIT_RECRUITMENT_COST.copy()
        for city_sprite in self.city_sprites:
            if city_sprite.country == city_obj.country:
                for building in city_sprite.buildings:
                    if building.name == "Forest":
                        ranged_discount += building.level
                    elif building.name == "Iron Mine":
                        melee_discount += building.level
        #print("Discounts", melee_discount, ranged_discount)
        for unit_type in recruitable_units:
            if unit_type in RANGED_UNITS:
                temp_unit_recruitment_cost[unit_type] = int(temp_unit_recruitment_cost[unit_type]*(100-ranged_discount)/100)
            if unit_type not in RANGED_UNITS:
                temp_unit_recruitment_cost[unit_type] = int(temp_unit_recruitment_cost[unit_type]*(100-melee_discount)/100)
            if unit_type in CAVALRY_UNITS:
                if country == "Ma Teng":
                    temp_unit_recruitment_cost[unit_type] = int(temp_unit_recruitment_cost[unit_type]*.8)
            if "militia" in unit_type:
                if country == "Liu Biao":
                    temp_unit_recruitment_cost[unit_type] = int(temp_unit_recruitment_cost[unit_type]*.7)

            if temp_unit_recruitment_cost[unit_type] <= 1:
                temp_unit_recruitment_cost[unit_type] = 1


        while recruitment_limit >= 10 and self.loaded_game_data["gold"][country] >= 5000 and self.loaded_game_data["food"][country]+10*self.calculate_projected_food_delta(country) > 0:
            is_random_unit_ranged = True if random() <= .4 else False
            possible_units = [unit_type for unit_type in recruitable_units if (unit_type in RANGED_UNITS) == is_random_unit_ranged]
            if not possible_units:
                break
            random_unit = choice(possible_units)
            cost = temp_unit_recruitment_cost[random_unit]*10
            recruit_random_unit = False
            if len(recruited_units) >= 8 and random_unit not in recruited_units:
                pass
            elif cost <= self.loaded_game_data["gold"][country] and (cost/100)**2 >= random():
                recruit_random_unit = True

            if recruit_random_unit:
                self.loaded_game_data["gold"][country] -= cost
                city_obj.units_recruited_this_turn += 10
                recruitment_limit = barracks.level * 50 - city_obj.units_recruited_this_turn
                if random_unit in recruited_units:
                    recruited_units[random_unit] += 10
                else:
                    recruited_units[random_unit] = 10
                print("Enemy recruited units in...", city_obj.name)

        updated_garrison = concat_dicts(city_obj.garrison, recruited_units)
        city_obj.garrison = updated_garrison.copy()
        if city_obj.defender:
            for army_sprite in self.army_sprites:
                if army_sprite.commander.name == city_obj.defender:
                    army_sprite.army_composition = city_obj.garrison.copy()


    def enemy_upgrade_buildings(self, country):

        if self.loaded_game_data["gold"][country] <= 4000:
            return

        if self.loaded_game_data["food"][country] < 100:
            priority_dict = {"Market": 0, "Farm": 1, "Walls": 0, "Barracks": 0, "Gold Mine": 0,
                             "Iron Mine": 0, "Forest": 0, "Quarry": 0, "Academy": 0}
        else:
            priority_dict = {"Market":.1, "Farm":.2, "Walls":.05, "Barracks":.05, "Gold Mine":.1, "Iron Mine":.025,
                             "Forest":.02, "Quarry":.02, "Academy":.1}

        for city in self.city_sprites:
            if city.country == country:
                for building_name, priority in priority_dict.items():
                    if random() <= priority:
                        try:
                            building = [b for b in city.buildings if b.name == building_name][0]
                            if building.level < building.max_level:
                                cond_1 = self.loaded_game_data["gold"][country] >= building.upgrade_cost[building.level]
                                cond_2 = building.upgrade_progress == -1
                                if cond_1 and cond_2:
                                    upgrade_turns_multiplier = 1
                                    for army_sprite in self.army_sprites:
                                        if army_sprite.commander.country == country:
                                            if "commissioner_of_development" in army_sprite.commander.skills:
                                                upgrade_turns_multiplier -= .3
                                                break

                                    self.loaded_game_data["gold"][country] -= building.upgrade_cost[building.level]
                                    building.upgrade_progress = math.ceil(building.upgrade_turns[building.level]*upgrade_turns_multiplier)
                                    print("Upgrading", building_name)
                        except:
                            pass


    def find_closest_target(self, x1, y1, easy_targets_range=10, quick_search=False): #look for closest target for enemy, pass in x,y coords for enemy army
        potential_target_paths = {}

        if easy_targets_range: #look for "free" enemy armies within 10 cells
            for army in self.army_sprites:
                mh = calculate_manhattan_distance(army, self.selected_enemy_army)
                if army.commander.country in self.hostile_relationships[self.selected_enemy_army.commander.country] and mh <= min([easy_targets_range, 30]):
                    power_ratio = self.calculate_army_power(self.selected_enemy_army)/(self.calculate_army_power(army) + 1)
                    x2, y2 = army.grid_position_x, army.grid_position_y
                    if x1 == x2 and y1 == y2:
                        if random() <= .5:
                            x2 += choice([1, -1])
                        else:
                            y2 += choice([1, -1])
                    shortest_path_to_target = self.calculate_shortest_path(self.weighted_grid, x1, y1, x2, y2,
                                                                         self.selected_enemy_army)
                    if shortest_path_to_target["cost"] < self.searchable_range:
                        if power_ratio >= 4:
                            return army, shortest_path_to_target

            for city in self.city_sprites:
                mh = calculate_manhattan_distance(city, self.selected_enemy_army)
                if city.country in self.hostile_relationships[self.selected_enemy_army.commander.country] and mh <= easy_targets_range and not city.defender:
                    walls = [building for building in city.buildings if building.name == "Walls"][0]
                    power_ratio = self.calculate_army_power(self.selected_enemy_army)/(walls.level**2*1000 + 1)
                    x2, y2 = city.grid_position_x, city.grid_position_y
                    shortest_path_to_target = self.calculate_shortest_path(self.weighted_grid, x1, y1, x2, y2,
                                                                         self.selected_enemy_army)
                    if shortest_path_to_target["cost"] < self.searchable_range:
                        if power_ratio >= 4:
                            return city, shortest_path_to_target

        if quick_search:
            return None, None

        for city in self.city_sprites:
            mh = calculate_manhattan_distance(city, self.selected_enemy_army)
            if city.country in self.hostile_relationships[self.selected_enemy_army.commander.country] and mh <= self.searchable_range:
                if city.defender:
                    defending_commander = [army for army in self.army_sprites if army.commander.name == city.defender][0].commander
                    power_ratio = self.calculate_army_power(self.selected_enemy_army) / (self.calculate_city_power(city, defending_commander))
                else:
                    power_ratio = self.calculate_army_power(self.selected_enemy_army) / (self.calculate_city_power(city, self.no_general_armies[city.country]))
                x2, y2 = city.grid_position_x, city.grid_position_y
                if x1 == x2 and y1 == y2:
                    if random() <= .5:
                        x2 += choice([1, -1])
                    else:
                        y2 += choice([1, -1])
                shortest_path_results = self.calculate_shortest_path(self.weighted_grid, x1, y1, x2, y2, self.selected_enemy_army)
                # print(city.name, shortest_path_results["cost"]/power_ratio)
                if shortest_path_results["cost"] <= self.searchable_range:
                    city_population_bonus = 5000/city.population
                    if power_ratio >= 10:
                        power_ratio = 10
                    potential_target_paths[city] = shortest_path_results["cost"]/power_ratio*city_population_bonus

            elif city.country == self.selected_enemy_army.commander.country and mh <= self.searchable_range and self.selected_enemy_army.embedded_in_city != city.name:
                #print(self.selected_enemy_army.commander.name, "target own city", city.name)
                garrisoned_troop_bonus = sum(city.garrison.values()) // 10
                if self.check_if_army_weak(self.selected_enemy_army) and not city.defender and garrisoned_troop_bonus >= 20: #see if any cities contain own units to be picked up
                    #print(self.selected_enemy_army.commander.name, "picking up troops from", city.name)
                    x2, y2 = city.grid_position_x, city.grid_position_y
                    if x1 == x2 and y1 == y2:
                        if random() <= .5:
                            x2 += choice([1, -1])
                        else:
                            y2 += choice([1, -1])
                    shortest_path_results = self.calculate_shortest_path(self.weighted_grid, x1, y1, x2, y2, self.selected_enemy_army)
                    if garrisoned_troop_bonus >= shortest_path_results["cost"]:
                        garrisoned_troop_bonus = shortest_path_results["cost"]
                    potential_target_paths[city] = shortest_path_results["cost"] - garrisoned_troop_bonus
                    #if shortest_path_results["cost"] - garrisoned_troop_bonus < self.searchable_range:
                        #shortest_distance = shortest_path_results["cost"] - garrisoned_troop_bonus

        for army in self.army_sprites:
            mh = calculate_manhattan_distance(army, self.selected_enemy_army)
            power_ratio = self.calculate_army_power(self.selected_enemy_army)/(self.calculate_army_power(army)+1)
            if army.commander.country in self.hostile_relationships[self.selected_enemy_army.commander.country] and mh <= self.searchable_range:
                x2, y2 = army.grid_position_x, army.grid_position_y
                if x1 == x2 and y1 == y2:
                    if random() <= .5:
                        x2 += choice([1, -1])
                    else:
                        y2 += choice([1, -1])
                shortest_path_results = self.calculate_shortest_path(self.weighted_grid, x1, y1, x2, y2, self.selected_enemy_army)
                if shortest_path_results["cost"] < self.searchable_range:
                    if power_ratio >= 10:
                        power_ratio = 10
                    potential_target_paths[army] = shortest_path_results["cost"]/power_ratio

        if potential_target_paths:
            closest_target = min(potential_target_paths, key=potential_target_paths.get)
            if type(closest_target) == CampaignMapArmySprite:
                print(self.selected_enemy_army.commander.name, "found closest target...", closest_target.commander.name)
            elif type(closest_target) == CampaignMapCitySprite:
                print(self.selected_enemy_army.commander.name, "found closest target...", closest_target.name)
            return closest_target, shortest_path_results
        else:
            print(self.selected_enemy_army.commander.name, "could not find suitable target...")
            return None, None


    def check_if_army_weak(self, army, search_range=75):
        for army_sprite in self.army_sprites:
            if army_sprite.commander.country in self.hostile_relationships[army.commander.country]:
                mh = calculate_manhattan_distance(army_sprite, army)
                if mh <= search_range and self.calculate_army_power(army_sprite)/self.calculate_army_power(army) >= 1.25:
                    return True


    def enemy_end_turn(self):
        self.enemy_moving = False
        self.resume_enemy_moving = False

        xu_chang_target = [city_sprite for city_sprite in self.city_sprites if city_sprite.name == "Xu Chang"][0]
        if "realm_divide" in self.current_turn_dilemmas:
            self.current_turn_dilemmas = {"objective": {"short_summary": DECISION_TEXT["objective"],
                                                        "target": xu_chang_target},
                                          "view_military_report": {"short_summary": DECISION_TEXT["view_military_report"],
                                                              "target": self.view_military_report},
                                          "realm_divide": {"short_summary": DECISION_TEXT["realm_divide"],
                                                           "target": None}}
        else:
            self.current_turn_dilemmas = {"objective": {"short_summary": DECISION_TEXT["objective"],
                                                        "target": xu_chang_target},
                                          "view_military_report": {"short_summary": DECISION_TEXT["view_military_report"],
                                                        "target": self.view_military_report},}

        for city_sprite in self.city_sprites:
            if "natural_disaster_fire" not in city_sprite.effects: #update buildings unless city is on fire
                for building in city_sprite.buildings:
                    building_leveled_up = building.update()
                    if building_leveled_up and city_sprite.country == self.player_country:
                        if "construction" in self.dynamic_decision_text:
                            if DECISION_TEXT["construction"].format(city_sprite.name) not in self.dynamic_decision_text["construction"]:
                                self.queue_dilemma("construction", target_object=city_sprite)
                                self.dynamic_decision_text["construction"].append(DECISION_TEXT["construction"].format(city_sprite.name))
                        else:
                            self.queue_dilemma("construction", target_object=city_sprite)
                            self.dynamic_decision_text["construction"] = [DECISION_TEXT["construction"].format(city_sprite.name)]

            city_sprite.update_state()
        for army_sprite in self.army_sprites:
            army_sprite.reset_state()
        for country in self.available_countries:
            self.loaded_game_data["gold"][country] += self.calculate_upkeep_cost(country)
            self.loaded_game_data["gold"][country] += self.calculate_total_trade_revenue(country)
            if country == self.player_country:
                self.loaded_game_data["gold_produced"] += self.calculate_total_trade_revenue(country)
                print("upkeep", self.calculate_upkeep_cost(country))
                print("trade", self.calculate_total_trade_revenue(country))
            if country != self.player_country:
                self.enemy_recruit_characters(country)
                self.enemy_upgrade_buildings(country)
        for country in self.available_countries:
            self.loaded_game_data["food"][country] += self.calculate_projected_food_delta(country)
            if self.loaded_game_data["food"][country] < 0 and country != self.player_country:
                print("{} Food shortage!".format(country))
                # if AI runs out of food, address shortage by paying gold
                while self.loaded_game_data["gold"][country] >= 5000 and self.loaded_game_data["food"][country] < 0:
                    self.loaded_game_data["food"][country] += 10
                    self.loaded_game_data["gold"][country] -= 2000


        print(self.loaded_game_data["gold"], "|", self.loaded_game_data["food"])

        self.turn_number += 1
        self.update_tech_tree()
        self.update_recruitable_characters_from_pool()
        self.pre_turn_start_check()
        self.set_espionage_variables()
        self.restore_toolbar_icons()

        self.toolbar_end_turn_icon.tooltip = "Click to end your turn. \n\nCurrent turn: {}{} \n\nYear {} ({})".format(self.turn_number, "                    ", 200 + self.turn_number//2, "Spring" if self.turn_number%2==1 else "Fall")
        try: #bring camera back to one of player's armies or cities
            self.zoom_to_next_army()
        except:
            try:
                self.zoom_to_next_city()
            except:
                pass

        print("Turn ended. Beginning turn", self.turn_number)
        print("Hostile relationships", self.hostile_relationships)


    def update_tech_tree(self):
        for country in self.available_countries:
            self.tech_tree[country][self.current_tech[country]] += 1
            turns_till_mastery = math.ceil(TECH_TREE[self.current_tech[country]]["turns"]/self.calculate_learning_rate(country)) - self.tech_tree[country][self.current_tech[country]]
            if turns_till_mastery <= 0 and self.current_tech[country] not in self.acquired_tech[country]:
                self.acquired_tech[country].append(self.current_tech[country])
                if country == self.player_country:
                    tech_friendly_name = TECH_TREE[self.current_tech[country]]["friendly_name"]
                    self.queue_dilemma("new_tech_mastered", short_summary="We have mastered the following tech: {}!".format(tech_friendly_name))
                    self.dynamic_decision_text["new_tech_mastered"] = [DECISION_TEXT["new_tech_mastered"].format(tech_friendly_name)]
                self.assign_random_tech(country)


    def assign_random_tech(self, country):
        learnable_tech = []
        for tech, values in TECH_TREE.items():
            if tech not in self.acquired_tech[country]:
                if tech in self.tech_tree[country]:
                    turns_till_mastery = math.ceil(TECH_TREE[tech]["turns"]/self.calculate_learning_rate(country)) - self.tech_tree[country][tech]
                else:
                    turns_till_mastery = math.ceil(TECH_TREE[tech]["turns"] / self.calculate_learning_rate(country))

                if turns_till_mastery <= 0: #if already mastered (due to new buildings or other bonus, add to acquired tech
                    self.acquired_tech[country].append(self.current_tech[country])
                elif tech in self.tech_tree[country]: #if already learning, auto assign
                    self.current_tech[country] = tech
                    return
                else: #otherwise, check if can be added to pool to be randomly selected
                    prereqs = values["prerequisites"]
                    prereqs_met = True
                    for req in prereqs:
                        if req not in self.acquired_tech[country]:
                            prereqs_met = False
                    if prereqs_met:
                        learnable_tech.append(tech)

        if learnable_tech:
            random_tech = choice(learnable_tech)
            self.current_tech[country] = random_tech
            self.tech_tree[country][random_tech] = 0


    def update_recruitable_characters_from_pool(self):
        current_year = 200 + self.loaded_game_data["turn_number"]//2 #2 turns per year
        current_recruitable_characters = []
        current_active_characters = []
        for val in self.recruitable_characters.values():
            current_recruitable_characters += val
        for army_sprite in self.army_sprites:
            current_active_characters.append(army_sprite.commander.simple_name)


        if self.turn_number == 1:
            return

        new_character_probabilities = {"Shu":0, "Wei":.3, "Wu":0, "Ma Teng":0, "Liu Biao":0, "Yuan Shao":0,
                                       "Liu Zhang":0, "Zhang Lu":0, "Southern Barbarians":0}

        for army_sprite in self.army_sprites:
            if "referral" in army_sprite.commander.skills:
                new_character_probabilities[army_sprite.commander.country] += .25
        for city_sprite in self.city_sprites:
            if city_sprite.country in new_character_probabilities:
                try:
                    academy = [building for building in city_sprite.buildings if building.name == "Academy"][0]
                    new_character_probabilities[city_sprite.country] += academy.level*.05
                except:
                    pass

        # print("character pool", list(self.character_pool.keys()))
        for character in self.character_pool:
            character_obj = self.character_pool[character]

            cond_1 = character in self.loaded_game_data["recruited_characters"] + current_active_characters + current_recruitable_characters + self.dead_characters
            cond_2 = current_year < character_obj.earliest_start_year
            cond_3 = random() <= new_character_probabilities[character_obj.country]

            for country in self.available_countries:
                if not (cond_1 or cond_2):
                    character_country = ""
                    if character_obj.hometown:
                        for city_sprite in self.city_sprites:
                            if city_sprite.name == character_obj.hometown:
                                character_country = city_sprite.country
                                character_obj.country = city_sprite.country
                                break
                    else:
                        character_country = character_obj.country

                    #if character_country == self.player_country and cond_3:
                    if character_country == country and cond_3:
                        if character not in current_recruitable_characters:
                            if character_country == self.player_country:
                                if "new_recruitable_character" not in self.upcoming_decisions:
                                    self.queue_dilemma("new_recruitable_character", short_summary="New Recruitable Character! Go to any city with an Academy building to view details.")
                                    #print("****************Alert:", character, country, self.upcoming_decisions)
                                self.game_alerts["building"].append("Academy")
                                #print(self.game_alerts)
                            if character_country in self.recruitable_characters:
                                self.recruitable_characters[character_country].append(character)
                            else:
                                self.recruitable_characters[character_country] = [character]
        #print("Active:", current_active_characters)
        #print("Dead:", self.dead_characters)
        #print("Recruitable:", self.recruitable_characters)


    def enemy_recruit_characters(self, country):
        for city_sprite in self.city_sprites:
            if city_sprite.country == country:
                if [building for building in city_sprite.buildings if building.name == "Academy"] or country != self.player_country:
                    for recruitable_character in self.recruitable_characters[country]:
                        if country.lower() == "wei":
                            cost_to_recruit_character = 0
                        else:
                            cost_to_recruit_character = 2 * (self.character_pool[recruitable_character].combat_prowess + self.character_pool[recruitable_character].intellect + self.character_pool[recruitable_character].command + self.character_pool[recruitable_character].charisma + len(self.character_pool[recruitable_character].learnable_skills) * 300 + len(self.character_pool[recruitable_character].learnable_attacks) * 300)
                        if cost_to_recruit_character + 3000 <= self.loaded_game_data["gold"][country] or cost_to_recruit_character == 0:
                            self.loaded_game_data["gold"][country] -= cost_to_recruit_character
                            base_exp = 50*(200 + self.turn_number//2 - self.character_pool[recruitable_character].earliest_start_year)
                            if self.difficulty == 1:
                                new_army_composition = {"spear_militia":50}
                                self.character_pool[recruitable_character].gain_exp(base_exp)
                            elif self.difficulty == 2:
                                new_army_composition = {"spear_infantry":50}
                                self.character_pool[recruitable_character].gain_exp(500+base_exp)
                            else:
                                new_army_composition = {"spear_infantry":100}
                                self.character_pool[recruitable_character].gain_exp(1200+base_exp)

                            self.character_pool[recruitable_character].country = country
                            new_army = Army(city_sprite.grid_position_x + randrange(-2, 3),
                                            city_sprite.grid_position_y + randrange(-1, 2),
                                            commander=self.character_pool[recruitable_character],
                                            army_composition=new_army_composition, action_points=-1, retreat_penalty=0, anchor="midleft")
                            self.recruitable_characters[country].remove(recruitable_character)

                            print(country, "Recruited", recruitable_character)
                            if not city_sprite.defender:
                                self.add_army_to_map(new_army, target_city=city_sprite.name, offset_x=randrange(-2, 3),
                                                     offset_y=randrange(-1, 2), embed_in_city=True)
                            else:
                                self.add_army_to_map(new_army, target_city=city_sprite.name, offset_x=randrange(-2, 3),
                                                     offset_y=randrange(-1, 2))


    def set_espionage_variables(self):
        self.espionage_rate = randrange(1,11)/100
        for army_sprite in self.army_sprites:
            if army_sprite.commander.country == self.player_country:
                if "messenger_pigeon" in army_sprite.commander.skills:
                    self.espionage_rate += .1
                if "emergency_messenger" in army_sprite.commander.skills:
                    self.espionage_rate += .2
                if "military_advisor" in army_sprite.commander.skills:
                    self.espionage_rate += .3
                if "foresight" in army_sprite.commander.skills:
                    self.espionage_rate += .4


        self.unit_espionage_variables = {"other": [random() <= self.espionage_rate for i in range(8)]}
        self.unit_size_espionage_variables = {"other": [random() <= self.espionage_rate for i in range(8)]}

        for city_sprite in self.city_sprites:
            #calculate "visibility" w/ respect to the espionage radius
            visibility_reduction = self.calculate_distance_to_closest_player_object(city_sprite)/(15+self.espionage_rate*15)
            self.unit_espionage_variables[city_sprite.name] = [random()*visibility_reduction <= self.espionage_rate for i in range(8)]
            self.unit_size_espionage_variables[city_sprite.name] = [random()*visibility_reduction <= self.espionage_rate for i in range(8)]
        for army_sprite in self.army_sprites:
            visibility_reduction = self.calculate_distance_to_closest_player_object(army_sprite)/(15+self.espionage_rate*15)
            self.unit_espionage_variables[army_sprite.commander.name] = [random()*visibility_reduction <= self.espionage_rate for i in range(8)]
            self.unit_size_espionage_variables[army_sprite.commander.name] = [random()*visibility_reduction <= self.espionage_rate for i in range(8)]


    def generate_new_country(self, country):
        self.alliance_relationships[country] = []
        self.trade_relationships[country] = []
        self.hostile_relationships[country] = []
        self.peace_relationships[country] = 0
        self.diplomacy_penalty[country] = 0
        self.general_kill_release[country] = 0
        self.loaded_game_data["tax"][country] = .1
        self.loaded_game_data["gold"][country] = 30000
        self.loaded_game_data["food"][country] = 3000
        self.loaded_game_data["tech_tree"][country] = {"official_currency":0}
        self.loaded_game_data["current_tech"][country] = ""
        self.loaded_game_data["acquired_tech"][country] = []
        self.assign_random_tech(country)
        self.no_general_armies[country] = Commander(name="No General", simple_name="none", age=0, loyalty=0,
                                                    country=country, combat_prowess=1, intellect=1, command=50,
                                                    charisma=1, action_points=0, level=1, experience=0, upgrade_points=0)


    def calculate_learning_rate(self, country):
        learning_rate = 1
        if country == "Yuan Shao":
            learning_rate += .15
        for city_sprite in self.city_sprites:
            if city_sprite.country == country:
                try:
                    academy = [building for building in city_sprite.buildings if building.name == "Academy"][0]
                    learning_rate += academy.level/10
                except:
                    pass
        for army_sprite in self.army_sprites:
            if army_sprite.commander.country == country:
                if "master_inventor" in army_sprite.commander.skills:
                    learning_rate += .5
                if "master_of_writing" in army_sprite.commander.skills:
                    learning_rate += .3

        if self.loaded_game_data["dilemma_effects"]["intellectual_revolution"] > 0:
            learning_rate += .3

        return learning_rate


    def set_diplomacy_variables(self):
        self.available_countries = [self.player_country]
        for city_sprite in self.city_sprites:
            if city_sprite.country not in self.available_countries:
                self.available_countries.append(city_sprite.country)
            if city_sprite.country not in self.peace_relationships:
                self.peace_relationships[city_sprite.country] = 0
        for army_sprite in self.army_sprites:
            if army_sprite.commander.country not in self.available_countries:
                self.available_countries.append(army_sprite.commander.country)
            if army_sprite.commander.country not in self.peace_relationships:
                self.peace_relationships[army_sprite.commander.country] = 0
        for country in self.available_countries:
            if country != self.player_country:
                self.diplomatic_relationships[country] = self.calculate_diplomatic_relationship(self.player_country, country)["Total"]
                if country not in self.peace_relationships:
                    self.peace_relationships[country] = 0
        for key, value in self.alliance_relationships.items():
            for country in value:
                if country not in self.available_countries:
                    value.remove(country)
        for key, value in self.trade_relationships.items():
            for country in value:
                if country not in self.available_countries:
                    value.remove(country)
        for key, value in self.hostile_relationships.items():
            for country in value:
                if country not in self.available_countries:
                    value.remove(country)
        for country, value in self.peace_relationships.items():
            if country not in self.available_countries:
                self.peace_relationships[country] = 0

        sum_ai_economic_growth = 0
        player_economic_growth = self.calculate_tax_and_additional_income(self.player_country) * 5 + self.loaded_game_data["gold"][self.player_country]
        for country in self.available_countries:
            sum_ai_economic_growth += self.calculate_tax_and_additional_income(country) * 5 + self.loaded_game_data["gold"][country]

        if not self.realm_divide and self.enemy_moving:
            if player_economic_growth/sum_ai_economic_growth + self.difficulty*.05 >= .4:
                self.realm_divide = True
                self.queue_dilemma("realm_divide", add_to_alerts=True)
            else:
                player_owned_cities = 0
                for city_sprite in self.city_sprites:
                    if city_sprite.country == self.player_country:
                        player_owned_cities += 1
                if player_owned_cities/len(self.city_sprites) >= .5 - self.difficulty*.05:
                    self.realm_divide = True
                    self.queue_dilemma("realm_divide", add_to_alerts=True)
                print(player_owned_cities/len(self.city_sprites), .5 - self.difficulty*.05)


    def calculate_diplomatic_relationship(self, initiating_country, target_country):
        relationship = {"Total":0}

        # check for military strength
        military_strength1 = self.calculate_military_strength(initiating_country)
        military_strength2 = self.calculate_military_strength(target_country)
        military_strength_delta = military_strength1 - military_strength2
        if military_strength_delta >= 100000:
            military_strength_delta = 100000
        elif military_strength_delta <= -100000:
            military_strength_delta = -100000

        if target_country in self.alliance_relationships[initiating_country]:
            relationship["Alliance"] = 50
            relationship["Total"] += 50
        if target_country in self.trade_relationships[initiating_country]:
            relationship["Trade"] = 30
            relationship["Total"] += 30
        if target_country in self.hostile_relationships[initiating_country]:
            relationship["At War"] = -50
            relationship["Total"] -= 50
            relationship["Military Strength"] = military_strength_delta//1000
            relationship["Total"] += military_strength_delta//1000
        else:
            if military_strength_delta < -20000 and target_country not in self.trade_relationships[initiating_country] + self.alliance_relationships[initiating_country]:
                relationship["Military Strength"] = military_strength_delta//2000
                relationship["Total"] += military_strength_delta//2000


        #check for past conflicts
        if initiating_country == self.player_country:
            if target_country in self.past_conflicts:
                relationship["Past Conflicts"] = -self.past_conflicts[target_country]*30
                relationship["Total"] -= self.past_conflicts[target_country]*30

        #check for peace treaties
        if initiating_country == self.player_country:
            if self.peace_relationships[target_country] > 0:
                relationship["Peace Treaty ({})".format(self.peace_relationships[target_country])] = 25
                relationship["Total"] += 25

        #check for common enemies
        s1 = set(self.hostile_relationships[initiating_country])
        s2 = set(self.hostile_relationships[target_country])
        num_common_enemies = len(s1.intersection(s2))
        if num_common_enemies > 0:
            relationship["Common Enemies"] = num_common_enemies*10
            relationship["Total"] += num_common_enemies*10

        #grant AI diplomacy bonus towards each other
        if initiating_country != self.player_country and target_country != self.player_country:
            relationship["Total"] += 50

        #check for jealousy on territorial expansion/economic growth
        num_cities_1 = 0
        num_cities_2 = 0
        for city_sprite in self.city_sprites:
            if city_sprite.country == initiating_country:
                num_cities_1 += 1
            elif city_sprite.country == target_country:
                num_cities_2 += 1


        economic_growth_1 = self.calculate_tax_and_additional_income(initiating_country) * 5 + self.loaded_game_data["gold"][initiating_country]
        economic_growth_2 = self.calculate_tax_and_additional_income(target_country) * 5 + self.loaded_game_data["gold"][target_country]

        if initiating_country == self.player_country:
            cities_diff = num_cities_1 - num_cities_2
            economic_growth_ratio = economic_growth_1/economic_growth_2
        else:
            cities_diff = num_cities_2 - num_cities_1
            economic_growth_ratio = economic_growth_2 / economic_growth_1

        if cities_diff > 0:
            relationship["Territorial Expansion"] = cities_diff *-10
            relationship["Total"] -= cities_diff * 10
        #do not apply territorial expansion to Southern Barbarians after they've been subdued
        #print(initiating_country, target_country, self.player_country, self.task_progress["southern_barbarians"])
        if economic_growth_ratio >= .75 and not (initiating_country == self.player_country and target_country == "Southern Barbarians" and self.task_progress["southern_barbarians"] == 5):
            if "Territorial Expansion" in relationship:
                relationship["Territorial Expansion"] -= int((economic_growth_ratio - .75) * 10)
            else:
                relationship["Territorial Expansion"] = -int((economic_growth_ratio - .75) * 10)
            relationship["Total"] -= int((economic_growth_ratio - .75) * 10)

        #check for diplomacy bonus
        bonus = self.calculate_diplomacy_bonus(initiating_country)
        if bonus:
            relationship["Diplomacy Bonus"] = bonus
            relationship["Total"] += bonus

        #check for diplomacy penalty (for breaking agreements, etc...)
        if self.diplomacy_penalty["All"] > 0:
            relationship["Treacherous Reputation"] = -self.diplomacy_penalty["All"]
            relationship["Total"] -= self.diplomacy_penalty["All"]

        if self.diplomacy_penalty[target_country] > 0:
            relationship["Broken Agreements"] = -self.diplomacy_penalty[target_country]
            relationship["Total"] -= self.diplomacy_penalty[target_country]

        #check for penalty/bonus from killing/releasing enemy generals
        if self.general_kill_release[target_country] > 0:
            relationship["Generals Released"] = self.general_kill_release[target_country]
            relationship["Total"] += self.general_kill_release[target_country]
        elif self.general_kill_release[target_country] < 0:
            relationship["Generals Killed"] = self.general_kill_release[target_country]
            relationship["Total"] += self.general_kill_release[target_country]

        #print(initiating_country, target_country, relationship)
        return relationship


    def enemy_diplomacy(self, country):
        country_military_strength = self.calculate_military_strength(country)
        for c in self.available_countries:
            if c != country:
                if self.realm_divide and country != "Southern Barbarians":
                    if c == self.player_country and self.player_country not in self.hostile_relationships[country]:
                        self.declare_war(country, c)
                    elif c != self.player_country:
                        if c in self.hostile_relationships[country]:
                            self.terminate_relationship("hostile", country, c)
                        if c not in self.alliance_relationships[country]:
                            self.begin_relationship("alliance", country, c)
                        if c not in self.trade_relationships[country]:
                            self.begin_relationship("trade", country, c)

                else:
                    c_military_strength = self.calculate_military_strength(c)
                    relationship = self.calculate_diplomatic_relationship(country, c)["Total"]

                    if c == self.player_country:
                        cond_1 = random() >= 1 + relationship/100
                        cond_2 = country_military_strength >= c_military_strength
                        cond_3 = country_military_strength >= 2*c_military_strength
                        cond_4 = relationship < -50
                        cond_5 = random() >= 1.5 + relationship/100
                        cond_6 = random() <= country_military_strength / c_military_strength
                        cond_7 = self.peace_relationships[country] <= 0
                    else:
                        cond_1 = random() >= 1.3 + relationship/100
                        cond_2 = country_military_strength >= c_military_strength*1.3
                        cond_3 = country_military_strength >= 3*c_military_strength
                        cond_4 = relationship < -50
                        cond_5 = random() >= 2 + relationship/100
                        cond_6 = random() <= country_military_strength/c_military_strength
                        cond_7 = True

                    if ((cond_1 and cond_2) or (cond_3 and cond_4) or (cond_5 and cond_6)) and cond_7:
                        if c not in self.hostile_relationships[country] and random() >= len(self.hostile_relationships[country])/3:
                            self.declare_war(country, c)


    def terminate_relationship(self, relationship, country_1, country_2):
        if relationship == "hostile":
            if country_2 in self.hostile_relationships[country_1]:
                self.hostile_relationships[country_1].remove(country_2)
                self.hostile_relationships[country_2].remove(country_1)
        elif relationship == "alliance":
            if country_2 in self.alliance_relationships[country_1]:
                self.alliance_relationships[country_1].remove(country_2)
                self.alliance_relationships[country_2].remove(country_1)
        elif relationship == "trade":
            if country_2 in self.trade_relationships[country_1]:
                self.trade_relationships[country_1].remove(country_2)
                self.trade_relationships[country_2].remove(country_1)


    def begin_relationship(self, relationship, country_1, country_2):
        if relationship == "hostile":
            if country_2 not in self.hostile_relationships[country_1]:
                self.hostile_relationships[country_1].append(country_2)
                self.hostile_relationships[country_2].append(country_1)
        elif relationship == "alliance":
            if country_2 not in self.alliance_relationships[country_1]:
                self.alliance_relationships[country_1].append(country_2)
                self.alliance_relationships[country_2].append(country_1)
        elif relationship == "trade":
            if country_2 not in self.trade_relationships[country_1]:
                self.trade_relationships[country_1].append(country_2)
                self.trade_relationships[country_2].append(country_1)


    def declare_war(self, initiating_country, target_country):
        self.begin_relationship("hostile", initiating_country, target_country)
        if target_country in self.trade_relationships[initiating_country]:
            self.terminate_relationship("trade", initiating_country, target_country)
        if target_country in self.alliance_relationships[initiating_country]:
            self.terminate_relationship("alliance", initiating_country, target_country)
        if target_country == self.player_country:
            self.queue_dilemma("country_declare_war_" + initiating_country.lower())
            if self.peace_relationships[initiating_country] > 0:
                self.peace_relationships[initiating_country] = 0
            if initiating_country in self.past_conflicts:
                self.past_conflicts[initiating_country] += 1
            else:
                self.past_conflicts[initiating_country] = 1

        if initiating_country == self.player_country:
            if self.peace_relationships[target_country] > 0:
                self.peace_relationships[target_country] = 0
            if target_country in self.past_conflicts:
                self.past_conflicts[target_country] += 1
            else:
                self.past_conflicts[target_country] = 1


    def peace_agreement(self, target_country):
        self.terminate_relationship("hostile", self.player_country, target_country)
        self.peace_relationships[target_country] = 10


    def fix_stuck_armies(self):
        for army_sprite in self.army_sprites:
            if not self.is_cell_walkable(army_sprite.grid_position_x, army_sprite.grid_position_y):
                walkable_cells = []
                for i in range(army_sprite.grid_position_x-2, army_sprite.grid_position_x+3):
                    for j in range(army_sprite.grid_position_y-2, army_sprite.grid_position_y+3):
                        if self.is_cell_walkable(i, j):
                            walkable_cells.append((i, j))

                if walkable_cells:
                    delta_x = walkable_cells[0][0] - army_sprite.grid_position_x
                    delta_y = walkable_cells[0][1] - army_sprite.grid_position_y
                    army_sprite.grid_position_x += delta_x
                    army_sprite.grid_position_y += delta_y
                    army_sprite.position_x += int(delta_x * int(self.zoom_scale * 32))
                    army_sprite.position_y += int(delta_y * int(self.zoom_scale * 32))
                else:
                    self.kill_army(army_sprite)


    def pre_turn_start_check(self):
        self.set_diplomacy_variables()
        for country in self.available_countries:
            if self.player_country != country:
                self.enemy_diplomacy(country)
                if self.peace_relationships[country] > 0:
                    self.peace_relationships[country] -= 1
        player_city_army_count = 0
        # check for rebellion (check for player lost during loop)
        unavailable_rebel_commanders = [army_sprite.commander.name for army_sprite in
                                        self.army_sprites] + self.dead_characters
        filtered_rebel_commanders = [commander_name for commander_name in self.rebel_commanders if
                                     commander_name not in unavailable_rebel_commanders]
        for city_sprite in self.city_sprites:
            if city_sprite.country == self.player_country:
                if city_sprite.name == "Xu Chang":
                    player_city_army_count += 101
                else:
                    player_city_army_count += 1
                if random() >= (10 + city_sprite.happiness*(.3+self.difficulty/10))/10 and filtered_rebel_commanders and self.hostile_relationships[self.player_country]:
                    city_happiness = city_sprite.happiness
                    city_population = city_sprite.population
                    rebel_population = int(city_population * (-city_happiness * .03))
                    rebel_army = {}
                    if rebel_population <= 200:
                        rebel_population = 200
                    city_sprite.population -= rebel_population
                    city_sprite.effects = {}
                    city_sprite.update_happiness()
                    rebel_counter = 0
                    while rebel_counter <= rebel_population:
                        if random() >= (15 + city_happiness) / 10:  # must be at least -6 happiness for possibility of infantry rebels
                            rebel_unit = choice(["spear_infantry", "sword_infantry", "bow_infantry", "spear_militia",
                                 "sword_militia", "bow_militia"])
                        else:
                            rebel_unit = choice(["spear_militia", "sword_militia", "bow_militia"])
                        if rebel_unit in rebel_army:
                            rebel_army[rebel_unit] += 10
                        else:
                            rebel_army[rebel_unit] = 10
                        rebel_counter += 10

                    rebel_country = choice(self.hostile_relationships[self.player_country])
                    rebel_commander = self.rebel_commanders[choice(filtered_rebel_commanders)]
                    filtered_rebel_commanders.remove(rebel_commander.name)
                    rebel_commander.country = rebel_country
                    rebel_army = Army(0, 0, commander=rebel_commander,
                                      army_composition=rebel_army,
                                      action_points=-1, retreat_penalty=0, anchor="midleft")

                    self.add_army_to_map(rebel_army, target_city=city_sprite.name,
                                         offset_x=choice([-2,-1,1,2]), offset_y=choice([-2,-1,1,2]))
                    self.queue_dilemma("rebellion", target_object=city_sprite)
                    if "rebellion" in self.dynamic_decision_text:
                        self.dynamic_decision_text["rebellion"].append(DECISION_TEXT["rebellion"].format(city_sprite.name))
                    else:
                        self.dynamic_decision_text["rebellion"] = [DECISION_TEXT["rebellion"].format(city_sprite.name)]
                    if not filtered_rebel_commanders:  # if no more rebel commanders available, break loop
                        break

        #natural disasters
        natural_disaster_prob = .03
        #natural_disaster_prob = 1
        if "Divination" in self.acquired_tech[self.player_country]:
            natural_disaster_prob *= .5
        for city_sprite in self.city_sprites:
            if random() <= natural_disaster_prob and city_sprite.country == self.player_country:
                natural_disaster = choice(["natural_disaster_storm", "natural_disaster_fire", "natural_disaster_drought"])
                if natural_disaster not in city_sprite.effects:
                    city_sprite.effects[natural_disaster] = 4
                    self.queue_dilemma(natural_disaster, target_object=city_sprite)
                    if natural_disaster in self.dynamic_decision_text:
                        self.dynamic_decision_text[natural_disaster].append(DECISION_TEXT[natural_disaster].format(city_sprite.name))
                    else:
                        self.dynamic_decision_text[natural_disaster] = [DECISION_TEXT[natural_disaster].format(city_sprite.name)]

        #random events
        if self.turn_number%30 + randrange(3) == 8 and self.loaded_game_data["dilemma_effects"]["intellectual_revolution"] == 0:
            self.loaded_game_data["dilemma_effects"]["intellectual_revolution"] = 8
            self.queue_dilemma("intellectual_revolution")

        if self.turn_number%25 + randrange(3) in [3,4,5] and self.loaded_game_data["dilemma_effects"]["flourishing_economy"] == 0 and len(self.trade_relationships[self.player_country]) > 0:
            self.loaded_game_data["dilemma_effects"]["flourishing_economy"] = 4
            self.queue_dilemma("flourishing_economy")

        if self.turn_number%25 + randrange(3) in [7,8,9] and self.loaded_game_data["dilemma_effects"]["nationalism"] == 0 and len(self.trade_relationships[self.player_country]) > 0:
            self.loaded_game_data["dilemma_effects"]["nationalism"] = 2
            self.queue_dilemma("nationalism")

        #events with choice


        #check for other dilemmas to present, if applicable
        owns_southern_territories = False
        for city in self.city_sprites:
            if city.country == self.player_country and city.name in ["Jiang Zhou", "Shang Yong", "Hu Nan", "He Pu", "Jian Ning"]:
                owns_southern_territories = True
                break
        if self.turn_number >= 45 and self.task_progress["southern_barbarians"] == -1 and owns_southern_territories and len(self.available_countries) < 8:
            self.queue_dilemma("southern_barbarians")


        #check for winning/losing condition
        for army in self.army_sprites:
            if army.commander.country == self.player_country:
                player_city_army_count += .001

                #Check for character rebellion due to loyalty
                if ((army.commander.loyalty+50)/100)**4 <= random() and self.hostile_relationships[self.player_country]: #character rebellion
                    rebel_country = choice(self.hostile_relationships[self.player_country])
                    army.switch_country(rebel_country, reset_army=False)
                    if len(army.army_composition) > 0:
                        for unit_type, count in army.army_composition.items():
                            army.army_composition[unit_type] += 50*self.difficulty
                    else:
                        army.army_composition = {"spear_infantry": 100+self.difficulty*randrange(30, 51),
                                                 "sword_infantry": self.difficulty*randrange(30, 51),
                                                 "bow_infantry": self.difficulty*randrange(30, 51)}

                    for city in self.city_sprites:
                        if city.country == self.player_country and city.defender == army.commander.name:
                            city.captured(army)

                    self.player_army_sprites.remove(army)
                    self.enemy_army_sprites.add(army)

                    if "character_rebellion" in self.dynamic_decision_text:
                        if DECISION_TEXT["character_rebellion"].format(army.commander.name) not in self.dynamic_decision_text["character_rebellion"]:
                            self.queue_dilemma("character_rebellion", DECISION_TEXT["character_rebellion"].format(army.commander.name), target_object=army)
                            self.dynamic_decision_text["character_rebellion"].append(DECISION_TEXT["character_rebellion"].format(army.commander.name))
                    else:
                        self.queue_dilemma("character_rebellion", DECISION_TEXT["character_rebellion"].format(army.commander.name), target_object=army)
                        self.dynamic_decision_text["character_rebellion"] = [DECISION_TEXT["character_rebellion"].format(army.commander.name)]

                elif army.commander.idle_turns >= 5:
                    if "disgruntled_general" in self.dynamic_decision_text:
                        if DECISION_TEXT["disgruntled_general"].format(army.commander.name) not in self.dynamic_decision_text["disgruntled_general"]:
                            self.queue_dilemma("disgruntled_general", DECISION_TEXT["disgruntled_general"].format(army.commander.name), target_object=army)
                            self.dynamic_decision_text["disgruntled_general"].append(DECISION_TEXT["disgruntled_general"].format(army.commander.name))
                    else:
                        self.queue_dilemma("disgruntled_general", DECISION_TEXT["disgruntled_general"].format(army.commander.name), target_object=army)
                        self.dynamic_decision_text["disgruntled_general"] = [DECISION_TEXT["disgruntled_general"].format(army.commander.name)]


        if player_city_army_count >= 145:
            self.queue_dilemma("campaign_won")
        elif player_city_army_count == 0:
            self.queue_dilemma("campaign_lost")

        #notifications
        if self.upcoming_decisions:
            self.present_next_dilemma()

        #check for applicable advice
        if self.loaded_game_data["food"][self.player_country] + 5*self.calculate_projected_food_delta(self.player_country) <= 0:
            self.give_advice("help_food_shortage")


    def present_next_dilemma(self):
        dilemma = self.upcoming_decisions[-1]
        self.viewing_decision_window = True
        self.current_decision = dilemma
        if self.current_decision in self.dynamic_decision_text:
            if len(self.dynamic_decision_text[self.current_decision]) <= 0:
                del self.dynamic_decision_text[self.current_decision]
                self.accept_task(self.current_decision)
                return
            else:
                self.current_decision_text = self.dynamic_decision_text[self.current_decision].pop()
        else:
            self.current_decision_text = DECISION_TEXT[self.current_decision]

        decision_window_bg = Static(LANDSCAPE_WIDTH // 2, LANDSCAPE_HEIGHT // 2, "decision_interface_bg.png",
                                    anchor="center")
        self.all_sprites.add(decision_window_bg)
        self.decision_window_sprites.add(decision_window_bg)


        if self.current_decision in DECISION_IMAGES and self.current_decision not in ["southern_barbarians", "view_military_report"]:
            notification_image = Static(LANDSCAPE_WIDTH//2, 250, DECISION_IMAGES[self.current_decision], anchor="center")
            self.all_sprites.add(notification_image)
            self.decision_window_sprites.add(notification_image)

            ok_button = Button(LANDSCAPE_WIDTH//2, 500, "menu_button_ok.png",
                               command=partial(self.player_dilemma_decision, dilemma, 0))
            self.all_sprites.add(ok_button)
            self.buttons.add(ok_button)
            self.decision_window_sprites.add(ok_button)

        elif self.current_decision == "view_military_report":
            ok_button = Button(LANDSCAPE_WIDTH//2, 500, "menu_button_ok.png",
                               command=partial(self.player_dilemma_decision, dilemma, 0))
            self.all_sprites.add(ok_button)
            self.buttons.add(ok_button)
            self.decision_window_sprites.add(ok_button)

        elif self.current_decision == "quit_game":
            quit_game_image = Static(LANDSCAPE_WIDTH // 2, 250, "dilemma_quit_game.png", anchor="center")
            self.all_sprites.add(quit_game_image)
            self.decision_window_sprites.add(quit_game_image)

            yes_button = Button(LANDSCAPE_WIDTH//2-200, 500, "menu_button_yes.png",
                                command=partial(self.quit_game_decision, True))
            self.all_sprites.add(yes_button)
            self.buttons.add(yes_button)
            self.decision_window_sprites.add(yes_button)

            no_button = Button(LANDSCAPE_WIDTH//2+200, 500, "menu_button_no.png",
                                    command=self.quit_game_decision)
            self.all_sprites.add(no_button)
            self.buttons.add(no_button)
            self.decision_window_sprites.add(no_button)

        elif self.current_decision == "capture_enemy_commander":
            captured_enemy_general_avatar = Static(LANDSCAPE_WIDTH // 2, 250, "general_avatar_{}.png".format(
                self.right_preview_army.commander.simple_name), anchor="center")
            self.all_sprites.add(captured_enemy_general_avatar)
            self.decision_window_sprites.add(captured_enemy_general_avatar)

            kill_button = Button(225, 500, "campaign_button_battle_preview_kill.png", command=self.kill_enemy_commander)
            self.all_sprites.add(kill_button)
            self.buttons.add(kill_button)
            self.decision_window_sprites.add(kill_button)

            recruit_button = Button(605, 500, "campaign_button_battle_preview_recruit.png",
                                    command=self.recruit_enemy_commander)
            self.all_sprites.add(recruit_button)
            self.buttons.add(recruit_button)
            self.decision_window_sprites.add(recruit_button)
            recruit_prob_multiplier = self.left_preview_army.commander.charisma/50
            if "forgery" in self.left_preview_army.commander.skills:
                recruit_prob_multiplier *= 1.25
            if self.right_preview_army.commander.loyalty >= 80*recruit_prob_multiplier or random() <= self.right_preview_army.commander.loyalty/100/recruit_prob_multiplier:
                print("Loyalty:", self.right_preview_army.commander.loyalty)
                recruit_button.disabled = True

            release_button = Button(985, 500, "campaign_button_battle_preview_release.png",
                                    command=self.release_enemy_commander)
            self.all_sprites.add(release_button)
            self.buttons.add(release_button)
            self.decision_window_sprites.add(release_button)

            if "oath_of_vengeance" in self.left_preview_army.commander.skills:
                recruit_button.disabled = True
                release_button.disabled = True

        elif self.current_decision == "southern_barbarians":
            captured_enemy_general_avatar = Static(LANDSCAPE_WIDTH // 2, 250, "general_avatar_meng_huo.png", anchor="center")
            self.all_sprites.add(captured_enemy_general_avatar)
            self.decision_window_sprites.add(captured_enemy_general_avatar)

            ok_button = Button(LANDSCAPE_WIDTH // 2, 500, "menu_button_ok.png",
                                    command=partial(self.accept_task, "southern_barbarians"))
            self.all_sprites.add(ok_button)
            self.buttons.add(ok_button)
            self.decision_window_sprites.add(ok_button)


    def accept_task(self, task_key, value=0):
        self.task_progress[task_key] = value  # indicate task started
        if task_key == "southern_barbarians":
            self.generate_new_country("Southern Barbarians")
            zhu_rong = Commander(name="Zhu Rong", simple_name="zhu_rong", age=31, loyalty=80,
                                 country="Southern Barbarians", combat_prowess=72, intellect=60, command=85,
                                 charisma=69, action_points=27, level=5, experience=1200, upgrade_points=0)
            meng_huo = Commander(name="Meng Huo", simple_name="meng_huo", age=40, loyalty=80,
                                 country="Southern Barbarians", combat_prowess=70, intellect=10, command=115,
                                 charisma=25, action_points=24, level=5, experience=1200, upgrade_points=0)
            king_duosi = Commander(name="King Duosi", simple_name="king_duosi", age=38, loyalty=80,
                                 country="Southern Barbarians", combat_prowess=60, intellect=80, command=85,
                                 charisma=45, action_points=28, level=5, experience=1200, upgrade_points=0)
            wutugu = Commander(name="Wutugu", simple_name="wutugu", age=46, loyalty=80,
                                 country="Southern Barbarians", combat_prowess=80, intellect=5, command=111,
                                 charisma=10, action_points=25, level=5, experience=1200, upgrade_points=0)

            num_player_cities = len([city_sprite for city_sprite in self.city_sprites if city_sprite.country == self.player_country])
            zhu_rong_army = Army(25, 104, commander=zhu_rong, army_composition={"barbarian_spear_woman": 200+num_player_cities*10, "barbarian_assassin": 200+num_player_cities*10, "barbarian_archer": 50+num_player_cities*10},
                                    action_points=-1, retreat_penalty=0, anchor="midleft")
            meng_huo_army = Army(30, 103, commander=meng_huo, army_composition={"barbarian_spear_woman": 200+num_player_cities*10, "barbarian_assassin": 100+num_player_cities*10, "barbarian_archer": 50+num_player_cities*10, "elephant_rider":50+num_player_cities*5},
                                    action_points=-1, retreat_penalty=0, anchor="midleft")
            wutugu_army = Army(23, 105, commander=wutugu, army_composition={"rattan_armor_infantry": 200+num_player_cities*10, "barbarian_archer": 200+num_player_cities*10},
                                    action_points=-1, retreat_penalty=0, anchor="midleft")
            king_duosi_army = Army(27, 108, commander=king_duosi,
                               army_composition={"barbarian_assassin": 100+num_player_cities*20, "barbarian_archer": 100+num_player_cities*15},
                               action_points=-1, retreat_penalty=0, anchor="midleft")

            self.add_army_to_map(zhu_rong_army, target_city="Jiang Zhou", offset_x=-28, offset_y=9)
            self.add_army_to_map(meng_huo_army, target_city="Jiang Zhou", offset_x=-21, offset_y=-8)
            self.add_army_to_map(king_duosi_army, target_city="Jiang Zhou", offset_x=-24, offset_y=12)
            self.add_army_to_map(wutugu_army, target_city="Jiang Zhou", offset_x=-26, offset_y=10)
            try:
                self.current_turn_dilemmas["southern_barbarians"]["target"] = [army_sprite for army_sprite in self.army_sprites if army_sprite.commander.name == "Meng Huo"][0]
            except Exception as e:
                logging.exception(e)
            self.declare_war("Southern Barbarians", self.player_country)

        self.upcoming_decisions.remove(task_key)
        for sp in self.decision_window_sprites:
            sp.kill()
            if sp in self.buttons:
                self.buttons.remove(sp)
        self.viewing_decision_window = False
        self.current_decision = None

        if self.upcoming_decisions:
            self.present_next_dilemma()
        else:
            if self.resume_enemy_moving:
                self.enemy_moving = True


    def player_dilemma_decision(self, dilemma, decision=0):
        #if dilemma == "view_military_report":
        #    self.persistent_game_message = ""
        #    self.viewing_decision_window = False
        #    self.current_decision = None
        #    return
        if decision == 0:
            self.upcoming_decisions.remove(dilemma)
            for sp in self.decision_window_sprites:
                if sp in self.buttons:
                    self.buttons.remove(sp)
                try:
                    sp.kill()
                except:
                    pass
            self.viewing_decision_window = False
            self.current_decision = None
            self.persistent_game_message = ""

            if self.upcoming_decisions:
                self.present_next_dilemma()
            else:
                if self.resume_enemy_moving:
                    self.enemy_moving = True


    def queue_dilemma(self, dilemma, short_summary="", target_object=None, add_to_alerts=True):
        self.upcoming_decisions.append(dilemma)
        if not short_summary:
            short_summary = DECISION_TEXT[dilemma]
        if target_object: #append city/army name if applicable
            try:
                dilemma += "|||"+target_object.name
            except Exception as e:
                print(e)
                try:
                    dilemma += "|||"+target_object.commander.name
                except Exception as e:
                    print(e)
        if add_to_alerts:
            self.current_turn_dilemmas[dilemma] = {"short_summary": short_summary,
                                                   "target": target_object}


    def hide_toolbar_icons(self):
        self.toolbar_visible = False
        for sp in self.icons:
            sp.rect.y -= 10000
            if type(sp) == CampaignMapSprite:
                sp.position_y -= 10000


    def restore_toolbar_icons(self):
        self.toolbar_visible = True
        for sp in self.icons:
            sp.rect.y += 10000
            if type(sp) == CampaignMapSprite:
                sp.position_y += 10000


    def end_turn(self):
        #for grid_sprite in self.walkable_grid_sprites:
        #    grid_sprite.kill()
        self.save_game("auto_save.p")
        if self.selected_player_army:
            self.selected_player_army.selected = False
            self.selected_player_army = None
        if self.selected_player_city:
            self.selected_player_city = None

        self.viewing_city_info = None
        self.viewing_army_info = None
        for sp in self.alert_sprites:
            sp.kill()
        for sp in self.info_sprites:
            sp.kill()
        for sp in self.walkable_grid_sprites:
            sp.kill()

        self.clear_list_sprites(True)
        self.hide_toolbar_icons()

        #update effect duration
        for key, value in self.loaded_game_data["dilemma_effects"].items():
            if value > 0:
                self.loaded_game_data["dilemma_effects"][key] -= 1

        #fix any armies stuck in mountain
        self.fix_stuck_armies()

        self.enemy_moving = True


    def run(self):
        while self.playing:
            if self.running:
                self.clock.tick(FPS)
                self.events()
                self.update()
                self.draw()


    def events(self):
        if self.enemy_moving:
            pg.event.pump()
            return


        pos = pg.mouse.get_pos()
        now = pg.time.get_ticks()
        #check for hover scrolling when viewing list
        if now - self.last_action >= 100:
            if self.viewing_list == "generals":
                max_offset = len([army_sprite for army_sprite in self.army_sprites if army_sprite.commander.country == self.player_country]) - 10
                if self.list_arrow_down.rect.collidepoint(pos) and self.viewing_list_offset < max_offset:
                    self.viewing_list_offset += 1
                    self.last_action = now
                    self.list_generals(reset_offset=False)
                elif self.list_arrow_up.rect.collidepoint(pos) and self.viewing_list_offset > 0:
                    self.viewing_list_offset -= 1
                    self.last_action = now
                    self.list_generals(reset_offset=False)
            elif self.viewing_list == "cities":
                max_offset = len([city_sprite for city_sprite in self.city_sprites if city_sprite.country == self.player_country]) - 10
                if self.list_arrow_down.rect.collidepoint(pos) and self.viewing_list_offset < max_offset:
                    self.viewing_list_offset += 1
                    self.last_action = now
                    self.list_cities(reset_offset=False)
                elif self.list_arrow_up.rect.collidepoint(pos) and self.viewing_list_offset > 0:
                    self.viewing_list_offset -= 1
                    self.last_action = now
                    self.list_cities(reset_offset=False)


        # check for tooltip display
        hovering_over_tooltip_ui_element = False
        for sp_atk_icon in self.sp_atk_icons_list + self.skill_icons_list:
            if sp_atk_icon.rect.collidepoint(pos):
                sp_atk_icon.hovering = True
                hovering_over_tooltip_ui_element = True
                if sp_atk_icon.tooltip_description:
                    self.special_attack_tooltip_description = sp_atk_icon.tooltip_description
                    self.special_attack_tooltip_x = sp_atk_icon.tooltip.rect.left
                    self.special_attack_tooltip_y = sp_atk_icon.tooltip.rect.top
            else:
                sp_atk_icon.hovering = False

        if not hovering_over_tooltip_ui_element:
            self.special_attack_tooltip_description = ""


        #If previewing battle or battle summary or viewing other interface
        if self.previewing_battle or self.viewing_battle_summary or self.viewing_decision_window or self.viewing_army_info or self.viewing_city_info or self.viewing_list or self.viewing_army_merge_interface:

            for button in self.buttons:
                if button.rect.collidepoint(pos):
                    button.hovering = True
                    for event in pg.event.get():
                        # left clicks
                        if event.type == pg.MOUSEBUTTONUP and event.button == 1:
                            self.play_sound_effect(self.click_sound)
                            button.pressed = True
                            if not button.disabled:
                                try:
                                    button.hovering = False
                                    button.kill_tooltip()
                                except Exception as e:
                                    print(e)
                                button.command()
                                if self.viewing_list:
                                    self.clear_list_sprites(True)
                        else:
                            button.pressed = False
                else:
                    button.hovering = False

            # check for unit selection during army merge interface
            if self.viewing_army_merge_interface:
                for event in pg.event.get():
                    # left clicks
                    if event.type == pg.MOUSEBUTTONUP and event.button == 1:
                        self.play_sound_effect(self.click_sound)
                        for unit_icon in self.army_merge_unit_icons:
                            if unit_icon.rect.collidepoint(pos):
                                self.selected_army_merge_unit_index = self.army_merge_unit_icons.index(unit_icon)
                                unit_icon.selected = True
                            else:
                                unit_icon.selected = False

        #Enemy attack zone/own city zone
        if self.selected_player_army:
            hovering_over_enemy = False
            hovering_over_own_army = False
            hovering_over_own_city = False

            for enemy_army in self.enemy_army_sprites:
                if abs(enemy_army.rect.centerx - 5 - pos[0]) <= 48 and abs(enemy_army.rect.bottom - 12.5 - pos[1]) <= 48 and enemy_army.commander.country in self.hostile_relationships[self.player_country]:
                    hovering_over_enemy = True
                    self.attack_icon_sprite.position_x, self.attack_icon_sprite.position_y = pos
                    break

            for player_army in self.player_army_sprites:
                if abs(player_army.rect.centerx - 5 - pos[0]) <= 16 and abs(player_army.rect.bottom - 12.5 - pos[1]) <= 16 and not (self.selected_player_army.position_x == player_army.position_x and self.selected_player_army.position_y == player_army.position_y):
                    hovering_over_own_army = True
                    self.army_merge_icon_sprite.position_x, self.army_merge_icon_sprite.position_y = pos
                    break

            for city_sprite in self.city_sprites:
                if city_sprite.country in self.hostile_relationships[self.player_country]:
                    delta_x = (pos[0] - city_sprite.rect.centerx)//int(self.zoom_scale*32)
                    delta_y = (pos[1] - city_sprite.rect.centery)//int(self.zoom_scale*32)
                    if delta_x in range(-3, 1+city_sprite.fortress_level) and delta_y in range(-2,2):
                        hovering_over_enemy = True
                        self.attack_icon_sprite.position_x, self.attack_icon_sprite.position_y = pos
                        break

                if city_sprite.country == self.player_country and not self.selected_player_army.embedded_in_city and not city_sprite.defender:
                    delta_x = (pos[0] - city_sprite.rect.centerx) // int(self.zoom_scale*32)
                    delta_y = (pos[1] - city_sprite.rect.centery) // int(self.zoom_scale*32)
                    if delta_x in range(-3, 1+city_sprite.fortress_level) and delta_y in range(-2, 3):
                        hovering_over_own_city = True
                        self.defend_icon_sprite.position_x, self.defend_icon_sprite.position_y = pos
                        break

            if not hovering_over_enemy:
                self.attack_icon_sprite.position_x, self.attack_icon_sprite.position_y = -30000, -30000
            if not hovering_over_own_army:
                self.army_merge_icon_sprite.position_x, self.army_merge_icon_sprite.position_y = -30000, -30000
            if not hovering_over_own_city:
                self.defend_icon_sprite.position_x, self.defend_icon_sprite.position_y = -30000, -30000


        #Scrolling
        if not (self.previewing_battle or self.viewing_battle_summary or self.viewing_decision_window or self.viewing_army_merge_interface):
            keys = pg.key.get_pressed()
            if keys[pg.K_LEFT] or keys[pg.K_a]:
                self.scroll_camera("left")

            elif keys[pg.K_RIGHT] or keys[pg.K_d]:
                self.scroll_camera("right")

            if keys[pg.K_UP] or keys[pg.K_w]:
                self.scroll_camera("up")

            elif keys[pg.K_DOWN] or (keys[pg.K_s] and not (keys[pg.K_LCTRL] or keys[pg.K_RCTRL])):
                self.scroll_camera("down")


        #In game interactions
        if not (self.player_army_movement_animation or self.enemy_army_movement_animation or self.player_army_retreat_animation > 0 or self.enemy_army_retreat_animation > 0):

            # gold icon or food icon hover
            if self.gold_icon.rect.collidepoint(pos):
                self.gold_icon.hovering = True
                if self.gold_icon.tooltip_background:
                    self.game_tooltip_description = "Projected Tax/Building Income: {}\nProjected Trade Revenue: {}\nProjected Upkeep Cost: {}\nProjected Net Income: {}"
            else:
                self.gold_icon.hovering = False
                if self.food_icon.rect.collidepoint(pos):
                    self.food_icon.hovering = True
                    if self.food_icon.tooltip_background:
                        self.game_tooltip_description = "Projected Food Production: {}\nProjected Food Consumption: {}\nProjected Food Surplus: {}"
                else:
                    self.food_icon.hovering = False
                    self.game_tooltip_description = ""

            # sp atk icons/clickable icons: diplomacy, tech tree, etc...
            for icon in self.icons:
                if type(icon) == Button:
                    if icon.rect.collidepoint(pos):
                        icon.hovering = True
                        hovering_over_tooltip_ui_element = True
                        if icon.tooltip_bg:
                            self.special_attack_tooltip_description = icon.tooltip
                            self.special_attack_tooltip_x = icon.tooltip_bg.rect.left
                            self.special_attack_tooltip_y = icon.tooltip_bg.rect.top
                        for event in pg.event.get():
                            if event.type == pg.MOUSEBUTTONUP and event.button == 1:
                                self.play_sound_effect(self.click_sound)
                                if not icon.disabled and not (self.previewing_battle or self.viewing_decision_window or self.viewing_battle_summary):
                                    try:
                                        icon.hovering = False
                                        icon.pressed = False
                                        self.special_attack_tooltip_description = ""
                                        icon.kill_tooltip()
                                    except:
                                        pass
                                    icon.command()
                            else:
                                icon.pressed = False
                    else:
                        icon.hovering = False

                elif type(icon) == TerrainIcon:
                    if icon.rect.collidepoint(pos):
                        icon.hovering = True
                        hovering_over_tooltip_ui_element = True
                        if icon.tooltip:
                            self.special_attack_tooltip_description = icon.tooltip_description
                            self.special_attack_tooltip_x = icon.tooltip.rect.left
                            self.special_attack_tooltip_y = icon.tooltip.rect.top
                    else:
                        icon.hovering = False

            if not hovering_over_tooltip_ui_element:
                self.special_attack_tooltip_description = ""


            for event in pg.event.get():
                #quit
                if event.type == pg.QUIT:
                    choice = pg.display.message_box("Quit", "Are you sure you wish to quit? You will lose any unsaved progress.",
                                                    buttons=("Yes", "No"), return_button=1, escape_button=None
                                                    )

                    if choice == 0:
                        pg.quit()
                        sys.exit()

                #left clicks
                if event.type == pg.MOUSEBUTTONUP and event.button == 1:
                    self.play_sound_effect(self.click_sound)
                    if self.game_message:
                        self.game_message = ""
                    if not (self.previewing_battle or self.viewing_battle_summary):
                        if self.selected_player_army:
                            self.selected_player_army.selected = False
                            self.selected_player_army = None
                            print("army de-selected")
                            for grid_sprite in self.walkable_grid_sprites:
                                grid_sprite.kill()

                        if self.selected_player_city:
                            self.selected_player_city = None

                        hovering_over_army = False
                        for army_sprite in self.army_sprites:
                            if army_sprite.rect.collidepoint(pos):
                                own_army = army_sprite.commander.country == self.player_country
                                hovering_over_army = True
                                self.toggle_selected_army_info(army_sprite, select=own_army)
                                break

                        # if not hovering over army, check if hovering over city
                        if not hovering_over_army:
                            if self.viewing_army_info:
                                self.toggle_selected_army_info(None)  # toggle army info off if on

                            hovering_over_city = False
                            for city_sprite in self.city_sprites:
                                if city_sprite.rect.collidepoint(pos):
                                    own_city = city_sprite.country == self.player_country
                                    self.toggle_selected_city_info(city_sprite, select=own_city)
                                    hovering_over_city = True
                                    break
                            if not hovering_over_city:
                                self.toggle_selected_city_info(None)

                        #if viewing list, clear associated sprites:
                        if self.viewing_list:
                            self.clear_list_sprites(True)

                    for button in self.buttons:
                        if button.rect.collidepoint(pos):
                            button.pressed = True
                            if not button.disabled:
                                try:
                                    button.hovering = False
                                    button.kill_tooltip()
                                except Exception as e:
                                    print(e)
                                button.command()
                            break


                #right clicks
                if event.type == pg.MOUSEBUTTONUP and event.button == 3:
                    self.play_sound_effect(self.click_sound)
                    self.toggle_selected_army_info(None)
                    if self.game_message:
                        self.game_message = ""

                    for grid_sprite in self.walkable_grid_sprites:
                        if grid_sprite.rect.collidepoint(pos) and "unwalkable" not in grid_sprite.filename:
                            x1, y1 = self.selected_player_army.grid_position_x, self.selected_player_army.grid_position_y
                            x2, y2 = grid_sprite.grid_position_x, grid_sprite.grid_position_y

                            self.grid_offset_x = max([0, x1 - self.walkable_range])
                            self.grid_offset_y = max([0, y1 - self.walkable_range])

                            shortest_path_results = self.calculate_shortest_path(self.weighted_grid, x1, y1, x2, y2, self.selected_player_army)
                            for coord in shortest_path_results["path"]:
                                self.army_movement_grid_coords.append(coord)
                            self.army_movement_grid_coords.reverse()
                            #self.selected_player_army.action_points -= shortest_path_results["cost"]

                            self.player_army_movement_animation = True
                            for gs in self.walkable_grid_sprites:
                                gs.kill()
                            break

                # key interactions at any time
                if event.type == pg.KEYUP:
                    if event.key == pg.K_f:
                        if pg.FULLSCREEN and self.screen.get_flags():
                            pg.display.set_mode((self.game_width, self.game_height))
                        else:
                            pg.display.set_mode((self.game_width, self.game_height), pg.FULLSCREEN|pg.DOUBLEBUF)
                    elif event.key == pg.K_SPACE:
                        if self.animation_delay == ANIMATION_DELAY:
                            self.animation_delay = ANIMATION_DELAY//4
                        else:
                            self.animation_delay = ANIMATION_DELAY
                    elif event.key == pg.K_ESCAPE:
                        self.queue_dilemma("quit_game")
                        self.present_next_dilemma()

                    elif event.key == pg.K_RETURN and not (self.enemy_moving or self.resume_enemy_moving or self.viewing_decision_window):
                        keys = pg.key.get_pressed()
                        if keys[pg.K_LCTRL] or keys[pg.K_RCTRL]:
                            self.end_turn()

                    elif event.key == pg.K_l and not (self.enemy_moving or self.resume_enemy_moving or self.viewing_decision_window):
                        keys = pg.key.get_pressed()
                        if keys[pg.K_LCTRL] or keys[pg.K_RCTRL]:
                            self.new("auto_save.p")

                    elif event.key == pg.K_TAB and not (self.viewing_army_info or self.viewing_city_info or self.viewing_decision_window or self.viewing_battle_summary):
                        keys = pg.key.get_pressed()
                        if keys[pg.K_LSHIFT] or keys[pg.K_RSHIFT]:
                            self.zoom_to_next_army(-1)
                        else:
                            self.zoom_to_next_army()

                    elif event.key == pg.K_z and not (self.viewing_army_info or self.viewing_city_info or self.viewing_decision_window or self.viewing_battle_summary):
                        keys = pg.key.get_pressed()
                        if keys[pg.K_LSHIFT] or keys[pg.K_RSHIFT]:
                            self.zoom_to_next_city(-1)
                        else:
                            self.zoom_to_next_city()

                    elif event.key == pg.K_p:
                        print(self.loaded_game_data["ongoing_achievements"])
                        #self.generate_military_report()


                if event.type == pg.KEYDOWN:
                    if event.key in [pg.K_s, pg.K_LCTRL, pg.K_RCTRL] and not (self.enemy_moving or self.resume_enemy_moving):
                        keys = pg.key.get_pressed()
                        if (keys[pg.K_LCTRL] or keys[pg.K_RCTRL]) and keys[pg.K_s]:
                            self.save_game("quick_save.p")
                            self.game_message = "Game saved."
                            self.game_message_color = WHITE
                            self.last_action = pg.time.get_ticks()
                    if event.key in [pg.K_m, pg.K_LCTRL, pg.K_RCTRL]:
                        keys = pg.key.get_pressed()
                        if keys[pg.K_m] and (keys[pg.K_LCTRL] or keys[pg.K_RCTRL]):
                            if self.is_mute:
                                self.is_mute = False
                                self.game_message_color = GREEN
                                self.game_message = "Toggled sound ON"
                                self.last_action = now
                                pg.mixer.music.load(self.current_bgm)
                                pg.mixer.music.play(20)
                            else:
                                self.is_mute = True
                                self.game_message_color = RED
                                self.game_message = "Toggled sound OFF"
                                self.last_action = now
                                pg.mixer.music.stop()
                    if event.key in [pg.K_n, pg.K_LCTRL, pg.K_RCTRL]:
                        keys = pg.key.get_pressed()
                        if keys[pg.K_n] and (keys[pg.K_LCTRL] or keys[pg.K_RCTRL]):
                            if not self.is_mute:
                                self.load_random_bgm()
                                pg.mixer.music.play(20)


    def load_random_bgm(self, exclude_current=True):
        bgm_list = ["determined_pursuit_loop.wav", "light_battle.ogg", "undisputed_stefan_grossmann.ogg",
                        "Fantasy Armies.ogg", "battleThemeA.mp3", "wly.ogg"]
        if exclude_current:
            bgm_list.remove(self.current_bgm)
        self.current_bgm = choice(bgm_list)
        pg.mixer.music.load(self.current_bgm)


    def return_to_menu(self):
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = self.return_to_menu
        elif not self.transition.fading:
            for sp in self.all_sprites:
                sp.kill()
            self.transition_clean_up()
            self.main_game.new()


    def scroll_camera(self, direction):
        if direction == "left":
            if self.map.position_x < -int(self.zoom_scale * 32):
                self.map.position_x += int(self.zoom_scale * 32)
                self.camera_current_x -= int(self.zoom_scale * 32)
                for fixed_sprite in self.fixed_sprites:
                    fixed_sprite.position_x += int(self.zoom_scale * 32)

        elif direction == "right":
            if self.map.position_x > -(self.map_width - LANDSCAPE_WIDTH - int(self.zoom_scale * 32)):
                self.map.position_x -= int(self.zoom_scale * 32)
                self.camera_current_x += int(self.zoom_scale * 32)
                for fixed_sprite in self.fixed_sprites:
                    fixed_sprite.position_x -= int(self.zoom_scale * 32)

        elif direction == "up":
            if self.map.position_y < -int(self.zoom_scale * 32):
                self.map.position_y += int(self.zoom_scale * 32)
                self.camera_current_y -= int(self.zoom_scale * 32)
                for fixed_sprite in self.fixed_sprites:
                    fixed_sprite.position_y += int(self.zoom_scale * 32)

        elif direction == "down":
            if self.map.position_y > -(self.map_height - LANDSCAPE_HEIGHT - int(self.zoom_scale * 32)):
                self.map.position_y -= int(self.zoom_scale * 32)
                self.camera_current_y += int(self.zoom_scale * 32)
                for fixed_sprite in self.fixed_sprites:
                    fixed_sprite.position_y -= int(self.zoom_scale * 32)


    def zoom_to_next_army(self, direction=1):
        self.camera_zoom_target_army_index += direction
        player_army_zoom_targets = []
        for army_sprite in self.army_sprites:
            if army_sprite.commander.country.lower() == self.player_country.lower():
                player_army_zoom_targets.append(army_sprite)
        if len(player_army_zoom_targets) == 0: #no army to zoom to
            return
        if self.camera_zoom_target_army_index >= len(self.player_army_sprites):
            self.camera_zoom_target_army_index = 0
        elif self.camera_zoom_target_army_index < 0:
            self.camera_zoom_target_army_index = len(self.player_army_sprites) - 1
        self.camera_zoom(player_army_zoom_targets[self.camera_zoom_target_army_index])


    def zoom_to_next_city(self, direction=1):
        self.camera_zoom_target_city_index += direction
        player_city_zoom_targets = []
        for city_sprite in self.city_sprites:
            if city_sprite.country.lower() == self.player_country.lower():
                player_city_zoom_targets.append(city_sprite)

        if len(player_city_zoom_targets) == 0: #no army to zoom to
            return
        if self.camera_zoom_target_city_index >= len(player_city_zoom_targets):
            self.camera_zoom_target_city_index = 0
        elif self.camera_zoom_target_city_index < 0:
            self.camera_zoom_target_city_index = len(player_city_zoom_targets) - 1
        self.camera_zoom(player_city_zoom_targets[self.camera_zoom_target_city_index])


    def load_diplomacy(self):
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = self.load_diplomacy
        elif not self.transition.fading:
            self.transition_clean_up()
            self.toggle_selected_army_info(None)
            self.save_game(save_to="diplomacy_autosave.p")
            self.clear_list_sprites(True)
            diplomacy_scene = Diplomacy_Scene(self)
            diplomacy_scene.new()


    def list_generals(self, reset_offset=True):
        self.clear_list_sprites(reset_offset)
        self.viewing_list = "generals"
        counter = 0
        start_x, start_y = self.toolbar_general_icon.rect.left, self.toolbar_bg.rect.bottom
        army_sprites_to_display = []

        for army_sprite in self.army_sprites:
            if army_sprite.commander.country == self.player_country:
                army_sprites_to_display.append(army_sprite)
                counter += 1

        #army_sprites_to_display_sorted = sorted(army_sprites_to_display, key=lambda x: x.commander.level, reverse=True)
        army_sprites_to_display_sorted = sorted(army_sprites_to_display, key=lambda x: x.commander.simple_name, reverse=False)
        counter = 0
        for army_sprite in army_sprites_to_display_sorted:
            if counter in range(self.viewing_list_offset, self.viewing_list_offset+10):
                bg_button = Button(start_x, start_y+50*(counter-self.viewing_list_offset), "sprite_list_bg.png",
                                   command=partial(self.camera_zoom, army_sprite, None, True), anchor="topleft")
                #Static(start_x, start_y+50*counter, "sprite_list_bg.png", anchor="topleft")
                self.buttons.add(bg_button)
                self.list_sprites.add(bg_button)
                self.all_sprites.add(bg_button)

                general_avatar = Static(start_x+1, start_y+1+50*(counter-self.viewing_list_offset),
                                        "general_avatar_{}.png".format(army_sprite.commander.simple_name),
                                        anchor="topleft", scale=(34, 48))
                self.list_sprites.add(general_avatar)
                self.all_sprites.add(general_avatar)

            counter += 1


    def list_cities(self, reset_offset=True):
        self.clear_list_sprites(reset_offset)
        self.viewing_list = "cities"

        counter = 0
        start_x, start_y = self.toolbar_cities_icon.rect.left, self.toolbar_bg.rect.bottom
        city_sprites_to_display = []

        for city_sprite in self.city_sprites:
            if city_sprite.country == self.player_country:
                city_sprites_to_display.append(city_sprite)
                counter += 1

        city_sprites_to_display_sorted = sorted(city_sprites_to_display, key=lambda x: x.population, reverse=True)
        counter = 0

        for city_sprite in city_sprites_to_display_sorted:
            if counter in range(self.viewing_list_offset, self.viewing_list_offset + 10):
                bg_button = Button(start_x, start_y + 50*(counter-self.viewing_list_offset), "sprite_list_bg_wide.png",
                                   command=partial(self.camera_zoom, city_sprite, None, True), anchor="topleft")
                # Static(start_x, start_y+50*counter, "sprite_list_bg.png", anchor="topleft")
                self.buttons.add(bg_button)
                self.list_sprites.add(bg_button)
                self.all_sprites.add(bg_button)

                city_avatar = Static(start_x + 50, start_y + 1 + 50*(counter-self.viewing_list_offset),
                                     "sprite_fortress_{}_icon_{}.png".format(city_sprite.fortress_level, city_sprite.country.lower()),
                                     anchor="midtop", scale=(28, 24))
                self.list_sprites.add(city_avatar)
                self.all_sprites.add(city_avatar)

            counter += 1


    def list_alerts(self, reset_offset=True):
        self.clear_list_sprites(reset_offset)
        self.viewing_list = "alerts"

        start_x, start_y = self.toolbar_alerts_icon.rect.left - 200, self.toolbar_bg.rect.bottom
        counter = 0

        for dilemma in self.current_turn_dilemmas:
            dilemma_attrs = self.current_turn_dilemmas[dilemma]
            if counter in range(self.viewing_list_offset, self.viewing_list_offset + 10):
                if dilemma_attrs["target"]:
                    if type(dilemma_attrs["target"]) in [CampaignMapCitySprite, CampaignMapArmySprite]:
                        bg_button = Button(start_x, start_y + 50 * (counter - self.viewing_list_offset), "sprite_list_bg_widest.png",
                                           command=partial(self.camera_zoom, dilemma_attrs["target"]), anchor="topleft")
                    else:
                        bg_button = Button(start_x, start_y + 50 * (counter - self.viewing_list_offset), "sprite_list_bg_widest.png",
                                           command=dilemma_attrs["target"], anchor="topleft")
                else:
                    bg_button = Button(start_x, start_y + 50 * (counter - self.viewing_list_offset), "sprite_list_bg_widest.png",
                                       command=partial(print, ""), anchor="topleft")
                # Static(start_x, start_y+50*counter, "sprite_list_bg.png", anchor="topleft")
                self.buttons.add(bg_button)
                self.list_sprites.add(bg_button)
                self.all_sprites.add(bg_button)

                dilemma_avatar = Static(start_x + 1, start_y + 1 + 50 * (counter - self.viewing_list_offset),
                                     DECISION_IMAGES[dilemma.split("|||")[0]], anchor="topleft", scale=(48, 48))
                self.list_sprites.add(dilemma_avatar)
                self.all_sprites.add(dilemma_avatar)

            counter += 1


    def clear_list_sprites(self, reset_offset):
        for sp in self.list_sprites:
            self.all_sprites.remove(sp)
            self.list_sprites.remove(sp)
            try:
                sp.kill()
            except:
                pass
        if reset_offset:
            self.viewing_list = None
            self.viewing_list_offset = 0
            self.list_arrow_down.rect.center = -30000, -30000
            self.list_arrow_up.rect.center = -30000, -30000


    def load_tech_tree(self):
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = self.load_tech_tree
        elif not self.transition.fading:
            self.transition_clean_up()
            self.toggle_selected_army_info(None)
            self.save_game(save_to="tech_tree_autosave.p")
            self.clear_list_sprites(True)
            tech_tree_scene = Tech_Tree_Scene(self)
            tech_tree_scene.new()


    def load_world_map(self):
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = self.load_world_map
        elif not self.transition.fading:
            self.transition_clean_up()
            self.toggle_selected_army_info(None)
            self.save_game(save_to="world_map_autosave.p")
            self.clear_list_sprites(True)
            world_map_scene = World_Map_Scene(self)
            world_map_scene.new()


    def toggle_selected_army_info(self, army_sprite, select=True):
        for sp in self.alert_sprites:
            sp.kill()
        for sp in self.info_sprites:
            sp.kill()
        self.attack_icon_sprite.position_x, self.attack_icon_sprite.position_y = -30000, -30000
        self.defend_icon_sprite.position_x, self.defend_icon_sprite.position_y = -30000, -30000
        #self.army_merge_icon_sprite.position_x, self.army_merge_icon_sprite.position_y = -30000, -30000
        self.viewing_city_info = None

        if army_sprite:
            #print(army_sprite.grid_position_x, army_sprite.grid_position_y)
            info_bg = Static(LANDSCAPE_WIDTH - 350, LANDSCAPE_HEIGHT//2, "sprite_army_info_bg.png", anchor="midleft")
            self.all_sprites.add(info_bg)
            self.info_sprites.add(info_bg)

            info_army_general_avatar = Static(LANDSCAPE_WIDTH-310, 50, "general_avatar_{}.png".format(army_sprite.commander.simple_name),
                                                               anchor="topleft", scale=(70, 100))
            self.all_sprites.add(info_army_general_avatar)
            self.info_sprites.add(info_army_general_avatar)

            self.sp_atk_icons_list = []
            counter = 0
            for sp_atk in army_sprite.commander.sp_atks:
                sp_atk_icon = SpecialAttackIcon(self, LANDSCAPE_WIDTH-310 + 55 * counter, 205, sp_atk, anchor="topleft")
                self.all_sprites.add(sp_atk_icon)
                self.info_sprites.add(sp_atk_icon)
                self.sp_atk_icons_list.append(sp_atk_icon)
                counter += 1

            self.skill_icons_list = []
            counter = 0
            for skill in army_sprite.commander.skills:
                sp_atk_icon = SpecialAttackIcon(self, LANDSCAPE_WIDTH-310 + 55 * counter, 305, skill, anchor="topleft", type="skill")
                self.all_sprites.add(sp_atk_icon)
                self.info_sprites.add(sp_atk_icon)
                self.skill_icons_list.append(sp_atk_icon)
                counter += 1

            self.info_army_unit_types = list(army_sprite.army_composition.keys())
            if army_sprite.commander.name in self.unit_espionage_variables:
                unit_esp_vars = self.unit_espionage_variables[army_sprite.commander.name]
            else:
                unit_esp_vars = self.unit_espionage_variables["other"]
            count = 0
            for row in range(2):
                for column in range(4):
                    if count < len(self.info_army_unit_types):
                        if unit_esp_vars[count] or select:
                            unit_type = self.info_army_unit_types[count]
                            if "barbarian" in unit_type.lower() or "elephant" in unit_type.lower() or "rattan" in unit_type.lower():
                                unit_country_suffix = "shu"
                            else:
                                unit_country_suffix = army_sprite.commander.country
                            unit_icon = UnitIcon(self, "left", column * 65 + LANDSCAPE_WIDTH - 310, row * 62 + 405,
                                                 unit_type, unit_country_suffix)
                        else:
                            unit_icon = Static(column * 65 + LANDSCAPE_WIDTH - 310, row * 62 + 405,
                                               "sprite_unknown_unit_icon.png")

                    else:
                        unit_icon = UnitIcon(self, "left", column * 65 + LANDSCAPE_WIDTH - 310, row * 62 + 405, None,
                                             None)
                    self.info_sprites.add(unit_icon)
                    self.all_sprites.add(unit_icon)
                    count += 1

            self.viewing_army_info = army_sprite.commander.name

            if select:
                self.selected_player_army = army_sprite
                print("army selected", army_sprite.action_points, (army_sprite.grid_position_x, army_sprite.grid_position_y), army_sprite.embedded_in_city)
                if not army_sprite.selected:
                    army_sprite.selected = True

                manage_button = Button(LANDSCAPE_WIDTH - 170, 575, "campaign_button_manage.png",
                                      command=partial(self.manage_army, army_sprite))
                self.info_sprites.add(manage_button)
                self.buttons.add(manage_button)
                self.all_sprites.add(manage_button)

                if self.selected_player_army.commander.name in self.game_alerts["army"] or self.selected_player_army.commander.upgrade_points > 0:
                    alert = Static(manage_button.rect.centerx, manage_button.rect.centery, "sprite_exclamation_mark_1.png",
                                   [pg.image.load("sprite_exclamation_mark_{}.png".format(i)).convert_alpha() for i in [2,3,4,5,4,3,2,1]],
                                   frame_delta=85, anchor="center")
                    self.all_sprites.add(alert)
                    self.alert_sprites.add(alert)

                #advice
                hostile_target_nearby = False
                for hostile_country in self.hostile_relationships[self.player_country]:
                    for hostile_army_sprite in self.army_sprites:
                        if hostile_army_sprite.commander.country == hostile_country:
                            md = calculate_manhattan_distance(army_sprite, hostile_army_sprite)
                            if md <= 20:
                                self.give_advice("help_army_attack")
                                hostile_target_nearby = True
                                break
                    if not hostile_target_nearby:
                        for hostile_city_sprite in self.city_sprites:
                            if hostile_city_sprite.country == hostile_country:
                                md = calculate_manhattan_distance(army_sprite, hostile_city_sprite)
                                if md <= 30:
                                    self.give_advice("help_army_attack")
                                    break
                if not hostile_target_nearby:
                    self.give_advice("help_army_info")

        else:
            self.viewing_army_info = None


    def manage_army(self, army_sprite):
        if self.selected_player_army:
            self.selected_player_army.selected = False
            self.selected_player_army = None
            for grid_sprite in self.walkable_grid_sprites:
                grid_sprite.kill()
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = lambda: self.manage_army(army_sprite)
        elif not self.transition.fading:
            self.transition_clean_up()
            self.toggle_selected_army_info(None)
            self.save_game(save_to="army_management_autosave.p")
            manage_army_scene = Army_Management_Scene(self, army_sprite)
            manage_army_scene.new()


    def toggle_selected_city_info(self, city_sprite, select=True):
        for sp in self.alert_sprites:
            sp.kill()
        for sp in self.info_sprites:
            sp.kill()
        self.attack_icon_sprite.position_x, self.attack_icon_sprite.position_y = -30000, -30000
        self.defend_icon_sprite.position_x, self.defend_icon_sprite.position_y = -30000, -30000
        self.army_merge_icon_sprite.position_x, self.army_merge_icon_sprite.position_y = -30000, -30000
        self.viewing_army_info = None

        if city_sprite:
            info_bg = Static(LANDSCAPE_WIDTH - 350, LANDSCAPE_HEIGHT//2, "sprite_army_info_bg.png", anchor="midleft")
            self.all_sprites.add(info_bg)
            self.info_sprites.add(info_bg)

            if city_sprite.country.lower() not in ["shu", "wei", "wu", "liu biao", "liu zhang", "ma teng", "zhang lu"]:
                city_country = "rogue"
            else:
                city_country = city_sprite.country.lower()
            info_city_avatar = Static(LANDSCAPE_WIDTH-325, 50, "sprite_fortress_{}_icon_{}.png".format(city_sprite.fortress_level, city_country),
                                                               anchor="topleft", scale=(70, 60))
            self.all_sprites.add(info_city_avatar)
            self.info_sprites.add(info_city_avatar)

            self.info_army_unit_types = list(city_sprite.garrison.keys())
            if city_sprite.name in self.unit_espionage_variables:
                unit_esp_vars = self.unit_espionage_variables[city_sprite.name]
            else:
                unit_esp_vars = self.unit_espionage_variables["other"]
            count = 0
            for row in range(2):
                for column in range(4):
                    if count < len(self.info_army_unit_types):
                        if unit_esp_vars[count] or select:
                            unit_type = self.info_army_unit_types[count]
                            if "barbarian" in unit_type.lower() or "elephant" in unit_type.lower() or "rattan" in unit_type.lower():
                                unit_country_suffix = "shu"
                            else:
                                unit_country_suffix = city_sprite.country
                            unit_icon = UnitIcon(self, "left", column * 65 + LANDSCAPE_WIDTH - 300, row * 62 + 200,
                                                 unit_type, unit_country_suffix)
                        else:
                            unit_icon = Static(column * 65 + LANDSCAPE_WIDTH - 300, row * 62 + 200,
                                               "sprite_unknown_unit_icon.png")

                    else:
                        unit_icon = UnitIcon(self, "left", column * 65 + LANDSCAPE_WIDTH - 300, row * 62 + 200, None,
                                             None)
                    self.info_sprites.add(unit_icon)
                    self.all_sprites.add(unit_icon)
                    count += 1

            wall_icon = Static(LANDSCAPE_WIDTH-325, 400, "city_management_walls_upgrading.png", scale=(100,100))
            self.info_sprites.add(wall_icon)
            self.all_sprites.add(wall_icon)


            self.viewing_city_info = city_sprite.name

            if select:
                self.selected_player_city = city_sprite
                manage_button = Button(LANDSCAPE_WIDTH - 170, 575, "campaign_button_manage.png",
                                       command=partial(self.manage_city, city_sprite))
                self.info_sprites.add(manage_button)
                self.buttons.add(manage_button)
                self.all_sprites.add(manage_button)

                cond_1 = self.selected_player_city.name in self.game_alerts["city"]
                cond_2 = False
                for building in self.selected_player_city.buildings:
                    if building.name in self.game_alerts["building"]:
                        cond_2 = True
                if cond_1 or cond_2:
                    alert = Static(manage_button.rect.centerx, manage_button.rect.centery,"sprite_exclamation_mark_1.png",
                                   [pg.image.load("sprite_exclamation_mark_{}.png".format(i)).convert_alpha() for i in [2, 3, 4, 5, 4, 3, 2, 1]],
                                   frame_delta=85, anchor="center")
                    self.all_sprites.add(alert)
                    self.alert_sprites.add(alert)

                if city_sprite.happiness <= 0:
                    self.give_advice("help_city_low_happiness")
                else:
                    self.give_advice("help_city_info")

        else:
            self.viewing_city_info = None


    def manage_city(self, city_sprite):
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = lambda: self.manage_city(city_sprite)
        elif not self.transition.fading:
            self.transition_clean_up()
            self.toggle_selected_city_info(None)
            self.save_game(save_to="city_management_autosave.p")
            city_management_scene = City_Management_Scene(self, city_sprite)
            city_management_scene.new()


    def check_for_merge_conflict(self, a, b):
        set_a = set(a.keys())
        set_b = set(b.keys())
        combined_set = set_a.union(set_b)
        return len(combined_set) > 8


    def check_for_own_army_collision(self): #see if player is trying to merge with friendly army
        if self.player_army_retreat_animation > 0:
            return False
        for player_army in self.player_army_sprites:
            if player_army != self.selected_player_army and self.selected_player_army:
                delta_x = (self.selected_player_army.position_x - player_army.position_x) // int(self.zoom_scale*32)
                delta_y = (self.selected_player_army.position_y - player_army.position_y) // int(self.zoom_scale*32)
                army_merge_icon_delta_x = (self.selected_player_army.position_x - self.army_merge_icon_sprite.position_x) // int(self.zoom_scale*32)
                army_merge_icon_delta_y = (self.selected_player_army.position_y - self.army_merge_icon_sprite.position_y) // int(self.zoom_scale*32)
                #if abs(delta_x) + abs(delta_y) <= 1 and not (self.army_merge_icon_sprite.position_x + self.army_merge_icon_sprite.position_y == -60000):
                if abs(delta_x) + abs(delta_y) <= 1 and abs(army_merge_icon_delta_x) + abs(army_merge_icon_delta_y) <= 2:
                    self.army_merge_icon_sprite.position_x, self.army_merge_icon_sprite.position_y = -30000, -30000
                    self.selected_player_army.selected = False
                    self.player_army_movement_animation = False
                    self.army_movement_grid_coords = []
                    self.display_army_merge_interface(self.selected_player_army, player_army)
                    self.selected_player_army = None
                    return True


    def display_army_merge_interface(self, left_army_obj, right_army_obj):
        self.viewing_army_merge_interface = True
        self.hide_toolbar_icons()
        self.left_preview_army = left_army_obj
        self.right_preview_army = right_army_obj

        self.army_merge_unit_icons = []
        self.selected_army_merge_unit_index = None

        self.sp_atk_icons_list = []
        self.skill_icons_list = []

        self.battle_preview_bg = Static(LANDSCAPE_WIDTH // 2, LANDSCAPE_HEIGHT // 2, "sprite_battle_preview_bg.png",
                                        anchor="center")
        self.all_sprites.add(self.battle_preview_bg)
        self.battle_preview_sprites.add(self.battle_preview_bg)

        self.close_army_merge_interface = Button(self.battle_preview_bg.rect.right, self.battle_preview_bg.rect.top,
                                                 "sprite_close_button_large.png",
                                                 command=self.exit_army_merge_interface, anchor="center")
        self.buttons.add(self.close_army_merge_interface)
        self.all_sprites.add(self.close_army_merge_interface)
        self.battle_preview_sprites.add(self.close_army_merge_interface)

        # left side preview
        if self.left_preview_army.commander.simple_name != "none":
            self.battle_preview_player_general_avatar = Static(150, 50, "general_avatar_{}.png".format(self.left_preview_army.commander.simple_name),
                                                               anchor="topleft", scale=(70, 100))
            self.all_sprites.add(self.battle_preview_player_general_avatar)
            self.battle_preview_sprites.add(self.battle_preview_player_general_avatar)
        counter = 0
        for sp_atk in self.left_preview_army.commander.sp_atks:
            sp_atk_icon = SpecialAttackIcon(self, 150 + 55 * counter, 200, sp_atk, anchor="topleft")
            self.all_sprites.add(sp_atk_icon)
            self.battle_preview_sprites.add(sp_atk_icon)
            self.sp_atk_icons_list.append(sp_atk_icon)
            counter += 1

        counter = 0
        for skill in self.left_preview_army.commander.skills:
            sp_atk_icon = SpecialAttackIcon(self, 150 + 55 * counter, 300, skill, anchor="topleft", type="skill")
            self.all_sprites.add(sp_atk_icon)
            self.battle_preview_sprites.add(sp_atk_icon)
            self.skill_icons_list.append(sp_atk_icon)
            counter += 1

        self.left_army_unit_types = list(self.left_preview_army.army_composition.keys())
        count = 0
        for row in range(2):
            for column in range(4):
                if count < len(self.left_army_unit_types):
                    unit_type = self.left_army_unit_types[count]
                    unit_icon = UnitIcon(self, "left", column * 65 + 150, row * 62 + 400, unit_type,
                                         self.left_preview_army.commander.country)
                    self.army_merge_unit_icons.append(unit_icon)

                else:
                    unit_icon = UnitIcon(self, "left", column * 65 + 150, row * 62 + 400, None, None)
                self.battle_preview_sprites.add(unit_icon)
                self.all_sprites.add(unit_icon)
                count += 1

        # option buttons
        merge_button = Button(LANDSCAPE_WIDTH//2, LANDSCAPE_HEIGHT//2+120,
                                   "sprite_campaign_map_army_merge_button.png",
                                   command=self.merge_army)
        self.battle_preview_sprites.add(merge_button)
        self.buttons.add(merge_button)
        self.all_sprites.add(merge_button)

        # right side preview
        if self.right_preview_army.commander.simple_name != "none":
            self.battle_preview_enemy_general_avatar = Static(LANDSCAPE_WIDTH // 2 + 180, 50, "general_avatar_{}.png".format(self.right_preview_army.commander.simple_name),
                                                              anchor="topleft", scale=(70, 100))
            self.all_sprites.add(self.battle_preview_enemy_general_avatar)
            self.battle_preview_sprites.add(self.battle_preview_enemy_general_avatar)

        counter = 0
        for sp_atk in self.right_preview_army.commander.sp_atks:
            sp_atk_icon = SpecialAttackIcon(self, LANDSCAPE_WIDTH // 2 + 180 + 55 * counter, 200, sp_atk,
                                            anchor="topleft")
            self.all_sprites.add(sp_atk_icon)
            self.battle_preview_sprites.add(sp_atk_icon)
            self.sp_atk_icons_list.append(sp_atk_icon)
            counter += 1

        counter = 0
        for sp_atk in self.right_preview_army.commander.skills:
            sp_atk_icon = SpecialAttackIcon(self, LANDSCAPE_WIDTH // 2 + 180 + 55 * counter, 300, sp_atk,
                                            anchor="topleft", type="skill")
            self.all_sprites.add(sp_atk_icon)
            self.battle_preview_sprites.add(sp_atk_icon)
            self.sp_atk_icons_list.append(sp_atk_icon)
            counter += 1

        self.right_army_unit_types = list(self.right_preview_army.army_composition.keys())
        count = 0
        for row in range(2):
            for column in range(4):
                if count < len(self.right_army_unit_types):
                    unit_type = self.right_army_unit_types[count]
                    unit_icon = UnitIcon(self, "left", column * 65 + 180 + LANDSCAPE_WIDTH // 2, row * 62 + 400,
                                         unit_type, self.right_preview_army.commander.country)
                    self.army_merge_unit_icons.append(unit_icon)

                else:
                    unit_icon = UnitIcon(self, "left", column * 65 + 180 + LANDSCAPE_WIDTH // 2, row * 62 + 400, None,
                                         None)
                self.battle_preview_sprites.add(unit_icon)
                self.all_sprites.add(unit_icon)
                count += 1


    def merge_army(self):

        if self.selected_army_merge_unit_index == None:
            #self.game_message = "No unit selected for merging"
            #self.game_message_color = RED
            #self.last_action = pg.time.get_ticks()
            return

        combined_unit_types_list = list(self.left_preview_army.army_composition.keys()) + list(self.right_preview_army.army_composition.keys())
        print(self.selected_army_merge_unit_index, combined_unit_types_list)
        selected_unit = combined_unit_types_list[self.selected_army_merge_unit_index]
        if self.selected_army_merge_unit_index >= len(self.left_preview_army.army_composition):
            selected_unit_dict = {selected_unit:self.right_preview_army.army_composition[selected_unit]}
            merge_conflict = self.check_for_merge_conflict(selected_unit_dict, self.left_preview_army.army_composition)
            if not merge_conflict:
                self.left_preview_army.army_composition = concat_dicts(selected_unit_dict, self.left_preview_army.army_composition)
                del self.right_preview_army.army_composition[selected_unit]

        else:
            selected_unit_dict = {selected_unit: self.left_preview_army.army_composition[selected_unit]}
            merge_conflict = self.check_for_merge_conflict(selected_unit_dict, self.right_preview_army.army_composition)
            if not merge_conflict:
                self.right_preview_army.army_composition = concat_dicts(selected_unit_dict, self.right_preview_army.army_composition)
                if self.right_preview_army.embedded_in_city:
                    for city_sprite in self.city_sprites:
                        if city_sprite.name == self.right_preview_army.embedded_in_city:
                            city_sprite.garrison = self.right_preview_army.army_composition.copy()
                del self.left_preview_army.army_composition[selected_unit]

        self.exit_army_merge_interface(hard_exit=False)
        self.display_army_merge_interface(self.left_preview_army, self.right_preview_army)


    def exit_army_merge_interface(self, hard_exit=True):
        for sp in self.battle_preview_sprites:
            sp.kill()
        if hard_exit:
            self.left_preview_army.action_points -= 5
            self.right_preview_army.action_points -= 5
            if self.left_preview_army.action_points <= 0:
                self.left_preview_army.action_points = 0
            if self.right_preview_army.action_points <= 0:
                self.right_preview_army.action_points = 0
            self.left_preview_army = None
            self.right_preview_army = None
        self.restore_toolbar_icons()
        self.viewing_army_merge_interface = False


    def check_for_own_city_collision(self): #see if player is going inside own city
        if self.player_army_retreat_animation > 0 or self.selected_player_army.embedded_in_city:
            return False
        for city_sprite in self.city_sprites:
            if city_sprite.country in self.player_country and not self.selected_player_army.embedded_in_city and not city_sprite.defender:
                delta_x = (self.selected_player_army.position_x - city_sprite.position_x) // int(self.zoom_scale*32)
                delta_y = (self.selected_player_army.position_y - city_sprite.position_y) // int(self.zoom_scale*32)
                if delta_x in range(-3, 1+city_sprite.fortress_level) and delta_y in range(-2, 3):
                    if self.check_for_merge_conflict(city_sprite.garrison, self.selected_player_army.army_composition):
                        #defender_army = self.generate_no_general_army(city_sprite)
                        #self.display_army_merge_interface(self.selected_player_army, defender_army)
                        self.game_message = "Cannot merge army with existing city garrison. Max 8 different unit types."
                        self.game_message_color = ORANGE
                        return
                    self.embed_army_in_city(self.selected_player_army, city_sprite)
                    self.defend_icon_sprite.position_x, self.defend_icon_sprite.position_y = -30000, -30000
                    self.selected_player_army.selected = False
                    self.player_army_movement_animation = False
                    self.army_movement_grid_coords = []
                    self.selected_player_army = None
                    return True


    def check_for_enemy_city_collision(self):
        if self.player_army_retreat_animation > 0:
            return False
        for city_sprite in self.city_sprites:
            if city_sprite.country in self.hostile_relationships[self.player_country]:
                delta_x = (self.selected_player_army.position_x - city_sprite.position_x) // int(self.zoom_scale*32)
                delta_y = (self.selected_player_army.position_y - city_sprite.position_y) // int(self.zoom_scale*32)
                if delta_x in range(-3, 1+city_sprite.fortress_level) and delta_y in range(-2, 3):

                    if not city_sprite.defender:
                        defending_army = self.generate_no_general_army(city_sprite)
                    else:
                        defending_army = None
                        for army_sprite in self.army_sprites:
                            if army_sprite.commander.name == city_sprite.defender:
                                defending_army = army_sprite

                    if not defending_army:
                        defending_army = self.generate_no_general_army(city_sprite)

                    walls = [building for building in city_sprite.buildings if building.name == "Walls"][0]
                    retainers = FORTRESS_WALL_RETAINERS[walls.level - 1]
                    for retainer, amount in retainers.items():
                        if retainer in defending_army.army_composition:
                            defending_army.army_composition[retainer] += amount
                        elif len(defending_army.army_composition) < 8:
                            defending_army.army_composition[retainer] = amount

                    self.battle_preview(self.selected_player_army, defending_army,
                                        "right", city_sprite)

                    self.player_army_movement_animation = False
                    self.army_movement_grid_coords = []

                    return True


    def check_for_player_city_collision(self): #enemy collide w/ player city?
        if self.enemy_army_retreat_animation > 0:
            return False
        for city_sprite in self.city_sprites:
            if city_sprite.country in self.hostile_relationships[self.selected_enemy_army.commander.country]:
                delta_x = (self.selected_enemy_army.position_x - city_sprite.position_x) // int(self.zoom_scale*32)
                delta_y = (self.selected_enemy_army.position_y - city_sprite.position_y) // int(self.zoom_scale*32)
                if delta_x in range(-3, 1+city_sprite.fortress_level) and delta_y in range(-2, 3):
                    self.army_movement_grid_coords = []
                    self.enemy_moving = False
                    self.resume_enemy_moving = True

                    if not city_sprite.defender:
                        defending_army = self.generate_no_general_army(city_sprite)
                    else:
                        defending_army = None
                        for army_sprite in self.army_sprites:
                            if army_sprite.commander.name == city_sprite.defender:
                                defending_army = army_sprite

                    if not defending_army:
                        defending_army = self.generate_no_general_army(city_sprite)

                    walls = [building for building in city_sprite.buildings if building.name == "Walls"][0]
                    retainers = FORTRESS_WALL_RETAINERS[walls.level - 1]
                    for retainer, amount in retainers.items():
                        if retainer in defending_army.army_composition:
                            defending_army.army_composition[retainer] += amount
                        elif len(defending_army.army_composition) < 8:
                            defending_army.army_composition[retainer] = amount

                    if city_sprite.country == self.player_country:
                        self.battle_preview(defending_army, self.selected_enemy_army, "left", city_sprite)
                    else:
                        self.auto_resolve_battle(self.selected_enemy_army, defending_army, "right", city_sprite)
                    self.selected_enemy_army.attack_target = None
                    self.selected_enemy_army.defend_target = None
                    return True


    def check_if_left_city(self, army):
        for army_sprite in self.army_sprites:
            if army_sprite.commander.name == army.commander.name:
                for city_sprite in self.city_sprites:
                    if city_sprite.name == army.embedded_in_city:
                        delta_x = army_sprite.grid_position_x - city_sprite.grid_position_x
                        delta_y = army_sprite.grid_position_y - city_sprite.grid_position_y
                        #if delta_x != 0 or delta_y != 0:
                        if delta_x not in range(-3, 1+city_sprite.fortress_level)  or delta_y not in range(-2, 3):
                            army_sprite.embedded_in_city = None
                            city_sprite.defender = None
                            city_sprite.garrison = {}
                            city_sprite.update_happiness()
                        break
                break


    def check_for_enemy_embed_into_city(self):
        for city_sprite in self.city_sprites:
            if city_sprite.country == self.selected_enemy_army.commander.country and not city_sprite.defender:
                delta_x = (self.selected_enemy_army.position_x - city_sprite.position_x) // int(self.zoom_scale*32)
                delta_y = (self.selected_enemy_army.position_y - city_sprite.position_y) // int(self.zoom_scale*32)
                if delta_x in range(-3, 1 + city_sprite.fortress_level) and delta_y in range(-2, 3):
                    self.embed_army_in_city(self.selected_enemy_army, city_sprite)
                    self.enemy_army_movement_animation = False
                    self.selected_enemy_army.attack_target = None
                    self.selected_enemy_army.defend_target = None
                    self.army_movement_grid_coords = []

                    self.selected_enemy_army = None
                    return True


    def embed_army_in_city(self, army, city):
        for army_sprite in self.army_sprites:
            if army_sprite.commander.name == army.commander.name:
                for city_sprite in self.city_sprites:
                    if city_sprite.name == city.name:
                        delta_x = city.grid_position_x - army_sprite.grid_position_x
                        delta_y = city.grid_position_y - army_sprite.grid_position_y
                        army_sprite.grid_position_x += delta_x
                        army_sprite.grid_position_y += delta_y
                        army_sprite.position_x += int(delta_x * int(self.zoom_scale*32))
                        army_sprite.position_y += int(delta_y * int(self.zoom_scale*32))
                        army_sprite.embedded_in_city = city.name
                        city.defender = army_sprite.commander.name
                        city.update_happiness()

                        merged_army = concat_dicts(city.garrison, army_sprite.army_composition)
                        #print(len(merged_army), merged_army)
                        if self.check_for_merge_conflict(city_sprite.garrison, army_sprite.army_composition):  # if merge conflict, force merge by keeping top 8 and refunding the others
                        #if len(merged_army) > 8:
                            force_merged_army = {}
                            merged_army_sorted = sorted(merged_army.items(), key=lambda x: x[1], reverse=True)
                            country = army_sprite.commander.country
                            counter = 0
                            for unit_type, amount in merged_army_sorted:
                                if counter < 8:
                                    force_merged_army[unit_type] = amount
                                else:
                                    reimbursement = UNIT_RECRUITMENT_COST[unit_type]*amount
                                    self.loaded_game_data["gold"][country] += reimbursement
                                counter += 1
                            army_sprite.army_composition = force_merged_army.copy()
                            city.garrison = force_merged_army.copy()

                        else:
                            army_sprite.army_composition = merged_army.copy()
                            city.garrison = merged_army.copy()


    def update(self):
        now = pg.time.get_ticks()
        if self.transition:
            if self.transition.fading:
                self.transition.update()
            else:
                if self.post_transition_function:
                    self.post_transition_function()
                else:
                    self.transition = None

        if now - self.last_action > 2500:
            self.game_message = ""
            self.game_message_color = WHITE

        if self.current_advice:
            if now - self.last_advice_given_time >= 1000 + VOICE_OVER_INFO[self.current_advice]["duration"]*1000:
                self.current_advice = None
                pg.mixer.music.set_volume(.5)
        else:
            if now >= 20000:
                self.give_advice("help_camera_scroll")
            if self.loaded_game_data["advice_given"]:
                if now - self.last_advice_given_time >= VOICE_OVER_INFO[self.loaded_game_data["advice_given"][-1]]["duration"]*1000 + 3000:
                    self.give_advice("help_army_movement")
                if self.loaded_game_data["advice_given"][-1] == "help_army_movement" and now - self.last_advice_given_time >= VOICE_OVER_INFO[self.loaded_game_data["advice_given"][-1]]["duration"]*1000:
                    self.give_advice("help_movement_cost")


        if self.player_army_movement_animation:

            if self.selected_player_army.embedded_in_city:
                self.check_if_left_city(self.selected_player_army)

            if self.army_movement_grid_coords: #if player's army still has somewhere to go
                if now - self.army_movement_last_animated >= self.animation_delay:
                    self.army_movement_last_animated = now
                    new_coords = self.army_movement_grid_coords.pop()# + vec(self.grid_offset_x, self.grid_offset_y)
                    delta_x = int(new_coords[0] - self.selected_player_army.grid_position_x)
                    delta_y = int(new_coords[1] - self.selected_player_army.grid_position_y)
                    self.selected_player_army.grid_position_x = int(new_coords[0])
                    self.selected_player_army.grid_position_y = int(new_coords[1])
                    self.selected_player_army.position_x += delta_x*int(self.zoom_scale*32)
                    self.selected_player_army.position_y += delta_y*int(self.zoom_scale*32)

                    cost = self.calculate_grid_movement_cost(self.selected_player_army)
                    self.selected_player_army.action_points -= cost
                    self.last_player_movement_direction = vec(delta_x, delta_y)

                    if self.check_for_enemy_city_collision():
                        pass
                    else:
                        enemy_collision = pg.sprite.spritecollide(self.selected_player_army, self.enemy_army_sprites,
                                                                  False, pg.sprite.collide_circle_ratio(.85))
                        if enemy_collision:
                            collided_enemy = enemy_collision[0]
                            if collided_enemy.commander.country in self.hostile_relationships[self.player_country]:
                                self.player_army_movement_animation = False
                                self.army_movement_grid_coords = []
                                if collided_enemy.embedded_in_city:
                                    siege_city_object = [city for city in self.city_sprites if city.name == collided_enemy.embedded_in_city][0]
                                    walls = [building for building in siege_city_object.buildings if building.name == "Walls"][0]
                                    retainers = FORTRESS_WALL_RETAINERS[walls.level - 1]
                                    for retainer, amount in retainers.items():
                                        if retainer in collided_enemy.army_composition:
                                            collided_enemy.army_composition[retainer] += amount
                                        elif len(collided_enemy.army_composition) < 8:
                                            collided_enemy.army_composition[retainer] = amount
                                    self.battle_preview(self.selected_player_army, collided_enemy, "right", siege_city_object)
                                else:
                                    self.battle_preview(self.selected_player_army, collided_enemy)
                        elif not self.selected_player_army.embedded_in_city:
                            self.check_for_own_city_collision()
                            self.check_for_own_army_collision()

            else: #if player has nowhere else to go based on latest move command
                if self.check_for_enemy_city_collision():
                    pass
                else:
                    enemy_collision = pg.sprite.spritecollide(self.selected_player_army, self.enemy_army_sprites, False,
                                                        pg.sprite.collide_circle_ratio(.85))
                    if enemy_collision:
                        self.player_army_movement_animation = False
                        self.army_movement_grid_coords = []
                        collided_enemy = enemy_collision[0]
                        if collided_enemy.commander.country in self.hostile_relationships[self.player_country]:

                            if collided_enemy.embedded_in_city:
                                siege_city_object = [city for city in self.city_sprites if city.name == collided_enemy.embedded_in_city][0]
                                walls = [building for building in siege_city_object.buildings if building.name == "Walls"][0]
                                retainers = FORTRESS_WALL_RETAINERS[walls.level - 1]
                                for retainer, amount in retainers.items():
                                    if retainer in collided_enemy.army_composition:
                                        collided_enemy.army_composition[retainer] += amount
                                    elif len(collided_enemy.army_composition) < 8:
                                        collided_enemy.army_composition[retainer] = amount
                                self.battle_preview(self.selected_player_army, collided_enemy, "right", siege_city_object)
                            else:
                                self.battle_preview(self.selected_player_army, collided_enemy)

                    elif not self.selected_player_army.embedded_in_city:
                        if not self.check_for_own_city_collision():
                            self.player_army_movement_animation = False
                            self.army_movement_grid_coords = []
                            self.selected_player_army.selected = False
                            self.selected_player_army = None
                    else:
                        self.player_army_movement_animation = False
                        self.army_movement_grid_coords = []
                        self.selected_player_army.selected = False
                        self.selected_player_army = None

        else:
            if self.selected_player_army:
                self.show_shortest_path()


        if self.enemy_moving and not self.previewing_battle and not self.viewing_decision_window and self.player_army_retreat_animation==0:
            self.game_message = "AI moving..."
            self.game_message_color = WHITE
            self.game_message_size = 20
            if self.army_movement_grid_coords:
                if self.selected_enemy_army.action_points > 0:
                    if now - self.army_movement_last_animated >= self.animation_delay:
                        self.army_movement_last_animated = now
                        new_coords = self.army_movement_grid_coords.pop()
                        delta_x = new_coords[0] - self.selected_enemy_army.grid_position_x
                        delta_y = new_coords[1] - self.selected_enemy_army.grid_position_y
                        self.selected_enemy_army.grid_position_x += int(delta_x)
                        self.selected_enemy_army.grid_position_y += int(delta_y)
                        self.selected_enemy_army.position_x += int(delta_x * int(self.zoom_scale*32))
                        self.selected_enemy_army.position_y += int(delta_y * int(self.zoom_scale*32))

                        grid_cost = self.calculate_grid_movement_cost(self.selected_enemy_army)
                        self.selected_enemy_army.action_points -= grid_cost

                        if len(self.army_movement_grid_coords) == 0:
                            self.selected_enemy_army.action_points = 0
                        self.camera_zoom(self.selected_enemy_army)

                        self.check_if_left_city(self.selected_enemy_army)
                        if not self.check_for_player_city_collision():
                            if not self.check_for_enemy_embed_into_city():
                                if self.enemy_army_retreat_animation == 0:
                                    for army_sprite in self.army_sprites:
                                        if army_sprite.commander.country in self.hostile_relationships[self.selected_enemy_army.commander.country]:
                                            cond_1 = abs(self.selected_enemy_army.grid_position_x-army_sprite.grid_position_x) <= 1
                                            cond_2 = abs(self.selected_enemy_army.grid_position_y-army_sprite.grid_position_y) <= 1
                                            if cond_1 and cond_2:
                                                self.army_movement_grid_coords = []
                                                print("Resetting target variables before battle...")
                                                self.selected_enemy_army.attack_target = None
                                                self.selected_enemy_army.defend_target = None
                                                self.enemy_moving = False
                                                self.resume_enemy_moving = True
                                                if army_sprite.commander.country == self.player_country:
                                                    self.battle_preview(army_sprite, self.selected_enemy_army)
                                                else:
                                                    self.auto_resolve_battle(self.selected_enemy_army, army_sprite)
                                                break

                else:
                    self.army_movement_grid_coords = []

            else:
                enemy_make_move_results = self.enemy_make_move()
                if not enemy_make_move_results:
                    self.enemy_end_turn()
                    #self.enemy_moving = False


        if self.enemy_army_retreat_animation > 0:
            print(self.right_preview_army.commander.name, "retreating...")
            if now - self.army_movement_last_animated >= self.animation_delay:
                self.army_movement_last_animated = now
                if self.right_preview_army.grid_position_x - self.victorious_army.grid_position_x > 0:
                    delta_x = 1
                elif self.right_preview_army.grid_position_x - self.victorious_army.grid_position_x < 0:
                    delta_x = -1
                else:
                    delta_x = 0
                    
                if self.right_preview_army.grid_position_y - self.victorious_army.grid_position_y > 0:
                    delta_y = 1
                elif self.right_preview_army.grid_position_y - self.victorious_army.grid_position_y < 0:
                    delta_y = -1
                else:
                    delta_y = 0

                possible_retreat_directions = []
                if delta_x == 0:
                    possible_retreat_directions.append((1, 0))
                    possible_retreat_directions.append((-1, 0))
                else:
                    possible_retreat_directions.append((delta_x, 0))

                if delta_y == 0:
                    possible_retreat_directions.append((0, 1))
                    possible_retreat_directions.append((0, -1))
                else:
                    possible_retreat_directions.append((0, delta_y))

                retreat_direction = None
                for rd in possible_retreat_directions:
                    if self.is_cell_walkable(self.right_preview_army.grid_position_x+rd[0], self.right_preview_army.grid_position_y+rd[1]):
                        retreat_direction = rd
                        break

                if retreat_direction:
                    print("Retreating to...", self.right_preview_army.grid_position_x+retreat_direction[0], self.right_preview_army.grid_position_y + retreat_direction[1])
                    self.right_preview_army.grid_position_x += retreat_direction[0]
                    self.right_preview_army.grid_position_y += retreat_direction[1]
                    self.right_preview_army.position_x += retreat_direction[0]*int(self.zoom_scale*32)
                    self.right_preview_army.position_y += retreat_direction[1]*int(self.zoom_scale*32)

                    self.enemy_army_retreat_animation += 1
                    if self.enemy_army_retreat_animation >= 5:
                        self.enemy_army_retreat_animation = 0
                        if self.resume_enemy_moving:
                            self.enemy_moving = True
                            self.resume_enemy_moving = False

                else:
                    self.enemy_army_retreat_animation = 0
                    if self.resume_enemy_moving:
                        self.enemy_moving = True
                        self.resume_enemy_moving = False


        elif self.player_army_retreat_animation > 0:
            if now - self.army_movement_last_animated >= self.animation_delay:
                self.army_movement_last_animated = now
                if self.left_preview_army.grid_position_x - self.right_preview_army.grid_position_x > 0:
                    delta_x = 1
                elif self.left_preview_army.grid_position_x - self.right_preview_army.grid_position_x < 0:
                    delta_x = -1
                else:
                    delta_x = 0

                if self.left_preview_army.grid_position_y - self.right_preview_army.grid_position_y > 0:
                    delta_y = 1
                elif self.left_preview_army.grid_position_y - self.right_preview_army.grid_position_y < 0:
                    delta_y = -1
                else:
                    delta_y = 0

                possible_retreat_directions = []
                if delta_x == 0:
                    possible_retreat_directions.append((1, 0))
                    possible_retreat_directions.append((-1, 0))
                else:
                    possible_retreat_directions.append((delta_x, 0))

                if delta_y == 0:
                    possible_retreat_directions.append((0, 1))
                    possible_retreat_directions.append((0, -1))
                else:
                    possible_retreat_directions.append((0, delta_y))

                retreat_direction = None
                for rd in possible_retreat_directions:
                    if self.is_cell_walkable(self.left_preview_army.grid_position_x + rd[0],
                                             self.left_preview_army.grid_position_y + rd[1]):
                        retreat_direction = rd
                        break

                if retreat_direction:
                    self.left_preview_army.grid_position_x += retreat_direction[0]
                    self.left_preview_army.grid_position_y += retreat_direction[1]
                    self.left_preview_army.position_x += retreat_direction[0] * int(self.zoom_scale*32)
                    self.left_preview_army.position_y += retreat_direction[1] * int(self.zoom_scale*32)

                    self.player_army_retreat_animation += 1
                    if self.player_army_retreat_animation >= 5:
                        self.player_army_retreat_animation = 0
                        if self.resume_enemy_moving:
                            self.enemy_moving = True
                            self.resume_enemy_moving = False

                else:
                    self.player_army_retreat_animation = 0
                    if self.resume_enemy_moving:
                        self.enemy_moving = True
                        self.resume_enemy_moving = False

        self.all_sprites.update()


    def calculate_population(self, country):
        total_population = 0
        for city_sprite in self.city_sprites:
            if city_sprite.country == country:
                total_population += city_sprite.population
        if total_population > 0:
            return total_population
        else:
            return "N/A"


    def calculate_military_strength(self, country, return_description=False):
        military_strength = 1
        for city_sprite in self.city_sprites:
            if city_sprite.garrison and city_sprite.country == country:
                for unit_type, amount in city_sprite.garrison.items():
                    military_strength += UNIT_RECRUITMENT_COST[unit_type]*amount

        for army_sprite in self.army_sprites:
            if army_sprite.commander.country.lower() == country.lower():
                military_strength += 100 * army_sprite.commander.level
                if not army_sprite.embedded_in_city:
                    for unit_type, amount in army_sprite.army_composition.items():
                        military_strength += UNIT_RECRUITMENT_COST[unit_type] * amount

        if return_description:
            if military_strength >= 500000:
                return "Terrifying"
            elif military_strength >= 200000:
                return "Powerful"
            elif military_strength >= 100000:
                return "Strong"
            elif military_strength >= 50000:
                return "Moderate"
            elif military_strength >= 25000:
                return "Weak"
            else:
                return "Feeble"
        else:
            return military_strength


    def calculate_diplomacy_bonus(self, country):
        bonus = 0
        if country == "Liu Zhang":
            bonus += 60
        if "hospitality" in self.acquired_tech[country]:
            bonus += 25

        for army_sprite in self.army_sprites:
            if army_sprite.commander.country.lower() == country.lower():
                if "skilled_persuader" in army_sprite.commander.skills:
                    bonus += 20
                if "commissioner_of_internal_affairs" in army_sprite.commander.skills:
                    bonus += 10
                if "diplomat" in army_sprite.commander.skills:
                    bonus += 10
                if "straightforward" in army_sprite.commander.skills:
                    bonus -= 10
                if "commissioner_of_foreign_affairs" in army_sprite.commander.skills:
                    bonus += 50

        return bonus


    def calculate_projected_gold_delta(self, country):
        total = self.calculate_tax_and_additional_income(country) + self.calculate_upkeep_cost(country) + self.calculate_total_trade_revenue(country)
        return total


    def calculate_total_trade_revenue(self, country):
        trade_revenue = 100 #set base trade value
        trade_revenue_bonus = 1.0
        for trade_partner in self.trade_relationships[country]:
            if country == "Southern Barbarians":
                trade_revenue += 1000
            else:
                trade_revenue += self.calculate_trade_revenue(country, trade_partner)[0]

        for city_sprite in self.city_sprites:
            if city_sprite.country == country:
                try:
                    trade_port = [building for building in city_sprite.buildings if building.name == "Trade Port"][0]
                    trade_revenue_bonus += trade_port.level*3/100
                except:
                    pass

        if country == "Wu":
            trade_revenue_bonus += .2
        if "official_currency" in self.acquired_tech[country]:
            trade_revenue_bonus += .1
        if self.loaded_game_data["dilemma_effects"]["flourishing_economy"] > 0:
            trade_revenue_bonus += .1

        trade_revenue = int(trade_revenue * trade_revenue_bonus)
        return trade_revenue


    def calculate_trade_revenue(self, country, trade_partner):
        if trade_partner == "Southern Barbarians":
            return 1000, 1000
        country_economic_growth = self.calculate_tax_and_additional_income(country)
        partner_economic_growth = self.calculate_tax_and_additional_income(trade_partner)
        total = country_economic_growth + partner_economic_growth + 1
        return int(.1*country_economic_growth**2/total), int(.1*partner_economic_growth**2/total)


    def calculate_tax_and_additional_income(self, country, return_description=False):
        total = 0
        tax_bonus = 1.0
        for city_sprite in self.city_sprites:
            if city_sprite.country == country:
                total += city_sprite.estimate_projected_income()

        #tax logic already taken care of in city_sprite.estimate_projected_income
        #for army_sprite in self.army_sprites:
        #    if army_sprite.commander.country == country:
        #        if "commissioner_of_internal_affairs" in army_sprite.commander.skills:
        #            tax_bonus += .03

        #if "tax_reform" in self.acquired_tech[country]:
        #    tax_bonus += .03
        #total = int(total * tax_bonus)

        if return_description:
            if total >= 30000:
                return "Prosperous"
            elif total >= 20000:
                return "Affluent"
            elif total >= 10000:
                return "Wealthy"
            elif total >= 5000:
                return "Moderate"
            elif total >= 2000:
                return "Poor"
            else:
                return "Destitute"
        else:
            return total


    def calculate_upkeep_cost(self, country):
        total = 0
        upkeep_reduction = 0
        for city_sprite in self.city_sprites:
            if city_sprite.garrison and city_sprite.country == country and not city_sprite.defender:
                for unit_type, amount in city_sprite.garrison.items():
                    total -= int(UNIT_RECRUITMENT_COST[unit_type] / 10 * amount)

        for army_sprite in self.army_sprites:
            if army_sprite.commander.country.lower() == country.lower():
                total -= 100*army_sprite.commander.level
                local_upkeep_rate = .1
                for unit_type, amount in army_sprite.army_composition.items():
                    if "logistician" in army_sprite.commander.skills:
                        local_upkeep_rate *= .5
                    if "hidden_ambition" in army_sprite.commander.skills:
                        local_upkeep_rate *= 1.2
                    if "chief_advisor" in army_sprite.commander.skills:
                        local_upkeep_rate *= .8

                    total -= int(UNIT_RECRUITMENT_COST[unit_type] * amount * local_upkeep_rate)

                if "commissioner_of_supply" in army_sprite.commander.skills:
                    upkeep_reduction += .03

        if "improved_warehousing" in self.acquired_tech[country]:
            upkeep_reduction += .3

        return int(total*(1-upkeep_reduction))


    def calculate_projected_food_delta(self, country):
        ai_bonus = 1.0
        if country != self.player_country:
            ai_bonus = 1.1
            ai_bonus += self.difficulty/10  # give AI food bonus, up to 50%
        total = int(self.calculate_food_production(country)*ai_bonus) + self.calculate_food_consumption(country)
        return total


    def calculate_food_production(self, country):
        total = 0
        bonus = 1.0
        for city_sprite in self.city_sprites:
            if city_sprite.country == country and "natural_disaster_drought" not in city_sprite.effects:
                for building in city_sprite.buildings:
                    if building.name == "Farm":
                        total += int(5 * building.level ** 1.25)

        for army_sprite in self.army_sprites:
            if army_sprite.commander.country.lower() == country.lower():
                if "tuntian" in army_sprite.commander.skills:
                    bonus += army_sprite.commander.level/100

        if "tuntian" in self.acquired_tech[country]:
            bonus += .05
        if "wooden_ox_flowing_horse" in self.acquired_tech[country]:
            bonus += .1
        if country == "Zhang Lu":
            bonus += .33

        total = int(total*bonus)
        return total


    def calculate_food_consumption(self, country):
        total = 0
        for army_sprite in self.army_sprites:
            if army_sprite.commander.country == country and not army_sprite.embedded_in_city:
                total -= sum(army_sprite.army_composition.values())//10
        for city_sprite in self.city_sprites:
            if city_sprite.country == country and city_sprite.garrison:
                total -= sum(city_sprite.garrison.values()) // 10
        return total


    def calculate_distance_to_closest_player_object(self, target_obj):
        min_distance = 99999999
        for army_sprite in self.army_sprites:
            if army_sprite.commander.country == self.player_country:
                distance = calculate_manhattan_distance(army_sprite, target_obj)
                if distance < min_distance:
                    min_distance = distance
        for city_sprite in self.city_sprites:
            if city_sprite.country == self.player_country:
                distance = calculate_manhattan_distance(army_sprite, target_obj)
                if distance < min_distance:
                    min_distance = distance
        return min_distance


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)

            if not self.viewing_quick_guide:
                self.draw_text("{}".format(self.game_message), FONT_FUNCTION(DEFAULT_FONT, self.game_message_size),
                               self.game_message_color, self.game_width // 2, 75, align="center")

                for city_sprite in self.city_sprites:
                    on_screen_cond_1 = abs(self.camera_current_x - city_sprite.grid_position_x*self.zoom_scale*32) <= LANDSCAPE_WIDTH
                    on_screen_cond_2 = abs(self.camera_current_y - city_sprite.grid_position_y*self.zoom_scale*32) <= LANDSCAPE_HEIGHT
                    if on_screen_cond_1 and on_screen_cond_2:
                        self.draw_text(city_sprite.name, FONT_FUNCTION(DEFAULT_FONT, 24),
                                       YELLOW, city_sprite.rect.centerx, city_sprite.rect.bottom, align="center")

                for army_sprite in self.army_sprites:
                    on_screen_cond_1 = abs(self.camera_current_x - army_sprite.grid_position_x*self.zoom_scale*32) <= LANDSCAPE_WIDTH
                    on_screen_cond_2 = abs(self.camera_current_y - army_sprite.grid_position_y*self.zoom_scale*32) <= LANDSCAPE_HEIGHT
                    if on_screen_cond_1 and on_screen_cond_2:
                        self.draw_text(army_sprite.commander.name, FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                                       WHITE, army_sprite.rect.centerx, army_sprite.rect.bottom, align="center")

                        if army_sprite.army_composition:
                            army_size = sum(army_sprite.army_composition.values())
                        else:
                            army_size = 0
                        #max_army_size = army_sprite.commander.max_army_size

                        self.draw_text("({})".format(army_size), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                                       WHITE, army_sprite.rect.centerx, army_sprite.rect.bottom+16, align="center")


            #Toolbar labels
            estimated_gold_delta = self.calculate_projected_gold_delta(self.player_country)
            estimated_food_delta = self.calculate_projected_food_delta(self.player_country)
            gold_label = "{} ({})".format(self.gold[self.player_country], estimated_gold_delta)
            food_label = "{} ({})".format(self.food[self.player_country], estimated_food_delta)
            gold_label_color = WHITE
            food_label_color = WHITE
            if estimated_gold_delta < 0:
                gold_label_color = DARK_RED
            if estimated_food_delta < 0:
                food_label_color = DARK_RED

            if self.toolbar_visible:
                self.draw_text(gold_label, FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE-len(gold_label)//6), gold_label_color,
                               55, 28, align="midleft")
                self.draw_text(food_label, FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE-len(food_label)//6), food_label_color,
                               255, 27, align="midleft")


            # ARMY INFO
            self.info_sprites.draw(self.screen)
            if self.viewing_army_info:
                info_army = None
                for army_sprite in self.army_sprites:
                    if self.viewing_army_info == army_sprite.commander.name:
                        info_army = army_sprite
                        break

                if info_army:
                    self.draw_text(info_army.commander.name, FONT_FUNCTION(DEFAULT_FONT, 22), WHITE,
                                   LANDSCAPE_WIDTH - 210, 55, align="midleft")

                    if info_army.commander.simple_name != "none":
                        self.draw_text("Faction: {}".format(info_army.commander.country),
                                       FONT_FUNCTION(DEFAULT_FONT, 13), WHITE, LANDSCAPE_WIDTH - 210, 75,
                                       align="midleft")
                        self.draw_text("Combat Prowess: {}".format(info_army.commander.combat_prowess),
                                       FONT_FUNCTION(DEFAULT_FONT, 13), WHITE, LANDSCAPE_WIDTH - 210, 90,
                                       align="midleft")
                        self.draw_text("Intellect: {}".format(info_army.commander.intellect),
                                       FONT_FUNCTION(DEFAULT_FONT, 13), WHITE, LANDSCAPE_WIDTH - 210, 105,
                                       align="midleft")
                        self.draw_text("Command: {}".format(info_army.commander.command),
                                       FONT_FUNCTION(DEFAULT_FONT, 13), WHITE, LANDSCAPE_WIDTH - 210, 120,
                                       align="midleft")
                        self.draw_text("Charisma: {}".format(info_army.commander.charisma),
                                       FONT_FUNCTION(DEFAULT_FONT, 13), WHITE, LANDSCAPE_WIDTH - 210, 135,
                                       align="midleft")
                        self.draw_text("Action Pts: {}/{}".format(info_army.action_points, info_army.max_action_points),
                                       FONT_FUNCTION(DEFAULT_FONT, 13), WHITE, LANDSCAPE_WIDTH - 210, 150,
                                       align="midleft")

                    self.draw_text("Special Attacks:", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                   LANDSCAPE_WIDTH - 310,
                                   185, align="midleft")
                    self.draw_text("Skills:", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, LANDSCAPE_WIDTH - 310,
                                   285, align="midleft")
                    self.draw_text("Army:", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, LANDSCAPE_WIDTH - 310,
                                   385, align="midleft")

                    if info_army.commander.name in self.unit_size_espionage_variables:
                        unit_size_esp_var = self.unit_size_espionage_variables[info_army.commander.name]
                    else:
                        unit_size_esp_var = self.unit_size_espionage_variables["other"]

                    for i in range(len(self.info_army_unit_types)):
                        unit_type = self.info_army_unit_types[i]
                        text_col = i % 4
                        text_row = i // 4
                        text_x, text_y = text_col * 65 + LANDSCAPE_WIDTH - 310 + 65 - 10, text_row * 62 + 62 - 15 + 400
                        if unit_size_esp_var[i] or info_army.commander.country == self.player_country:
                            unit_size_label = "{}".format(info_army.army_composition[unit_type])
                        else:
                            unit_size_label = "?"
                        self.draw_text(unit_size_label, FONT_FUNCTION(DEFAULT_FONT, 14), WHITE, text_x, text_y)


            # CITY INFO
            if self.viewing_city_info:
                info_city = None
                for city_sprite in self.city_sprites:
                    if self.viewing_city_info == city_sprite.name:
                        info_city = city_sprite
                        break

                if info_city:
                    self.draw_text(info_city.name, FONT_FUNCTION(DEFAULT_FONT, 24), WHITE,
                                   LANDSCAPE_WIDTH - 225, 60, align="midleft")

                    self.draw_text("Population: {}".format(info_city.population),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, LANDSCAPE_WIDTH - 225, 90,
                                   align="midleft")
                    if info_city.happiness <= 0:
                        self.draw_text("Happiness: {}".format(info_city.happiness),
                                       FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), RED, LANDSCAPE_WIDTH - 225, 110,
                                       align="midleft")
                    else:
                        self.draw_text("Happiness: +{}".format(info_city.happiness),
                                       FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, LANDSCAPE_WIDTH - 225, 110,
                                       align="midleft")

                    self.draw_text("Faction: {}".format(info_city.country),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, LANDSCAPE_WIDTH - 225, 130,
                                   align="midleft")

                    self.draw_text("Garrisoned Troops:", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                   LANDSCAPE_WIDTH - 325, 180, align="midleft")

                    if info_city.name in self.unit_size_espionage_variables:
                        unit_size_esp_var = self.unit_size_espionage_variables[info_city.name]
                    else:
                        unit_size_esp_var = self.unit_size_espionage_variables["other"]

                    for i in range(len(self.info_army_unit_types)):
                        unit_type = self.info_army_unit_types[i]
                        text_col = i % 4
                        text_row = i // 4
                        text_x, text_y = text_col * 65 + LANDSCAPE_WIDTH - 300 + 65 - 10, text_row * 62 + 62 - 15 + 200
                        if unit_size_esp_var[i] or info_city.country == self.player_country:
                            unit_size_label = "{}".format(info_city.garrison[unit_type])
                        else:
                            unit_size_label = "?"
                        self.draw_text(unit_size_label, FONT_FUNCTION(DEFAULT_FONT, 14), WHITE, text_x, text_y)

                    self.draw_text("Fortification:", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                   LANDSCAPE_WIDTH - 325, 380, align="midleft")
                    walls = [building for building in info_city.buildings if building.name == "Walls"][0]
                    self.draw_text("Level {}".format(walls.level), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                   LANDSCAPE_WIDTH - 325 + 50, 450, align="center")


            # ARMY LIST INFO
            if self.viewing_list == "generals":
                counter = 0
                start_x, start_y = self.toolbar_general_icon.rect.left, self.toolbar_bg.rect.bottom
                army_sprites_to_display = []

                for army_sprite in self.army_sprites:
                    if army_sprite.commander.country == self.player_country:
                        army_sprites_to_display.append(army_sprite)
                        counter += 1

                #army_sprites_to_display_sorted = sorted(army_sprites_to_display, key=lambda x: x.commander.level, reverse=True)
                army_sprites_to_display_sorted = sorted(army_sprites_to_display, key=lambda x: x.commander.simple_name,
                                                        reverse=False)
                counter = 0
                for army_sprite in army_sprites_to_display_sorted:
                    if counter in range(self.viewing_list_offset, self.viewing_list_offset + 10):
                        army_size = sum(army_sprite.army_composition.values())
                        self.draw_text(army_sprite.commander.name, FONT_FUNCTION(DEFAULT_FONT, 12), YELLOW,
                                       start_x+40, start_y+50*(counter-self.viewing_list_offset)+5, align="topleft")
                        self.draw_text("({})".format(army_size), FONT_FUNCTION(DEFAULT_FONT, 12), WHITE,
                                       start_x+40, start_y+50*(counter-self.viewing_list_offset)+25, align="topleft")
                    counter += 1

                if len(army_sprites_to_display) - 10 > self.viewing_list_offset:
                    self.list_arrow_down.rect.center = start_x+75, start_y+50*9+25
                else:
                    self.list_arrow_down.rect.center = -30000, -30000
                if self.viewing_list_offset > 0:
                    self.list_arrow_up.rect.center = start_x + 75, start_y + 25
                else:
                    self.list_arrow_up.rect.center = -30000, -30000


            # CITY LIST INFO
            if self.viewing_list == "cities":
                counter = 0
                start_x, start_y = self.toolbar_cities_icon.rect.left, self.toolbar_bg.rect.bottom
                city_sprites_to_display = []

                for city_sprite in self.city_sprites:
                    if city_sprite.country == self.player_country:
                        city_sprites_to_display.append(city_sprite)
                        counter += 1

                city_sprites_to_display_sorted = sorted(city_sprites_to_display, key=lambda x: x.population,
                                                        reverse=True)
                counter = 0
                for city_sprite in city_sprites_to_display_sorted:
                    if counter in range(self.viewing_list_offset, self.viewing_list_offset + 10):
                        self.draw_text(city_sprite.name, FONT_FUNCTION(DEFAULT_FONT, 12), YELLOW,
                                       start_x + 50, start_y+50*(counter-self.viewing_list_offset)+25, align="midtop")
                        self.draw_text("Population: {}".format(city_sprite.population),
                                       FONT_FUNCTION(DEFAULT_FONT, 12), WHITE,
                                       start_x + 100, start_y+50*(counter-self.viewing_list_offset)+5, align="topleft")
                        if city_sprite.happiness <= 0:
                            population_color = RED
                        else:
                            population_color = WHITE
                        self.draw_text("Happiness: {}".format(city_sprite.happiness),
                                       FONT_FUNCTION(DEFAULT_FONT, 12), population_color,
                                       start_x + 100, start_y + 50 * (counter-self.viewing_list_offset) + 25, align="topleft")
                    counter += 1

                if len(city_sprites_to_display) - 10 > self.viewing_list_offset:
                    self.list_arrow_down.rect.center = start_x+75, start_y+50*9+25
                else:
                    self.list_arrow_down.rect.center = -30000, -30000
                if self.viewing_list_offset > 0:
                    self.list_arrow_up.rect.center = start_x + 75, start_y + 25
                else:
                    self.list_arrow_up.rect.center = -30000, -30000


            # ALERT LIST INFO
            if self.viewing_list == "alerts":
                start_x, start_y = self.toolbar_alerts_icon.rect.left - 200, self.toolbar_bg.rect.bottom + 2
                counter = 0

                for dilemma in self.current_turn_dilemmas:
                    dilemma_attrs = self.current_turn_dilemmas[dilemma]
                    if counter in range(self.viewing_list_offset, self.viewing_list_offset + 10):
                        if dilemma_attrs["target"]:
                            try:
                                short_summary = dilemma_attrs["short_summary"].format(dilemma_attrs["target"].name).replace("\n", " ")
                            except:
                                short_summary = dilemma_attrs["short_summary"].replace("\n", " ")
                        else:
                            short_summary = dilemma_attrs["short_summary"].replace("\n", " ")
                        #self.drawWrappedText(short_summary, WHITE,
                        #                     pg.Rect(start_x + 55,
                        #                             start_y + 50 * (counter-self.viewing_list_offset),
                        #                             335, 48), FONT_FUNCTION(DEFAULT_FONT, 12),
                        #                     lineSpacing=1)
                        self.drawWrappedTextByCharLength(short_summary, FONT_FUNCTION(DEFAULT_FONT, 13), WHITE,
                                                         start_x+55, start_y+50*(counter-self.viewing_list_offset),
                                                         60, align="topleft", lineSpacing=15)
                    counter += 1

                if len(self.current_turn_dilemmas) - 10 > self.viewing_list_offset:
                    self.list_arrow_down.rect.center = start_x + 75, start_y + 50 * 9 + 25
                else:
                    self.list_arrow_down.rect.center = -30000, -30000
                if self.viewing_list_offset > 0:
                    self.list_arrow_up.rect.center = start_x + 75, start_y + 25
                else:
                    self.list_arrow_up.rect.center = -30000, -30000



            self.priority_sprites.draw(self.screen)

            if self.game_tooltip_description:
                if "Income" in self.game_tooltip_description:
                    projected_tax_and_additional_income = self.calculate_tax_and_additional_income(self.player_country)
                    projected_trade_revenue = self.calculate_total_trade_revenue(self.player_country)
                    projected_upkeep_cost = self.calculate_upkeep_cost(self.player_country)
                    projected_total_income = self.calculate_projected_gold_delta(self.player_country)
                    tooltip_description_split = self.game_tooltip_description.split("\n")

                    self.draw_text(tooltip_description_split[0].format(projected_tax_and_additional_income),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                   self.gold_icon.tooltip_background.rect.left+20,
                                   self.gold_icon.tooltip_background.rect.top+30, align="midleft")

                    self.draw_text(tooltip_description_split[1].format(projected_trade_revenue),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                   self.gold_icon.tooltip_background.rect.left+20,
                                   self.gold_icon.tooltip_background.rect.top+60, align="midleft")

                    self.draw_text(tooltip_description_split[2].format(projected_upkeep_cost),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), RED,
                                   self.gold_icon.tooltip_background.rect.left+20,
                                   self.gold_icon.tooltip_background.rect.top+90, align="midleft")

                    if projected_total_income >= 0:
                        self.draw_text(tooltip_description_split[3].format(projected_total_income),
                                       FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                       self.gold_icon.tooltip_background.rect.left+20,
                                       self.gold_icon.tooltip_background.rect.top+120, align="midleft")
                    else:
                        self.draw_text(tooltip_description_split[3].format(projected_total_income),
                                       FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), RED,
                                       self.gold_icon.tooltip_background.rect.left+20,
                                       self.gold_icon.tooltip_background.rect.top+120, align="midleft")

                else:
                    projected_food_production = self.calculate_food_production(self.player_country)
                    projected_food_consumption = self.calculate_food_consumption(self.player_country)
                    projected_food_surplus = self.calculate_projected_food_delta(self.player_country)
                    tooltip_description_split = self.game_tooltip_description.split("\n")

                    self.draw_text(tooltip_description_split[0].format(projected_food_production),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                   self.food_icon.tooltip_background.rect.left + 20,
                                   self.food_icon.tooltip_background.rect.top + 30, align="midleft")

                    self.draw_text(tooltip_description_split[1].format(projected_food_consumption),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), RED,
                                   self.food_icon.tooltip_background.rect.left + 20,
                                   self.food_icon.tooltip_background.rect.top + 60, align="midleft")

                    if projected_food_surplus >= 0:
                        self.draw_text(tooltip_description_split[2].format(projected_food_surplus),
                                       FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                       self.food_icon.tooltip_background.rect.left + 20,
                                       self.food_icon.tooltip_background.rect.top + 90, align="midleft")
                    else:
                        self.draw_text(tooltip_description_split[2].format(projected_food_surplus),
                                       FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), RED,
                                       self.food_icon.tooltip_background.rect.left + 20,
                                       self.food_icon.tooltip_background.rect.top + 90, align="midleft")

            self.alert_sprites.draw(self.screen)
            self.battle_preview_sprites.draw(self.screen)
            self.decision_window_sprites.draw(self.screen)
            self.transition_sprites.draw(self.screen)


            if self.previewing_battle or self.viewing_battle_summary or self.viewing_army_merge_interface:
                #player side
                self.draw_text(self.left_preview_army.commander.name, FONT_FUNCTION(DEFAULT_FONT, 24), WHITE,
                               250, 60, align="midleft")
                if self.left_preview_army.commander.simple_name != "none":
                    self.draw_text("Combat Prowess: {}".format(self.left_preview_army.commander.combat_prowess),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 250, 80, align="midleft")
                    self.draw_text("Intellect: {}".format(self.left_preview_army.commander.intellect),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 250, 100, align="midleft")
                    self.draw_text("Command: {}".format(self.left_preview_army.commander.command),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 250, 120, align="midleft")

                self.draw_text("Special Attacks:", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 150,
                               180, align="midleft")
                self.draw_text("Skills:", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 150,
                               280, align="midleft")
                self.draw_text("Army:", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 150,
                               380, align="midleft")

                for i in range(len(self.left_army_unit_types)):
                    unit_type = self.left_army_unit_types[i]
                    text_col = i % 4
                    text_row = i // 4
                    text_x, text_y = text_col * 65 + 150 + 65 - 10, text_row * 62 + 62 - 15 + 400
                    self.draw_text("{}".format(self.left_preview_army.army_composition[unit_type]), FONT_FUNCTION(DEFAULT_FONT, 14),
                                   WHITE, text_x, text_y)
                #enemy side
                self.draw_text(self.right_preview_army.commander.name, FONT_FUNCTION(DEFAULT_FONT, 24), WHITE,
                               LANDSCAPE_WIDTH // 2 + 275, 60, align="midleft")

                if self.right_preview_army.commander.simple_name != "none":
                    self.draw_text("Combat Prowess: {}".format(self.right_preview_army.commander.combat_prowess),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, LANDSCAPE_WIDTH // 2 + 275, 80, align="midleft")
                    self.draw_text("Intellect: {}".format(self.right_preview_army.commander.intellect),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, LANDSCAPE_WIDTH // 2 + 275, 100, align="midleft")
                    self.draw_text("Command: {}".format(self.right_preview_army.commander.command),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, LANDSCAPE_WIDTH // 2 + 275, 120, align="midleft")

                self.draw_text("Special Attacks:", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, LANDSCAPE_WIDTH//2+180,
                               180, align="midleft")
                self.draw_text("Skills:", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, LANDSCAPE_WIDTH//2+180,
                               280, align="midleft")
                self.draw_text("Army:", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, LANDSCAPE_WIDTH//2+180,
                               380, align="midleft")


                for i in range(len(self.right_army_unit_types)):
                    unit_type = self.right_army_unit_types[i]
                    text_col = i % 4
                    text_row = i // 4
                    text_x, text_y = text_col * 65 + 180 + LANDSCAPE_WIDTH//2 + 65 - 10, text_row * 62 + 62 - 15 + 400
                    self.draw_text("{}".format(self.right_preview_army.army_composition[unit_type]), FONT_FUNCTION(DEFAULT_FONT, 14),
                                   WHITE, text_x, text_y)

                if self.viewing_army_merge_interface:
                    self.draw_text("Select a unit and press the switch button to move unit from one commander to the other.", FONT_FUNCTION(DEFAULT_FONT, 20),
                                   YELLOW, self.game_width // 2, self.game_height//2 + 220)
                if self.viewing_battle_summary:
                    if self.battle_won:
                        self.draw_text("Victory!", FONT_FUNCTION(DEFAULT_FONT, 40), YELLOW, self.game_width // 2, self.game_height*3//5)
                    else:
                        self.draw_text("Defeat!", FONT_FUNCTION(DEFAULT_FONT, 40), RED, self.game_width // 2, self.game_height*3//5)


            #DECISIONS CAPTION
            if self.current_decision:
                if self.current_decision not in ["view_military_report"]: #exclude 'decisions' that have custom msg displayed via persistent game message
                    text_size = 600//len(self.current_decision_text)+15
                    line_count = 0
                    for line in self.current_decision_text.split("\n"):
                        self.draw_text(line, FONT_FUNCTION(DEFAULT_FONT, text_size), BLACK, self.game_width // 2,
                                       self.game_height*3//5 + line_count*text_size)
                        line_count += 1


            #SP ATTACK TOOLTIPS
            self.tooltip_sprites.draw(self.screen)
            if self.special_attack_tooltip_description:
                #self.drawWrappedText(self.special_attack_tooltip_description, WHITE,
                #                     pg.Rect(self.special_attack_tooltip_x + 16, self.special_attack_tooltip_y + 20,
                #                             268, 360), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE))
                self.draw_text(self.special_attack_tooltip_description, FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                               WHITE, self.special_attack_tooltip_x + 16, self.special_attack_tooltip_y + 20,
                               align="topleft", max_width=268)
            #FPS
            #self.draw_text("{}".format(int(self.clock.get_fps())), FONT_FUNCTION(DEFAULT_FONT, 24),
            #               self.game_message_color, 30, 30, align="topleft")

            #Persistent Game Message
            p_msg_line_counter = 0
            for p_msg in self.persistent_game_message.split("\n"):
                self.draw_text(p_msg, FONT_FUNCTION(DEFAULT_FONT, 22), BLACK, LANDSCAPE_WIDTH//2,
                               120 + p_msg_line_counter*24, align="midtop")
                p_msg_line_counter += 1


            pg.display.flip()

    def draw_text(self, text, font, color, x, y, align="midtop", max_width=None):
        if max_width:
            text_surface = font.render(text, True, color, None, max_width)
        else:
            text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        if align == "midleft":
            text_rect.midleft = (x, y)
        elif align == "topleft":
            text_rect.topleft = (x, y)
        else:
            text_rect.midtop = (x, y)
        self.screen.blit(text_surface, text_rect)

    def drawWrappedTextByCharLength(self, text, font, color, x, y, max_width, align, lineSpacing):
        lines = []
        line_text = ""
        character_counter = 0
        text_split = text.split(" ")
        for i in range(len(text_split)):
            word = text_split[i]
            character_counter += len(word)+1
            if character_counter > max_width:
                lines.append(line_text)
                line_text = word + " "
                character_counter = 0
            else:
                line_text += word + " "
            if i == len(text_split) - 1:
                lines.append(line_text)

        for i in range(len(lines)):
            line = lines[i]
            self.draw_text(line, font, color, x, y+i*lineSpacing, align=align)


    def drawWrappedText(self, text, color, rect, font, aa=False, bkg=None, lineSpacing=10):
        rect = pg.Rect(rect)
        y = rect.top

        # get the height of the font
        fontHeight = font.size("Tg")[1]

        while text:
            i = 1

            # determine if the row of text will be outside our area
            if y + fontHeight > rect.bottom:
                break

            # determine maximum width of line
            while font.size(text[:i])[0] < rect.width and i < len(text):
                i += 1

            # if we've wrapped the text, then adjust the wrap to the last word
            if i < len(text):
                i = text.rfind(" ", 0, i) + 1

            # render the line and blit it to the surface
            if bkg:
                image = font.render(text[:i], 1, color, bkg)
                image.set_colorkey(bkg)
            else:
                image = font.render(text[:i], aa, color)

            self.screen.blit(image, (rect.left, y))
            y += fontHeight + lineSpacing

            # remove the text we just blitted
            text = text[i:]

        return text