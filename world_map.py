from game_classes import*
from sprites import*
from functools import partial
import pickle

class World_Map_Scene:
    def __init__(self, campaign_map_scene):
        self.campaign_map_scene = campaign_map_scene
        self.game_width, self.game_height = LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT
        if DEMO:
            self.screen = pg.display.set_mode((self.game_width, self.game_height))
        else:
            self.screen = pg.display.set_mode((self.game_width, self.game_height), pg.FULLSCREEN|pg.DOUBLEBUF)
        os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (50, 50)
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()

        pg.mixer.init()
        pg.font.init()
        self.hover_sound = pg.mixer.Sound("sfx_hover.ogg")
        self.click_sound = pg.mixer.Sound("sfx_click.ogg")
        self.morale_boost_sound = pg.mixer.Sound("sfx_restore_status.ogg")


    def new(self):
        self.running = True
        self.playing = True
        pg.mouse.set_visible(True)

        self.all_sprites = pg.sprite.Group()
        self.fixed_sprites = pg.sprite.Group()
        self.city_sprites = pg.sprite.Group()
        self.army_sprites = pg.sprite.Group()
        self.priority_sprites = pg.sprite.Group()

        self.zoom_scale = .25
        self.map_width, self.map_height = self.campaign_map_scene.map_width*self.zoom_scale, self.campaign_map_scene.map_height*self.zoom_scale
        self.map_grid_width = self.map_width // int(self.zoom_scale * 32)
        self.map_grid_height = self.map_height // int(self.zoom_scale * 32)

        self.camera_current_x, self.camera_current_y = self.campaign_map_scene.loaded_game_data["camera_position"]  # starting pt on entire map
        self.camera_current_x = int(self.camera_current_x*self.zoom_scale)
        self.camera_current_y = int(self.camera_current_y*self.zoom_scale)

        self.x_offset, self.y_offset = 0, 0
        #if self.camera_current_x <= LANDSCAPE_WIDTH:
        #    self.x_offset = 0
        #elif self.camera_current_x >= self.map_width - LANDSCAPE_WIDTH:
        #    self.x_offset = self.map_width - LANDSCAPE_WIDTH
        #else:
        #    self.x_offset = self.camera_current_x - LANDSCAPE_WIDTH

        #if self.camera_current_y <= LANDSCAPE_HEIGHT:
        #    self.y_offset = 0
        #elif self.camera_current_y >= self.map_height - LANDSCAPE_HEIGHT:
        #    self.y_offset = self.map_height - LANDSCAPE_HEIGHT
        #else:
        #    self.y_offset = self.camera_current_y - LANDSCAPE_HEIGHT

        if self.camera_current_x <= 0:
            self.camera_current_x = 0
        elif self.camera_current_x >= self.map_width:
            self.camera_current_x = self.map_width
        if self.camera_current_y <= 0:
            self.camera_current_y = 0
        elif self.camera_current_y >= self.map_height:
            self.camera_current_y = self.map_height

        #print("camera:", self.camera_current_x, self.camera_current_y)
        #print("offsets:", self.x_offset, self.y_offset)
        self.map = CampaignMap(self.campaign_map_scene.main_game, -(self.camera_current_x+self.x_offset-LANDSCAPE_WIDTH//2*self.zoom_scale-LANDSCAPE_WIDTH//2),
                               -(self.camera_current_y+self.y_offset-LANDSCAPE_HEIGHT//2*self.zoom_scale-LANDSCAPE_HEIGHT//2),
                               anchor="topleft", world_map=True, source_image=self.campaign_map_scene.map.image)
        #self.map = CampaignMap(self.campaign_map_scene.main_game, int(-self.x_offset),
        #                       int(-self.y_offset), anchor="topleft", scale_percentage=self.zoom_scale)
        self.all_sprites.add(self.map)
        self.campaign_map_scene.map.kill()


        for city in self.campaign_map_scene.loaded_game_data["city"]:
            city_sprite = self.city_obj_conversion(city, "to_sprite")
            city_sprite.position_x -= self.camera_current_x+self.x_offset-LANDSCAPE_WIDTH//2*self.zoom_scale-LANDSCAPE_WIDTH//2
            city_sprite.position_y -= self.camera_current_y+self.y_offset-LANDSCAPE_HEIGHT//2*self.zoom_scale-LANDSCAPE_HEIGHT//2
            self.all_sprites.add(city_sprite)
            self.fixed_sprites.add(city_sprite)
            self.city_sprites.add(city_sprite)


        for army in self.campaign_map_scene.loaded_game_data["army"]:
            self.add_army_to_map(army)


        self.map_frame = Static(0,0,"sprite_world_map_frame.png", anchor="topleft")
        self.all_sprites.add(self.map_frame)
        self.priority_sprites.add(self.map_frame)

        self.map_focus_frame = Static(LANDSCAPE_WIDTH//2, LANDSCAPE_HEIGHT//2,
                                      "sprite_world_map_focus_frame_1.png",
                                      [pg.image.load("sprite_world_map_focus_frame_{}.png".format(i)).convert_alpha() for i in[2, 3, 4, 3, 2, 1]],
                                      frame_delta=165, anchor="topleft")
        self.all_sprites.add(self.map_focus_frame)
        self.priority_sprites.add(self.map_focus_frame)

        self.close_map_button = Button(LANDSCAPE_WIDTH-30, 30, "sprite_close_button_large.png",
                                       command=self.return_to_campaign_map)
        self.all_sprites.add(self.close_map_button)
        self.priority_sprites.add(self.close_map_button)

        # transitions
        self.transition_sprites = pg.sprite.Group()
        self.transition = None
        self.post_transition_function = None
        self.initiate_transition("in")

        # compensate for full screen
        self.extended_borders = []
        if not DEMO:
            info = pg.display.Info()
            max_x, max_y = info.current_w, info.current_h
            self.extended_borders = [pg.Rect((0, LANDSCAPE_HEIGHT), (max_x, max_y-LANDSCAPE_HEIGHT)),
                     pg.Rect((LANDSCAPE_WIDTH, 0), (max_x - LANDSCAPE_WIDTH, max_y))]

        self.run()


    def initiate_transition(self, direction):
        if direction == "in":
            self.transition = FadeBlackTransition(80, "in")
            self.transition_sprites.add(self.transition)
        else:
            self.transition = FadeBlackTransition(80, "out")
            self.transition_sprites.add(self.transition)


    def transition_clean_up(self):
        self.transition.kill()
        self.transition = None
        self.post_transition_function = None
        self.playing = False
        self.running = False


    def army_obj_conversion(self, from_obj, conversion="from_sprite"):
        if conversion == "from_sprite":
            return Army(from_obj.grid_position_x, from_obj.grid_position_y,
                         from_obj.commander, from_obj.army_composition, from_obj.action_points,
                         from_obj.retreat_penalty, from_obj.embedded_in_city, from_obj.anchor)
        else:
            return CampaignMapArmySprite(self, from_obj.grid_position_x, from_obj.grid_position_y,
                                         from_obj.commander, from_obj.army_composition, from_obj.action_points,
                                         from_obj.retreat_penalty, from_obj.embedded_in_city, anchor="midleft",
                                         scale_percentage=self.zoom_scale, display_stars=False)


    def city_obj_conversion(self, from_obj, conversion="from_sprite"):
        if conversion == "from_sprite":
            return City(grid_position_x=from_obj.grid_position_x, grid_position_y=from_obj.grid_position_y,
                             name=from_obj.name, fortress_level=from_obj.fortress_level,
                             country=from_obj.country, population=from_obj.population,
                             population_growth=from_obj.population_growth, happiness=from_obj.happiness,
                             defender=from_obj.defender, anchor=from_obj.anchor, raw_buildings=from_obj.raw_buildings,
                             buildings=from_obj.buildings, garrison=from_obj.garrison, units_recruited_this_turn=from_obj.units_recruited_this_turn)
        else:
            return CampaignMapCitySprite(self, grid_position_x=from_obj.grid_position_x, grid_position_y=from_obj.grid_position_y,
                                                name=from_obj.name, fortress_level=from_obj.fortress_level,
                                                country=from_obj.country, population=from_obj.population, population_growth=from_obj.population_growth,
                                                happiness=from_obj.happiness, defender=from_obj.defender, anchor=from_obj.anchor,
                                                raw_buildings=from_obj.raw_buildings, buildings=from_obj.buildings,
                                                garrison=from_obj.garrison, units_recruited_this_turn=from_obj.units_recruited_this_turn,
                                         scale_percentage=self.zoom_scale)


    def add_army_to_map(self, army):
        army_sprite = self.army_obj_conversion(army, "to_sprite")
        army_sprite.position_x -= self.camera_current_x+self.x_offset-LANDSCAPE_WIDTH//2*self.zoom_scale-LANDSCAPE_WIDTH//2
        army_sprite.position_y -= self.camera_current_y+self.y_offset-LANDSCAPE_HEIGHT//2*self.zoom_scale-LANDSCAPE_HEIGHT//2
        self.all_sprites.add(army_sprite)
        self.fixed_sprites.add(army_sprite)
        self.army_sprites.add(army_sprite)



    def run(self):
        while self.playing:
            if self.running:
                self.clock.tick(FPS*2)
                self.events()
                self.update()
                self.draw()


    def events(self):
        pos = pg.mouse.get_pos()

        keys = pg.key.get_pressed()
        SCROLL_SPEED = 64
        if keys[pg.K_LEFT] or keys[pg.K_a]:
            if self.map.position_x < -int(self.zoom_scale * SCROLL_SPEED) + LANDSCAPE_WIDTH//3:
                self.map.position_x += int(self.zoom_scale * SCROLL_SPEED)
                self.camera_current_x -= int(self.zoom_scale * SCROLL_SPEED)
                for fixed_sprite in self.fixed_sprites:
                    fixed_sprite.position_x += int(self.zoom_scale * SCROLL_SPEED)
                self.x_offset -= SCROLL_SPEED

        elif keys[pg.K_RIGHT] or keys[pg.K_d]:
            if self.map.position_x > -(self.map_width - LANDSCAPE_WIDTH*3/4 - int(self.zoom_scale * SCROLL_SPEED)):
                self.map.position_x -= int(self.zoom_scale * SCROLL_SPEED)
                self.camera_current_x += int(self.zoom_scale * SCROLL_SPEED)
                for fixed_sprite in self.fixed_sprites:
                    fixed_sprite.position_x -= int(self.zoom_scale * SCROLL_SPEED)
                self.x_offset += SCROLL_SPEED

        if keys[pg.K_UP] or keys[pg.K_w]:
            if self.map.position_y < -int(self.zoom_scale * SCROLL_SPEED) + LANDSCAPE_HEIGHT//3:
                self.map.position_y += int(self.zoom_scale * SCROLL_SPEED)
                self.camera_current_y -= int(self.zoom_scale * SCROLL_SPEED)
                for fixed_sprite in self.fixed_sprites:
                    fixed_sprite.position_y += int(self.zoom_scale * SCROLL_SPEED)
                self.y_offset -= SCROLL_SPEED
        elif keys[pg.K_DOWN] or keys[pg.K_s]:
            if self.map.position_y > -(self.map_height - LANDSCAPE_HEIGHT*3/4 - int(self.zoom_scale * SCROLL_SPEED)):
                self.map.position_y -= int(self.zoom_scale * SCROLL_SPEED)
                self.camera_current_y += int(self.zoom_scale * SCROLL_SPEED)
                for fixed_sprite in self.fixed_sprites:
                    fixed_sprite.position_y -= int(self.zoom_scale * SCROLL_SPEED)
                self.y_offset += SCROLL_SPEED

        events = pg.event.get()
        for event in events:
            # quit
            if event.type == pg.QUIT:
                choice = pg.display.message_box("Quit",
                                                "Are you sure you wish to quit? You will lose any unsaved progress.",
                                                buttons=("Yes", "No"), return_button=1, escape_button=None
                                                )

                if choice == 0:
                    pg.quit()
                    sys.exit()

            if event.type == pg.KEYUP:
                if event.key == pg.K_f and DEMO:
                    if pg.FULLSCREEN and self.screen.get_flags():
                        pg.display.set_mode((self.game_width, self.game_height))
                    else:
                        pg.display.set_mode((self.game_width, self.game_height), pg.FULLSCREEN|pg.DOUBLEBUF)
                if event.key == pg.K_ESCAPE:
                    self.return_to_campaign_map()

            if self.close_map_button.rect.collidepoint(pos):
                self.close_map_button.hovering = True
                if event.type == pg.MOUSEBUTTONUP:
                    self.close_map_button.command()
            else:
                self.close_map_button.hovering = False



    def return_to_campaign_map(self):
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = self.return_to_campaign_map
        elif not self.transition.fading:
            self.transition_clean_up()
            for sp in self.all_sprites:
                sp.kill()
            self.campaign_map_scene.new(load_from="world_map_autosave.p",
                                        post_battle_cmd=partial(self.campaign_map_scene.camera_zoom, None, (self.x_offset, self.y_offset)))


    def update(self):
        now = pg.time.get_ticks()
        self.all_sprites.update()
        if self.transition:
            if self.transition.fading:
                self.transition.update()
            else:
                if self.post_transition_function:
                    self.post_transition_function()
                else:
                    self.transition = None


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)

            for city_sprite in self.city_sprites:
                on_screen_cond_1 = abs(self.camera_current_x - city_sprite.grid_position_x * self.zoom_scale * 32) <= LANDSCAPE_WIDTH*.6
                on_screen_cond_2 = abs(self.camera_current_y - city_sprite.grid_position_y * self.zoom_scale * 32) <= LANDSCAPE_HEIGHT*.6
                if on_screen_cond_1 and on_screen_cond_2:
                    self.draw_text(city_sprite.name, FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                                   YELLOW, city_sprite.rect.centerx, city_sprite.rect.bottom, align="center")

            #for army_sprite in self.army_sprites:
            #    self.draw_text(army_sprite.commander.name, FONT_FUNCTION(DEFAULT_FONT, 14),
            #                   WHITE, army_sprite.rect.centerx, army_sprite.rect.bottom, align="center")

            self.priority_sprites.draw(self.screen)
            self.transition_sprites.draw(self.screen)

            # compensate for full screen
            for border_rect in self.extended_borders:
                pg.draw.rect(self.screen, (0,0,0), border_rect)

            pg.display.flip()


    def draw_text(self, text, font, color, x, y, align="midtop"):
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        if align == "midleft":
            text_rect.midleft = (x, y)
        elif align == "topleft":
            text_rect.topleft = (x, y)
        elif align == "center":
            text_rect.center = (x, y)
        else:
            text_rect.midtop = (x, y)
        self.screen.blit(text_surface, text_rect)


    def drawWrappedText(self, text, color, rect, font, aa=False, bkg=None):
        rect = pg.Rect(rect)
        y = rect.top
        lineSpacing = 10

        # get the height of the font
        fontHeight = font.size("Tg")[1]

        while text:
            i = 1

            # determine if the row of text will be outside our area
            if y + fontHeight > rect.bottom:
                break

            # determine maximum width of line
            while font.size(text[:i])[0] < rect.width and i < len(text):
                i += 1

            # if we've wrapped the text, then adjust the wrap to the last word
            if i < len(text):
                i = text.rfind(" ", 0, i) + 1

            # render the line and blit it to the surface
            if bkg:
                image = font.render(text[:i], 1, color, bkg)
                image.set_colorkey(bkg)
            else:
                image = font.render(text[:i], aa, color)

            self.screen.blit(image, (rect.left, y))
            y += fontHeight + lineSpacing

            # remove the text we just blitted
            text = text[i:]

        return text