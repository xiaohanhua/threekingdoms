from cx_Freeze import setup, Executable
import os
import sys
#import ffpyplayer
#import ffpyplayer.player.player
#from ffpyplayer.player import MediaPlayer

resource_dir = os.path.join(os.getcwd(), "Resources")
base = None

if sys.platform == "win32":
    base = "Win32GUI"
setup(name='Imperial Ambitions 3K',
      version='1.3',
      description='Alpha',
      #options={"build_exe": {"packages":packages, "includes":includes, "include_msvcr":True, "add_to_path": True}},
      #options={"build_exe": {"include_msvcr":True, "add_to_path": True}},
      executables = [Executable("imperial_ambitions_3k.py", icon=os.path.join(resource_dir, "game_main_icon.ico"),
                                base=base)])

#C:\Users\xndl1\PycharmProjects\threekingdoms\venv\Scripts\python.exe setup.py build
#C:\Users\xndl1\AppData\Local\Programs\Python\Python37\python.exe setup.py build
#C:\Users\xndl1\AppData\Local\Programs\Python\Python312\python.exe setup.py build