#from battle import*
from custom_battle_settings import*
from game_classes import*
from functools import partial


class Commander_Select_Scene:
    def __init__(self, main_menu):
        self.main_menu = main_menu
        self.game_width, self.game_height = LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT
        if DEMO:
            self.screen = pg.display.set_mode((self.game_width, self.game_height))
            os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (50, 50)
        else:
            self.screen = pg.display.set_mode((self.game_width, self.game_height), pg.FULLSCREEN|pg.DOUBLEBUF)
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()

        pg.mixer.init()
        pg.font.init()
        self.hover_sound = pg.mixer.Sound("sfx_hover.ogg")
        self.click_sound = pg.mixer.Sound("sfx_click.ogg")


    def new(self):

        self.running = True
        self.playing = True
        pg.mouse.set_visible(True)

        #transitions
        self.transition_sprites = pg.sprite.Group()
        self.transition = None
        self.post_transition_function = None
        self.initiate_transition("in")

        self.all_sprites = pg.sprite.Group()
        self.priority_sprites = pg.sprite.Group()
        self.fixed_sprites = pg.sprite.Group()
        self.buttons = pg.sprite.Group()
        self.available_generals = [Commander(name="Zhang Fei", simple_name="zhang_fei", age=36, loyalty=100,
                                        country="Shu", combat_prowess=100, intellect=62, command=88, charisma=25,
                                        action_points=20, level=5, experience=0, upgrade_points=0),
                                    Commander(name="Guan Yu", simple_name="guan_yu", age=41, loyalty=100,
                                              country="Shu", combat_prowess=98, intellect=75, command=95,  charisma=85,
                                              action_points=20, level=1, experience=0, upgrade_points=0),
                                    Commander(name="Zhao Yun", simple_name="zhao_yun", age=41, loyalty=100,
                                                          country="Shu", combat_prowess=99, intellect=70, command=85, charisma=80,
                                                          action_points=24, level=5, experience=0, upgrade_points=0),
                                    Commander(name="Ma Chao", simple_name="ma_chao", age=24, loyalty=75,
                                             country="Shu", combat_prowess=97, intellect=70, command=90,
                                             charisma=84, action_points=25, level=5, experience=0, upgrade_points=0),
                                    Commander(name="Huang Zhong", simple_name="huang_zhong", age=56, loyalty=85,
                                                        country="Shu", combat_prowess=92, intellect=65, command=85, charisma=65,
                                                        action_points=20, level=5, experience=0, upgrade_points=0),
                                    Commander(name="Wei Yan", simple_name="wei_yan", age=34, loyalty=70,
                                             country="Shu", combat_prowess=88, intellect=75,
                                             command=80, charisma=50, action_points=21, level=5,
                                             experience=0, upgrade_points=0),
                                    Commander(name="Zhuge Liang", simple_name="zhuge_liang", age=19,
                                                loyalty=100, country="Shu", combat_prowess=35, intellect=100,
                                                command=100, charisma=93, action_points=22, level=5,
                                                experience=0, upgrade_points=0, earliest_start_year=207),
                                    Commander(name="Zhu Rong", simple_name="zhu_rong", age=31, loyalty=80,
                                                            country="Shu", combat_prowess=72, intellect=55, command=70, charisma=69,
                                                            action_points=24, level=5, experience=0, upgrade_points=0),
                                    Commander(name="King Duosi", simple_name="king_duosi", age=38, loyalty=80,
                                                country="Shu", combat_prowess=60, intellect=80,
                                                command=85, charisma=45, action_points=28, level=5, experience=1200, upgrade_points=0),
                                    Commander(name="Zhou Cang", simple_name="zhou_cang", age=37, loyalty=90,
                                                            country="Shu", combat_prowess=70, intellect=50, command=58, charisma=73,
                                                            action_points=17, level=5, experience=0, upgrade_points=0),
                                    Commander(name="Xu Chu", simple_name="xu_chu", age=29, loyalty=100,
                                                            country="Wei", combat_prowess=96, intellect=40, command=75, charisma=20,
                                                            action_points=20, level=5, experience=0, upgrade_points=0),
                                    Commander(name="Xu Huang", simple_name="xu_huang", age=34, loyalty=90,
                                                            country="Wei", combat_prowess=87, intellect=60, command=80, charisma=76,
                                                            action_points=20, level=5, experience=0, upgrade_points=0),
                                    Commander(name="Pang De", simple_name="pang_de", age=39, loyalty=95,
                                             country="Wei", combat_prowess=96, intellect=70, command=90,
                                             charisma=74, action_points=25, level=5, experience=0, upgrade_points=0),
                                    Commander(name="Xiahou Yuan", simple_name="xiahou_yuan", age=38, loyalty=100,
                                                                 country="Wei", combat_prowess=78, intellect=60, command=72, charisma=72,
                                                                 action_points=30, level=5, experience=0, upgrade_points=0),
                                    Commander(name="Xiahou Dun", simple_name="xiahou_dun", age=43, loyalty=100,
                                                                 country="Wei", combat_prowess=83, intellect=50, command=65, charisma=90,
                                                                 action_points=20, level=5, experience=0, upgrade_points=0),
                                    Commander(name="Cao Ren", simple_name="cao_ren", age=32, loyalty=95, country="Wei",
                                             combat_prowess=77, intellect=61, command=90, charisma=82,
                                             action_points=20, level=5, experience=0, upgrade_points=0),
                                    Commander(name="Zhang Liao", simple_name="zhang_liao", age=31, loyalty=90,
                                                                 country="Wei", combat_prowess=88, intellect=82, command=90, charisma=88,
                                                                 action_points=22, level=5, experience=0, upgrade_points=0),
                                    Commander(name="Zhang He", simple_name="zhang_he", age=31, loyalty=85,
                                                                 country="Wei", combat_prowess=91, intellect=90, command=84, charisma=75,
                                                                 action_points=20, level=5, experience=0, upgrade_points=0),
                                    Commander(name="Yu Jin", simple_name="yu_jin", age=39, loyalty=65,
                                                           country="Wei", combat_prowess=74, intellect=65, command=87, charisma=50,
                                                           action_points=18, level=5, experience=0, upgrade_points=0),
                                    Commander(name="Yue Jin", simple_name="yue_jin", age=35, loyalty=85,
                                                           country="Wei", combat_prowess=80, intellect=60, command=90, charisma=55,
                                                           action_points=20, level=5, experience=0, upgrade_points=0),
                                    Commander(name="Cheng Yu", simple_name="cheng_yu", age=59, loyalty=85,
                                                     country="Wei", combat_prowess=20, intellect=91, command=77, charisma=78,
                                                     action_points=17, level=1, experience=0, upgrade_points=0),
                                    #Commander(name="Deng Ai", simple_name="deng_ai", age=3, loyalty=85,
                                    #         country="Wei", combat_prowess=76, intellect=88, command=88, charisma=82,
                                    #         action_points=20, level=5, experience=0, upgrade_points=0),
                                    Commander(name="Zhou Yu", simple_name="zhou_yu", age=27, loyalty=100,
                                                    country="Wu", combat_prowess=65, intellect=98, command=95, charisma=100,
                                                    action_points=26, level=5, experience=0, upgrade_points=0),
                                    Commander(name="Han Dang", simple_name="han_dang", age=35, loyalty=90,
                                                     country="Wu", combat_prowess=82, intellect=50, command=84, charisma=50,
                                                     action_points=20, level=5, experience=0, upgrade_points=0),
                                    Commander(name="Huang Gai", simple_name="huang_gai", age=46, loyalty=90,
                                                     country="Wu", combat_prowess=86, intellect=55, command=85, charisma=70,
                                                     action_points=20, level=5, experience=0, upgrade_points=0),
                                    Commander(name="Taishi Ci", simple_name="taishi_ci", age=34, loyalty=94,
                                                     country="Wu", combat_prowess=97, intellect=69, command=86, charisma=90,
                                                     action_points=20, level=5, experience=0, upgrade_points=0),
                                   Commander(name="Zhou Tai", simple_name="zhou_tai", age=37, loyalty=95,
                                                    country="Wu", combat_prowess=92, intellect=58, command=77, charisma=56,
                                                    action_points=20, level=5, experience=0, upgrade_points=0),
                                   Commander(name="Xu Sheng", simple_name="xu_sheng", age=30, loyalty=84, country="Wu",
                                             combat_prowess=86, intellect=77, command=87, charisma=65, action_points=20, level=5,
                                             experience=0, upgrade_points=0),
                                   Commander(name="Lu Xun", simple_name="lu_xun", age=17, loyalty=96, country="Wu",
                                             combat_prowess=60, intellect=95, command=94, charisma=90, action_points=22,
                                             level=5, experience=0, upgrade_points=0),
                                   Commander(name="Pang Tong", simple_name="pang_tong", age=21, loyalty=75,
                                             country="Wu", combat_prowess=30, intellect=97, command=79, charisma=79,
                                             action_points=18, level=5, experience=0, upgrade_points=0),
                                   #Commander(name="Ding Feng", simple_name="ding_feng", age=-3, loyalty=96,
                                   #          country="Wu", combat_prowess=80, intellect=71, command=81, charisma=56,
                                   #          action_points=22, level=1, experience=0,
                                   #          upgrade_points=0, earliest_start_year=228, hometown="He Nan")
                                   ]
        self.current_player_general_preview = 0
        self.current_enemy_general_preview = 0

        self.all_general_tooltips = []
        for general in self.available_generals:
            name = general.simple_name
            self.all_general_tooltips.append(pg.image.load("commander_tooltip_{}.png".format(name)).convert_alpha())

        self.background = Static(0, self.game_height - 650, "sprite_menu_bg.png")
        self.all_sprites.add(self.background)

        self.player_general_preview = Static(100, 50, "commander_tooltip_{}.png".format(self.available_generals[0].simple_name),
                                             anchor="topleft")
        self.all_sprites.add(self.player_general_preview)
        self.enemy_general_preview = Static(650, 50, "commander_tooltip_{}.png".format(self.available_generals[0].simple_name),
                                            anchor="topleft")
        self.all_sprites.add(self.enemy_general_preview)


        #buttons
        self.toggle_player_general_left_button = Button(80, LANDSCAPE_HEIGHT // 2, "menu_button_left.png",
                                      command=partial(self.toggle_general, "player", -1))
        self.buttons.add(self.toggle_player_general_left_button)
        self.all_sprites.add(self.toggle_player_general_left_button)


        self.toggle_player_general_right_button = Button(570, LANDSCAPE_HEIGHT // 2, "menu_button_right.png",
                                      command=partial(self.toggle_general, "player", 1))
        self.buttons.add(self.toggle_player_general_right_button)
        self.all_sprites.add(self.toggle_player_general_right_button)



        self.toggle_enemy_general_left_button = Button(630, LANDSCAPE_HEIGHT // 2, "menu_button_left.png",
                                      command=partial(self.toggle_general, "enemy", -1))
        self.buttons.add(self.toggle_enemy_general_left_button)
        self.all_sprites.add(self.toggle_enemy_general_left_button)

        self.toggle_enemy_general_right_button = Button(1120, LANDSCAPE_HEIGHT // 2, "menu_button_right.png",
                                                       command=partial(self.toggle_general, "enemy", 1))
        self.buttons.add(self.toggle_enemy_general_right_button)
        self.all_sprites.add(self.toggle_enemy_general_right_button)



        self.back_button = Button(LANDSCAPE_WIDTH//2-300, LANDSCAPE_HEIGHT-30, "menu_button_back.png",
                                  command=self.return_to_main_menu, scale=(210,50))
        self.buttons.add(self.back_button)
        self.all_sprites.add(self.back_button)

        self.next_button = Button(LANDSCAPE_WIDTH//2+300, LANDSCAPE_HEIGHT-30, "menu_button_next.png",
                                  command=self.load_settings_scene, scale=(210,50))
        self.buttons.add(self.next_button)
        self.all_sprites.add(self.next_button)


        #sp_atk/skill icons

        self.sp_atk_icons_list = []
        self.skill_icons_list = []
        self.special_attack_tooltip_description = ""
        self.special_attack_tooltip_x = 0
        self.special_attack_tooltip_y = 0
        self.reload_sp_atk_icons()
        self.reload_skill_icons()

        self.run()


    def initiate_transition(self, direction):
        if direction == "in":
            self.transition = FadeBlackTransition(45, "in")
            self.transition_sprites.add(self.transition)
        else:
            self.transition = FadeBlackTransition(45, "out")
            self.transition_sprites.add(self.transition)


    def transition_clean_up(self):
        self.transition.kill()
        self.transition = None
        self.post_transition_function = None
        self.playing = False
        self.running = False
        for s in self.all_sprites:
            s.kill()


    def reload_sp_atk_icons(self):
        for item in self.sp_atk_icons_list:
            item.kill()

        player_general = self.available_generals[self.current_player_general_preview]
        counter = 0
        for sp_atk in player_general.sp_atks:
            sp_atk_icon = SpecialAttackIcon(self, 120+70*counter, 400, sp_atk, anchor="topleft")
            self.all_sprites.add(sp_atk_icon)
            self.sp_atk_icons_list.append(sp_atk_icon)
            counter += 1

        enemy_general = self.available_generals[self.current_enemy_general_preview]
        counter = 0
        for sp_atk in enemy_general.sp_atks:
            sp_atk_icon = SpecialAttackIcon(self, 670 + 70 * counter, 400, sp_atk, anchor="topleft")
            self.all_sprites.add(sp_atk_icon)
            self.sp_atk_icons_list.append(sp_atk_icon)
            counter += 1


    def reload_skill_icons(self):
        for item in self.skill_icons_list:
            item.kill()

        player_general = self.available_generals[self.current_player_general_preview]
        counter = 0
        for skill in player_general.skills:
            sp_atk_icon = SpecialAttackIcon(self, 120 + 70 * counter, 490, skill, anchor="topleft", type="skill")
            self.all_sprites.add(sp_atk_icon)
            self.skill_icons_list.append(sp_atk_icon)
            counter += 1

        enemy_general = self.available_generals[self.current_enemy_general_preview]
        counter = 0
        for skill in enemy_general.skills:
            sp_atk_icon = SpecialAttackIcon(self, 670 + 70 * counter, 490, skill, anchor="topleft", type="skill")
            self.all_sprites.add(sp_atk_icon)
            self.skill_icons_list.append(sp_atk_icon)
            counter += 1


    def load_settings_scene(self):
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = self.load_settings_scene
        elif not self.transition.fading:
            self.transition_clean_up()
            scene = Custom_Battle_Settings_Scene(self, self.available_generals[self.current_player_general_preview],
                                                 self.available_generals[self.current_enemy_general_preview])
            scene.new()


    def toggle_general(self, side="player", direction=1):
        if side == "player":
            self.current_player_general_preview += direction
            if self.current_player_general_preview >= len(self.available_generals):
                self.current_player_general_preview = 0
            elif self.current_player_general_preview < 0:
                self.current_player_general_preview = len(self.available_generals) - 1

            self.player_general_preview.image = self.all_general_tooltips[self.current_player_general_preview]

        else:
            self.current_enemy_general_preview += direction
            if self.current_enemy_general_preview >= len(self.available_generals):
                self.current_enemy_general_preview = 0
            elif self.current_enemy_general_preview < 0:
                self.current_enemy_general_preview = len(self.available_generals) - 1

            self.enemy_general_preview.image = self.all_general_tooltips[self.current_enemy_general_preview]

        self.reload_skill_icons()
        self.reload_sp_atk_icons()


    def run(self):
        while self.playing:
            if self.running:
                self.clock.tick(FPS)
                self.events()
                self.update()
                self.draw()


    def events(self):
        pos = pg.mouse.get_pos()
        for button in self.buttons:
            if button.rect.collidepoint(pos):
                if pg.mouse.get_pressed()[0]:
                    button.pressed = True
                else:
                    button.pressed = False
                if not button.hovering:
                    button.hovering = True
                    self.hover_sound.play()
            else:
                button.pressed = False
                button.hovering = False

        for event in pg.event.get():
            if event.type == pg.QUIT:
                choice = pg.display.message_box("Quit",
                                                "Are you sure you wish to quit?",
                                                buttons=("Yes", "No"), return_button=1, escape_button=None
                                                )

                if choice == 0:
                    pg.quit()
                    sys.exit()

            if event.type == pg.MOUSEBUTTONUP:
                self.click_sound.play()
                for button in self.buttons:
                    if button.rect.collidepoint(pos):
                        button.pressed = False
                        button.command()
                        break

            if event.type == pg.KEYUP:
                if event.key == pg.K_ESCAPE:
                    self.return_to_main_menu()

        hovering_over_sp_atk_icons = False
        for sp_atk_icon in self.sp_atk_icons_list + self.skill_icons_list:
            if sp_atk_icon.rect.collidepoint(pos):
                sp_atk_icon.hovering = True
                hovering_over_sp_atk_icons = True
                if sp_atk_icon.tooltip_description:
                    self.special_attack_tooltip_description = sp_atk_icon.tooltip_description
                    self.special_attack_tooltip_x = sp_atk_icon.tooltip.rect.left
                    self.special_attack_tooltip_y = sp_atk_icon.tooltip.rect.top
            else:
                sp_atk_icon.hovering = False

        if not hovering_over_sp_atk_icons:
            self.special_attack_tooltip_description = ""


    def return_to_main_menu(self):
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = self.return_to_main_menu
        elif not self.transition.fading:
            self.transition_clean_up()
            self.main_menu.new(start_music=False)


    def update(self):
        self.all_sprites.update()
        if self.transition:
            if self.transition.fading:
                self.transition.update()
            else:
                if self.post_transition_function:
                    self.post_transition_function()
                else:
                    self.transition = None


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)

            self.drawWrappedText("Choose your general:", YELLOW,
                                 pg.Rect(LANDSCAPE_WIDTH//6, 20, 500, 50), FONT_FUNCTION(DEFAULT_FONT, 20))

            player_general = self.available_generals[self.current_player_general_preview]

            self.draw_text("Level: {}".format(player_general.level), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 330,
                           50+50, align="midleft")
            self.draw_text("Exp: {}/{}".format(player_general.experience, player_general.level_thresholds[player_general.level]),
                           FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 330, 50+85, align="midleft")
            self.draw_text("Age: {}".format(player_general.age), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 330,
                           50+120, align="midleft")
            self.draw_text("Country: {}".format(player_general.country), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 330,
                           50+155, align="midleft")
            self.draw_text("Loyalty: {}".format(player_general.loyalty), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 330,
                           50+190, align="midleft")
            self.draw_text("Combat Prowess: {}".format(player_general.combat_prowess),
                           FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 330, 50+225, align="midleft")
            self.draw_text("Intellect: {}".format(player_general.intellect), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                           WHITE, 330, 50+260, align="midleft")
            self.draw_text("Command: {}".format(player_general.command), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                           WHITE, 330, 50+295, align="midleft")


            self.draw_text("Special Attacks:", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 120,
                           380, align="midleft")

            self.draw_text("Skills:", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 120,
                           470, align="midleft")



            self.drawWrappedText("Choose enemy general:", YELLOW,
                                 pg.Rect(LANDSCAPE_WIDTH*4//6-50, 20, 500, 50), FONT_FUNCTION(DEFAULT_FONT, 20))

            enemy_general = self.available_generals[self.current_enemy_general_preview]
            self.draw_text("Level: {}".format(enemy_general.level), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 880,
                           50 + 50, align="midleft")
            self.draw_text(
                "Exp: {}/{}".format(enemy_general.experience, enemy_general.level_thresholds[enemy_general.level]),
                FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 880, 50 + 85, align="midleft")
            self.draw_text("Age: {}".format(enemy_general.age), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 880,
                           50 + 120, align="midleft")
            self.draw_text("Country: {}".format(enemy_general.country), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 880,
                           50 + 155, align="midleft")
            self.draw_text("Loyalty: {}".format(enemy_general.loyalty), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                           880, 50 + 190, align="midleft")
            self.draw_text("Combat Prowess: {}".format(enemy_general.combat_prowess),
                           FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 880, 50 + 225, align="midleft")
            self.draw_text("Intellect: {}".format(enemy_general.intellect), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                           WHITE, 880, 50 + 260, align="midleft")
            self.draw_text("Command: {}".format(enemy_general.command), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                           WHITE, 880, 50 + 295, align="midleft")


            self.draw_text("Special Attacks:", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 670,
                           380, align="midleft")

            self.draw_text("Skills:", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 670,
                           470, align="midleft")


            self.priority_sprites.draw(self.screen)
            self.fixed_sprites.draw(self.screen)
            self.transition_sprites.draw(self.screen)

            if self.special_attack_tooltip_description:
                #self.drawWrappedText(self.special_attack_tooltip_description, WHITE,
                #                     pg.Rect(self.special_attack_tooltip_x+16, self.special_attack_tooltip_y+20,
                #                             268, 360), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE))
                self.draw_text(self.special_attack_tooltip_description, FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                               WHITE, self.special_attack_tooltip_x + 16, self.special_attack_tooltip_y + 20,
                               align="topleft", max_width=268)

            pg.display.flip()


    def draw_text(self, text, font, color, x, y, align="midtop", max_width=None):
        if max_width:
            text_surface = font.render(text, True, color, None, max_width)
        else:
            text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        if align == "midleft":
            text_rect.midleft = (x, y)
        elif align == "topleft":
            text_rect.topleft = (x, y)
        else:
            text_rect.midtop = (x, y)
        self.screen.blit(text_surface, text_rect)


    def drawWrappedText(self, text, color, rect, font, aa=False, bkg=None):
        rect = pg.Rect(rect)
        y = rect.top
        lineSpacing = 10

        # get the height of the font
        fontHeight = font.size("Tg")[1]

        while text:
            i = 1

            # determine if the row of text will be outside our area
            if y + fontHeight > rect.bottom:
                break

            # determine maximum width of line
            if VERSION[-2:] == "CH":
                i = 15
            else:
                while font.size(text[:i])[0] < rect.width and i < len(text):
                    i += 1

                # if we've wrapped the text, then adjust the wrap to the last word
                if i < len(text):
                    i = text.rfind(" ", 0, i) + 1

            # render the line and blit it to the surface
            if bkg:
                image = font.render(text[:i], 1, color, bkg)
                image.set_colorkey(bkg)
            else:
                image = font.render(text[:i], aa, color)

            self.screen.blit(image, (rect.left, y))
            y += fontHeight + lineSpacing

            # remove the text we just blitted
            text = text[i:]

        return text