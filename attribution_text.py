#TODO add Bitbucket link for LPC credits.csv file
ATTRIBUTION_STR = """
***Thank you to the following artists for generously providing free assets!***


<<<<<LPC Sprites By>>>>>
David Conway Jr. (JaidynReiman), Nila122, Johannes Sjölund (wulax), Stephen Challener (Redshrike), Luke Mehl,
Michael Whitlock (bigbeargames), Matthew Krohn (makrohn), Carlo Enrico Victoria (Nemisys), Thane Brimhall (pennomi),
bluecarrot16, kheftel, Marcel van de Steeg (MadMarcel), Mark Weyer, Lanea Zimmerman (Sharm), Manuel Riecke (MrBeast),
Charles Sanchez (CharlesGabriel), Zi Ye, Benjamin K. Smith (BenCreating), William.Thompsonj,
Sander Frenken (castelonia), dalonedrau, Daniel Eddeland (daneeklu), ElizaWy, castelonia., Evert, Durrani,
MuffinElZangano, Pierre Vigier (pvigier), Rhimlock, laetissima, kirts, Joe White, Mandi Paugh, Barbara Riviera,
thecilekli, Yamilian, Fabzy, Skorpio, Radomir Dopieralski, Emilio J. Sanchez-Sierra, kcilds/Rocetti/Eredah,
Cobra Hubbard (BlueVortexGames), DarkwallLKE, Shaun Williams, Tuomo Untinen (reemax), Stafford McIntyre,
PlatForge project, Tracy, drjamgo@hotmail.com, Lori Angela Nagel (jastiv), gr3yh47, pswerlang, XOR, tskaufma,
Inboxninja, Dr. Jamgo, LordNeo
Sprites contributed as part of the Liberated Pixel Cup project from OpenGameArt.org:
http://opengameart.org/content/lpc-collection
License: Creative Commons Attribution-ShareAlike 3.0 (CC-BY-SA 3.0) http://creativecommons.org/licenses/by-sa/3.0/
Detailed credits: https://bitbucket.org/xiaohanhua/threekingdomsreadme/src/master/LPC_sprites_all_credits.csv

http://gaurav.munjal.us/Universal-LPC-Spritesheet-Character-Generator/
https://sanderfrenken.github.io/Universal-LPC-Spritesheet-Character-Generator/#?cape=male_red&weapon=scythe&body=skeleton



<<<<<UI/Icons>>>>>
https://opengameart.org/content/fantasy-ui-elements-by-ravenmore (Ravenmore: dycha.net,
                                                          www.facebook.com/RavenmoreArt, www.twitter.com/RavenmoreArt)

https://opengameart.org/content/rpg-gui-construction-kit-v10 (Lamoot)
https://opengameart.org/content/moderna-graphical-interface (Jorge Avila)
https://opengameart.org/content/modal-panel-dark-source-included (netgfx)
https://opengameart.org/content/dungeon-crawl-selected-upscale (drummyfish)
https://opengameart.org/content/spell-set (Scribe)
https://opengameart.org/content/attack-icons-wesnoth (eleazzaar)
https://opengameart.org/content/farming-set-pixel-art (ScratchIO)
https://opengameart.org/content/a-star (jeffshee)
https://www.aigei.com/view/72968.html (Cburdeau)
https://opengameart.org/content/game-icons-0 (chabull)
https://opengameart.org/content/fantasy-sword-set (Melle)
https://opengameart.org/content/an-isometric-tileable-wall-section (Hansjörg Malthaner http://opengameart.org/users/varkalandar)
https://opengameart.org/node/121423 (knik1985)
https://opengameart.org/content/elephant-rework (Created by lawnjelly & Jordan Irwin (AntumDeluge))
https://opengameart.org/content/asteroids (phaelax)
https://opengameart.org/content/simple-map-tiles (Melle)
https://opengameart.org/content/physica-assets-expansion (Buch: https://opengameart.org/users/buch)
https://opengameart.org/content/dark-soldier-on-a-horse-valyria-tear (from Lilou (Valyria Tear))
https://opengameart.org/content/siege-0 (santigo iborra)
https://opengameart.org/content/lpc-siege-weapons ("[LPC] Siege Weapons" by bluecarrot16)
https://game-icons.net/1x1/skoll/footsteps.html#download (Skoll)
https://opengameart.org/content/computers-and-network-from-qnetwalk (Andi Peredri)


(J. W. Bjerk (eleazzaar) -- www.jwbjerk.com/art -- find this and other open art at: http://opengameart.org)
https://opengameart.org/content/painterly-spell-icons-part-1 
https://opengameart.org/content/painterly-spell-icons-part-2
https://opengameart.org/content/painterly-spell-icons-part-3
https://opengameart.org/content/painterly-spell-icons-part-4


https://opengameart.org/content/topographic-map-images (Of Far Different Nature)
https://www.aigei.com/view/71655.html?page=2 (Sound Science)
https://opengameart.org/content/dark-wingless-dragon (Dwapook)
https://game-icons.net/1x1/delapouite/shaking-hands.html (Delapouite)
https://opengameart.org/content/armor-icons-by-equipment-slot (Clint Bellanger, Blarumyrran, crowline, Justin Nichol)
https://game-icons.net/1x1/lorc/one-eyed.html (Lorc)
https://opengameart.org/content/hero-spritesheets-ars-notoria (Balmer)
https://opengameart.org/content/weapon-icons-32x32px-painterly (Scrittl)

(Heather Lee Harvey: EmeraldActivities.com & http://opengameart.org/users/emerald)
https://opengameart.org/content/loyalty-lies-equipment-bows

https://www.aigei.com/view/3971.html#items
https://www.aigei.com/view/74287.html (Illusion of Safety)
https://www.aigei.com/view/71439.html (Steven B. Shapiro (BMI) 100% (IPI# 129552761))
https://opengameart.org/content/poison-skull (Jorge Avila)


This work by Florian R. A. Angermeier (fraang) is licensed under the Creative Commons Attribution - ShareAlike 3.0 Unported License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/.
https://opengameart.org/content/wheat-field 


https://opengameart.org/content/assassin-character-on-horse (Stagnation)
https://opengameart.org/content/enemy-health-bars (Paul Wortmann, www.PhysHexGames.com)
https://game-icons.net/1x1/delapouite/evil-love.html#download (Delapuite)
https://opengameart.org/content/spikey-stuff (dravenx)
https://game-icons.net/1x1/lorc/inner-self.html#download (Lorc)
https://game-icons.net/1x1/delapouite/silenced.html#download (Delapuite)
https://game-icons.net/1x1/heavenly-dog/defensive-wall.html#download (HeavenlyDog)
https://game-icons.net/1x1/delapouite/load.html#download (Delapuite)
https://game-icons.net/1x1/delapouite/save.html#download (Delapuite)
https://game-icons.net/1x1/delapouite/team-upgrade.html#download (Delapuite)
https://game-icons.net/1x1/lorc/envelope.html (Lorc)
https://game-icons.net/1x1/willdabeast/white-book.html (Willdabeast)
https://game-icons.net/1x1/willdabeast/round-shield.html (Willdabeast)
https://opengameart.org/content/keyboard-keys-1 (Rallix)
https://opengameart.org/content/arrow-keys-wsad-mouse-icon (InanZen)
https://opengameart.org/content/simple-ornamented-arrow ()
https://www.aigei.com/view/73141.html (gamjutsu)
https://www.aigei.com/s?dim=scene_background&q=%E4%B8%89%E5%9B%BD+%E6%88%98%E4%BA%89&type=2d (Charles Schlacter)



<<<<<Campaign Map Icons>>>>>
https://www.aigei.com/view/72781.html (chrystal.jean)
https://www.aigei.com/view/71258.html (Robyerto)



<<<<<Character avatars>>>>>
http://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220619698683&categoryNo=0&parentCategoryNo=186
http://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220626106747&categoryNo=0&parentCategoryNo=186
http://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220625879828&categoryNo=0&parentCategoryNo=186
http://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220626106747&categoryNo=0&parentCategoryNo=186
http://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220620208420&categoryNo=0&parentCategoryNo=186
http://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220625748008&categoryNo=0&parentCategoryNo=186
http://blog.naver.com/PostView.nhn?blogId=zzangogo7&logNo=220620026073&categoryNo=0&parentCategoryNo=186
https://www.aigei.com/view/24706.html#items

https://s.aigei.com/src/img/png/4e/4e13c4496b9744a288dc099b8f83b49a.png?imageMogr2/auto-orient/
thumbnail/!169x201r/gravity/Center/crop/169x201/quality/85/&e=1735488000&token=
P7S2Xpzfz11vAkASLTkfHN7Fw-oOZBecqeJaxypL:3XH8K1tUVFsNMrPHHSlb9dyNZ-Y=

https://www.aigei.com/view/74292-34937330.html#items
https://www.aigei.com/view/72968.html#items

https://s.aigei.com/src/img/jpg/11/115d784fe5ab4be9afedf9869171c30e.jpg?imageMogr2/auto-orient/
thumbnail/!500x375r/gravity/Center/crop/500x375/quality/85/imageView2/2/w/500/|watermark/3/text/
54ix57uZ572R57Sg5p2Q57yW5Y-3IzE5NTE4NDA=/font/5b6u6L2v6ZuF6buR/fontsize/361/fill/cmVk/dissolve/
41/gravity/SouthEast/dx/19/dy/19/text/5bm75oOz5LiJ5Zu95b-XMi3miJjmlpfog4zmma_lm74=/font
/5b6u6L2v6ZuF6buR/fontsize/361/fill/cmVk/dissolve/41/gravity/NorthWest/dx/19/dy/19/text/
NjQww5c0ODA=/font/5b6u6L2v6ZuF6buR/fontsize/361/fill/cmVk/dissolve/41/gravity/SouthWest/
dx/19/dy/19/text/5Zu-5YOPMDUw/font/5b6u6L2v6ZuF6buR/fontsize/361/fill/cmVk/dissolve/41/
gravity/NorthEast/dx/19/dy/19//text/54ix57uZ572RIGFpZ2VpLmNvbQ==/font/5b6u6L2v6ZuF6buR/
fontsize/300/fill/TWFyb29u/dissolve/54/gravity/NorthWest/dx/94/dy/226&e=1735488000&token=
P7S2Xpzfz11vAkASLTkfHN7Fw-oOZBecqeJaxypL:gXkKSxrzIXIx9TG2w8GRSC4a7bo=


https://www.aigei.com/view/70694.html#items
https://www.aigei.com/view/8940.html?page=2
https://www.aigei.com/view/1176.html
https://s.aigei.com/src/img/png/94/948893b4106246018bf49f3098b12bb8.png?imageMogr2/auto-orient/
thumbnail/!53x53r/gravity/Center/crop/53x53/quality/85/&e=1735488000&token=
P7S2Xpzfz11vAkASLTkfHN7Fw-oOZBecqeJaxypL:FmWzfxLJmJDdvVY8NFKmPVBfQl4=



<<<<<Backgrounds>>>>>
https://www.aigei.com/view/2613.html#items (zenthex)
https://www.aigei.com/view/70413.html#items (Nobody's Bizness)



<<<<<Tileset>>>>>
https://www.mapeditor.org/
https://opengameart.org/content/16x16-game-assets (George_)
https://opengameart.org/content/isometric-64x64-outside-tileset (Yar)


Daniel Cook, Jetrel, Saphy (TMW), Zabin, Bertram
https://opengameart.org/content/2d-lost-garden-zelda-style-tiles-resized-to-32x32-with-additions 
https://opengameart.org/content/2d-lost-garden-zelda-style-tiles-winter-theme-with-additions


Hyptosis, Zabin, Daniel Cook https://opengameart.org/content/castle-tiles-for-rpgs


(Credit goes to: Zabin, Daneeklu, Jetrel, Hyptosis, Redshrike, Bertram)
https://opengameart.org/content/rpg-tiles-cobble-stone-paths-town-objects


 Richard Kettering aka (Jetrel),Zachariah Husiar aka (Zabin), Hyptosis, Sharm and Open Pixel Project
https://opengameart.org/content/zrpg-tiles 



<<<<<SFX>>>>>
https://opengameart.org/content/swishes-sound-pack (artisticdude)
https://opengameart.org/content/horse-trotting (EZduzziteh)
https://soundbible.com/1140-Elephant.html (Mike Koenig)
https://soundbible.com/1296-Horse-Neigh.html (Mike Koenig)
https://opengameart.org/content/fire-evil-spell (artisticdude)
https://opengameart.org/content/20-sword-sound-effects-attacks-and-clashes 
(StarNinjas: https://opengameart.org/users/starninjas)
https://opengameart.org/content/bow-arrow-shot (dorkster)
https://opengameart.org/content/sea-and-river-wave-sounds (RandomMind)


<<<<<Music>>>>>
https://opengameart.org/content/rpg-battle-theme-the-last-encounter-0 (Matthew Pablo http://www.matthewpablo.com)
https://opengameart.org/content/determined-pursuit-epic-orchestra-loop (Emma_MA)
https://opengameart.org/content/battle-theme-a (cynicmusic)
https://opengameart.org/content/fantasy-armies (Alexandr Zhelanov https://soundcloud.com/alexandr-zhelanov)
https://opengameart.org/content/light-battle-theme (Alexandr Zhelanov https://soundcloud.com/alexandr-zhelanov)
https://opengameart.org/content/undisputed (Stefan Grossmann)



<<<<<Shortest Path Finding Code>>>>>
https://github.com/kidscancode/pygame_tutorials/blob/master/examples/pathfinding/part5.py



<<<<<Map of Three Kingdoms territories>>>>>
https://en.wikipedia.org/wiki/File:Topographical_3K_gif.gif (Author: Qiushufang)
This file is licensed under the Creative Commons Attribution-Share Alike 4.0 International license.
https://creativecommons.org/licenses/by-sa/4.0/deed.en

"""