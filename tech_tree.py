from game_classes import*
from sprites import*
from functools import partial
import pickle

class Tech_Tree_Scene:
    def __init__(self, campaign_map_scene):
        self.campaign_map_scene = campaign_map_scene
        self.game_width, self.game_height = LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT
        if DEMO:
            self.screen = pg.display.set_mode((self.game_width, self.game_height))
        else:
            self.screen = pg.display.set_mode((self.game_width, self.game_height), pg.FULLSCREEN|pg.DOUBLEBUF)
        os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (50, 50)
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()

        pg.mixer.init()
        pg.font.init()
        self.hover_sound = pg.mixer.Sound("sfx_hover.ogg")
        self.click_sound = pg.mixer.Sound("sfx_click.ogg")
        self.morale_boost_sound = pg.mixer.Sound("sfx_restore_status.ogg")
        self.deployment_fail_sound = pg.mixer.Sound("sfx_deployment_failed.ogg")


    def new(self):
        self.running = True
        self.playing = True
        pg.mouse.set_visible(True)
        self.learning_rate_bonus = int((self.campaign_map_scene.calculate_learning_rate(self.campaign_map_scene.player_country) - 1) * 100)

        # transitions
        self.transition_sprites = pg.sprite.Group()
        self.transition = None
        self.post_transition_function = None
        self.initiate_transition("in")

        self.tooltip_description = ""
        self.tooltip_description_x, self.tooltip_description_y = -3000, -3000

        self.all_sprites = pg.sprite.Group()
        self.icons = pg.sprite.Group()
        self.priority_sprites = pg.sprite.Group()
        self.tech_icons = {}

        self.default_tech_tree = TECH_TREE.copy()
        self.tech_tree = self.campaign_map_scene.tech_tree[self.campaign_map_scene.player_country].copy()
        self.acquired_tech = self.campaign_map_scene.acquired_tech[self.campaign_map_scene.player_country]
        self.current_tech = self.campaign_map_scene.current_tech[self.campaign_map_scene.player_country]

        self.background = Static(0, self.game_height - 650, "sprite_tech_tree_bg.png")
        self.all_sprites.add(self.background)

        self.skeleton = Static(0, self.game_height - 650, "sprite_tech_tree_skeleton.png")
        self.all_sprites.add(self.skeleton)

        self.gold_icon = CampaignMapSprite(self, 0, 0, "campaign_icon_gold.png", anchor="topleft")
        self.all_sprites.add(self.gold_icon)
        self.icons.add(self.gold_icon)

        self.food_icon = CampaignMapSprite(self, 7, 0, "campaign_icon_food.png", anchor="topleft")
        self.all_sprites.add(self.food_icon)
        self.icons.add(self.food_icon)

        self.back_button = Static(LANDSCAPE_WIDTH-80, 20, "campaign_button_back.png", anchor="topleft")
        self.all_sprites.add(self.back_button)

        self.game_message = ""
        self.last_displayed_message = 0
        self.game_tooltip_description = ""

        self.load_tech_tree_icons()
        self.run()


    def load_tech_tree_icons(self):
        for tech, value in self.default_tech_tree.items():
            x,y = value["coords"]
            icon_tooltip = value["friendly_name"] + "\n-------------------------------------------\n" + value["description"] + "\n\n" + "Turns till mastery: {}".format(self.calculate_turns_remaining(tech))
            button = Button(151+x*100, 101+y*100, "sprite_tech_{}.png".format(tech),
                            partial(self.toggle_tech_tree_icon, tech),
                            tooltip=icon_tooltip,
                            anchor="topleft", scale=None, master=self,
                            custom_suffix={"default": "_learnable",
                                           "selected": "_learning",
                                           "disabled": "",
                                           "pressed": "_learning"})

            if self.calculate_turns_remaining(tech) == 0 and tech not in self.acquired_tech:
                self.campaign_map_scene.acquired_tech[self.campaign_map_scene.player_country].append(tech)
                self.acquired_tech.append(tech)
                self.campaign_map_scene.assign_random_tech(self.campaign_map_scene.player_country)

            if tech == self.current_tech:
                button.pressed = True
            elif tech in self.acquired_tech:
                button.disabled = True
            self.tech_icons[tech] = button
            self.all_sprites.add(button)


    def toggle_tech_tree_icon(self, tech):
        prereqs = self.default_tech_tree[tech]["prerequisites"]
        for req in prereqs:
            if req not in self.acquired_tech:
                self.game_message = "Cannot research tech. Pre-requisites not met."
                self.last_displayed_message = pg.time.get_ticks()
                self.campaign_map_scene.play_sound_effect(self.deployment_fail_sound)
                return
        self.tech_icons[self.current_tech].pressed = False
        self.tech_icons[tech].pressed = True
        self.current_tech = tech
        if tech not in self.tech_tree:
            self.tech_tree[tech] = 0


    def calculate_turns_remaining(self, tech):
        learning_rate = self.campaign_map_scene.calculate_learning_rate(self.campaign_map_scene.player_country)
        if tech in self.tech_tree:
            turns_remaining = math.ceil(self.default_tech_tree[tech]["turns"]/learning_rate) - self.tech_tree[tech]
        else:
            turns_remaining = math.ceil(self.default_tech_tree[tech]["turns"] / learning_rate)

        if turns_remaining <= 0:
            if tech in self.campaign_map_scene.acquired_tech[self.campaign_map_scene.player_country]:
                turns_remaining = 0
            else:
                turns_remaining = 1
        return turns_remaining


    def initiate_transition(self, direction):
        if direction == "in":
            self.transition = FadeBlackTransition(80, "in")
            self.transition_sprites.add(self.transition)
        else:
            self.transition = FadeBlackTransition(80, "out")
            self.transition_sprites.add(self.transition)


    def transition_clean_up(self):
        self.transition.kill()
        self.transition = None
        self.post_transition_function = None
        self.playing = False
        self.running = False


    def run(self):
        while self.playing:
            if self.running:
                self.clock.tick(FPS)
                self.events()
                self.update()
                self.draw()


    def events(self):
        pos = pg.mouse.get_pos()

        # gold icon or food icon hover
        if self.gold_icon.rect.collidepoint(pos):
            self.gold_icon.hovering = True
            if self.gold_icon.tooltip_background:
                self.game_tooltip_description = "Projected Tax/Building Income: {}\nProjected Trade Revenue: {}\nProjected Upkeep Cost: {}\nProjected Net Income: {}"
        else:
            self.gold_icon.hovering = False
            if self.food_icon.rect.collidepoint(pos):
                self.food_icon.hovering = True
                if self.food_icon.tooltip_background:
                    self.game_tooltip_description = "Projected Food Production: {}\nProjected Food Consumption: {}\nProjected Food Surplus: {}"
            else:
                self.food_icon.hovering = False
                self.game_tooltip_description = ""

        for event in pg.event.get():
            # quit
            if event.type == pg.QUIT:
                choice = pg.display.message_box("Quit",
                                                "Are you sure you wish to quit? You will lose any unsaved progress.",
                                                buttons=("Yes", "No"), return_button=1, escape_button=None
                                                )

                if choice == 0:
                    pg.quit()
                    sys.exit()

            # Left clicks
            if event.type == pg.MOUSEBUTTONUP:
                self.campaign_map_scene.play_sound_effect(self.click_sound)

                if self.back_button.rect.collidepoint(pos):
                    self.return_to_campaign_map()

                for tech, button in self.tech_icons.items():
                    if button.rect.collidepoint(pos) and not button.disabled:
                        button.hovering = False
                        if button.tooltip_bg:
                            button.kill_tooltip()
                        button.command()

            #key presses
            if event.type == pg.KEYUP:
                if event.key == pg.K_f and DEMO:
                    if pg.FULLSCREEN and self.screen.get_flags():
                        pg.display.set_mode((self.game_width, self.game_height))
                    else:
                        pg.display.set_mode((self.game_width, self.game_height), pg.FULLSCREEN|pg.DOUBLEBUF)
                if event.key == pg.K_ESCAPE:
                    self.return_to_campaign_map()

        # Hover
        hovering_over_button = False
        for tech, button in self.tech_icons.items():
            if button.rect.collidepoint(pos):
                button.hovering = True
                hovering_over_button = True
                if button.tooltip_bg:
                    self.tooltip_description = button.tooltip
                    self.tooltip_description_x, self.tooltip_description_y = button.tooltip_bg.rect.left, button.tooltip_bg.rect.top
            else:
                button.hovering = False
                if button.tooltip_bg:
                    button.kill_tooltip()

        if not hovering_over_button:
            self.tooltip_description = ""
            self.tooltip_description_x = -3000
            self.tooltip_description_y = -3000


    def return_to_campaign_map(self):
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = self.return_to_campaign_map
        elif not self.transition.fading:
            self.transition_clean_up()
            campaign_data = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "tech_tree_autosave.p"), "rb"))
            campaign_data["tech_tree"][self.campaign_map_scene.player_country] = self.tech_tree
            campaign_data["current_tech"][self.campaign_map_scene.player_country] = self.current_tech
            campaign_data["acquired_tech"][self.campaign_map_scene.player_country] = self.acquired_tech
            pickle.dump(campaign_data, open(os.path.join(SAVE_SLOTS_DIR, "tech_tree_autosave.p"), "wb"))

            for sp in self.all_sprites:
                sp.kill()
            self.campaign_map_scene.new(load_from="tech_tree_autosave.p")


    def update(self):
        now = pg.time.get_ticks()
        self.all_sprites.update()
        if now - self.last_displayed_message > 2000:
            self.game_message = ""
        if self.transition:
            if self.transition.fading:
                self.transition.update()
            else:
                if self.post_transition_function:
                    self.post_transition_function()
                else:
                    self.transition = None


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)

            estimated_gold_delta = self.campaign_map_scene.calculate_projected_gold_delta(self.campaign_map_scene.player_country)
            estimated_food_delta = self.campaign_map_scene.calculate_projected_food_delta(self.campaign_map_scene.player_country)
            gold_label = "{} ({})".format(self.campaign_map_scene.gold[self.campaign_map_scene.player_country], estimated_gold_delta)
            food_label = "{} ({})".format(self.campaign_map_scene.food[self.campaign_map_scene.player_country], estimated_food_delta)
            gold_label_color = WHITE
            food_label_color = WHITE
            if estimated_gold_delta < 0:
                gold_label_color = DARK_RED
            if estimated_food_delta < 0:
                food_label_color = DARK_RED

            self.draw_text(gold_label, FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE - len(gold_label) // 6), gold_label_color,
                           55, 28, align="midleft")
            self.draw_text(food_label, FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE - len(food_label) // 6), food_label_color,
                           270, 27, align="midleft")

            current_tech_info = self.default_tech_tree[self.current_tech]
            current_tech_turns_remaining = self.calculate_turns_remaining(self.current_tech)

            self.draw_text("{}".format(current_tech_turns_remaining), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                           current_tech_info["coords"][0]*100+175, current_tech_info["coords"][1]*100+125,
                           align="center")

            if self.learning_rate_bonus >= 50:
                learning_rate_text_color = GREEN
            elif self.learning_rate_bonus > 20:
                learning_rate_text_color = YELLOW
            else:
                learning_rate_text_color = RED

            self.draw_text("Current learning rate bonus: {}%".format(self.learning_rate_bonus),
                           FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), learning_rate_text_color,
                           900, 600, align="center")


            self.draw_text(self.game_message, FONT_FUNCTION(DEFAULT_FONT, 24), ORANGE,
                           LANDSCAPE_WIDTH//2, LANDSCAPE_HEIGHT-150, align="center")


            if self.tooltip_description:
                self.priority_sprites.draw(self.screen)
                counter = 1
                for line in self.tooltip_description.split("\n"):
                    self.draw_text(line, FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                   self.tooltip_description_x + 16, self.tooltip_description_y + counter*20,
                                   align="midleft")
                    counter += 1


            if self.game_tooltip_description:
                self.priority_sprites.draw(self.screen)
                if "Income" in self.game_tooltip_description:
                    projected_tax_and_additional_income = self.campaign_map_scene.calculate_tax_and_additional_income(self.campaign_map_scene.player_country)
                    projected_trade_revenue = self.campaign_map_scene.calculate_total_trade_revenue(self.campaign_map_scene.player_country)
                    projected_upkeep_cost = self.campaign_map_scene.calculate_upkeep_cost(self.campaign_map_scene.player_country)
                    projected_total_income = self.campaign_map_scene.calculate_projected_gold_delta(self.campaign_map_scene.player_country)
                    tooltip_description_split = self.game_tooltip_description.split("\n")

                    self.draw_text(tooltip_description_split[0].format(projected_tax_and_additional_income),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                   self.gold_icon.tooltip_background.rect.left + 20,
                                   self.gold_icon.tooltip_background.rect.top + 30, align="midleft")

                    self.draw_text(tooltip_description_split[1].format(projected_trade_revenue),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                   self.gold_icon.tooltip_background.rect.left + 20,
                                   self.gold_icon.tooltip_background.rect.top + 60, align="midleft")

                    self.draw_text(tooltip_description_split[2].format(projected_upkeep_cost),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), RED,
                                   self.gold_icon.tooltip_background.rect.left + 20,
                                   self.gold_icon.tooltip_background.rect.top + 90, align="midleft")

                    if projected_total_income >= 0:
                        self.draw_text(tooltip_description_split[3].format(projected_total_income),
                                       FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                       self.gold_icon.tooltip_background.rect.left + 20,
                                       self.gold_icon.tooltip_background.rect.top + 120, align="midleft")
                    else:
                        self.draw_text(tooltip_description_split[3].format(projected_total_income),
                                       FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), RED,
                                       self.gold_icon.tooltip_background.rect.left + 20,
                                       self.gold_icon.tooltip_background.rect.top + 120, align="midleft")

                else:
                    projected_food_production = self.campaign_map_scene.calculate_food_production(self.campaign_map_scene.player_country)
                    projected_food_consumption = self.campaign_map_scene.calculate_food_consumption(self.campaign_map_scene.player_country)
                    projected_food_surplus = self.campaign_map_scene.calculate_projected_food_delta(self.campaign_map_scene.player_country)
                    tooltip_description_split = self.game_tooltip_description.split("\n")

                    self.draw_text(tooltip_description_split[0].format(projected_food_production),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                   self.food_icon.tooltip_background.rect.left + 20,
                                   self.food_icon.tooltip_background.rect.top + 30, align="midleft")

                    self.draw_text(tooltip_description_split[1].format(projected_food_consumption),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), RED,
                                   self.food_icon.tooltip_background.rect.left + 20,
                                   self.food_icon.tooltip_background.rect.top + 60, align="midleft")

                    if projected_food_surplus >= 0:
                        self.draw_text(tooltip_description_split[2].format(projected_food_surplus),
                                       FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                       self.food_icon.tooltip_background.rect.left + 20,
                                       self.food_icon.tooltip_background.rect.top + 90, align="midleft")
                    else:
                        self.draw_text(tooltip_description_split[2].format(projected_food_surplus),
                                       FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), RED,
                                       self.food_icon.tooltip_background.rect.left + 20,
                                       self.food_icon.tooltip_background.rect.top + 90, align="midleft")


            self.transition_sprites.draw(self.screen)
            pg.display.flip()


    def draw_text(self, text, font, color, x, y, align="midtop"):
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        if align == "midleft":
            text_rect.midleft = (x, y)
        elif align == "topleft":
            text_rect.topleft = (x, y)
        elif align == "center":
            text_rect.center = (x, y)
        else:
            text_rect.midtop = (x, y)
        self.screen.blit(text_surface, text_rect)


    def drawWrappedText(self, text, color, rect, font, aa=False, bkg=None):
        rect = pg.Rect(rect)
        y = rect.top
        lineSpacing = 10

        # get the height of the font
        fontHeight = font.size("Tg")[1]

        while text:
            i = 1

            # determine if the row of text will be outside our area
            if y + fontHeight > rect.bottom:
                break

            # determine maximum width of line
            while font.size(text[:i])[0] < rect.width and i < len(text):
                i += 1

            # if we've wrapped the text, then adjust the wrap to the last word
            if i < len(text):
                i = text.rfind(" ", 0, i) + 1

            # render the line and blit it to the surface
            if bkg:
                image = font.render(text[:i], 1, color, bkg)
                image.set_colorkey(bkg)
            else:
                image = font.render(text[:i], aa, color)

            self.screen.blit(image, (rect.left, y))
            y += fontHeight + lineSpacing

            # remove the text we just blitted
            text = text[i:]

        return text