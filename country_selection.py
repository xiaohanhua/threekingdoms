from campaign_map import *
from game_classes import *
from functools import partial
from pyvidplayer import Video

class Country_Select_Scene:
    def __init__(self, main_menu):
        self.main_menu = main_menu
        self.game_width, self.game_height = LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT
        if DEMO:
            self.screen = pg.display.set_mode((self.game_width, self.game_height))
            os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (50, 50)
        else:
            self.screen = pg.display.set_mode((self.game_width, self.game_height), pg.FULLSCREEN|pg.DOUBLEBUF)
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()

        pg.mixer.init()
        pg.font.init()
        self.hover_sound = pg.mixer.Sound("sfx_hover.ogg")
        self.click_sound = pg.mixer.Sound("sfx_click.ogg")


    def new(self):
        self.running = True
        self.playing = True
        pg.mouse.set_visible(True)
        self.all_sprites = pg.sprite.Group()
        self.priority_sprites = pg.sprite.Group()
        self.fixed_sprites = pg.sprite.Group()
        self.buttons = pg.sprite.Group()

        #tooltips
        self.tooltip_description = ""
        self.tooltip_description_x, self.tooltip_description_y = -3000, -3000

        #transitions
        self.transition_sprites = pg.sprite.Group()
        self.transition = None
        self.post_transition_function = None
        self.initiate_transition("in")


        self.available_countries = ["Liu Biao", "Liu Zhang", "Ma Teng", "Shu", "Wei", "Wu", "Yuan Shao", "Zhang Lu"]
        self.selected_country_index = 0
        self.selected_country = self.available_countries[self.selected_country_index]
        self.difficulty = 2
        self.show_advice = False

        self.all_country_icons = []
        for country in self.available_countries:
            self.all_country_icons.append(pg.image.load("sprite_country_{}.png".format(country.lower())).convert_alpha())

        self.background = Static(0, self.game_height - 650, "sprite_menu_bg.png")
        self.all_sprites.add(self.background)

        self.black_bg = Static(LANDSCAPE_WIDTH//2, LANDSCAPE_HEIGHT//2-15, "sprite_battle_preview_bg.png", anchor="center")
        self.all_sprites.add(self.black_bg)

        self.selected_country_icon = Static(200, 100, "sprite_country_{}.png".format(self.selected_country.lower()))
        self.all_sprites.add(self.selected_country_icon)


        #buttons
        self.toggle_country_left_button = Button(200, 400, "menu_button_left.png",
                                      command=partial(self.toggle_country, -1))
        self.buttons.add(self.toggle_country_left_button)
        self.all_sprites.add(self.toggle_country_left_button)


        self.toggle_country_right_button = Button(450, 400, "menu_button_right.png",
                                      command=partial(self.toggle_country, 1))
        self.buttons.add(self.toggle_country_right_button)
        self.all_sprites.add(self.toggle_country_right_button)


        self.toggle_difficulty_button = Button(320, 525, "sprite_ai_difficulty_bar.png", anchor="topleft",
                                                  command=self.toggle_ai_difficulty, tooltip=DIFFICULTY_DESCRIPTION,
                                                  master=self)
        self.buttons.add(self.toggle_difficulty_button)
        self.all_sprites.add(self.toggle_difficulty_button)

        self.difficulty_toggle = Static(self.toggle_difficulty_button.rect.left + self.toggle_difficulty_button.image.get_width()/3*(self.difficulty-1) - self.difficulty*2,
                                        526, "sprite_ai_difficulty_toggle.png", anchor="topleft")
        self.all_sprites.add(self.difficulty_toggle)


        self.toggle_show_advice_button = Button(690, 494, "sprite_check_button.png", anchor="topleft",
                                                  command=self.toggle_show_advice, is_checkbutton=True)
        self.buttons.add(self.toggle_show_advice_button)
        self.all_sprites.add(self.toggle_show_advice_button)
        self.toggle_show_advice_button.pressed = self.show_advice



        self.start_game_button = Button(LANDSCAPE_WIDTH*2//3, LANDSCAPE_HEIGHT-30, "menu_button_start_game.png",
                                  command=self.load_campaign, scale=(150,40))
        self.buttons.add(self.start_game_button)
        self.all_sprites.add(self.start_game_button)

        self.back_button = Button(LANDSCAPE_WIDTH // 3, LANDSCAPE_HEIGHT - 30, "menu_button_back.png",
                                  command=self.back_to_menu, scale=(150,40))
        self.buttons.add(self.back_button)
        self.all_sprites.add(self.back_button)


        self.skip_intro_vid_button = Static(-30000, -30000, "sprite_skip_button.png", anchor="topleft",
                                            scale=(50, 50))
        self.fixed_sprites.add(self.skip_intro_vid_button)

        self.run()


    def initiate_transition(self, direction):
        if direction == "in":
            self.transition = FadeBlackTransition(45, "in")
            self.transition_sprites.add(self.transition)
        else:
            self.transition = FadeBlackTransition(45, "out")
            self.transition_sprites.add(self.transition)


    def transition_clean_up(self):
        self.transition.kill()
        self.transition = None
        self.post_transition_function = None
        self.playing = False
        self.running = False
        for s in self.all_sprites:
            s.kill()


    def back_to_menu(self):
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = self.back_to_menu
        elif not self.transition.fading:
            self.transition_clean_up()
            self.main_menu.new(start_music=False)


    def load_campaign(self):
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = self.load_campaign
        elif not self.transition.fading:
            self.transition_clean_up()

            #Shu
            zhang_fei = Commander(name="Zhang Fei", simple_name="zhang_fei", age=36, loyalty=100,
                                            country="Shu", combat_prowess=100, intellect=62, command=88, charisma=25,
                                            action_points=20, level=1, experience=0, upgrade_points=0)
            guan_yu = Commander(name="Guan Yu", simple_name="guan_yu", age=41, loyalty=100,
                                  country="Shu", combat_prowess=98, intellect=75, command=95,  charisma=85,
                                  action_points=20, level=1, experience=0, upgrade_points=0)
            zhao_yun = Commander(name="Zhao Yun", simple_name="zhao_yun", age=41, loyalty=100,
                                  country="Shu", combat_prowess=99, intellect=75, command=85, charisma=80,
                                  action_points=24, level=1, experience=0, upgrade_points=0)
            mi_zhu = Commander(name="Mi Zhu", simple_name="mi_zhu", age=42, loyalty=95,
                                    country="Shu", combat_prowess=35, intellect=70, command=30, charisma=86,
                                    action_points=14, level=1, experience=0, upgrade_points=0)
            jian_yong = Commander(name="Jian Yong", simple_name="jian_yong", age=38, loyalty=95,
                                    country="Shu", combat_prowess=29, intellect=78, command=27, charisma=93,
                                    action_points=18, level=1, experience=0, upgrade_points=0)
            sun_qian = Commander(name="Sun Qian", simple_name="sun_qian", age=45, loyalty=90,
                                            country="Shu", combat_prowess=35, intellect=79, command=38, charisma=87,
                                            action_points=18, level=1, experience=0, upgrade_points=0)

            #Wei
            xu_chu = Commander(name="Xu Chu", simple_name="xu_chu", age=29, loyalty=100,
                                    country="Wei", combat_prowess=96, intellect=40, command=75, charisma=20,
                                    action_points=20, level=1, experience=0, upgrade_points=0)
            xu_huang = Commander(name="Xu Huang", simple_name="xu_huang", age=34, loyalty=90,
                                    country="Wei", combat_prowess=87, intellect=60, command=80, charisma=76,
                                    action_points=20, level=1, experience=0, upgrade_points=0)
            xiahou_yuan = Commander(name="Xiahou Yuan", simple_name="xiahou_yuan", age=38, loyalty=100,
                                         country="Wei", combat_prowess=78, intellect=60, command=72, charisma=72,
                                         action_points=30, level=1, experience=0, upgrade_points=0)
            xiahou_dun = Commander(name="Xiahou Dun", simple_name="xiahou_dun", age=43, loyalty=100,
                                         country="Wei", combat_prowess=81, intellect=50, command=65, charisma=90,
                                         action_points=20, level=1, experience=0, upgrade_points=0)
            zhang_liao = Commander(name="Zhang Liao", simple_name="zhang_liao", age=31, loyalty=90,
                                         country="Wei", combat_prowess=88, intellect=82, command=90, charisma=88,
                                         action_points=22, level=1, experience=0, upgrade_points=0)
            yu_jin = Commander(name="Yu Jin", simple_name="yu_jin", age=39, loyalty=65,
                                   country="Wei", combat_prowess=74, intellect=65, command=87, charisma=50,
                                   action_points=21, level=1, experience=0, upgrade_points=0)
            yue_jin = Commander(name="Yue Jin", simple_name="yue_jin", age=35, loyalty=85,
                                   country="Wei", combat_prowess=80, intellect=60, command=90, charisma=55,
                                   action_points=20, level=1, experience=0, upgrade_points=0)
            cheng_yu = Commander(name="Cheng Yu", simple_name="cheng_yu", age=59, loyalty=85,
                                   country="Wei", combat_prowess=20, intellect=91, command=77, charisma=70,
                                   action_points=17, level=1, experience=0, upgrade_points=0)
            guo_jia = Commander(name="Guo Jia", simple_name="guo_jia", age=30, loyalty=95,
                                   country="Wei", combat_prowess=30, intellect=100, command=84, charisma=92,
                                   action_points=16, level=1, experience=0, upgrade_points=0)
            xun_yu = Commander(name="Xun Yu", simple_name="xun_yu", age=37, loyalty=95,
                                   country="Wei", combat_prowess=25, intellect=95, command=54, charisma=97,
                                   action_points=16, level=1, experience=0, upgrade_points=0)
            xun_you = Commander(name="Xun You", simple_name="xun_you", age=43, loyalty=95,
                                   country="Wei", combat_prowess=25, intellect=94, command=73, charisma=87,
                                   action_points=16, level=1, experience=0, upgrade_points=0)
            cao_ren = Commander(name="Cao Ren", simple_name="cao_ren", age=32, loyalty=95,
                                   country="Wei", combat_prowess=77, intellect=61, command=90, charisma=82,
                                   action_points=20, level=1, experience=0, upgrade_points=0)
            cao_hong = Commander(name="Cao Hong", simple_name="cao_hong", age=28, loyalty=95,
                                   country="Wei", combat_prowess=67, intellect=53, command=80, charisma=56,
                                   action_points=21, level=1, experience=0, upgrade_points=0)
            li_dian = Commander(name="Li Dian", simple_name="li_dian", age=21, loyalty=85,
                                   country="Wei", combat_prowess=77, intellect=73, command=78, charisma=70,
                                   action_points=20, level=1, experience=0, upgrade_points=0)
            kong_rong = Commander(name="Kong Rong", simple_name="kong_rong", age=49, loyalty=75,
                                   country="Wei", combat_prowess=25, intellect=72, command=5, charisma=70,
                                   action_points=18, level=1, experience=0, upgrade_points=0)

            #Wu
            zhou_yu = Commander(name="Zhou Yu", simple_name="zhou_yu", age=27, loyalty=100,
                                country="Wu", combat_prowess=65, intellect=98, command=95, charisma=100,
                                action_points=26, level=1, experience=0, upgrade_points=0)
            han_dang = Commander(name="Han Dang", simple_name="han_dang", age=35, loyalty=90,
                                         country="Wu", combat_prowess=82, intellect=50, command=84, charisma=50,
                                         action_points=20, level=1, experience=0, upgrade_points=0)
            huang_gai = Commander(name="Huang Gai", simple_name="huang_gai", age=46, loyalty=90,
                                             country="Wu", combat_prowess=86, intellect=55, command=85, charisma=70,
                                             action_points=20, level=1, experience=0, upgrade_points=0)
            taishi_ci = Commander(name="Taishi Ci", simple_name="taishi_ci", age=34, loyalty=94,
                                             country="Wu", combat_prowess=97, intellect=69, command=86, charisma=90,
                                             action_points=20, level=1, experience=0, upgrade_points=0)
            cheng_pu = Commander(name="Cheng Pu", simple_name="cheng_pu", age=53, loyalty=95,
                                             country="Wu", combat_prowess=82, intellect=79, command=85, charisma=81,
                                             action_points=20, level=1, experience=0, upgrade_points=0)
            zhou_tai = Commander(name="Zhou Tai", simple_name="zhou_tai", age=37, loyalty=95,
                                country="Wu", combat_prowess=92, intellect=58, command=77, charisma=56,
                                action_points=20, level=1, experience=0, upgrade_points=0)
            zhang_zhao = Commander(name="Zhang Zhao", simple_name="zhang_zhao", age=44, loyalty=90,
                                             country="Wu", combat_prowess=25, intellect=87, command=50, charisma=81,
                                             action_points=18, level=1, experience=0, upgrade_points=0)
            yu_fan = Commander(name="Yu Fan", simple_name="yu_fan", age=36, loyalty=85,
                                             country="Wu", combat_prowess=40, intellect=87, command=56, charisma=69,
                                             action_points=18, level=1, experience=0, upgrade_points=0)

            #Yuan Shao
            zhang_he = Commander(name="Zhang He", simple_name="zhang_he", age=31, loyalty=80,
                                         country="Yuan Shao", combat_prowess=91, intellect=90, command=84, charisma=75,
                                         action_points=20, level=1, experience=0, upgrade_points=0)

            yan_liang = Commander(name="Yan Liang", simple_name="yan_liang", age=39, loyalty=85,
                                 country="Yuan Shao", combat_prowess=94, intellect=35, command=80, charisma=35,
                                 action_points=20, level=1, experience=0, upgrade_points=0)

            wen_chou = Commander(name="Wen Chou", simple_name="wen_chou", age=38, loyalty=85,
                                 country="Yuan Shao", combat_prowess=94, intellect=44, command=80, charisma=35,
                                 action_points=20, level=1, experience=0, upgrade_points=0)

            gao_lan = Commander(name="Gao Lan", simple_name="gao_lan", age=42, loyalty=80,
                                 country="Yuan Shao", combat_prowess=92, intellect=50, command=75, charisma=45,
                                 action_points=20, level=1, experience=0, upgrade_points=0)

            chunyu_qiong = Commander(name="Chunyu Qiong", simple_name="chunyu_qiong", age=44, loyalty=85,
                                 country="Yuan Shao", combat_prowess=68, intellect=52, command=72, charisma=49,
                                 action_points=20, level=1, experience=0, upgrade_points=0)

            guo_tu = Commander(name="Guo Tu", simple_name="guo_tu", age=34, loyalty=85,
                                 country="Yuan Shao", combat_prowess=43, intellect=81, command=75, charisma=59,
                                 action_points=19, level=1, experience=0, upgrade_points=0)

            shen_pei = Commander(name="Shen Pei", simple_name="shen_pei", age=36, loyalty=100,
                                         country="Yuan Shao", combat_prowess=60, intellect=83, command=84, charisma=71,
                                         action_points=19, level=1, experience=0, upgrade_points=0)

            tian_feng = Commander(name="Tian Feng", simple_name="tian_feng", age=48, loyalty=90,
                                 country="Yuan Shao", combat_prowess=32, intellect=90, command=60, charisma=46,
                                 action_points=18, level=1, experience=0, upgrade_points=0)

            xu_you = Commander(name="Xu You", simple_name="xu_you", age=43, loyalty=50,
                                 country="Yuan Shao", combat_prowess=25, intellect=78, command=45, charisma=36,
                                 action_points=17, level=1, experience=0, upgrade_points=0)

            ju_shou = Commander(name="Ju Shou", simple_name="ju_shou", age=46, loyalty=95,
                                 country="Yuan Shao", combat_prowess=32, intellect=88, command=72, charisma=83,
                                 action_points=18, level=1, experience=0, upgrade_points=0)

            #TODO: add Shen Pei, Chen Lin

            #Ma Teng
            ma_chao = Commander(name="Ma Chao", simple_name="ma_chao", age=24, loyalty=75,
                                  country="Ma Teng", combat_prowess=97, intellect=70, command=90, charisma=84,
                                  action_points=25, level=1, experience=0, upgrade_points=0)
            pang_de = Commander(name="Pang De", simple_name="pang_de", age=30, loyalty=95,
                                  country="Ma Teng", combat_prowess=96, intellect=70, command=90, charisma=74,
                                  action_points=25, level=1, experience=0, upgrade_points=0)
            ma_dai = Commander(name="Ma Dai", simple_name="ma_dai", age=22, loyalty=80,
                                  country="Ma Teng", combat_prowess=81, intellect=70, command=86, charisma=80,
                                  action_points=23, level=1, experience=0, upgrade_points=0)
            han_sui = Commander(name="Han Sui", simple_name="han_sui", age=55, loyalty=65,
                                  country="Ma Teng", combat_prowess=70, intellect=75, command=86, charisma=84,
                                  action_points=18, level=1, experience=0, upgrade_points=0)


            #Zhang Lu
            zhang_wei = Commander(name="Zhang Wei", simple_name="zhang_wei", age=56, loyalty=95,
                                  country="Zhang Lu", combat_prowess=70, intellect=50, command=60, charisma=54,
                                  action_points=20, level=1, experience=0, upgrade_points=0)
            yang_bo = Commander(name="Yang Bo", simple_name="yang_bo", age=29, loyalty=92,
                                  country="Zhang Lu", combat_prowess=40, intellect=45, command=50, charisma=33,
                                  action_points=20, level=1, experience=0, upgrade_points=0)
            yang_ren = Commander(name="Yang Ren", simple_name="yang_ren", age=38, loyalty=89,
                                  country="Zhang Lu", combat_prowess=75, intellect=48, command=72, charisma=38,
                                  action_points=20, level=1, experience=0, upgrade_points=0)
            yan_pu = Commander(name="Yan Pu", simple_name="yan_pu", age=27, loyalty=90,
                                  country="Zhang Lu", combat_prowess=36, intellect=82, command=40, charisma=83,
                                  action_points=19, level=1, experience=0, upgrade_points=0)


            #Liu Biao
            cai_mao = Commander(name="Cai Mao", simple_name="cai_mao", age=45, loyalty=75,
                                country="Liu Biao", combat_prowess=69, intellect=74, command=70, charisma=64,
                                action_points=20, level=1, experience=0, upgrade_points=0)
            zhang_yun = Commander(name="Zhang Yun", simple_name="zhang_yun", age=45, loyalty=75,
                                country="Liu Biao", combat_prowess=63, intellect=44, command=75, charisma=47,
                                action_points=20, level=1, experience=0, upgrade_points=0)
            huang_zu = Commander(name="Huang Zu", simple_name="huang_zu", age=54, loyalty=85,
                                country="Liu Biao", combat_prowess=59, intellect=54, command=75, charisma=35,
                                action_points=20, level=1, experience=0, upgrade_points=0)
            huang_zhong = Commander(name="Huang Zhong", simple_name="huang_zhong", age=56, loyalty=80,
                                country="Liu Biao", combat_prowess=92, intellect=65, command=85, charisma=65,
                                action_points=20, level=1, experience=0, upgrade_points=0)
            wei_yan = Commander(name="Wei Yan", simple_name="wei_yan", age=34, loyalty=70,
                                 country="Liu Biao", combat_prowess=88, intellect=75,
                                 command=80, charisma=50, action_points=21, level=1,
                                 experience=0, upgrade_points=0)
            wen_pin = Commander(name="Wen Pin", simple_name="wen_pin", age=36, loyalty=80,
                                country="Liu Biao", combat_prowess=79, intellect=60, command=82, charisma=68,
                                action_points=20, level=1, experience=0, upgrade_points=0)
            kuai_liang = Commander(name="Kuai Liang", simple_name="kuai_liang", age=28, loyalty=80,
                                country="Liu Biao", combat_prowess=43, intellect=87, command=60, charisma=77,
                                action_points=19, level=1, experience=0, upgrade_points=0)
            kuai_yue = Commander(name="Kuai Yue", simple_name="kuai_yue", age=26, loyalty=80,
                                country="Liu Biao", combat_prowess=31, intellect=82, command=49, charisma=83,
                                action_points=18, level=1, experience=0, upgrade_points=0)

            # Liu Zhang
            meng_da = Commander(name="Meng Da", simple_name="meng_da", age=34, loyalty=60,
                                country="Liu Zhang", combat_prowess=60, intellect=62, command=67, charisma=32,
                                action_points=20, level=1, experience=0, upgrade_points=0)

            yan_yan = Commander(name="Yan Yan", simple_name="yan_yan", age=47, loyalty=90,
                                country="Liu Zhang", combat_prowess=63, intellect=70, command=69, charisma=82,
                                action_points=19, level=1, experience=0, upgrade_points=0)

            wu_yi = Commander(name="Wu Yi", simple_name="wu_yi", age=32, loyalty=70,
                                country="Liu Zhang", combat_prowess=68, intellect=65, command=76, charisma=79,
                                action_points=21, level=1, experience=0, upgrade_points=0)

            fa_zheng = Commander(name="Fa Zheng", simple_name="fa_zheng", age=24, loyalty=70,
                                country="Liu Zhang", combat_prowess=41, intellect=95, command=84, charisma=38,
                                action_points=20, level=1, experience=0, upgrade_points=0)

            huang_quan = Commander(name="Huang Quan", simple_name="huang_quan", age=26, loyalty=95,
                                country="Liu Zhang", combat_prowess=48, intellect=81, command=75, charisma=86,
                                action_points=20, level=1, experience=0, upgrade_points=0)


            #set initial game settings based on difficulty
            #ry:
            #    self.difficulty = int(open("difficulty.txt", "r").read())
            #    if self.difficulty not in range(1,4):
            #        self.difficulty = 2
            #except:
            #    self.difficulty = 2


            initial_gold = {"Shu": 14000, "Wu": 19000, "Wei": 27000, "Ma Teng":18500, "Liu Biao": 21000,
                            "Liu Zhang": 16000, "Zhang Lu": 17500, "Yuan Shao": 20000}
            initial_food = {"Shu": 400, "Wu": 900, "Wei": 1000, "Ma Teng":450, "Liu Biao": 800,
                            "Liu Zhang": 550, "Zhang Lu": 1500, "Yuan Shao": 950}
            initial_peace_relationships = {"Shu": {"Liu Biao":20, "Yuan Shao":10},
                                           "Wu": {},
                                           "Wei": {},
                                           "Ma Teng": {"Zhang Lu":10},
                                           "Liu Biao": {"Shu": 20},
                                           "Liu Zhang": {},
                                           "Zhang Lu": {"Ma Teng":10},
                                           "Yuan Shao": {"Shu":10}}
            initial_past_conflicts = {"Shu": {"Wei":1},
                                      "Wu": {"Liu Biao":2},
                                      "Wei": {"Shu":1, "Ma Teng":2, "Yuan Shao":3},
                                      "Ma Teng": {"Wei":2},
                                      "Liu Biao": {"Wu":2},
                                      "Liu Zhang": {},
                                      "Zhang Lu": {},
                                      "Yuan Shao": {"Wei":3}}


            for k,v in initial_gold.items():
                if k!= self.selected_country:
                    initial_gold[k] = v*self.difficulty

            for k,v in initial_food.items():
                if k!= self.selected_country:
                    initial_food[k] = v*self.difficulty


            MAP_GRID_WIDTH = 368
            self.game_data = {"camera_position": [LANDSCAPE_WIDTH//2,LANDSCAPE_HEIGHT//2],
                              "animation_delay": 30,
                              "army":[Army(MAP_GRID_WIDTH - 152, 53, commander=zhang_fei, army_composition= {"spear_militia":80, "sword_militia":100, "axe_militia":120}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(MAP_GRID_WIDTH - 156, 48, commander=guan_yu, army_composition= {"spear_militia":60, "sword_militia":80, "bow_militia":40}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(MAP_GRID_WIDTH - 152, 50, commander=zhao_yun, army_composition= {"spear_militia":100, "sword_militia":80, "bow_militia":60}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(MAP_GRID_WIDTH - 151, 44, commander=mi_zhu, army_composition={},action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(MAP_GRID_WIDTH - 153, 46, commander=jian_yong, army_composition={}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(MAP_GRID_WIDTH - 151, 45, commander=sun_qian, army_composition={}, action_points=-1, retreat_penalty=0, anchor="midleft"),

                                      #Wei
                                      Army(MAP_GRID_WIDTH-85, 121, commander=xu_huang, army_composition={"spear_militia":50, "sword_militia":75, "axe_militia":120, "bow_militia":120, "battleaxe_infantry":50}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(MAP_GRID_WIDTH-96, 89, commander=xu_chu, army_composition={"sword_infantry":90, "spear_militia":75, "axe_militia":100}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(199, 140, commander=xiahou_yuan, army_composition={"spear_militia":150, "sword_militia":120, "bow_militia":160}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(MAP_GRID_WIDTH-148, 78, commander=xiahou_dun, army_composition={"spear_infantry":100, "sword_militia":60, "bow_infantry":60}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(MAP_GRID_WIDTH-208, 99, commander=yu_jin, army_composition={"sword_militia":80, "spear_militia":200, "bow_militia":100}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(298, 120, commander=yue_jin, army_composition={"spear_militia":80, "bow_infantry":80, "sword_militia":80}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(290, 113, commander=zhang_liao, army_composition={"sword_militia":200, "spear_militia":100, "bow_militia":80}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(350, 111, commander=li_dian, army_composition={"sword_militia":150, "sword_infantry":30, "bow_infantry":30}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(344, 151, commander=cheng_yu, army_composition={"sword_militia":90, "spear_militia":90, "bow_militia":50}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(MAP_GRID_WIDTH - 146, 62, commander=cao_hong, army_composition={"sword_militia": 120, "spear_militia": 120}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(MAP_GRID_WIDTH-135, 92, commander=cao_ren, army_composition={"shielded_sword_infantry": 50, "spear_militia": 120, "bow_infantry": 60}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(MAP_GRID_WIDTH-100, 86, commander=guo_jia, army_composition={"spear_infantry":60, "sword_infantry":60, "axe_militia":100, "sword_militia":80}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(290, 120, commander=xun_yu, army_composition={}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(285, 121, commander=xun_you, army_composition={}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(293, 114, commander=kong_rong, army_composition={}, action_points=-1, retreat_penalty=0, anchor="midleft"),

                                      #Wu
                                      Army(250, 193, commander=zhou_yu, army_composition={"sword_militia":120, "spear_militia":120, "bow_militia":100}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(240, 190, commander=han_dang, army_composition={"sword_militia":100, "bow_militia":60, "spear_militia":80}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(239, 194, commander=cheng_pu, army_composition={"sword_infantry":50, "spear_infantry":40, "axe_militia":80}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(242, 196, commander=zhou_tai,army_composition={"sword_militia": 100, "bow_militia": 60,"axe_militia": 40}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(336, 182, commander=taishi_ci, army_composition={"sword_militia":120, "spear_militia":60, "bow_militia":40}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(340, 188, commander=huang_gai, army_composition={"spear_militia":50, "bow_militia":80, "sword_militia":120}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(358, 220, commander=zhang_zhao, army_composition={}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(248, 198, commander=yu_fan, army_composition={}, action_points=-1, retreat_penalty=0, anchor="midleft"),


                                      #Yuan Shao
                                      Army(MAP_GRID_WIDTH - 73, 60, commander=zhang_he, army_composition={"spear_militia": 100, "sword_militia": 200, "bow_militia": 80, "battleaxe_infantry": 40}, action_points=-1, retreat_penalty=0,anchor="midleft"),
                                      Army(MAP_GRID_WIDTH - 75, 68, commander=yan_liang, army_composition={"spear_infantry": 80, "sword_militia": 200}, action_points=-1, retreat_penalty=0,anchor="midleft"),
                                      Army(MAP_GRID_WIDTH - 59, 81, commander=wen_chou, army_composition={"spear_infantry": 40, "sword_militia": 160, "bow_militia": 80}, action_points=-1, retreat_penalty=0,anchor="midleft"),
                                      Army(MAP_GRID_WIDTH - 136, 40, commander=gao_lan, army_composition={"spear_militia": 120, "sword_infantry": 80}, action_points=-1, retreat_penalty=0,anchor="midleft"),
                                      Army(MAP_GRID_WIDTH - 28, 70, commander=chunyu_qiong, army_composition={"bow_infantry": 80, "battleaxe_infantry": 100}, action_points=-1, retreat_penalty=0,anchor="midleft"),
                                      Army(MAP_GRID_WIDTH - 156, 43, commander=guo_tu, army_composition={"axe_militia": 240, "sword_militia":150}, action_points=-1, retreat_penalty=0,anchor="midleft"),
                                      Army(MAP_GRID_WIDTH - 8, 10, commander=tian_feng, army_composition={}, action_points=-1, retreat_penalty=0,anchor="midleft"),
                                      Army(MAP_GRID_WIDTH - 33, 64, commander=xu_you, army_composition={}, action_points=-1, retreat_penalty=0,anchor="midleft"),
                                      Army(MAP_GRID_WIDTH - 36, 54, commander=shen_pei, army_composition={}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(MAP_GRID_WIDTH - 48, 72, commander=ju_shou, army_composition={"spear_militia": 300, "bow_militia": 160}, action_points=-1, retreat_penalty=0,anchor="midleft"),


                                      #Liu Zhang
                                      Army(48, 155, commander=meng_da, army_composition={"spear_infantry": 100, "sword_militia": 300, "axe_militia": 200, "bow_infantry": 80}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(130, 245, commander=yan_yan, army_composition={"spear_militia": 240, "bow_infantry": 80}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(135, 235, commander=wu_yi, army_composition={"axe_militia": 200, "sword_militia": 300}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(45, 156, commander=fa_zheng, army_composition={}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(42, 153, commander=huang_quan, army_composition={}, action_points=-1, retreat_penalty=0, anchor="midleft"),


                                      #Ma Teng
                                      Army(MAP_GRID_WIDTH-182, 61, commander=ma_chao, army_composition={"sword_militia": 100, "spear_militia": 120, "light_cavalry": 80}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(MAP_GRID_WIDTH-180, 52, commander=pang_de, army_composition={"sword_militia": 100, "spear_militia": 80, "light_cavalry": 60, "bow_militia":40}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(MAP_GRID_WIDTH-300, 24, commander=ma_dai, army_composition={"sword_militia": 120, "spear_militia": 100, "light_cavalry": 40}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(MAP_GRID_WIDTH-185, 54, commander=han_sui, army_composition={"sword_militia": 100, "spear_militia": 80, "bow_militia": 80, "light_cavalry": 60}, action_points=-1, retreat_penalty=0,anchor="midleft"),

                                      #Zhang Lu
                                      Army(50, 94, commander=zhang_wei,army_composition={"spear_militia": 250, "sword_militia": 420, "bow_militia": 160}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(105, 140, commander=yang_bo,army_composition={"spear_militia": 300, "bow_militia": 400}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(110, 141, commander=yang_ren,army_composition={"spear_militia": 100, "bow_militia": 100, "sword_militia":100, "axe_militia":100}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(98, 138, commander=yan_pu,army_composition={}, action_points=-1, retreat_penalty=0, anchor="midleft"),


                                      #Liu Biao
                                      Army(215, 208, commander=huang_zhong, army_composition= {"spear_militia":40, "sword_militia":40, "bow_infantry":80}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(180, 196, commander=wei_yan, army_composition= {"spear_militia":100, "sword_militia":160, "bow_militia":120}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(175, 194, commander=kuai_liang, army_composition= {}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(184, 190, commander=kuai_yue, army_composition= {}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(220, 200, commander=cai_mao, army_composition= {"spear_militia":80, "sword_militia":120, "bow_militia":80}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(218, 203, commander=zhang_yun, army_composition= {"spear_militia":80, "sword_militia":140, "bow_militia":60}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(182, 197, commander=huang_zu, army_composition= {"spear_militia":200, "bow_militia":240}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      Army(118, 170, commander=wen_pin, army_composition= {"spear_militia":80, "sword_militia":100, "bow_infantry":40}, action_points=-1, retreat_penalty=0, anchor="midleft"),
                                      ],

                              "city":[City(grid_position_x=MAP_GRID_WIDTH-47, grid_position_y=74, name="Ping Yuan", fortress_level=1,
                                      country="Yuan Shao", population=11800, population_growth=1, happiness=2, defender=[],
                                      anchor="center", raw_buildings={"Walls":2, "Barracks":1, "Farm":2, "Market":2}),

                                      City(grid_position_x=MAP_GRID_WIDTH-136, grid_position_y=45, name="Shang Dang", fortress_level=2,
                                           country="Yuan Shao", population=12900, population_growth=1, happiness=2,
                                           defender=[], anchor="center",
                                           raw_buildings={"Walls": 1, "Barracks": 1, "Farm": 3, "Market": 3}),

                                      City(grid_position_x=MAP_GRID_WIDTH - 121, grid_position_y=13, name="Jin Yang",
                                           fortress_level=3, country="Yuan Shao", population=16780, population_growth=2,
                                           happiness=2, defender=[], anchor="center",
                                           raw_buildings={"Walls": 3, "Barracks": 1, "Farm": 3, "Market": 3}),

                                      City(grid_position_x=MAP_GRID_WIDTH - 42, grid_position_y=19, name="Yanmen Pass",
                                           fortress_level=1,
                                           country="Yuan Shao", population=3330, population_growth=1, happiness=1,
                                           defender=[], anchor="center",
                                           raw_buildings={"Walls": 3, "Barracks": 1, "Farm": 1, "Market": 1}),

                                      City(grid_position_x=MAP_GRID_WIDTH - 76, grid_position_y=54, name="Yu She",
                                           fortress_level=1,
                                           country="Yuan Shao", population=8810, population_growth=2, happiness=2,
                                           defender=[], anchor="center",
                                           raw_buildings={"Walls": 1, "Barracks": 1, "Farm": 2, "Market": 2}),

                                      City(grid_position_x=MAP_GRID_WIDTH - 9, grid_position_y=14, name="Bei Ping",
                                           fortress_level=1,
                                           country="Yuan Shao", population=18700, population_growth=2, happiness=3,
                                           defender=[], anchor="center",
                                           raw_buildings={"Walls": 3, "Barracks": 2, "Farm": 2, "Market": 4}),

                                      City(grid_position_x=MAP_GRID_WIDTH - 27, grid_position_y=61, name="Cang Zhou",
                                           fortress_level=1,
                                           country="Yuan Shao", population=9100, population_growth=3, happiness=2,
                                           defender=[], anchor="center",
                                           raw_buildings={"Walls": 3, "Barracks": 1, "Farm": 3, "Market": 3}),

                                      City(grid_position_x=208, grid_position_y=41, name="Tong Pass",
                                           fortress_level=1, country="Yuan Shao", population=2200, population_growth=1,
                                           happiness=2, defender=[], anchor="center",
                                           raw_buildings={"Walls": 3, "Barracks": 1, "Farm": 1, "Market": 1}),

                                      #Wei
                                      City(grid_position_x=MAP_GRID_WIDTH-146, grid_position_y=66, name="Hu Pass",
                                           fortress_level=2,
                                           country="Wei", population=1800, population_growth=1, happiness=0,
                                           defender=[], anchor="center",
                                           raw_buildings={"Walls": 2, "Barracks": 1, "Farm": 1, "Market": 1}),

                                      City(grid_position_x=MAP_GRID_WIDTH - 215, grid_position_y=96, name="Chang An",
                                           fortress_level=3, country="Wei", population=29000, population_growth=3,
                                           happiness=3, defender=[], anchor="center",
                                           raw_buildings={"Walls": 4, "Barracks": 3, "Farm": 5, "Market": 5}),


                                      City(grid_position_x=255, grid_position_y=145, name="Hulao Pass",
                                           fortress_level=1, country="Wei", population=1700, population_growth=1,
                                           happiness=2, defender=[], anchor="center",
                                           raw_buildings={"Walls": 2, "Barracks": 2, "Farm": 1, "Market": 1}),

                                      City(grid_position_x=291, grid_position_y=119, name="Xu Chang",
                                           fortress_level=4, country="Wei", population=56400, population_growth=3,
                                           happiness=2, defender=[], anchor="center",
                                           raw_buildings={"Walls": 5, "Barracks": 4, "Farm": 5, "Market": 5}),

                                      City(grid_position_x=286, grid_position_y=168, name="Ru Nan",
                                           fortress_level=2, country="Wei", population=8100, population_growth=1,
                                           happiness=2, defender=[], anchor="center",
                                           raw_buildings={"Walls": 2, "Barracks": 1, "Farm": 3, "Market": 2}),




                                      City(grid_position_x=MAP_GRID_WIDTH-8, grid_position_y=105, name="Bei Hai", fortress_level=2,
                                           country="Wei", population=7500, population_growth=3, happiness=2,
                                           defender=[], anchor="center",
                                           raw_buildings={"Walls": 2, "Barracks": 2, "Farm": 4, "Market": 3}),

                                      City(grid_position_x=334, grid_position_y=156, name="Xu Zhou",
                                           fortress_level=3, country="Wei", population=16800, population_growth=3,
                                           happiness=3, defender=[], anchor="center",
                                           raw_buildings={"Walls": 4, "Barracks": 2, "Farm": 4, "Market": 4}),

                                      City(grid_position_x=MAP_GRID_WIDTH-95, grid_position_y=88, name="Chen Liu", fortress_level=3,
                                           country="Wei", population=5000, population_growth=2, happiness=3,
                                           defender=[], anchor="center", raw_buildings={"Walls":3, "Barracks":3, "Farm":3, "Market":3}),

                                      City(grid_position_x=MAP_GRID_WIDTH-138, grid_position_y=95, name="Luo Yang", fortress_level=3,
                                           country="Wei", population=8300, population_growth=3, happiness=8,
                                           defender=[], anchor="center",
                                           raw_buildings={"Walls": 2, "Barracks": 2, "Farm": 3, "Market": 2}),

                                      City(grid_position_x=197, grid_position_y=139, name="Xin Ye",
                                           fortress_level=2, country="Wei", population=5300,
                                           population_growth=1, happiness=2, defender=[], anchor="center",
                                           raw_buildings={"Walls": 2, "Barracks": 1, "Farm": 3, "Market": 2}),

                                      City(grid_position_x=225, grid_position_y=161, name="Wan Cheng",
                                           fortress_level=2, country="Wei", population=4700, population_growth=1,
                                           happiness=3, defender=[], anchor="center",
                                           raw_buildings={"Walls": 2, "Barracks": 1, "Farm": 2, "Market": 1}),

                                      City(grid_position_x=MAP_GRID_WIDTH - 39, grid_position_y=99, name="Pu Yang",
                                           fortress_level=2, country="Wei", population=7600, population_growth=1, happiness=3,
                                           defender=[], anchor="center",
                                           raw_buildings={"Walls": 2, "Barracks": 1, "Farm": 2, "Market": 2}),



                                      #Wu
                                      City(grid_position_x=354, grid_position_y=223, name="Jian Ye",
                                           fortress_level=3, country="Wu", population=14500, population_growth=3,
                                           happiness=3, defender=[], anchor="center",
                                           raw_buildings={"Walls": 4, "Barracks": 4, "Farm": 5, "Market": 5}),
                                      City(grid_position_x=329, grid_position_y=190, name="Lu Jiang",
                                           fortress_level=2, country="Wu", population=6200, population_growth=1,
                                           happiness=3, defender=[], anchor="center",
                                           raw_buildings={"Walls": 2, "Barracks": 1, "Farm": 2, "Market": 3}),
                                      City(grid_position_x=296, grid_position_y=197, name="Shou Chun",
                                           fortress_level=3, country="Wu", population=10870, population_growth=2,
                                           happiness=3, defender=[], anchor="center",
                                           raw_buildings={"Walls": 3, "Barracks": 1, "Farm": 2, "Market": 2}),
                                      City(grid_position_x=284, grid_position_y=221, name="Yu Zhang",
                                           fortress_level=1, country="Wu", population=3520, population_growth=1,
                                           happiness=3, defender=[], anchor="center",
                                           raw_buildings={"Walls": 2, "Barracks": 1, "Farm": 2, "Market": 2}),
                                      City(grid_position_x=262, grid_position_y=210, name="Chai Sang",
                                           fortress_level=1, country="Wu", population=2800, population_growth=1,
                                           happiness=2, defender=[], anchor="center",
                                           raw_buildings={"Walls": 2, "Barracks": 1, "Farm": 2, "Market": 2}),
                                      City(grid_position_x=246, grid_position_y=191, name="Jiang Xia",
                                           fortress_level=1, country="Wu", population=3690, population_growth=1,
                                           happiness=3, defender=[], anchor="center",
                                           raw_buildings={"Walls": 2, "Barracks": 2, "Farm": 2, "Market": 3}),
                                      City(grid_position_x=306, grid_position_y=264, name="Kuai Ji",
                                           fortress_level=2, country="Wu", population=4740, population_growth=1,
                                           happiness=3, defender=[], anchor="center",
                                           raw_buildings={"Walls": 2, "Barracks": 1, "Farm": 2, "Market": 1}),


                                      #Shu
                                      #None
                                      

                                      #Ma Teng
                                      City(grid_position_x=MAP_GRID_WIDTH - 303, grid_position_y=19, name="Liang Zhou",
                                           fortress_level=2, country="Ma Teng", population=32700, population_growth=1,
                                           happiness=2, defender=[], anchor="center",
                                           raw_buildings={"Walls": 3, "Barracks": 4, "Farm": 5, "Market": 4}),

                                      City(grid_position_x=MAP_GRID_WIDTH - 185, grid_position_y=59, name="An Ding",
                                           fortress_level=2, country="Ma Teng", population=6100, population_growth=1,
                                           happiness=0,
                                           defender=[], anchor="center",
                                           raw_buildings={"Walls": 2, "Barracks": 1, "Farm": 1, "Market": 1}),

                                      City(grid_position_x=98, grid_position_y=96, name="Jie Ting",
                                           fortress_level=1, country="Ma Teng", population=1600, population_growth=1,
                                           happiness=0,
                                           defender=[], anchor="center",
                                           raw_buildings={"Walls": 2, "Barracks": 1, "Farm": 1, "Market": 1}),

                                      City(grid_position_x=10, grid_position_y=75, name="Yangping Pass",
                                           fortress_level=1, country="Ma Teng", population=1200, population_growth=1,
                                           happiness=3, defender=[], anchor="center",
                                           raw_buildings={"Walls": 1, "Barracks": 1, "Farm": 2, "Market": 1}),

                                      City(grid_position_x=24, grid_position_y=63, name="Jin Cheng",
                                           fortress_level=2, country="Ma Teng", population=5700, population_growth=1,
                                           happiness=3, defender=[], anchor="center",
                                           raw_buildings={"Walls": 2, "Barracks": 1, "Farm": 4, "Market": 2}),

                                      City(grid_position_x=83, grid_position_y=78, name="Tian Shui",
                                           fortress_level=2, country="Ma Teng", population=4600, population_growth=1,
                                           happiness=0, defender=[], anchor="center",
                                           raw_buildings={"Walls": 2, "Barracks": 1, "Farm": 3, "Market": 1}),


                                      #Liu Zhang
                                      City(grid_position_x=37, grid_position_y=158, name="Cheng Du",
                                           fortress_level=3, country="Liu Zhang", population=19800, population_growth=3,
                                           happiness=1, defender=[], anchor="center",
                                           raw_buildings={"Walls": 4, "Barracks": 3, "Farm": 4, "Market": 5}),
                                      City(grid_position_x=61, grid_position_y=152, name="Zi Tong",
                                           fortress_level=1, country="Liu Zhang", population=5280, population_growth=1,
                                           happiness=1, defender=[], anchor="center",
                                           raw_buildings={"Walls": 1, "Barracks": 1, "Farm": 2, "Market": 2}),
                                      City(grid_position_x=13, grid_position_y=200, name="Jian Ning",
                                           fortress_level=1, country="Liu Zhang", population=4600, population_growth=1,
                                           happiness=1, defender=[], anchor="center",
                                           raw_buildings={"Walls": 1, "Barracks": 1, "Farm": 2, "Market": 2}),
                                      City(grid_position_x=102, grid_position_y=271, name="He Pu",
                                           fortress_level=1, country="Liu Zhang", population=1083, population_growth=1,
                                           happiness=3, defender=[], anchor="center",
                                           raw_buildings={"Walls": 1, "Barracks": 1, "Farm": 1, "Market": 1}),
                                      City(grid_position_x=129, grid_position_y=240, name="Hu Nan",
                                           fortress_level=2, country="Liu Zhang", population=2262, population_growth=1,
                                           happiness=3, defender=[], anchor="center",
                                           raw_buildings={"Walls": 1, "Barracks": 1, "Farm": 1, "Market": 1}),

                                      #Zhang Lu

                                      City(grid_position_x=42, grid_position_y=97, name="Qi Shan",
                                           fortress_level=1, country="Zhang Lu", population=2400,
                                           population_growth=1, happiness=3, defender=[], anchor="center",
                                           raw_buildings={"Walls": 1, "Barracks": 1, "Farm": 3, "Market": 1}),
                                      
                                      City(grid_position_x=32, grid_position_y=117, name="Long Nan",
                                           fortress_level=2, country="Zhang Lu", population=7370, population_growth=1,
                                           happiness=3, defender=[], anchor="center",
                                           raw_buildings={"Walls": 2, "Barracks": 2, "Farm": 3, "Market": 2}),

                                      City(grid_position_x=97, grid_position_y=138, name="Han Zhong",
                                           fortress_level=3, country="Zhang Lu", population=14710,
                                           population_growth=2, happiness=3, defender=[], anchor="center",
                                           raw_buildings={"Walls": 3, "Barracks": 2, "Farm": 5, "Market": 2}),

                                      City(grid_position_x=120, grid_position_y=91, name="Chen Cang",
                                           fortress_level=2, country="Zhang Lu", population=3550, population_growth=1,
                                           happiness=3, defender=[], anchor="center",
                                           raw_buildings={"Walls": 2, "Barracks": 1, "Farm": 3, "Market": 2}),
                                      

                                      #Liu Biao
                                      City(grid_position_x=178, grid_position_y=171, name="Fan Cheng",
                                           fortress_level=2, country="Liu Biao", population=2700, population_growth=1,
                                           happiness=3, defender=[], anchor="center",
                                           raw_buildings={"Walls": 2, "Barracks": 1, "Farm": 2, "Market": 2}),

                                      City(grid_position_x=109, grid_position_y=174, name="Shang Yong",
                                           fortress_level=3, country="Liu Biao", population=11100,
                                           population_growth=2, happiness=2, defender=[], anchor="center",
                                           raw_buildings={"Walls": 3, "Barracks": 1, "Farm": 3, "Market": 2}),

                                      City(grid_position_x=182, grid_position_y=192, name="Xiang Yang",
                                           fortress_level=3, country="Liu Biao", population=16900,
                                           population_growth=2, happiness=2, defender=[], anchor="center",
                                           raw_buildings={"Walls": 3, "Barracks": 3, "Farm": 3, "Market": 3}),

                                      City(grid_position_x=131, grid_position_y=209, name="Jiang Zhou",
                                           fortress_level=2, country="Liu Biao", population=4900,
                                           population_growth=2, happiness=1, defender=[], anchor="center",
                                           raw_buildings={"Walls": 2, "Barracks": 1, "Farm": 2, "Market": 1}),

                                      City(grid_position_x=219, grid_position_y=203, name="Chang Sha",
                                           fortress_level=3, country="Liu Biao", population=9200,
                                           population_growth=2, happiness=1, defender=[], anchor="center",
                                           raw_buildings={"Walls": 3, "Barracks": 3, "Farm": 2, "Market": 2}),

                                      City(grid_position_x=162, grid_position_y=250, name="Wu Ling",
                                           fortress_level=2, country="Liu Biao", population=2644,
                                           population_growth=2, happiness=1, defender=[], anchor="center",
                                           raw_buildings={"Walls": 1, "Barracks": 1, "Farm": 2, "Market": 2}),

                                      City(grid_position_x=205, grid_position_y=255, name="Ling Ling",
                                           fortress_level=2, country="Liu Biao", population=2345,
                                           population_growth=2, happiness=1, defender=[], anchor="center",
                                           raw_buildings={"Walls": 1, "Barracks": 1, "Farm": 2, "Market": 2}),

                                      City(grid_position_x=244, grid_position_y=248, name="Gui Lin",
                                           fortress_level=2, country="Liu Biao", population=5921,
                                           population_growth=2, happiness=1, defender=[], anchor="center",
                                           raw_buildings={"Walls": 2, "Barracks": 1, "Farm": 2, "Market": 2}),

                                      ],

                              "country":self.selected_country,
                              "difficulty": self.difficulty,
                              "realm_divide": False,
                              "show_advice": self.show_advice,
                              "advice_given": [],
                              "player_turn": True,
                              "gold": initial_gold,
                              "food": initial_food,
                              "tax": {"Shu":.1, "Wu":.1,"Wei":.1, "Ma Teng":.1, "Liu Biao":.1, "Liu Zhang":.1,
                                      "Zhang Lu":.1, "Yuan Shao":.1},
                              "turn_number": 1,
                              "hostile_relationships": {"Shu": ["Wei"],
                                                        "Wu": ["Liu Biao"],
                                                        "Wei": ["Shu", "Ma Teng", "Yuan Shao"],
                                                        "Ma Teng": ["Wei"],
                                                        "Liu Biao": ["Wu"],
                                                        "Liu Zhang": [],
                                                        "Zhang Lu": [],
                                                        "Yuan Shao": ["Wei"]},
                              "trade_relationships": {"Shu": ["Yuan Shao"],
                                                      "Wu": [],
                                                      "Wei": [],
                                                      "Ma Teng": [],
                                                      "Liu Biao": [],
                                                      "Liu Zhang": [],
                                                      "Zhang Lu": [],
                                                      "Yuan Shao": ["Shu"]},
                              "alliance_relationships": {"Shu": ["Liu Biao", "Yuan Shao"],
                                                         "Wu": [],
                                                         "Wei": [],
                                                         "Ma Teng": [],
                                                         "Liu Biao": ["Shu"],
                                                         "Liu Zhang": [],
                                                         "Zhang Lu": [],
                                                         "Yuan Shao": ["Shu"]},
                              "peace_relationships": initial_peace_relationships[self.selected_country], # keeps track of countries with which player is at peace; dictionary of dictionaries
                              "diplomatic_relationships": {},
                              "past_conflicts": initial_past_conflicts[self.selected_country],  #keeps track of countries with which player has declared war in the past
                              "diplomacy_penalty": {"All":0,
                                                    "Shu":0,
                                                    "Wu":0,
                                                    "Wei":0,
                                                    "Ma Teng":0,
                                                    "Liu Biao":0,
                                                    "Liu Zhang":0,
                                                    "Zhang Lu": 0,
                                                    "Yuan Shao": 0,
                                                    },
                              "general_kill_release": {"Shu": 0,
                                                        "Wu": 0,
                                                        "Wei": 0,
                                                        "Ma Teng": 0,
                                                        "Liu Biao": 0,
                                                        "Liu Zhang": 0,
                                                        "Zhang Lu": 0,
                                                        "Yuan Shao": 0,
                                                        },
                              "enemy_troops_killed": 0,
                              "player_troops_killed": 0,
                              "enemy_generals_killed": 0,
                              "player_generals_killed": 0,
                              "num_generals_recruited": 0,
                              "num_generals_released": 0,
                              "battles_won": 0,
                              "battles_lost": 0,
                              "gold_produced": 0,
                              "task_progress": {"southern_barbarians":-1,},
                              "dilemma_effects": {"intellectual_revolution":0,
                                                  "famine":0,
                                                  "famine_resolved": 0,
                                                  "nationalism":0,
                                                  "flourishing_economy":0},
                              "tech_tree": {"Shu": {"official_currency":0},
                                            "Wu": {"official_currency": 0},
                                            "Wei": {"official_currency": 0},
                                            "Ma Teng": {"official_currency": 0},
                                            "Liu Biao": {"official_currency": 0},
                                            "Liu Zhang": {"official_currency": 0},
                                            "Zhang Lu": {"official_currency": 0},
                                            "Yuan Shao": {"official_currency": 0},
                                            },
                              "current_tech": {"Shu": "official_currency",
                                                "Wu": "official_currency",
                                                "Wei": "official_currency",
                                                "Ma Teng": "official_currency",
                                                "Liu Biao": "official_currency",
                                                "Liu Zhang": "official_currency",
                                                "Zhang Lu": "official_currency",
                                                "Yuan Shao": "official_currency",
                                                },
                              "acquired_tech": {"Shu": [],
                                                "Wu": [],
                                                "Wei": [],
                                                "Ma Teng": [],
                                                "Liu Biao": [],
                                                "Liu Zhang": [],
                                                "Zhang Lu": [],
                                                "Yuan Shao": [],
                                                },
                              "game_alerts": {"city":[],
                                              "army":[],
                                              "building":[]},
                              "character_pool": {"zhou_cang": Commander(name="Zhou Cang", simple_name="zhou_cang", age=37, loyalty=90,
                                                                        country="Shu", combat_prowess=70, intellect=50, command=58, charisma=73,
                                                                        action_points=17, level=1, experience=0, upgrade_points=0, earliest_start_year=200),
                                                 "xu_shu": Commander(name="Xu Shu", simple_name="xu_shu", age=20, loyalty=95,
                                                                country="Shu", combat_prowess=55, intellect=92, command=80, charisma=81,
                                                                action_points=20, level=1, experience=0, upgrade_points=0, earliest_start_year=206),
                                                 "zhuge_liang": Commander(name="Zhuge Liang", simple_name="zhuge_liang", age=19, loyalty=100,
                                                                country="Shu", combat_prowess=35, intellect=100, command=100, charisma=93,
                                                                action_points=22, level=1, experience=0, upgrade_points=0, earliest_start_year=207),
                                                 "ma_liang": Commander(name="Ma Liang", simple_name="ma_liang", age=13, loyalty=85,
                                                                          country="Shu", combat_prowess=31, intellect=83, command=48, charisma=90,
                                                                          action_points=18, level=1, experience=0, upgrade_points=0, earliest_start_year=205),
                                                 "ma_su": Commander(name="Ma Su", simple_name="ma_su", age=10, loyalty=85,
                                                                          country="Shu", combat_prowess=28, intellect=86, command=65, charisma=70,
                                                                          action_points=21, level=1, experience=0, upgrade_points=0, earliest_start_year=205),
                                                 "huang_zhong": Commander(name="Huang Zhong", simple_name="huang_zhong", age=56, loyalty=80, country="Shu",
                                                                          combat_prowess=92, intellect=65, command=85, charisma=65, action_points=20, level=4, experience=1200,
                                                                          upgrade_points=29, earliest_start_year= 205, hometown="Chang Sha"),
                                                 "wei_yan": Commander(name="Wei Yan", simple_name="wei_yan", age=34, loyalty=70,
                                                                        country="Shu", combat_prowess=88, intellect=75, command=80, charisma=50, action_points=21, level=2,
                                                                        experience=500, upgrade_points=11,  earliest_start_year=205, hometown="Chang Sha"),
                                                 "ma_zhong": Commander(name="Ma Zhong", simple_name="ma_zhong", age=6, loyalty=85,
                                                                        country="Shu", combat_prowess=68, intellect=83, command=75, charisma=96, action_points=20, level=1,
                                                                        experience=0, upgrade_points=0,  earliest_start_year=222),
                                                 "deng_zhi": Commander(name="Deng Zhi", simple_name="deng_zhi", age=22, loyalty=85,
                                                                        country="Shu", combat_prowess=56, intellect=83, command=70, charisma=90, action_points=20, level=1,
                                                                        experience=0, upgrade_points=0,  earliest_start_year=214),
                                                 "liu_feng": Commander(name="Liu Feng", simple_name="liu_feng", age=17, loyalty=90,
                                                                        country="Shu", combat_prowess=77, intellect=49, command=75, charisma=61, action_points=23, level=1,
                                                                        experience=0, upgrade_points=0,  earliest_start_year=207),
                                                 "fei_yi": Commander(name="Fei Yi", simple_name="fei_yi", age=15, loyalty=88,
                                                                        country="Shu", combat_prowess=26, intellect=89, command=70, charisma=98, action_points=18, level=1,
                                                                        experience=0, upgrade_points=0,  earliest_start_year=210, hometown="Cheng Du"),
                                                 "liao_hua": Commander(name="Liao Hua", simple_name="liao_hua", age=10, loyalty=96,
                                                                        country="Shu", combat_prowess=76, intellect=65, command=73, charisma=60, action_points=21, level=1,
                                                                        experience=0, upgrade_points=0,  earliest_start_year=217, hometown="Xiang Yang"),
                                                 "zhang_bao": Commander(name="Zhang Bao", simple_name="zhang_bao", age=15, loyalty=100,
                                                                        country="Shu", combat_prowess=87, intellect=48, command=75, charisma=57, action_points=22, level=1,
                                                                        experience=0, upgrade_points=0,  earliest_start_year=217),
                                                 "guan_xing": Commander(name="Guan Xing", simple_name="guan_xing", age=16, loyalty=100,
                                                                        country="Shu", combat_prowess=87, intellect=62, command=75, charisma=64, action_points=22, level=1,
                                                                        experience=0, upgrade_points=0,  earliest_start_year=216),
                                                 "guan_ping": Commander(name="Guan Ping", simple_name="guan_xing", age=18, loyalty=90,
                                                                        country="Shu", combat_prowess=80, intellect=70, command=75, charisma=65, action_points=23, level=1,
                                                                        experience=0, upgrade_points=0,  earliest_start_year=200),


                                                 #wei
                                                 "jia_xu": Commander(name="Jia Xu", simple_name="jia_xu", age=53, loyalty=76,
                                                                          country="Wei", combat_prowess=39, intellect=97, command=85, charisma=72,
                                                                          action_points=18, level=1, experience=0, upgrade_points=0, earliest_start_year=200),
                                                 "jiang_wei": Commander(name="Jiang Wei", simple_name="jiang_wei", age=-2, loyalty=89,
                                                                            country="Wei", combat_prowess=80, intellect=92, command=87, charisma=86, action_points=22, level=1, experience=0,
                                                                            upgrade_points=0, earliest_start_year=225, hometown="Tian Shui"),
                                                 "deng_ai": Commander(name="Deng Ai", simple_name="deng_ai", age=3, loyalty=84, country="Wei", combat_prowess=76,
                                                                        intellect=88, command=88, charisma=82, action_points=23, level=1, experience=0, upgrade_points=0,
                                                                        earliest_start_year=222, hometown="Xin Ye"),
                                                 "cao_zhen": Commander(name="Cao Zhen", simple_name="cao_zhen", age=20, loyalty=90, country="Wei", combat_prowess=65,
                                                                      intellect=68, command=85, charisma=73, action_points=21, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=215),
                                                 "cao_zhang": Commander(name="Cao Zhang", simple_name="cao_zhang", age=12, loyalty=95, country="Wei", combat_prowess=86,
                                                                      intellect=59, command=80, charisma=57, action_points=20, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=215),
                                                 "sima_yi": Commander(name="Sima Yi", simple_name="sima_yi", age=21, loyalty=82, country="Wei", combat_prowess=50,
                                                                      intellect=96, command=99, charisma=90, action_points=20, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=208),
                                                 "wang_shuang": Commander(name="Wang Shuang", simple_name="wang_shuang", age=16, loyalty=80, country="Wei", combat_prowess=90,
                                                                      intellect=36, command=48, charisma=29, action_points=20, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=218),
                                                 "hao_zhao": Commander(name="Hao Zhao", simple_name="hao_zhao", age=19, loyalty=100, country="Wei", combat_prowess=80,
                                                                      intellect=79, command=89, charisma=72, action_points=20, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=216),
                                                 "yang_xiu": Commander(name="Yang Xiu", simple_name="yang_xiu", age=25, loyalty=85, country="Wei", combat_prowess=5,
                                                                      intellect=84, command=13, charisma=59, action_points=18, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=204),
                                                 "zhong_yao": Commander(name="Zhong Yao", simple_name="zhong_yao", age=49, loyalty=80, country="Wei", combat_prowess=22,
                                                                      intellect=77, command=67, charisma=88, action_points=14, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=220),
                                                 "liu_ye": Commander(name="Liu Ye", simple_name="liu_ye", age=33, loyalty=77, country="Wei", combat_prowess=32,
                                                                      intellect=90, command=40, charisma=72, action_points=16, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=220, hometown="Xu Zhou"),


                                                 #wu
                                                 "pang_tong": Commander(name="Pang Tong", simple_name="pang_tong", age=21, loyalty=75, country="Wu", combat_prowess=30,
                                                                      intellect=97, command=79, charisma=79, action_points=18, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=209, hometown="Xiang Yang"),
                                                 "gu_yong": Commander(name="Gu Yong", simple_name="gu_yong", age=32, loyalty=90, country="Wu", combat_prowess=19,
                                                                      intellect=85, command=47, charisma=85, action_points=16, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=201),
                                                 "lu_su": Commander(name="Lu Su", simple_name="lu_su", age=28, loyalty=95, country="Wu", combat_prowess=58,
                                                                      intellect=92, command=81, charisma=90, action_points=20, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=200),
                                                 "ling_tong": Commander(name="Ling Tong", simple_name="ling_tong", age=11, loyalty=86, country="Wu", combat_prowess=89,
                                                                      intellect=60, command=77, charisma=51, action_points=21, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=208),
                                                 "lv_meng": Commander(name="Lv Meng", simple_name="lv_meng", age=11, loyalty=86, country="Wu", combat_prowess=89,
                                                                      intellect=60, command=77, charisma=51, action_points=21, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=208),
                                                 "xu_sheng": Commander(name="Xu Sheng", simple_name="xu_sheng", age=30, loyalty=84, country="Wu", combat_prowess=86,
                                                                      intellect=77, command=87, charisma=65, action_points=20, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=202),
                                                 "lu_xun": Commander(name="Lu Xun", simple_name="lu_xun", age=17, loyalty=96, country="Wu", combat_prowess=60,
                                                                      intellect=95, command=94, charisma=90, action_points=22, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=218),
                                                 "ding_feng": Commander(name="Ding Feng", simple_name="ding_feng", age=-3, loyalty=96, country="Wu", combat_prowess=80,
                                                                      intellect=71, command=81, charisma=56, action_points=22, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=228, hometown="He Nan"),
                                                 "zhuge_jin": Commander(name="Zhuge Jin", simple_name="zhuge_jin", age=26, loyalty=89, country="Wu", combat_prowess=33,
                                                                      intellect=76, command=70, charisma=90, action_points=18, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=200),
                                                 "zhu_huan": Commander(name="Zhu Huan", simple_name="zhu_huan", age=23, loyalty=87, country="Wu", combat_prowess=82,
                                                                      intellect=73, command=84, charisma=59, action_points=20, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=209),

                                                 #Liu Biao
                                                 "gan_ning": Commander(name="Gan Ning", simple_name="gan_ning", age=41,
                                                                      loyalty=76, country="Liu Biao", combat_prowess=93,
                                                                      intellect=69, command=87, charisma=48,
                                                                      action_points=22, level=1, experience=0,
                                                                      upgrade_points=0,  earliest_start_year=200),
                                                 "huo_jun": Commander(name="Huo Jun", simple_name="huo_jun", age=22,
                                                                      loyalty=80, country="Liu Biao", combat_prowess=69,
                                                                      intellect=73, command=80, charisma=65,
                                                                      action_points=20, level=1, experience=0,
                                                                      upgrade_points=0,  earliest_start_year=204),

                                                 #Ma Teng
                                                 "cheng_yi": Commander(name="Cheng Yi", simple_name="cheng_yi", age=36,
                                                                      loyalty=84, country="Ma Teng", combat_prowess=69,
                                                                      intellect=50, command=74, charisma=52,
                                                                      action_points=20, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=210),
                                                 "chenggong_ying": Commander(name="Chenggong Ying", simple_name="chenggong_ying", age=40,
                                                                      loyalty=96, country="Ma Teng", combat_prowess=73,
                                                                      intellect=71, command=76, charisma=64,
                                                                      action_points=20, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=211),
                                                 "yang_qiu": Commander(name="Yang Qiu", simple_name="yang_qiu", age=36,
                                                                      loyalty=75, country="Ma Teng", combat_prowess=61,
                                                                      intellect=54, command=70, charisma=56,
                                                                      action_points=21, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=211),

                                                 #Liu Zhang
                                                 "leng_bao": Commander(name="Leng Bao", simple_name="leng_bao", age=36,
                                                                      loyalty=90, country="Liu Zhang", combat_prowess=77,
                                                                      intellect=68, command=75, charisma=28,
                                                                      action_points=20, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=208,
                                                                      hometown="Cheng Du"),
                                                 "zhang_ren": Commander(name="Zhang Ren", simple_name="zhang_ren", age=29,
                                                                      loyalty=100, country="Liu Zhang", combat_prowess=79,
                                                                      intellect=76, command=82, charisma=66,
                                                                      action_points=20, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=207,
                                                                      hometown="Cheng Du"),
                                                 "wu_ban": Commander(name="Wu Ban", simple_name="wu_ban", age=29,
                                                                      loyalty=85, country="Liu Zhang", combat_prowess=72,
                                                                      intellect=56, command=72, charisma=67,
                                                                      action_points=20, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=206,
                                                                      hometown="Cheng Du"),
                                                 "liu_ba": Commander(name="Liu Ba", simple_name="liu_ba", age=27,
                                                                      loyalty=88, country="Liu Zhang", combat_prowess=28,
                                                                      intellect=79, command=21, charisma=76,
                                                                      action_points=18, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=207,
                                                                      hometown="Ling Ling"),
                                                 "li_yan": Commander(name="Li Yan", simple_name="li_yan", age=24,
                                                                      loyalty=82, country="Liu Zhang", combat_prowess=78,
                                                                      intellect=75, command=84, charisma=68,
                                                                      action_points=20, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=215,
                                                                      hometown="Xiang Yang"),
                                                 "zhang_song": Commander(name="Zhang Song", simple_name="zhang_song", age=34,
                                                                      loyalty=78, country="Liu Zhang", combat_prowess=9,
                                                                      intellect=88, command=14, charisma=62,
                                                                      action_points=19, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=204,
                                                                      hometown="Xiang Yang"),
                                                 "li_hui": Commander(name="Li Hui", simple_name="li_hui", age=24,
                                                                      loyalty=78, country="Liu Zhang", combat_prowess=66,
                                                                      intellect=78, command=79, charisma=80,
                                                                      action_points=20, level=1, experience=0,
                                                                      upgrade_points=0, earliest_start_year=212,
                                                                      hometown="Cheng Du"),
                                                },

                              "recruitable_characters": {"Shu":[],
                                                         "Wei":[],
                                                         "Wu":[],
                                                         "Ma Teng": [],
                                                         "Liu Biao": [],
                                                         "Liu Zhang": [],
                                                         "Zhang Lu": [],
                                                         "Yuan Shao": [],
                                                         "Southern Barbarians": [],
                                                         },
                              "recruited_characters": [],
                              "dead_characters": [],
                              "ongoing_achievements": {"ranged_units_only": True,
                                                       "no_ranged_units": True,
                                                       "cavalry_units_only": True,
                                                       },
                              }

            for i in range(2):
                self.apply_initial_tech_bonus("Yuan Shao", mode="acquire")
            self.apply_initial_tech_bonus("Yuan Shao", mode="assign")
            self.apply_initial_tech_bonus("Liu Biao", mode="acquire")
            self.apply_initial_tech_bonus("Liu Biao", mode="assign")

            self.play_intro_vid()
            #scene = Campaign_Map_Scene(self, self.game_data)
            #scene.new()


    def play_intro_vid(self):
        vid = Video(os.path.join(RESOURCES_DIR, "Imperial_Ambitions_3k_intro.mp4"))
        vid.set_size((LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT))
        pg.mixer.music.load("Fantasy Armies.ogg")
        pg.mixer.music.play(-1)
        self.skip_intro_vid_button.rect.topleft = 0, 0

        total_frames = int(vid.get_file_data()["frame count"])
        while vid.frames + 60 < total_frames:
            vid.draw(self.screen, (0, 0))
            self.fixed_sprites.draw(self.screen)
            pg.display.flip()
            for event in pg.event.get():
                if event.type == pg.MOUSEBUTTONUP:
                    mouse_x, mouse_y = pg.mouse.get_pos()
                    if self.skip_intro_vid_button.rect.collidepoint(mouse_x, mouse_y):
                        total_frames = -1
                        break
        vid.close()
        scene = Campaign_Map_Scene(self, self.game_data)
        scene.new()


    def apply_initial_tech_bonus(self, country, mode="acquire"):
        available_techs = list(TECH_TREE.keys())
        shuffle(available_techs)
        for tech in available_techs:
            if tech not in self.game_data["acquired_tech"][country]:
                values = TECH_TREE[tech]
                prereqs = values["prerequisites"]
                prereqs_met = True
                for req in prereqs:
                    if req not in self.game_data["acquired_tech"][country]:
                        prereqs_met = False
                if prereqs_met:
                    if mode == "acquire":
                        self.game_data["acquired_tech"][country].append(tech)
                        self.game_data["tech_tree"][country][tech] = values["turns"]
                        return
                    else:
                        self.game_data["tech_tree"][country][tech] = 0
                        self.game_data["current_tech"][country] = tech
                        return


    def toggle_country(self, direction=1):
        self.selected_country_index += direction
        if self.selected_country_index >= len(self.available_countries):
            self.selected_country_index = 0
        elif self.selected_country_index < 0:
            self.selected_country_index = len(self.available_countries) - 1

        self.selected_country = self.available_countries[self.selected_country_index]
        self.selected_country_icon.image = self.all_country_icons[self.selected_country_index]

    
    def toggle_ai_difficulty(self):
        mouse_x, mouse_y = pg.mouse.get_pos()
        ratio = (mouse_x - self.toggle_difficulty_button.rect.left)/self.toggle_difficulty_button.image.get_width()
        if ratio <= 1/6:
            self.difficulty = 1
        elif ratio <= 3/6:
            self.difficulty = 2
        elif ratio <= 5/6:
            self.difficulty = 3
        else:
            self.difficulty = 4
        self.difficulty_toggle.rect.left = self.toggle_difficulty_button.rect.left + self.toggle_difficulty_button.image.get_width()/3*(self.difficulty-1) - 2*self.difficulty
    
    
    def toggle_show_advice(self):
        self.show_advice = not self.show_advice
        self.toggle_show_advice_button.pressed = self.show_advice


    def run(self):
        while self.playing:
            if self.running:
                self.clock.tick(FPS)
                self.events()
                self.update()
                self.draw()


    def events(self):
        pos = pg.mouse.get_pos()
        for button in self.buttons:
            if not button.is_checkbutton:
                if button.rect.collidepoint(pos):
                    if pg.mouse.get_pressed()[0]:
                        button.pressed = True
                    else:
                        button.pressed = False
                    if not button.hovering:
                        button.hovering = True
                        self.hover_sound.play()
                else:
                    button.pressed = False
                    button.hovering = False

        for event in pg.event.get():
            if event.type == pg.MOUSEBUTTONUP:
                self.click_sound.play()
                for button in self.buttons:
                    if button.rect.collidepoint(pos):
                        if not button.is_checkbutton:
                            button.pressed = False
                        button.command()
                        break

            if event.type == pg.KEYUP:
                if event.key == pg.K_ESCAPE:
                    self.playing = False
                    self.running = False
                    self.main_menu.new()

            if event.type == pg.QUIT:
                choice = pg.display.message_box("Quit",
                                                "Are you sure you wish to quit?",
                                                buttons=("Yes", "No"), return_button=1, escape_button=None
                                                )

                if choice == 0:
                    pg.quit()
                    sys.exit()



    def update(self):
        self.all_sprites.update()
        if self.transition:
            if self.transition.fading:
                self.transition.update()
            else:
                if self.post_transition_function:
                    self.post_transition_function()
                else:
                    self.transition = None


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)

            self.draw_text("Choose a faction:", FONT_FUNCTION(DEFAULT_FONT, 30), YELLOW, 325, 50)
            self.draw_text(self.selected_country, FONT_FUNCTION(DEFAULT_FONT, 40), WHITE, 325, 375)

            counter = 0
            for line in COUNTRY_DESCRIPTIONS[self.selected_country].split("\n"):
                if ":" in line:
                    label, value = line.split(":")
                    self.draw_text(label.rstrip()+":", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), YELLOW, 550,
                                   100 + 18 * counter, align="midleft")
                    self.draw_text(value.strip(), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 550 + len(label)**.75*18,
                                   100 + 18 * counter, align="midleft")
                else:
                    self.draw_text(line.rstrip(), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 550, 100+18*counter, align="midleft")
                counter += 1


            str_difficulty = ["Easy", "Normal", "Hard", "Expert"][self.difficulty-1]
            self.draw_text("AI Difficulty:", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), YELLOW, 250, 500)
            self.draw_text(str_difficulty, FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 350, 500, align="topleft")

            self.draw_text("Campaign advice:", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), YELLOW, 620, 500)

            self.priority_sprites.draw(self.screen)
            self.fixed_sprites.draw(self.screen)
            if self.tooltip_description:
                self.drawWrappedText(self.tooltip_description, WHITE,
                                     pg.Rect(self.tooltip_description_x + 16, self.tooltip_description_y + 20,
                                             268, 360), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE))

            self.transition_sprites.draw(self.screen)

            pg.display.flip()


    def draw_text(self, text, font, color, x, y, align="midtop"):
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        if align == "midleft":
            text_rect.midleft = (x, y)
        elif align == "topleft":
            text_rect.topleft = (x, y)
        else:
            text_rect.midtop = (x, y)
        self.screen.blit(text_surface, text_rect)


    def drawWrappedText(self, text, color, rect, font, aa=False, bkg=None):
        rect = pg.Rect(rect)
        y = rect.top
        lineSpacing = 10

        # get the height of the font
        fontHeight = font.size("Tg")[1]

        while text:
            i = 1

            # determine if the row of text will be outside our area
            if y + fontHeight > rect.bottom:
                break

            # determine maximum width of line
            while font.size(text[:i])[0] < rect.width and i < len(text):
                i += 1

            # if we've wrapped the text, then adjust the wrap to the last word
            if i < len(text):
                i = text.rfind(" ", 0, i) + 1

            # render the line and blit it to the surface
            if bkg:
                image = font.render(text[:i], 1, color, bkg)
                image.set_colorkey(bkg)
            else:
                image = font.render(text[:i], aa, color)

            self.screen.blit(image, (rect.left, y))
            y += fontHeight + lineSpacing

            # remove the text we just blitted
            text = text[i:]

        return text