from helper_functions import time_conversion_to_string, avg
from math import ceil, floor
from sprites import*
from functools import partial
import os
import pickle

#os.chdir(RESOURCES_DIR)

class Battle:
    def __init__(self, main_game, player_general, enemy_general, player_army_object=None, enemy_army_object=None,
                 custom=True, siege_defense=None, siege_city_object=None, terrain="forest"):
        self.main_game = main_game
        self.player_general = player_general
        self.enemy_general = enemy_general
        self.player_army_object = player_army_object
        self.enemy_army_object = enemy_army_object
        #set initial variables in case of retry
        self.initial_player_general = player_general
        self.initial_enemy_general = enemy_general

        self.two_player = False

        if custom:
            self.initial_player_army_object = player_army_object.copy()
            self.initial_enemy_army_object = enemy_army_object.copy()
        else:
            self.initial_player_army_object = player_army_object.army_composition.copy()
            self.initial_enemy_army_object = enemy_army_object.army_composition.copy()

        self.custom = custom
        if not self.custom:
            self.ai_difficulty = self.main_game.loaded_game_data["difficulty"]
        else:
            self.ai_difficulty = 3
        self.siege_defense = siege_defense
        self.siege_city_object = siege_city_object
        self.terrain = terrain
        self.weather = "clear"
        self.weather_resets = 2
        if "foresight" in self.player_general.skills:
            self.weather_resets += 2
        if self.custom:
            self.weather_resets = 0

        self.game_width, self.game_height = LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT
        if DEMO:
            os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (50, 50)
            self.screen = pg.display.set_mode((self.game_width, self.game_height), pg.HWSURFACE|pg.DOUBLEBUF)
        else:
            self.screen = pg.display.set_mode((self.game_width, self.game_height), pg.FULLSCREEN|pg.HWSURFACE|pg.DOUBLEBUF)

        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()
        self.running = True
        self.playing = True
        self.retry = False
        self.paused_time = 0
        self.fps = 30
        pg.font.init()
        pg.mixer.init()

        pg.mouse.set_visible(False)
        self.arrow_sound_old = pg.mixer.Sound("sfx_whoosh.wav")
        self.arrow_sound = pg.mixer.Sound("sfx_arrow.ogg")
        self.rock_sound = pg.mixer.Sound("sfx_rock_throw.ogg")
        self.smoke_bomb_sound = pg.mixer.Sound("sfx_smoke_bomb.ogg")
        self.fire_sound = pg.mixer.Sound("sfx_fire.ogg")
        self.morale_boost_sound = pg.mixer.Sound("sfx_restore_status.ogg")
        self.deployment_fail_sound = pg.mixer.Sound("sfx_deployment_failed.ogg")
        self.click_sound = pg.mixer.Sound("sfx_click.ogg")

        self.battle_music = ["determined_pursuit_loop.wav", "determined_pursuit_loop.wav", "light_battle.ogg",
                             "undisputed_stefan_grossmann.ogg", "undisputed_stefan_grossmann.ogg",
                             "Fantasy Armies.ogg"]
        pg.mixer.music.load(choice(self.battle_music))

        self.sprite_quick_guide = Static(LANDSCAPE_WIDTH//2, LANDSCAPE_HEIGHT//2, "sprite_battle_quick_guide_revised.png")



    def generate_weather(self):
        r = random()
        #check for poison mist
        if self.terrain == "forest" and ("poison_mist" in self.player_general.skills or "poison_mist" in self.enemy_general.skills):
            self.weather = "poison_mist"
        elif self.siege_defense == "left": #enemy choosing the weather
            player_ranged_total = 1
            enemy_ranged_total = 1
            for unit_type in self.player_army:
                if unit_type in RANGED_UNITS:
                    player_ranged_total += self.player_army[unit_type] * UNIT_RECRUITMENT_COST[unit_type]
            for unit_type in self.enemy_army:
                if unit_type in RANGED_UNITS:
                    enemy_ranged_total += self.enemy_army[unit_type] * UNIT_RECRUITMENT_COST[unit_type]
            if player_ranged_total/enemy_ranged_total >= 1.1:
                if r <= .35:
                    self.weather = "rain"
                elif r <= .7:
                    self.weather = "fog"
                else:
                    self.weather = "clear"
            else:
                self.weather = "clear"
        else:
            if r <= .2:
                self.weather = "rain"
            elif r <= .4:
                self.weather = "fog"
            else:
                self.weather = "clear"

        if self.weather == "fog":
            if random() >= .5:
                fog = Fog("sprite_fog_1.png", -50, 150, -randrange(4,6), 0)
            else:
                fog = Fog("sprite_fog_1.png", -LANDSCAPE_WIDTH, 150, randrange(4,6), 0)
            self.all_sprites.add(fog)
            self.fixed_sprites.add(fog)
            self.weather_sprites.add(fog)
        elif self.weather == "rain":
            for i in range(2000):
                rain = Rain(randrange(0, LANDSCAPE_WIDTH), randrange(-1000,-50), randrange(11, 16))
                self.all_sprites.add(rain)
                self.fixed_sprites.add(rain)
                self.weather_sprites.add(rain)
        elif self.weather == "poison_mist":
            if random() >= .5:
                fog = Fog("sprite_poison_mist_1.png", -50, 150, -randrange(4, 6), 0)
            else:
                fog = Fog("sprite_poison_mist_1.png", -LANDSCAPE_WIDTH, 150, randrange(4, 6), 0)
            self.all_sprites.add(fog)
            self.fixed_sprites.add(fog)
            self.weather_sprites.add(fog)


    def ask_weather_confirmation(self):
        self.generate_weather()
        self.decision_bg = Static(LANDSCAPE_WIDTH // 2, LANDSCAPE_HEIGHT // 2, "decision_interface_bg_small.png",
                                  anchor="center")
        self.all_sprites.add(self.decision_bg)
        self.decision_sprites.add(self.decision_bg)

        self.weather_yes_button = Button(self.decision_bg.rect.left + 200, self.decision_bg.rect.bottom - 100,
                                 "menu_button_yes.png", command=self.reset_weather)
        self.all_sprites.add(self.weather_yes_button)
        self.decision_sprites.add(self.weather_yes_button)
        self.buttons.append(self.weather_yes_button)

        self.weather_no_button = Button(self.decision_bg.rect.right - 200, self.decision_bg.rect.bottom - 100,
                                    "menu_button_no.png", command=partial(self.reset_weather, False))
        self.all_sprites.add(self.weather_no_button)
        self.decision_sprites.add(self.weather_no_button)
        self.buttons.append(self.weather_no_button)

        self.decision_text = "Current weather: {} Would you like to wait for different weather? Attempts remaining: {}".format(WEATHER_DESCRIPTIONS[self.weather], self.weather_resets)


    def reset_weather(self, decision=True):
        for sp in self.decision_sprites:
            sp.kill()
        self.buttons.remove(self.weather_yes_button)
        self.buttons.remove(self.weather_no_button)

        self.decision_text = ""

        if decision:
            self.weather_resets -= 1
            if self.weather_resets == 0:
                self.running = True
                self.unpause()
            else:
                for sp in self.weather_sprites:
                    sp.kill()
        else:
            self.weather_resets = 0
            self.running = True
            self.unpause()



    def new(self):

        self.all_sprites = pg.sprite.Group()
        self.priority_sprites = pg.sprite.Group()
        self.fixed_sprites = pg.sprite.Group()
        self.player_unit_sprites = pg.sprite.Group()
        self.enemy_unit_sprites = pg.sprite.Group()
        self.player_projectiles = pg.sprite.Group()
        self.enemy_projectiles = pg.sprite.Group()
        self.post_battle_sprites = pg.sprite.Group()
        self.obstacle_sprites = pg.sprite.Group()
        self.weather_sprites = pg.sprite.Group()
        self.decision_sprites = pg.sprite.Group()
        self.buttons = []
        self.player_chained_fire_attack_sprites = []
        self.enemy_chained_fire_attack_sprites = []
        self.player_chained_fire_attack_targets = []
        self.enemy_chained_fire_attack_targets = []
        self.player_sp_atk_timestamp = 0
        self.enemy_sp_atk_timestamp = 0

        if self.retry:
            self.player_general = self.initial_player_general
            self.enemy_general = self.initial_enemy_general
            self.player_army_object = self.initial_player_army_object.copy()
            self.enemy_army_object = self.initial_enemy_army_object.copy()
            self.generate_weather()
        elif self.custom:
            self.generate_weather()

        if not self.custom:
            if not self.main_game.is_mute:
                pg.mixer.music.play(20)
        else:
            pg.mixer.music.play(20)

        self.last_action = 0
        self.last_deployment = 0
        self.last_deployment_slot = 0
        self.enemy_last_deployment_slot = 0
        self.enemy_last_deployment = 0

        self.player_general_effects = []
        self.enemy_general_effects = []

        self.consecutive_deployments = 0
        self.consecutive_deployment_limit = 4
        self.enemy_consecutive_deployments = 0
        self.enemy_consecutive_deployment_limit = 4
        self.player_ambush_count = 0
        self.enemy_ambush_count = 0

        self.camera_location = 0
        if self.terrain == "water":
            self.max_camera_location = 1200
        else:
            self.max_camera_location = 2400

        self.selected_unit_index = 0
        self.selected_slot = 0

        self.enemy_selected_unit_index = 0
        self.enemy_selected_slot = 0

        self.player_general_skills = self.player_general.skills
        self.enemy_general_skills = self.enemy_general.skills
        self.player_general_special_attack_icons_list = []
        self.enemy_general_special_attack_icons_list = []
        self.interactive_special_atk = ""
        self.special_attack_tooltip_description = ""
        self.special_attack_tooltip_x = 0
        self.special_attack_tooltip_y = 0
        self.earthquake_count = 0
        self.earthquake_offset = (0,0)
        self.last_earthquake = 0
        self.number_of_ranged_units = 0 #counter to determine ranged sfx rate and arrow sprite clean-up rate

        self.player_unit_command_cost = UNIT_COMMAND_COST.copy()
        self.enemy_unit_command_cost = UNIT_COMMAND_COST.copy()

        self.player_country = self.player_general.country.lower()
        self.enemy_country = self.enemy_general.country.lower()


        self.background = Static(0, self.game_height - 650, "battleground_{}.png".format(self.terrain))
        self.all_sprites.add(self.background)

        if self.siege_defense == "left":
            self.player_deployment_position_offset = 200
            self.enemy_deployment_position_offset = 50
            walls = [building for building in self.siege_city_object.buildings if building.name == "Walls"][0]
            self.wall = FortressWall(self, -25, self.game_height - 800, "fort_wall_modified.png",
                                     self.siege_defense, level=walls.level)
            self.all_sprites.add(self.wall)
            self.obstacle_sprites.add(self.wall)

            #add bonus from quarry resoure if applicable
            wall_bonus = 0
            for city_sprite in self.main_game.city_sprites:
                if city_sprite.country == self.player_general.country:
                    for building in self.siege_city_object.buildings:
                        if building.name == "Quarry":
                            wall_bonus += building.level
            self.wall.max_health += int(self.wall.max_health * wall_bonus / 100)
            # see if wall strength impacted by storm
            if "natural_disaster_storm" in self.siege_city_object.effects:
                self.wall.max_health = int(self.wall.max_health * .75)
            self.wall.health = self.wall.max_health


            self.player_morale = self.wall.max_health
            self.player_morale_max = self.wall.max_health
            self.enemy_morale = 100
            self.enemy_morale_max = 100

        elif self.siege_defense == "right":
            self.player_deployment_position_offset = 50
            self.enemy_deployment_position_offset = 200
            walls = [building for building in self.siege_city_object.buildings if building.name == "Walls"][0]
            self.wall = FortressWall(self, self.max_camera_location+25, self.game_height - 800, "fort_wall_modified.png",
                                     self.siege_defense, level=walls.level, anchor="topright")
            self.all_sprites.add(self.wall)
            self.obstacle_sprites.add(self.wall)

            #add bonus from quarry resoure if applicable
            wall_bonus = 0
            for city_sprite in self.main_game.city_sprites:
                if city_sprite.country == self.enemy_general.country:
                    for building in self.siege_city_object.buildings:
                        if building.name == "Quarry":
                            wall_bonus += building.level
            print(wall_bonus)
            self.wall.max_health += int(self.wall.max_health * wall_bonus / 100)
            self.wall.health = self.wall.max_health

            self.enemy_morale = self.wall.max_health
            self.enemy_morale_max = self.wall.max_health
            self.player_morale = 100
            self.player_morale_max = 100

        else:
            self.player_deployment_position_offset = 50
            self.enemy_deployment_position_offset = 50

            self.player_morale = 100
            self.player_morale_max = 100
            self.enemy_morale = 100
            self.enemy_morale_max = 100

        self.player_command = 100
        self.player_command_max = 100
        self.enemy_command = 100
        self.enemy_command_max = 100

        self.player_command_recovery_rate = 1.0
        self.enemy_command_recovery_rate = 1.0

        self.kill_streak = 0
        self.morale_boost_applied = 0

        if self.custom:
            self.player_army = self.player_army_object
            self.enemy_army = self.enemy_army_object
            #self.player_army = {"bow_infantry":1000, "catapult":50}
            #self.enemy_army = {"bow_infantry":1000}
            #self.siege_defense = "right"
            #self.siege_city_object = City(grid_position_x=102, grid_position_y=19, name="Yanmen Pass", fortress_level=1,
            #     country="Wei", population=2000, population_growth=1, happiness=.7,
            #     defender=[], anchor="center", raw_buildings={"Walls": 3, "Barracks": 1, "Farm": 1, "Market": 1})
            #self.wall = FortressWall(self, self.max_camera_location+25, self.game_height - 800, "fort_wall_modified.png",
            #                         self.siege_defense, level=3, anchor="topright")
            #self.all_sprites.add(self.wall)
            #self.obstacle_sprites.add(self.wall)

            #if self.player_country == "shu":
                #self.player_army = {"spear_infantry":100}
            #else:
            #    self.player_army = {"halberd_cavalry":100}
            #    self.player_army = {"spear_militia":70, "sword_militia":120, "flaming_arrow_infantry":70, "spear_infantry":30, "sword_infantry": 30,
            #                        "light_cavalry":30}

            #if self.enemy_country == "shu" or True:
            #    self.enemy_army = {"light_cavalry": 100}
            #    #self.enemy_army = {"spear_militia":40, "sword_militia":70, "bow_militia":40, "barbarian_spear_woman":30,
            #    #                    "barbarian_assassin":20, "barbarian_archer":20, "spear_infantry":30, "sword_infantry": 30,}
            #    #self.enemy_army = {"spear_infantry":30, "halberd_cavalry":50, "sword_militia":50, "bow_militia":30}
            #    self.enemy_army = {"rattan_armor_infantry": 300}
            #else:
            #    #self.enemy_army = {"spear_militia":90, "sword_militia":150, "bow_militia":90, "spear_infantry":30, "sword_infantry": 30,}
            #    self.enemy_army = {"spear_infantry": 30, "halberd_cavalry": 50, "sword_militia": 50, "bow_militia": 30}

        else:
            self.player_army = self.player_army_object.army_composition
            self.enemy_army = self.enemy_army_object.army_composition

        self.player_army_unit_types = list(self.player_army.keys())
        self.enemy_army_unit_types = list(self.enemy_army.keys())
        try:
            self.wall.generate_defenders()
        except:
            pass

        self.apply_initial_boosts()
        self.battle_experience_bonus = (sum(self.player_army.values()) + sum(self.enemy_army.values()))/1000


        self.deployment_action_bar = Static(0, 0, "sprite_action_bar.png")
        self.fixed_sprites.add(self.deployment_action_bar)
        self.all_sprites.add(self.deployment_action_bar)

        self.general_action_bar = Static(400, 0, "sprite_action_bar.png")
        self.fixed_sprites.add(self.general_action_bar)
        self.all_sprites.add(self.general_action_bar)

        self.special_move_action_bar = Static(800, 0, "sprite_action_bar.png")
        self.fixed_sprites.add(self.special_move_action_bar)
        self.all_sprites.add(self.special_move_action_bar)



        self.general_avatar_bar = Static(405, 5, "general_avatar_bar_{}.png".format(self.player_general.simple_name),
                                         anchor="topleft")
        self.fixed_sprites.add(self.general_avatar_bar)
        self.all_sprites.add(self.general_avatar_bar)

        self.player_morale_bar = DynamicBar(405+70, 5+17, "player_morale", self)
        self.fixed_sprites.add(self.player_morale_bar)
        self.all_sprites.add(self.player_morale_bar)
        self.player_command_bar = DynamicBar(405+70, 5+29, "player_command", self)
        self.fixed_sprites.add(self.player_command_bar)
        self.all_sprites.add(self.player_command_bar)


        self.enemy_general_avatar_bar = Static(795, 5, "general_avatar_bar_{}.png".format(self.enemy_general.simple_name),
                                         anchor="topright", reverse=True)
        self.fixed_sprites.add(self.enemy_general_avatar_bar)
        self.all_sprites.add(self.enemy_general_avatar_bar)

        self.enemy_morale_bar = DynamicBar(795 - 70, 5 + 17, "enemy_morale", self, reverse=True)
        self.fixed_sprites.add(self.enemy_morale_bar)
        self.all_sprites.add(self.enemy_morale_bar)
        self.enemy_command_bar = DynamicBar(795 - 70, 5 + 29, "enemy_command", self, reverse=True)
        self.fixed_sprites.add(self.enemy_command_bar)
        self.all_sprites.add(self.enemy_command_bar)


        if self.siege_defense == "left":
            if self.terrain == "water":
                self.slot_indicator = Static(470, self.selected_slot * 200 + 320, "sprite_slot_indicator_1.png",
                                             [pg.image.load("sprite_slot_indicator_{}.png".format(i)) for i in [2, 3, 2, 1]],
                                             frame_delta=150)
            else:
                self.slot_indicator = Static(470, self.selected_slot * 60 + 160, "sprite_slot_indicator_1.png",
                                             [pg.image.load("sprite_slot_indicator_{}.png".format(i)) for i in [2, 3, 2, 1]],
                                             frame_delta=150)
        else:
            if self.terrain == "water":
                self.slot_indicator = Static(30, self.selected_slot*200+320, "sprite_slot_indicator_1.png",
                                             [pg.image.load("sprite_slot_indicator_{}.png".format(i)) for i in [2,3,2,1]],
                                             frame_delta = 150)
                if self.two_player:
                    self.enemy_slot_indicator = Static(LANDSCAPE_WIDTH-50, self.enemy_selected_slot*200+320, "sprite_enemy_slot_indicator_1.png",
                                                 [pg.image.load("sprite_enemy_slot_indicator_{}.png".format(i)) for i in [2,3,2,1]],
                                                  frame_delta = 150)

            else:
                self.slot_indicator = Static(30, self.selected_slot*60+160, "sprite_slot_indicator_1.png",
                                             [pg.image.load("sprite_slot_indicator_{}.png".format(i)) for i in [2,3,2,1]],
                                             frame_delta = 150)
                if self.two_player:
                    self.enemy_slot_indicator = Static(LANDSCAPE_WIDTH-50, self.enemy_selected_slot*60+160, "sprite_enemy_slot_indicator_1.png",
                                                 [pg.image.load("sprite_enemy_slot_indicator_{}.png".format(i)) for i in [2,3,2,1]],
                                                 frame_delta = 150)

        self.fixed_sprites.add(self.slot_indicator)
        self.all_sprites.add(self.slot_indicator)
        if self.two_player:
            self.fixed_sprites.add(self.enemy_slot_indicator)
            self.all_sprites.add(self.enemy_slot_indicator)

        self.player_unit_icons_list = []
        self.enemy_unit_icons_list = []
        self.load_unit_icons()
        self.load_special_attack_icons()

        self.cursor = Static(0,0,"sprite_cursor.png")
        self.fixed_sprites.add(self.cursor)
        self.priority_sprites.add(self.cursor)
        self.all_sprites.add(self.cursor)


        self.decision_text = ""
        self.game_message = ""
        self.game_message_size = 16
        self.game_message_color = BLUE

        if self.terrain not in ["water"]:
            self.showing_minimap = True
            self.minimap = Static(LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT, "sprite_battle_minimap.png", anchor="bottomright")
            self.fixed_sprites.add(self.minimap)
            self.priority_sprites.add(self.minimap)
            self.all_sprites.add(self.minimap)

            self.close_minimap_button = Static(self.minimap.rect.right, self.minimap.rect.top, "sprite_close_button.png",
                                               anchor="bottomright")
            self.fixed_sprites.add(self.close_minimap_button)
            self.priority_sprites.add(self.close_minimap_button)
            self.all_sprites.add(self.close_minimap_button)

            self.minimap_camera = Static(LANDSCAPE_WIDTH - 300 + (self.camera_location+LANDSCAPE_WIDTH)//8, LANDSCAPE_HEIGHT,
                                         "sprite_battle_minimap_camera.png", anchor="bottomright")
            self.fixed_sprites.add(self.minimap_camera)
            self.priority_sprites.add(self.minimap_camera)
            self.all_sprites.add(self.minimap_camera)

            self.minimap_slot_indicator = Static(LANDSCAPE_WIDTH - 300,
                                                 LANDSCAPE_HEIGHT - 80 + self.selected_slot*10,
                                                "sprite_slot_indicator_1_tiny.png", anchor="center")

            self.fixed_sprites.add(self.minimap_slot_indicator)
            self.priority_sprites.add(self.minimap_slot_indicator)
            self.all_sprites.add(self.minimap_slot_indicator)

            if self.two_player:
                self.enemy_minimap_slot_indicator = Static(LANDSCAPE_WIDTH - 15,
                                                     LANDSCAPE_HEIGHT - 80 + self.enemy_selected_slot*10,
                                                    "sprite_enemy_slot_indicator_1_tiny.png", anchor="center")

                self.fixed_sprites.add(self.enemy_minimap_slot_indicator)
                self.priority_sprites.add(self.enemy_minimap_slot_indicator)
                self.all_sprites.add(self.enemy_minimap_slot_indicator)

            self.collapsed_minimap = Static(-3000, -3000, "sprite_collapsed_battle_minimap.png", anchor="bottomright")
            self.fixed_sprites.add(self.collapsed_minimap)
            self.priority_sprites.add(self.collapsed_minimap)
            self.all_sprites.add(self.collapsed_minimap)

        else:
            self.showing_minimap = False

        self.post_battle_cmd = None
        if not self.retry:
            self.show_start_screen()
        if "empty_fortress" in self.player_general.skills and self.siege_defense == "left":
            self.player_morale = 1
            self.show_game_over_screen(won=True)
        elif "empty_fortress" in self.enemy_general.skills and self.siege_defense == "right":
            self.enemy_morale = 1
            self.show_game_over_screen(won=False)

        self.run()


    def load_special_attack_icons(self):
        #player side
        counter = 0
        for sp_atk in self.player_general.sp_atks:
            sp_atk_icon = SpecialAttackIcon(self, 425+70*counter, 80, sp_atk, anchor="topleft", master=self.player_general)
            if not sp_atk_icon.static:
                self.fixed_sprites.add(sp_atk_icon)
                self.all_sprites.add(sp_atk_icon)
                self.player_general_special_attack_icons_list.append(sp_atk_icon)
                counter += 1

        #enemy side
        counter = 0
        for sp_atk in self.enemy_general.sp_atks:
            sp_atk_icon = SpecialAttackIcon(self, 725-70*counter, 80, sp_atk, anchor="topleft", master=self.enemy_general)
            if not sp_atk_icon.static:
                self.fixed_sprites.add(sp_atk_icon)
                self.all_sprites.add(sp_atk_icon)
                self.enemy_general_special_attack_icons_list.append(sp_atk_icon)
                counter += 1


    def load_unit_icons(self):
        #player side
        count = 0
        for row in range(2):
            for column in range(4):
                if count < len(self.player_army_unit_types):
                    unit_type = self.player_army_unit_types[count]
                    unit_icon = UnitIcon(self, "left", column*85+40, row*62+11, unit_type, self.player_country)
                    if count == self.selected_unit_index:
                        unit_icon.selected = True

                else:
                    unit_icon = UnitIcon(self, "left", column*85+40, row*62+11, None, None)

                self.player_unit_icons_list.append(unit_icon)
                self.fixed_sprites.add(unit_icon)
                self.all_sprites.add(unit_icon)
                count += 1

        #enemy side
        count = 0
        for row in range(2):
            for column in range(4):
                if count < len(self.enemy_army_unit_types):
                    unit_type = self.enemy_army_unit_types[count]
                    unit_icon = UnitIcon(self, "right", column * 85 + 840, row * 62 + 11, unit_type, self.enemy_country)

                else:
                    unit_icon = UnitIcon(self, "right", column * 85 + 840, row * 62 + 11, None, None)

                self.enemy_unit_icons_list.append(unit_icon)
                self.fixed_sprites.add(unit_icon)
                self.all_sprites.add(unit_icon)
                count += 1


    def apply_initial_boosts(self):
        army_size_ratio = (sum(self.player_army.values())+1)/(sum(self.enemy_army.values())+1)
        if army_size_ratio < 1:
            self.enemy_command_recovery_rate *= (1/army_size_ratio)**.9
        else:
            self.player_command_recovery_rate *= army_size_ratio**.9

        min_player_morale_boost = .75
        min_enemy_morale_boost = .75
        if self.siege_defense == "left":
            min_player_morale_boost = 1
        elif self.siege_defense == "right":
            min_enemy_morale_boost = 1
            
        player_morale_boost = max([1+(self.player_general.combat_prowess - 50)/50, min_player_morale_boost])**1.5
        player_command_boost = max([1 + (self.player_general.command - 50) / 50, 1])
        player_command_recovery_boost = 1 + self.player_general.intellect/50

        enemy_morale_boost = max([1 + (self.enemy_general.combat_prowess - 50) / 50, min_enemy_morale_boost])**1.5
        enemy_command_boost = max([1 + (self.enemy_general.command - 50) / 50, 1])
        enemy_command_recovery_boost = 1 + self.enemy_general.intellect/50


        for skill_name in self.player_general_skills:
            if skill_name == "renowned_commander":
                player_morale_boost += .2
                player_command_boost += .2
            elif skill_name == "barbarian_queen":
                for unit in self.player_unit_command_cost:
                    if "barbarian" in unit or "rattan" in unit:
                        self.player_unit_command_cost[unit] = int(self.player_unit_command_cost[unit]*.75)
            elif skill_name == "legendary_archer":
                for unit in self.player_unit_command_cost:
                    if unit in RANGED_UNITS:
                        self.player_unit_command_cost[unit] = int(self.player_unit_command_cost[unit]*.9)
            elif skill_name == "spearman_commander":
                for unit in self.player_unit_command_cost:
                    if unit in SPEAR_UNITS:
                        self.player_unit_command_cost[unit] = int(self.player_unit_command_cost[unit]*.8)
            elif skill_name == "swordsman_commander":
                for unit in self.player_unit_command_cost:
                    if unit in BLADE_UNITS:
                        self.player_unit_command_cost[unit] = int(self.player_unit_command_cost[unit]*.8)
            elif skill_name == "cavalry_commander":
                for unit in self.player_unit_command_cost:
                    if unit in CAVALRY_UNITS:
                        self.player_unit_command_cost[unit] = int(self.player_unit_command_cost[unit]*.8)
            elif skill_name == "valiant_defender" and self.siege_defense == "left":
                player_morale_boost += .3
                player_command_boost += .3
            elif skill_name == "siege_master" and self.siege_defense == "right":
                player_morale_boost += .3
                player_command_boost += .3
            elif skill_name == "basic_formation":
                self.consecutive_deployment_limit += 1
            elif skill_name == "advanced_formation":
                self.consecutive_deployment_limit += 2
            elif skill_name == "military_strategist":
                player_command_boost += .05*len(self.player_army)
            elif skill_name == "colossal_stature":
                enemy_morale_boost -= .2
            elif skill_name == "landlubber":
                if self.terrain == "water":
                    player_morale_boost -= .2
                    player_command_boost -= .2
                else:
                    player_morale_boost += .1
                    player_command_boost += .1
            elif skill_name == "ambush_tactics" and self.terrain in ["rock", "forest"]:
                self.player_ambush_count = 12
            elif skill_name == "master_of_ambush" and self.terrain in ["rock", "forest"]:
                self.player_ambush_count = 24
            elif skill_name == "brilliant_commander":
                player_command_boost += .15
            elif skill_name == "field_commander" and not self.siege_defense:
                player_command_boost += .25
            elif skill_name == "daredevil_commander" and army_size_ratio < 1:
                if army_size_ratio - 1 >= 1:
                    bonus = 1
                else:
                    bonus = army_size_ratio - 1
                player_morale_boost += bonus
            elif skill_name == "humble_upbringing":
                for unit_type in self.player_army_unit_types:
                    if "militia" in unit_type:
                        player_morale_boost += .1



        for skill_name in self.enemy_general_skills:
            if skill_name == "renowned_commander":
                    enemy_morale_boost += .2
                    enemy_command_boost += .2
            elif skill_name == "barbarian_queen":
                for unit in self.enemy_unit_command_cost:
                    if "barbarian" in unit or "rattan" in unit:
                        self.enemy_unit_command_cost[unit] = int(self.enemy_unit_command_cost[unit]*.75)
            elif skill_name == "legendary_archer":
                for unit in self.enemy_unit_command_cost:
                    if unit in RANGED_UNITS:
                        self.enemy_unit_command_cost[unit] = int(self.enemy_unit_command_cost[unit]*.9)
            elif skill_name == "spearman_commander":
                for unit in self.enemy_unit_command_cost:
                    if unit in SPEAR_UNITS:
                        self.enemy_unit_command_cost[unit] = int(self.enemy_unit_command_cost[unit]*.8)
            elif skill_name == "swordsman_commander":
                for unit in self.enemy_unit_command_cost:
                    if unit in BLADE_UNITS:
                        self.enemy_unit_command_cost[unit] = int(self.enemy_unit_command_cost[unit]*.8)
            elif skill_name == "cavalry_commander":
                for unit in self.enemy_unit_command_cost:
                    if unit in CAVALRY_UNITS:
                        self.enemy_unit_command_cost[unit] = int(self.enemy_unit_command_cost[unit]*.8)
            elif skill_name == "valiant_defender" and self.siege_defense == "right":
                    enemy_morale_boost += .2
                    enemy_command_boost += .2
            elif skill_name == "siege_master" and self.siege_defense == "left":
                    enemy_morale_boost += .3
                    enemy_command_boost += .3
            elif skill_name == "military_strategist":
                enemy_command_boost += .05*len(self.enemy_army)
            elif skill_name == "colossal_stature":
                player_morale_boost -= .2
            elif skill_name == "landlubber":
                if self.terrain == "water":
                    enemy_morale_boost -= .2
                    enemy_command_boost -= .2
                else:
                    enemy_morale_boost += .1
                    enemy_command_boost += .1
            elif skill_name == "ambush_tactics" and self.terrain in ["rock", "forest"]:
                self.enemy_ambush_count = 12
            elif skill_name == "master_of_ambush" and self.terrain in ["rock", "forest"]:
                self.enemy_ambush_count = 24
            elif skill_name == "brilliant_commander":
                enemy_command_boost += .15
            elif skill_name == "field_commander" and not self.siege_defense:
                enemy_command_boost += .25
            elif skill_name == "daredevil_commander" and army_size_ratio < 1:
                if army_size_ratio - 1 >= 1:
                    bonus = 1
                else:
                    bonus = army_size_ratio - 1
                enemy_morale_boost += bonus
            elif skill_name == "humble_upbringing":
                for unit_type in self.enemy_army_unit_types:
                    if "militia" in unit_type:
                        enemy_morale_boost += .1

        if "inspiring_commander" in self.main_game.acquired_tech[self.player_general.country]:
            player_morale_boost += .1
        if "inspiring_commander" in self.main_game.acquired_tech[self.enemy_general.country]:
            enemy_morale_boost += .1
        if "effective_commander" in self.main_game.acquired_tech[self.player_general.country]:
            player_command_boost += .1
        if "effective_commander" in self.main_game.acquired_tech[self.enemy_general.country]:
            enemy_command_boost += .1

        #Add morale boost from alliance
        if not self.custom:
            player_morale_boost += len(self.main_game.alliance_relationships[self.player_general.country])*.1
            enemy_morale_boost += len(self.main_game.alliance_relationships[self.enemy_general.country])*.1


        self.player_morale_max = int(self.player_morale_max*player_morale_boost)
        self.player_morale = self.player_morale_max
        self.player_command_max = int(self.player_command_max * player_command_boost)
        self.player_command = self.player_command_max
        self.player_command_recovery_rate *= player_command_recovery_boost**.85

        self.enemy_morale_max = int(self.enemy_morale_max * enemy_morale_boost)
        self.enemy_morale = self.enemy_morale_max
        self.enemy_command_max = int(self.enemy_command_max * enemy_command_boost)
        self.enemy_command = self.enemy_command_max
        self.enemy_command_recovery_rate *= enemy_command_recovery_boost**.85

        #depending on the size of the battle, apply equal boost to both sides
        shared_boost = math.sqrt(1 + (sum(self.player_army.values()) + sum(self.enemy_army.values()))/500)
        self.player_command_recovery_rate *= shared_boost
        self.enemy_command_recovery_rate *= shared_boost


    def run(self):
        while self.playing:
            self.clock.tick(self.fps)
            self.events()
            self.update()
            self.draw()


    def enemy_decision(self):
        for sp_atk_icon in self.enemy_general_special_attack_icons_list:
            if sp_atk_icon.remaining_recovery_time == 0 and not sp_atk_icon.static:

                if sp_atk_icon.special_attack_name in ["expert_marksmanship"]:
                    if len([u for u in self.enemy_unit_sprites if u.unit_class == "ranged"]) >= 10:
                        self.use_special("right", sp_atk_icon)
                        break
                elif sp_atk_icon.special_attack_name in ["bloodthirst", "fierce_assault", "rapid_advance", "breakthrough", "relentless_charge", "eye_for_an_eye", "deadly_blade", "second_wind", "anticipation", "piercing_blade", "inspire"]:
                    if len(self.enemy_unit_sprites) >= 10:
                        self.use_special("right", sp_atk_icon)
                        break
                elif sp_atk_icon.special_attack_name in ["supply_raid", "disarrayment"]:
                    if len(self.player_unit_sprites) >= 12:
                        self.use_special("right", sp_atk_icon)
                        break
                elif sp_atk_icon.special_attack_name == "feigned_surrender":
                    if random() <= (len(self.player_unit_sprites) - 10) / 10 and self.enemy_morale >= self.enemy_morale_max*.4:
                        self.use_special("right", sp_atk_icon)
                        break
                elif sp_atk_icon.special_attack_name == "rally":
                    if self.enemy_morale <= self.enemy_morale_max*.9:
                        self.use_special("right", sp_atk_icon)
                        break
                elif sp_atk_icon.special_attack_name in ["sow_discord", "drowning_the_seven_armies","fire_attack","chained_fire_attack"]:
                    if random() <= (len(self.player_unit_sprites)-15)/10:
                        self.use_special("right", sp_atk_icon)
                        break
                elif sp_atk_icon.special_attack_name == "caution":
                    if random() <= .01 or self.siege_defense == "right":
                        self.use_special("right", sp_atk_icon)
                        break
                elif sp_atk_icon.special_attack_name == "iron_chain_formation":
                    if random() <= .01 or self.siege_defense == "right":
                        self.use_special("right", sp_atk_icon)
                        break
                elif sp_atk_icon.special_attack_name == "fortification":
                    if self.siege_defense == "right":
                        if self.wall.level >= 3 and ((len(self.player_unit_sprites)+10)/(11+len(self.enemy_unit_sprites)) <= 1.5 or sum(self.enemy_army.values()) <= 10):
                            self.use_special("right", sp_atk_icon)
                            break
                elif sp_atk_icon.special_attack_name == "barricade":
                    if self.terrain != "water":
                        self.use_special("right", sp_atk_icon)
                        break
                elif sp_atk_icon.special_attack_name == "last_stand":
                    if self.enemy_morale/self.enemy_morale_max <= .3 and self.enemy_morale < self.player_morale:
                        self.use_special("right", sp_atk_icon)
                        break
                elif sp_atk_icon.special_attack_name == "psychological_warfare":
                    if self.player_morale - self.enemy_morale >= 25:
                        self.use_special("right", sp_atk_icon)
                        break
                elif sp_atk_icon.special_attack_name in ["cavalry_charge"]:
                    if len([u for u in self.enemy_unit_sprites if u.unit_class == "cavalry"]) >= 5:
                        self.use_special("right", sp_atk_icon)
                        break
                elif sp_atk_icon.special_attack_name == "focus_attack":
                    pass
                elif random() >= .5:
                    self.use_special("right", sp_atk_icon)
                    break

        #if fortifying, don't bother deploy any units
        for effect in self.enemy_general_effects:
            effect_name, effect_start_time = effect
            if effect_name == "fortification":
                return

        #if chain fire attack ongoing, slow deployment
        if len(self.player_chained_fire_attack_sprites) > 0 and random() >= .05:
            return

        #deployment freeze due to sp atk
        for effect in self.player_general_effects:
            effect_name, effect_start_time = effect
            if effect_name == "disarrayment":
                return

        if self.terrain == "water":
            num_slots = 2
        else:
            num_slots = 8


        slot_status = [0]*num_slots
        slot_status_ranged = [0]*num_slots
        for unit in self.player_unit_sprites:
            slot_status[unit.slot] += 1
            if unit.unit_class == "ranged":
                slot_status_ranged[unit.slot] += 1
        for unit in self.enemy_unit_sprites:
            slot_status[unit.slot] -= 1
            if unit.unit_class == "ranged":
                slot_status_ranged[unit.slot] -= 1

        highest_delta = max(slot_status)
        slot_with_highest_delta = slot_status.index(highest_delta)

        if self.ai_difficulty >= 3:

            available_ranged_units = []
            for unit_type in self.enemy_army_unit_types:
                if unit_type in RANGED_UNITS and self.enemy_army[unit_type] > 0:
                    available_ranged_units.append(unit_type)

            enemy_unit_choice = choice(self.enemy_army_unit_types)
            if random() <= highest_delta/4:
                enemy_slot_choice = slot_with_highest_delta
                slot_unit_positions = [unit.rect.centerx + self.camera_location for unit in self.player_unit_sprites if unit.slot==slot_with_highest_delta]
                slot_avg_unit_x_pos = avg(slot_unit_positions)
                max_unit_x_pos = max(slot_unit_positions)
                #print(slot_avg_unit_x_pos, max_unit_x_pos, self.max_camera_location)
                if slot_status_ranged[slot_with_highest_delta]/20 >= random() and abs(slot_avg_unit_x_pos/self.max_camera_location) <= .4:
                    #look for ranged unit to place in slot where player is spamming archers; if ranged unit found, replace chosen unit with ranged unit; if not, choose another slot
                    if available_ranged_units and "hawk_eye" not in self.player_general_skills:
                        enemy_unit_choice = choice(available_ranged_units)
                    elif abs(slot_avg_unit_x_pos - self.max_camera_location) <= 50:
                        enemy_unit_choice = choice(self.enemy_army_unit_types)
                    else:
                        enemy_slot_choice = randrange(num_slots)
                else:
                    #if player units are still far away and ranged units are available, deploy ranged
                    if available_ranged_units and abs(slot_avg_unit_x_pos/self.max_camera_location) <= .25 and abs(max_unit_x_pos/self.max_camera_location) <= .4:
                        enemy_unit_choice = choice(available_ranged_units)
                    else:
                        enemy_unit_choice = choice(self.enemy_army_unit_types)

            else:
                enemy_slot_choice = randrange(num_slots)
                # check if wall is being attacked by any slots and defend if so
                if self.siege_defense == "right":
                    unit_wall_collision = pg.sprite.spritecollide(self.wall, self.player_unit_sprites, False)
                    if unit_wall_collision:
                        enemy_slot_choice = unit_wall_collision[0].slot

        else:
            enemy_unit_choice = choice(self.enemy_army_unit_types)
            if random() <= .75:
                enemy_slot_choice = slot_with_highest_delta
            else:
                enemy_slot_choice = randrange(num_slots)


        command_cost = self.enemy_unit_command_cost[enemy_unit_choice]
        save_command_points = self.enemy_command/self.enemy_command_max <= .9 and random() <= .5
        if self.enemy_ambush_count > 0:
            command_cost = command_cost//2
        if random() >= .02 and self.enemy_command/self.enemy_command_max <= .5:
            return
        if self.enemy_army[enemy_unit_choice] > 0 and self.enemy_command >= command_cost and not save_command_points:
            if self.enemy_ambush_count > 0:
                self.enemy_ambush_count -= 1
            self.enemy_command -= command_cost
            self.deploy_unit(side="right", unit_type=enemy_unit_choice, slot=enemy_slot_choice)



    def check_kill_streak(self):
        morale_boost = 0
        if self.kill_streak == 3:
            morale_boost = 2
        elif self.kill_streak == 5:
            morale_boost = 3
        elif self.kill_streak == 10:
            morale_boost = 5
        elif self.kill_streak == 20:
            morale_boost = 10
        elif self.kill_streak == 50:
            morale_boost = 20

        elif self.kill_streak == -3:
            morale_boost = -2
        elif self.kill_streak == -5:
            morale_boost = -3
        elif self.kill_streak == -10:
            morale_boost = -5
        elif self.kill_streak == -20:
            morale_boost = -10
        elif self.kill_streak == -50:
            morale_boost = -20

        if "vanity" in self.player_general.skills or "vanity" in self.enemy_general_skills:
            morale_boost *= 2

        if self.morale_boost_applied != morale_boost:
            self.morale_boost_applied = morale_boost

            if morale_boost > 0:
                if "vanity" in self.player_general.skills and random() <= .5:
                    self.special_attack_animation("left")
                    sp_atk_sound = pg.mixer.Sound("sp_atk_vanity_{}.ogg".format(LANGUAGE))
                    self.play_sound_effect(sp_atk_sound)
                self.play_sound_effect(self.morale_boost_sound)
                self.morale_change("left", morale_boost)
                self.morale_change("right", -morale_boost)
                self.game_message = "Kill streak: {}! Received +{} morale boost!".format(self.kill_streak, morale_boost)
                self.game_message_color = GREEN
                self.game_message_size = 24
            elif morale_boost < 0:
                if "vanity" in self.enemy_general.skills and random() <= .5:
                    self.special_attack_animation("right")
                    sp_atk_sound = pg.mixer.Sound("sp_atk_vanity_{}.ogg".format(LANGUAGE))
                    self.play_sound_effect(sp_atk_sound)
                self.morale_change("left", morale_boost)
                self.morale_change("right",-morale_boost) #will be negative; so minus decrement
                self.game_message = "Enemy kill streak: {}! Enemy received +{} morale!".format(self.kill_streak, -morale_boost)
                self.game_message_color = RED
                self.game_message_size = 24



    def update(self):

        if self.weather_resets > 0 and not self.custom and not self.decision_text:
            if self.siege_defense != "left":
                self.paused_time = pg.time.get_ticks()
                self.running = False
                self.ask_weather_confirmation()
                return
            else:
                self.generate_weather()
                self.weather_resets = 0


        if not self.running:
            for sp in self.all_sprites:
                c1 = sp in self.enemy_unit_sprites
                c2 = sp in self.player_unit_sprites
                c3 = sp in self.enemy_projectiles
                c4 = sp in self.player_projectiles
                c5 = type(sp) in [DefenderArcher, DefenderRockThrower]
                c6 = sp in self.weather_sprites
                c7 = type(sp) == SmokeBomb
                if not (c1 or c2 or c3 or c4 or c5 or c6 or c7):
                    sp.update()

            for unit in self.player_unit_sprites:
                if unit.unit_class == "ranged":
                    unit.last_fired += self.fps
                unit.last_update += self.fps
                unit.last_special_effect_activation += self.fps

            for unit in self.enemy_unit_sprites:
                if unit.unit_class == "ranged":
                    unit.last_fired += self.fps
                unit.last_update += self.fps
                unit.last_special_effect_activation += self.fps

            for sp in self.all_sprites:
                if sp not in self.player_unit_sprites and sp not in self.enemy_unit_sprites:
                    try:
                        sp.last_fired += self.fps
                    except:
                        pass

            return

        self.all_sprites.update()
        self.check_kill_streak()
        now = pg.time.get_ticks()

        if now - self.last_action > 3000:
            self.game_message = ""
            self.game_message_size = 16
            self.game_message_color = BLUE

        if now - self.last_deployment >= 2500:
            self.consecutive_deployments = 0

        if now - self.enemy_last_deployment >= 2500:
            self.enemy_consecutive_deployments = 0

        if self.earthquake_count > 0:
            if now - self.last_earthquake >= 100:
                earthquake_offset_x, earthquake_offset_y = randrange(-2,3), randrange(-2,3)
                self.earthquake_offset += (earthquake_offset_x, earthquake_offset_y)
                self.background.rect.x += earthquake_offset_x
                self.background.rect.y += earthquake_offset_y
                self.earthquake_count -= 1
                self.last_earthquake = now
                if self.earthquake_count == 0:
                    self.background.rect.x -= self.earthquake_offset[0]
                    self.background.rect.y -= self.earthquake_offset[1]
                    self.earthquake_offset = (0,0)

        # check if timed effects have expired
        for effect in self.player_general_effects.copy():
            effect_name, effect_start_time = effect
            if "versatile_commander" in self.main_game.acquired_tech[self.player_general.country]:
                recovery_ratio = .8
            else:
                recovery_ratio = 1.0

            if "Effect_Length" in SPECIAL_ATTACK_INFO[effect_name]:
                effect_time_limit = SPECIAL_ATTACK_INFO[effect_name]["Effect_Length"]
            else:
                effect_time_limit = SPECIAL_ATTACK_INFO[effect_name]["Recovery"]
            if now - effect_start_time >= effect_time_limit*1000*recovery_ratio:
                self.player_general_effects.remove(effect)
                if effect_name == "last_stand":
                    self.player_morale = 0

        for effect in self.enemy_general_effects:
            effect_name, effect_start_time = effect
            if "Effect_Length" in SPECIAL_ATTACK_INFO[effect_name]:
                effect_time_limit = SPECIAL_ATTACK_INFO[effect_name]["Effect_Length"]
            else:
                effect_time_limit = SPECIAL_ATTACK_INFO[effect_name]["Recovery"]
            if now - effect_start_time >= effect_time_limit*1000:
                self.enemy_general_effects.remove(effect)
                if effect_name == "last_stand":
                    self.enemy_morale = 0


        self.player_command += .003*self.player_command_recovery_rate*self.player_command_max
        self.enemy_command += .003*self.enemy_command_recovery_rate*self.enemy_command_max
        #self.player_command += .3*self.player_command_recovery_rate
        #self.enemy_command += .3*self.enemy_command_recovery_rate
        if self.player_command >= self.player_command_max:
            self.player_command = self.player_command_max
        if self.enemy_command >= self.enemy_command_max:
            self.enemy_command = self.enemy_command_max


        if not self.two_player:
            self.enemy_decision()

        if self.interactive_special_atk:
            pos = pg.mouse.get_pos()
            self.interactive_special_atk_region.rect.centerx, self.interactive_special_atk_region.rect.centery = pos

        for projectile in self.player_projectiles:
            if not projectile.dead:
                target_sprites_same_slot = pg.sprite.Group()
                for enemy_unit in self.enemy_unit_sprites:
                    if projectile.slot == enemy_unit.slot:
                        target_sprites_same_slot.add(enemy_unit)
                col = pg.sprite.spritecollide(projectile, target_sprites_same_slot, False)
                if col:
                    hit_unit = col[0]
                    damage_type = None
                    if random() <= projectile.fatal_strike_rate and hit_unit.unit_type not in ["wall", "barricade"]:
                        damage_dealt = hit_unit.health
                    else:
                        damage_dealt = ((projectile.damage+2) / (hit_unit.defense+1)) ** 1.5 * randrange(10, 13)
                        if self.terrain in ["forest"] and projectile.name not in ["flaming_arrow"]:
                            damage_dealt = int(damage_dealt*.75)
                        if hit_unit.unit_class == "cavalry":
                            damage_dealt = int(damage_dealt*1.5)
                        if projectile.name in ["flaming_arrow"]:
                            damage_type = "fire"
                            if self.terrain == "forest":
                                damage_dealt *= 2
                            if self.weather == "rain":
                                damage_dealt = damage_dealt//2
                        if ("rattan" in hit_unit.unit_type or hit_unit.unit_type=="catapult" or hit_unit.unit_type == "barricade") and projectile.name in ["flaming_arrow"]:
                            damage_dealt *= 2

                    hit_unit.got_hit(damage_dealt, projectile.stun, projectile.repel, damage_type=damage_type)
                    if projectile.name != "catapult_rock":
                        projectile.kill()


        for projectile in self.enemy_projectiles:
            if not projectile.dead:
                target_sprites_same_slot = pg.sprite.Group()
                for enemy_unit in self.player_unit_sprites:
                    if projectile.slot == enemy_unit.slot:
                        target_sprites_same_slot.add(enemy_unit)
                col = pg.sprite.spritecollide(projectile, target_sprites_same_slot, False)
                if col:
                    hit_unit = col[0]
                    damage_type = None
                    if random() <= projectile.fatal_strike_rate and hit_unit.unit_type not in ["wall", "barricade"]:
                        damage_dealt = hit_unit.health
                    else:
                        damage_dealt = ((projectile.damage+2) / (hit_unit.defense+1)) ** 1.5 * randrange(10, 13)
                        if self.terrain in ["forest"] and projectile.name not in ["flaming_arrow"]:
                            damage_dealt = int(damage_dealt * .75)
                        if hit_unit.unit_class == "cavalry":
                            damage_dealt = int(damage_dealt * 1.5)
                        if projectile.name in ["flaming_arrow"]:
                            damage_type = "fire"
                            if self.terrain == "forest":
                                damage_dealt *= 2
                            if self.weather == "rain":
                                damage_dealt = damage_dealt // 2
                        if ("rattan" in hit_unit.unit_type or hit_unit.unit_type=="catapult" or hit_unit.unit_type == "barricade") and projectile.name in ["flaming_arrow"]:
                            damage_dealt *= 2

                    hit_unit.got_hit(damage_dealt, projectile.stun, projectile.repel, damage_type=damage_type)
                    if projectile.name != "catapult_rock":
                        projectile.kill()

        #Smoke bomb collision
        for sp in self.all_sprites:
            if type(sp) == SmokeBomb:
                if sp.dead:
                    col = pg.sprite.spritecollide(sp, self.enemy_unit_sprites, False)
                    for hit_unit in col:
                        damage_dealt = ((sp.damage + 2) / (hit_unit.defense + 1)) ** 1.5 * randrange(10, 13)
                        if self.terrain == "forest":
                            damage_dealt *= 2
                        if self.weather == "rain":
                            damage_dealt = damage_dealt // 2
                        if ("rattan" in hit_unit.unit_type or hit_unit.unit_type == "catapult" or hit_unit.unit_type == "barricade"):
                            damage_dealt *= 2
                        hit_unit.got_hit(damage_dealt, 0, 0,damage_type="fire")
                    col = pg.sprite.spritecollide(sp, self.player_unit_sprites, False)
                    for hit_unit in col:
                        damage_dealt = ((sp.damage + 2) / (hit_unit.defense + 1)) ** 1.5 * randrange(10, 13)
                        if self.terrain == "forest":
                            damage_dealt *= 2
                        if self.weather == "rain":
                            damage_dealt = damage_dealt // 2
                        if ("rattan" in hit_unit.unit_type or hit_unit.unit_type == "catapult" or hit_unit.unit_type == "barricade"):
                            damage_dealt *= 2
                        hit_unit.got_hit(damage_dealt, 0, 0,damage_type="fire")

                    if self.siege_defense:
                        col = pg.sprite.spritecollide(sp, self.obstacle_sprites, False)
                        for hit_obstacle in col:
                            damage_dealt = ((sp.damage + 2) / (hit_obstacle.defense + 1)) ** 1.5
                            hit_obstacle.got_hit(damage_dealt, 0, 0, damage_type="fire")


        #chained fire attack

        max_spread_distance = 100
        min_spread_prob = .2
        if now - self.player_sp_atk_timestamp >= 500:
            self.player_sp_atk_timestamp = now
            for cfas in self.player_chained_fire_attack_sprites:
                for enemy_unit in self.enemy_unit_sprites:
                    if enemy_unit not in self.player_chained_fire_attack_targets:
                        distance = calculate_diagonal_distance(cfas.rect.centerx, cfas.rect.centery,
                                                               enemy_unit.rect.centerx, enemy_unit.rect.centery)
                        if self.terrain == "forest":
                            max_spread_distance *= 1.25
                            min_spread_prob -= .1
                        if self.weather == "rain":
                            max_spread_distance *= .75
                            min_spread_prob += .1
                        if distance <= max_spread_distance and random() >= min_spread_prob:
                            self.generate_chained_fire_attack(target=enemy_unit, attacking_side="left")

                    elif random() <= (1-min_spread_prob)/100:
                        self.player_chained_fire_attack_targets.remove(enemy_unit)

        if now - self.enemy_sp_atk_timestamp >= 500:
            self.enemy_sp_atk_timestamp = now
            for cfas in self.enemy_chained_fire_attack_sprites:
                for player_unit in self.player_unit_sprites:
                    if player_unit not in self.enemy_chained_fire_attack_targets:
                        distance = calculate_diagonal_distance(cfas.rect.centerx, cfas.rect.centery,
                                                               player_unit.rect.centerx, player_unit.rect.centery)
                        if self.terrain == "forest":
                            max_spread_distance *= 1.25
                            min_spread_prob -= .1
                        if self.weather == "rain":
                            max_spread_distance *= .75
                            min_spread_prob += .1
                        if distance <= max_spread_distance and random() >= min_spread_prob:
                            self.generate_chained_fire_attack(target=player_unit, attacking_side="right")

                    elif random() <= (1-min_spread_prob)/100:
                        self.enemy_chained_fire_attack_targets.remove(player_unit)


        # check for projectiles hitting wall
        if self.siege_defense == "left":
            wall_col = pg.sprite.spritecollide(self.wall, self.enemy_projectiles, False, pg.sprite.collide_circle_ratio(.5))
            for projectile in wall_col:
                mask_col = pg.sprite.spritecollide(self.wall, wall_col, False, pg.sprite.collide_mask)
                if mask_col:
                    if projectile.name == "catapult_rock":
                        damage_dealt = ((projectile.damage*3) / (self.wall.defense*5 + 1)) ** 1.5 * randrange(30, 43)
                    else:
                        damage_dealt = 1
                    self.wall.got_hit(damage_dealt,0,0)
                    projectile.kill()

        elif self.siege_defense == "right":
            wall_col = pg.sprite.spritecollide(self.wall, self.player_projectiles, False, pg.sprite.collide_circle_ratio(.5))
            for projectile in wall_col:
                mask_col = pg.sprite.spritecollide(self.wall, wall_col, False, pg.sprite.collide_mask)
                if mask_col:
                    if projectile.name == "catapult_rock":
                        damage_dealt = ((projectile.damage*3) / (self.wall.defense*5 + 1)) ** 1.5 * randrange(30, 43)
                    else:
                        damage_dealt = 1
                    self.wall.got_hit(damage_dealt,0,0)
                    projectile.kill()


        if self.player_morale <= 0 or (sum(self.player_army.values()) + len(self.player_unit_sprites) == 0 and self.siege_defense != "left"):
            self.show_game_over_screen(won=False)
        elif self.enemy_morale <= 0 or (sum(self.enemy_army.values()) + len(self.enemy_unit_sprites) == 0 and self.siege_defense != "right"):
            self.show_game_over_screen(won=True)


    def check_deployment_conditions(self):

        for unit_type_index in range(len(self.player_army_unit_types)):
            unit_type = self.player_army_unit_types[unit_type_index]
            deployment_cost = self.player_unit_command_cost[unit_type]
            unit_icon = self.player_unit_icons_list[unit_type_index]
            if self.player_command < deployment_cost or self.player_army[unit_type] <= 0:
                unit_icon.disabled = True
            else:
                unit_icon.disabled = False

        for unit_type_index in range(len(self.enemy_army_unit_types)):
            unit_type = self.enemy_army_unit_types[unit_type_index]
            deployment_cost = self.enemy_unit_command_cost[unit_type]
            unit_icon = self.enemy_unit_icons_list[unit_type_index]
            if self.enemy_command < deployment_cost or self.enemy_army[unit_type] <= 0:
                unit_icon.disabled = True
            else:
                unit_icon.disabled = False

    def generate_projectile(self, side, projectile_type, shooter, target):
        if side == "left":
            projectile_x = shooter.rect.centerx
            target_x = target.rect.right
        else:
            projectile_x = shooter.rect.centerx
            target_x = target.rect.centerx
        projectile_y = shooter.rect.centery
        target_y = target.rect.centery

        if projectile_type == "arrow":
            self.play_sound_effect(self.arrow_sound, prob=(150-self.number_of_ranged_units)/100)
            if side == "left":
                x_dist = abs(target_x - projectile_x)
            else:
                x_dist = -abs(target_x - projectile_x)
            y_dist = abs(target_y - projectile_y)
            dist = (x_dist ** 2 + y_dist ** 2) ** .5 + 1

            randomness = (1 - shooter.firing_accuracy)/2*randrange(-15,16)
            x_vel = x_dist/dist*20
            y_vel = -.25*abs(x_dist/20)**.99 + randomness

            if target.unit_class != "obstacle":
                if not target.unit_class == "ranged" and not target.attacking:
                    adj_x_dist = abs(target_x - projectile_x) - 5*target.walking_acc**1.5
                    if adj_x_dist <= 150:
                        adj_x_dist = 150
                    y_vel = -.25*abs(adj_x_dist/20)**.975 + randomness
                else:
                    y_vel -= abs((x_dist / 1200) ** 2)


            projectile = Arrow(self, projectile_x, projectile_y, x_vel, y_vel, shooter.side, shooter.slot,
                               shooter.fatal_strike_rate, shooter.firing_damage, 0, 0, None)
            self.all_sprites.add(projectile)
            if side == "left":
                self.player_projectiles.add(projectile)
            else:
                self.enemy_projectiles.add(projectile)

        elif projectile_type == "flaming_arrow":
            self.play_sound_effect(self.arrow_sound, prob=(150-self.number_of_ranged_units)/100)
            if side == "left":
                x_dist = abs(target_x - projectile_x)
            else:
                x_dist = -abs(target_x - projectile_x)
            y_dist = abs(target_y - projectile_y)
            dist = (x_dist ** 2 + y_dist ** 2) ** .5 + 1

            randomness = (1 - shooter.firing_accuracy)/2*randrange(-15,16)
            x_vel = x_dist/dist*20
            y_vel = -.25*abs(x_dist/20)**.99 + randomness

            if target.unit_class != "obstacle":
                if not target.unit_class == "ranged" and not target.attacking:
                    adj_x_dist = abs(target_x - projectile_x) - 5*target.walking_acc**2
                    if adj_x_dist <= 150:
                        adj_x_dist = 150
                    y_vel = -.25*abs(adj_x_dist/20)**.975 + randomness
                else:
                    y_vel -= abs((x_dist / 1200) ** 2)

            projectile = Flaming_Arrow(self, projectile_x, projectile_y, x_vel, y_vel, shooter.side, shooter.slot,
                               shooter.fatal_strike_rate, shooter.firing_damage, 0, 0, None)
            self.all_sprites.add(projectile)
            if side == "left":
                self.player_projectiles.add(projectile)
            else:
                self.enemy_projectiles.add(projectile)

        elif projectile_type == "catapult_rock":
            self.play_sound_effect(self.rock_sound)
            if side == "left":
                direction = 1
            else:
                direction = -1

            randomness = (1 - shooter.firing_accuracy)/2*randrange(-15,0)
            x_vel = direction*randrange(35,41)
            if type(target) != FortressWall:
                y_vel = -7 + randomness
            else:
                y_vel = -12 + randomness

            projectile = Catapult_Rock(self, projectile_x, projectile_y-50, x_vel, y_vel, shooter.side, shooter.slot,
                                       shooter.fatal_strike_rate*2, shooter.firing_damage*2,
                                       shooter.unit_effects["stun"], shooter.unit_effects["repel"])
            self.all_sprites.add(projectile)
            if side == "left":
                self.player_projectiles.add(projectile)
            else:
                self.enemy_projectiles.add(projectile)

        elif projectile_type == "crossbow_bolt":
            self.play_sound_effect(self.arrow_sound_old, prob=(150-self.number_of_ranged_units)/100)
            if side == "left":
                direction = 1
            else:
                direction = -1
            randomness = (1 - shooter.firing_accuracy)/2*randrange(-3,4)
            x_vel = direction*50
            y_vel = -5 + randomness

            projectile = Arrow(self, projectile_x, projectile_y, x_vel, y_vel, shooter.side, shooter.slot,
                               shooter.fatal_strike_rate, shooter.firing_damage, 0, 0, None, arrow_type=projectile_type)
            self.all_sprites.add(projectile)
            if side == "left":
                self.player_projectiles.add(projectile)
            else:
                self.enemy_projectiles.add(projectile)

        elif projectile_type == "wall_arrow":
            self.play_sound_effect(self.arrow_sound, prob=(150-self.number_of_ranged_units)/100)
            if side == "left":
                if type(target) == Unit:
                    x_dist = abs(target_x - projectile_x) - target.walking_acc
                else:
                    x_dist = abs(target_x - projectile_x)
            else:
                if type(target) == Unit:
                    x_dist = -abs(target_x - projectile_x) + target.walking_acc
                else:
                    x_dist = -abs(target_x - projectile_x)
            y_dist = abs(target_y - projectile_y)
            randomness = (1 - shooter.firing_accuracy)*randrange(5, 11)
            dist = (x_dist ** 2 + y_dist ** 2) ** .5 + 1
            x_vel = (x_dist/dist)*randomness*10
            y_vel = abs(y_dist / dist)*15

            projectile = Arrow(self, projectile_x, projectile_y, x_vel, y_vel, shooter.side, shooter.slot,
                               shooter.fatal_strike_rate, shooter.firing_damage, 0, 0, None, arrow_type=projectile_type)
            self.all_sprites.add(projectile)
            if side == "left":
                self.player_projectiles.add(projectile)
            else:
                self.enemy_projectiles.add(projectile)


        elif projectile_type == "wall_rock":
            self.play_sound_effect(self.rock_sound)
            if side == "left":
                if type(target) == Unit:
                    x_dist = abs(target_x - projectile_x) - target.walking_acc**2
                else:
                    x_dist = abs(target_x - projectile_x)
            else:
                if type(target) == Unit:
                    x_dist = -abs(target_x - projectile_x) + target.walking_acc**2
                else:
                    x_dist = -abs(target_x - projectile_x)
            y_dist = abs(target_y - projectile_y)
            randomness = (1 - shooter.firing_accuracy)* randrange(5, 11)
            dist = (x_dist ** 2 + y_dist ** 2) ** .5 + 1
            x_vel = (x_dist/dist)*randomness*5
            y_vel = abs(y_dist / dist)*5

            projectile = Rock(projectile_x, projectile_y, x_vel, y_vel, shooter.side, shooter.slot,
                               shooter.fatal_strike_rate, shooter.firing_damage, 0, 0, None)
            self.all_sprites.add(projectile)
            if side == "left":
                self.player_projectiles.add(projectile)
            else:
                self.enemy_projectiles.add(projectile)


    def morale_change(self, side, amount):
        if side == "right":
            self.enemy_morale += amount
            if self.enemy_morale <= 0:
                if "unwavering_grit" in self.enemy_general.skills and random() <= .8 and self.enemy_command_recovery_rate >= .1:
                    self.special_attack_animation("right")
                    sp_atk_sound = pg.mixer.Sound("sp_atk_unwavering_grit_{}.ogg".format(LANGUAGE))
                    self.play_sound_effect(sp_atk_sound)
                    self.enemy_morale = int(self.enemy_morale_max*.25)
                    self.enemy_command_recovery_rate *= .75
                else:
                    self.enemy_morale = 0

            elif self.enemy_morale >= self.enemy_morale_max:
                self.enemy_morale = self.enemy_morale_max
        else:
            self.player_morale += amount
            if self.player_morale <= 0:
                if "unwavering_grit" in self.player_general.skills and random() <= .8 and self.player_command_recovery_rate >= .1:
                    self.special_attack_animation("left")
                    sp_atk_sound = pg.mixer.Sound("sp_atk_unwavering_grit_{}.ogg".format(LANGUAGE))
                    self.play_sound_effect(sp_atk_sound)
                    self.player_morale = int(self.player_morale_max*.25)
                    self.player_command_recovery_rate *= .75
                else:
                    self.player_morale = 0
            elif self.player_morale >= self.player_morale_max:
                self.player_morale = self.player_morale_max


    def deploy_unit(self, side, unit_type, slot, free=False):

        if unit_type in RANGED_UNITS:
            self.number_of_ranged_units += 1

        if self.terrain == "water":
            deployment_y_position = slot*200+340
        else:
            deployment_y_position = slot*60+180

        if side == "left":
            if self.siege_defense == "left":
                self.player_deployment_position_offset = 500 - 50*slot
                for effect in self.player_general_effects:
                    effect_name, effect_start_time = effect
                    if effect_name == "fortification":
                        self.game_message = "Cannot deploy units due to 'Fortification' effect."
                        self.game_message_color = RED
                        self.play_sound_effect(self.deployment_fail_sound)
                        self.last_action = pg.time.get_ticks()
                        return

            for effect in self.enemy_general_effects:
                effect_name, effect_start_time = effect
                if effect_name == "disarrayment":
                    self.game_message = "Cannot deploy units due to 'Disarrayment' effect."
                    self.game_message_color = RED
                    self.play_sound_effect(self.deployment_fail_sound)
                    self.last_action = pg.time.get_ticks()
                    return

            deployment_cost = self.player_unit_command_cost[unit_type]
            if self.player_ambush_count > 0:
                deployment_cost = deployment_cost//2
                self.player_ambush_count -= 1
            if free:
                self.consecutive_deployments = 0
                deployment_cost = 0

            if self.player_army[unit_type] <= 0:
                #self.play_sound_effect(self.deployment_fail_sound)
                if not self.game_message:
                    #self.game_message = "Deployment failed. No more of selected unit in army."
                    #self.game_message_color = RED
                    self.last_action = pg.time.get_ticks()
                return
            elif self.player_command < deployment_cost:
                #self.play_sound_effect(self.deployment_fail_sound)
                if not self.game_message:
                    #self.game_message = "Not enough Command Points to deploy unit."
                    #self.game_message_color = RED
                    self.last_action = pg.time.get_ticks()
                return
            elif self.consecutive_deployments >= self.consecutive_deployment_limit:
                self.play_sound_effect(self.deployment_fail_sound)
                self.game_message = "Consecutive deployment limit reached!"
                self.game_message_color = DARK_RED
                self.last_action = pg.time.get_ticks()
                return


            if unit_type == "spear_militia":
                unit = Spear_Militia(self, self.player_deployment_position_offset - self.camera_location, deployment_y_position,
                                     unit_type, self.player_country, side, slot)

            elif unit_type == "spear_infantry":
                unit = Spear_Infantry(self, self.player_deployment_position_offset-self.camera_location,
                                     deployment_y_position, unit_type, self.player_country, side, slot)

            elif unit_type == "longspear_infantry":
                unit = Longspear_Infantry(self, self.player_deployment_position_offset-self.camera_location,
                                     deployment_y_position, unit_type, self.player_country, side, slot)

            elif unit_type == "elite_spear_infantry":
                unit = Elite_Spear_Infantry(self, self.player_deployment_position_offset-self.camera_location,
                                     deployment_y_position, unit_type, self.player_country, side, slot)

            elif unit_type == "barbarian_spear_woman":
                unit = Barbarian_Spear_Woman(self, self.player_deployment_position_offset-self.camera_location,
                                             deployment_y_position, unit_type, self.player_country, side, slot)

            elif unit_type == "barbarian_assassin":
                unit = Barbarian_Assassin(self, self.player_deployment_position_offset-self.camera_location,
                                          deployment_y_position, unit_type, self.player_country, side, slot)

            elif unit_type == "rattan_armor_infantry":
                unit = Rattan_Armor_Infantry(self, self.player_deployment_position_offset-self.camera_location,
                                          deployment_y_position, unit_type, self.player_country, side, slot)

            elif unit_type == "barbarian_archer":
                unit = Barbarian_Archer(self, self.player_deployment_position_offset-self.camera_location,
                                        deployment_y_position, unit_type, self.player_country, side, slot)

            elif unit_type == "axe_militia":
                unit = Axe_Militia(self, self.player_deployment_position_offset-self.camera_location,
                                     deployment_y_position, unit_type, self.player_country, side, slot)

            elif unit_type == "sword_militia":
                unit = Sword_Militia(self, self.player_deployment_position_offset-self.camera_location,
                                     deployment_y_position, unit_type, self.player_country, side, slot)

            elif unit_type == "light_dagger_infantry":
                unit = Light_Dagger_Infantry(self, self.player_deployment_position_offset-self.camera_location,
                                     deployment_y_position, unit_type, self.player_country, side, slot)

            elif unit_type == "sword_infantry":
                unit = Sword_Infantry(self, self.player_deployment_position_offset-self.camera_location,
                                     deployment_y_position, unit_type, self.player_country, side, slot)

            elif unit_type == "bow_infantry":
                unit = Bow_Infantry(self, self.player_deployment_position_offset-self.camera_location,
                                     deployment_y_position, unit_type, self.player_country, side, slot)

            elif unit_type == "flaming_arrow_infantry":
                unit = Flaming_Arrow_Infantry(self, self.player_deployment_position_offset-self.camera_location,
                                     deployment_y_position, unit_type, self.player_country, side, slot)

            elif unit_type == "crossbow_infantry":
                unit = Crossbow_Infantry(self, self.player_deployment_position_offset-self.camera_location,
                                     deployment_y_position, unit_type, self.player_country, side, slot)

            elif unit_type == "catapult":
                unit = Catapult(self, self.player_deployment_position_offset-self.camera_location,
                                     deployment_y_position, unit_type, self.player_country, side, slot)

            elif unit_type == "bow_militia":
                unit = Bow_Militia(self, self.player_deployment_position_offset-self.camera_location,
                                   deployment_y_position, unit_type, self.player_country, side, slot)

            elif unit_type == "shielded_sword_infantry":
                unit = Shielded_Sword_Infantry(self, self.player_deployment_position_offset-self.camera_location,
                                               deployment_y_position, unit_type, self.player_country, side, slot)

            elif unit_type == "elite_sword_infantry":
                unit = Elite_Sword_Infantry(self, self.player_deployment_position_offset-self.camera_location,
                                               deployment_y_position, unit_type, self.player_country, side, slot)

            elif unit_type == "mace_infantry":
                unit = Mace_Infantry(self, self.player_deployment_position_offset-self.camera_location, deployment_y_position,
                                   unit_type, self.player_country, side, slot)

            elif unit_type == "battleaxe_infantry":
                unit = Battleaxe_Infantry(self, self.player_deployment_position_offset-self.camera_location, deployment_y_position,
                                   unit_type, self.player_country, side, slot)

            elif unit_type == "elephant_rider":
                unit = Elephant_Rider(self, self.player_deployment_position_offset-self.camera_location, deployment_y_position,
                                   unit_type, self.player_country, side, slot)

            elif unit_type == "halberd_cavalry":
                unit = Halberd_Cavalry(self, self.player_deployment_position_offset-self.camera_location, deployment_y_position,
                                   unit_type, self.player_country, side, slot)

            elif unit_type == "light_cavalry":
                unit = Light_Cavalry(self, self.player_deployment_position_offset-self.camera_location, deployment_y_position,
                                   unit_type, self.player_country, side, slot)

            elif unit_type == "tiger_knight_cavalry":
                unit = Tiger_Knight_Cavalry(self, self.player_deployment_position_offset-self.camera_location, deployment_y_position,
                                   unit_type, self.player_country, side, slot)

            self.player_unit_sprites.add(unit)
            self.all_sprites.add(unit)
            self.apply_unit_deployment_boosts(unit)
            self.player_army[unit_type] -= 1
            self.player_command -= deployment_cost
            #if self.player_army[unit_type] <= 0 or self.player_command < deployment_cost:
                #self.player_unit_icons_list[self.selected_unit_index].disabled = True

            if self.last_deployment_slot == self.selected_slot and self.player_ambush_count <= 0:
                self.consecutive_deployments += 1
            self.last_deployment_slot = self.selected_slot
            self.last_deployment = pg.time.get_ticks()

        else:
            if self.two_player:
                deployment_cost = self.enemy_unit_command_cost[unit_type]
                if self.enemy_ambush_count > 0:
                    deployment_cost = deployment_cost//2
                    self.enemy_ambush_count -= 1
                if free:
                    self.enemy_consecutive_deployments = 0
                    deployment_cost = 0

                if self.enemy_army[unit_type] <= 0:
                    #self.play_sound_effect(self.deployment_fail_sound)
                    if not self.game_message:
                        #self.game_message = "Deployment failed. No more of selected unit in army."
                        #self.game_message_color = RED
                        self.last_action = pg.time.get_ticks()
                    return
                elif self.enemy_command < deployment_cost:
                    #self.play_sound_effect(self.deployment_fail_sound)
                    if not self.game_message:
                        #self.game_message = "Not enough Command Points to deploy unit."
                        #self.game_message_color = RED
                        self.last_action = pg.time.get_ticks()
                    return
                elif self.enemy_consecutive_deployments >= self.enemy_consecutive_deployment_limit:
                    #self.play_sound_effect(self.deployment_fail_sound)
                    #self.game_message = "Consecutive deployment limit reached!"
                    #self.game_message_color = DARK_RED
                    self.last_action = pg.time.get_ticks()
                    return

            if self.siege_defense == "right":
                self.enemy_deployment_position_offset = 500 - 50*slot

            if unit_type == "spear_militia":
                unit = Spear_Militia(self, self.max_camera_location-self.camera_location-self.enemy_deployment_position_offset,
                                     deployment_y_position, unit_type, self.enemy_country, side, slot)

            elif unit_type == "spear_infantry":
                unit = Spear_Infantry(self, self.max_camera_location - self.camera_location - self.enemy_deployment_position_offset,
                                     deployment_y_position, unit_type, self.enemy_country, side, slot)

            elif unit_type == "longspear_infantry":
                unit = Longspear_Infantry(self, self.max_camera_location - self.camera_location - self.enemy_deployment_position_offset,
                                     deployment_y_position, unit_type, self.enemy_country, side, slot)

            elif unit_type == "elite_spear_infantry":
                unit = Elite_Spear_Infantry(self, self.max_camera_location - self.camera_location - self.enemy_deployment_position_offset,
                                     deployment_y_position, unit_type, self.enemy_country, side, slot)

            elif unit_type == "barbarian_spear_woman":
                unit = Barbarian_Spear_Woman(self, self.max_camera_location-self.camera_location-self.enemy_deployment_position_offset,
                                     deployment_y_position, unit_type, self.enemy_country, side, slot)

            elif unit_type == "barbarian_assassin":
                unit = Barbarian_Assassin(self, self.max_camera_location-self.camera_location-self.enemy_deployment_position_offset,
                                     deployment_y_position, unit_type, self.enemy_country, side, slot)

            elif unit_type == "rattan_armor_infantry":
                unit = Rattan_Armor_Infantry(self, self.max_camera_location-self.camera_location-self.enemy_deployment_position_offset,
                                     deployment_y_position, unit_type, self.enemy_country, side, slot)

            elif unit_type == "barbarian_archer":
                unit = Barbarian_Archer(self, self.max_camera_location-self.camera_location-self.enemy_deployment_position_offset,
                                     deployment_y_position, unit_type, self.enemy_country, side, slot)

            elif unit_type == "axe_militia":
                unit = Axe_Militia(self, self.max_camera_location-self.camera_location-self.enemy_deployment_position_offset,
                                     deployment_y_position, unit_type, self.enemy_country, side, slot)

            elif unit_type == "sword_militia":
                unit = Sword_Militia(self, self.max_camera_location-self.camera_location-self.enemy_deployment_position_offset,
                                     deployment_y_position, unit_type, self.enemy_country, side, slot)

            elif unit_type == "light_dagger_infantry":
                unit = Light_Dagger_Infantry(self, self.max_camera_location - self.camera_location - self.enemy_deployment_position_offset,
                                     deployment_y_position, unit_type, self.enemy_country, side, slot)

            elif unit_type == "sword_infantry":
                unit = Sword_Infantry(self, self.max_camera_location - self.camera_location - self.enemy_deployment_position_offset,
                                     deployment_y_position, unit_type, self.enemy_country, side, slot)

            elif unit_type == "bow_infantry":
                unit = Bow_Infantry(self, self.max_camera_location - self.camera_location - self.enemy_deployment_position_offset,
                                     deployment_y_position, unit_type, self.enemy_country, side, slot)

            elif unit_type == "flaming_arrow_infantry":
                unit = Flaming_Arrow_Infantry(self, self.max_camera_location - self.camera_location - self.enemy_deployment_position_offset,
                                     deployment_y_position, unit_type, self.enemy_country, side, slot)

            elif unit_type == "crossbow_infantry":
                unit = Crossbow_Infantry(self, self.max_camera_location - self.camera_location - self.enemy_deployment_position_offset,
                                     deployment_y_position, unit_type, self.enemy_country, side, slot)

            elif unit_type == "catapult":
                unit = Catapult(self, self.max_camera_location - self.camera_location - self.enemy_deployment_position_offset,
                                     deployment_y_position, unit_type, self.enemy_country, side, slot)

            elif unit_type == "bow_militia":
                unit = Bow_Militia(self, self.max_camera_location-self.camera_location-self.enemy_deployment_position_offset,
                                     deployment_y_position, unit_type, self.enemy_country, side, slot)

            elif unit_type == "shielded_sword_infantry":
                unit = Shielded_Sword_Infantry(self, self.max_camera_location - self.camera_location - self.enemy_deployment_position_offset,
                                     deployment_y_position, unit_type, self.enemy_country, side, slot)

            elif unit_type == "elite_sword_infantry":
                unit = Elite_Sword_Infantry(self, self.max_camera_location - self.camera_location - self.enemy_deployment_position_offset,
                                     deployment_y_position, unit_type, self.enemy_country, side, slot)

            elif unit_type == "mace_infantry":
                unit = Mace_Infantry(self, self.max_camera_location - self.camera_location - self.enemy_deployment_position_offset,
                                     deployment_y_position, unit_type, self.enemy_country, side, slot)

            elif unit_type == "battleaxe_infantry":
                unit = Battleaxe_Infantry(self, self.max_camera_location - self.camera_location - self.enemy_deployment_position_offset,
                                     deployment_y_position, unit_type, self.enemy_country, side, slot)

            elif unit_type == "elephant_rider":
                unit = Elephant_Rider(self, self.max_camera_location - self.camera_location - self.enemy_deployment_position_offset,
                                     deployment_y_position, unit_type, self.enemy_country, side, slot)

            elif unit_type == "halberd_cavalry":
                unit = Halberd_Cavalry(self, self.max_camera_location - self.camera_location - self.enemy_deployment_position_offset,
                                     deployment_y_position, unit_type, self.enemy_country, side, slot)

            elif unit_type == "light_cavalry":
                unit = Light_Cavalry(self, self.max_camera_location - self.camera_location - self.enemy_deployment_position_offset,
                                     deployment_y_position, unit_type, self.enemy_country, side, slot)

            elif unit_type == "tiger_knight_cavalry":
                unit = Tiger_Knight_Cavalry(self, self.max_camera_location - self.camera_location - self.enemy_deployment_position_offset,
                                     deployment_y_position, unit_type, self.enemy_country, side, slot)

            self.enemy_unit_sprites.add(unit)
            self.all_sprites.add(unit)
            self.enemy_army[unit_type] -= 1
            self.apply_unit_deployment_boosts(unit)

            if self.two_player:
                self.enemy_command -= deployment_cost
                #if self.enemy_army[unit_type] <= 0 or self.enemy_command < deployment_cost:
                    #self.enemy_unit_icons_list[self.enemy_selected_unit_index].disabled = True

                if self.enemy_last_deployment_slot == self.enemy_selected_slot and self.enemy_ambush_count <= 0:
                    self.enemy_consecutive_deployments += 1
                self.enemy_last_deployment_slot = self.enemy_selected_slot
                self.enemy_last_deployment = pg.time.get_ticks()

        self.check_deployment_conditions() #check if any icons need to be disabled/re-enabled


    def apply_unit_deployment_boosts(self, unit):
        if unit.side == "left":
            if unit.unit_class == "ranged":
                if "hawk_eye" in self.player_general.skills:
                    unit.attacking_range += 200
                if "penetrating_shot" in self.player_general.skills:
                    unit.firing_damage += 2
                    unit.fatal_strike_rate += .02
                if "improved_marksmanship" in self.main_game.acquired_tech[self.player_general.country]:
                    unit.firing_accuracy += .1
                    if unit.firing_accuracy >= 1:
                        unit.firing_accuracy = 1
            if unit.unit_class == "axe":
                if "great_axe" in self.player_general.skills:
                    unit.attack += 3
                if "fierce_axemen" in self.main_game.acquired_tech[self.player_general.country]:
                    unit.fatal_strike_rate *= 2
            if "barbarian_king" in self.player_general.skills and ("barbarian" in unit.unit_type or "rattan" in unit.unit_type):
                unit.health = int(unit.health*1.2)
                unit.health_upper_limit = unit.health
                unit.attack += 1
                unit.defense += 1
            if "hidden_ambition" in self.player_general.skills:
                unit.health = int(unit.health*1.5)
                unit.health_upper_limit = unit.health
            if "chief_advisor" in self.player_general.skills:
                unit.health = int(unit.health*1.2)
                unit.health_upper_limit = unit.health
            if unit.unit_class == "blade":
                if "skilled_swordsmanship" in self.main_game.acquired_tech[self.player_general.country]:
                    unit.attack += 2
                    unit.defense += 2
            if unit.unit_class == "spear":
                if "master_spearmen" in self.player_general.skills:
                    unit.attack += 2
                    unit.defense += 2
                if "enhanced_spearmanship" in self.main_game.acquired_tech[self.player_general.country]:
                    unit.attack += 2
                    unit.defense += 2
            if unit.unit_class == "cavalry":
                if "advanced_horsemanship" in self.main_game.acquired_tech[self.player_general.country]:
                    unit.charge_effects["damage"] = int(unit.charge_effects["damage"]*1.25)
                if "skilled_horseman" in self.player_general.skills:
                    unit.charge_effects["damage"] = int(unit.charge_effects["damage"] * 1.1)
                    unit.max_charge_range = int(unit.max_charge_range*1.1)
            if "militia" in unit.unit_type and "militia_commander" in self.player_general.skills:
                unit.attack += 1
                unit.defense += 1
                #unit.health_upper_limit = int(unit.health_upper_limit*1.1)
                unit.health = int(unit.health_upper_limit*1.1)
                unit.health_upper_limit = unit.health
            if unit.unit_type == "light_dagger_infantry" and "light_dagger_infantry" in self.player_general.skills:
                unit.attack += 2
                unit.defense += 1
                unit.speed += 1

            if "defensive_formation" in self.player_general.skills:
                unit.defense += 1
            if "strategy_of_attack" in self.main_game.acquired_tech[self.player_general.country]:
                unit.attack += 1
            if "strategy_of_defense" in self.main_game.acquired_tech[self.player_general.country]:
                unit.defense += 1
            if "improved_armor" in self.main_game.acquired_tech[self.player_general.country]:
                unit.health = int(unit.health*1.1)
                unit.health_upper_limit = unit.health
            if "superior_armor" in self.main_game.acquired_tech[self.player_general.country]:
                unit.health = int(unit.health*1.2)
                unit.health_upper_limit = unit.health
            if unit.unit_type == "light_dagger_infantry" and self.terrain == "water":
                unit.attack += 2
                unit.walking_acc += 2


            for effect, start_time in self.player_general_effects:
                if effect == "caution":
                    unit.defense += 2
                    if unit.walking_acc >= 2:
                        unit.walking_acc = unit.walking_acc//2
                    else:
                        unit.walking_acc = 1
                elif effect == "focus_attack":
                    unit.fatal_strike_rate *= 2
                elif effect == "iron_chain_formation":
                    if unit.walking_acc >= 5:
                        unit.walking_acc -= 3
                    else:
                        unit.walking_acc = 1
                    unit.attack += 2
                    unit.defense += 2
                    unit.unit_effects["chained"] = 1
                    unit.unit_effects["immunity"] = 1


            if self.weather == "poison_mist" and "poison_mist" not in self.player_general.skills:
                unit.attack = max([1, int(unit.attack*.5)])
                unit.defense = max([1, int(unit.defense*.5)])
                unit.walking_acc = max([1, int(unit.walking_acc*.5)])
                if "poison" in unit.unit_effects:
                    unit.unit_effects["poison"] += 1
                else:
                    unit.unit_effects["poison"] = 1


        else:
            if unit.unit_class == "ranged":
                if "hawk_eye" in self.enemy_general.skills:
                    unit.attacking_range += 200
                if "penetrating_shot" in self.enemy_general.skills:
                    unit.firing_damage += 2
                    unit.fatal_strike_rate += .02
                if "improved_marksmanship" in self.main_game.acquired_tech[self.enemy_general.country]:
                    unit.firing_accuracy += .1
                    if unit.firing_accuracy >= 1:
                        unit.firing_accuracy = 1
            if unit.unit_class == "axe":
                if "great_axe" in self.enemy_general.skills:
                    unit.attack += 3
                if "fierce_axemen" in self.main_game.acquired_tech[self.enemy_general.country]:
                    unit.fatal_strike_rate *= 2
            if "barbarian_king" in self.enemy_general.skills and ("barbarian" in unit.unit_type or "rattan" in unit.unit_type):
                unit.health = int(unit.health*1.2)
                unit.health_upper_limit = unit.health
                unit.attack += 1
                unit.defense += 1
            if "hidden_ambition" in self.enemy_general.skills:
                unit.health = int(unit.health*1.5)
                unit.health_upper_limit = unit.health
            if "chief_advisor" in self.enemy_general.skills:
                unit.health = int(unit.health*1.2)
                unit.health_upper_limit = unit.health
            if unit.unit_class == "blade":
                if "skilled_swordsmanship" in self.main_game.acquired_tech[self.enemy_general.country]:
                    unit.attack += 2
                    unit.defense += 2
            if unit.unit_class == "spear":
                if "master_spearmen" in self.enemy_general.skills:
                    unit.attack += 2
                    unit.defense += 2
                if "enhanced_spearmanship" in self.main_game.acquired_tech[self.enemy_general.country]:
                    unit.attack += 2
                    unit.defense += 2
            if unit.unit_class == "cavalry":
                if "advanced_horsemanship" in self.main_game.acquired_tech[self.enemy_general.country]:
                    unit.charge_effects["damage"] = int(unit.charge_effects["damage"]*1.25)
                if "skilled_horseman" in self.enemy_general.skills:
                    unit.charge_effects["damage"] = int(unit.charge_effects["damage"] * 1.1)
                    unit.max_charge_range = int(unit.max_charge_range*1.1)
            if "militia" in unit.unit_type and "militia_commander" in self.enemy_general.skills:
                unit.attack += 1
                unit.defense += 1
                #unit.health_upper_limit = int(unit.health_upper_limit*1.1)
                unit.health = int(unit.health_upper_limit*1.1)
                unit.health_upper_limit = unit.health
            if unit.unit_type == "light_dagger_infantry" and "light_dagger_infantry" in self.enemy_general.skills:
                unit.attack += 2
                unit.defense += 1
                unit.speed += 1

            if "defensive_formation" in self.enemy_general.skills:
                unit.defense += 1
            if "strategy_of_attack" in self.main_game.acquired_tech[self.enemy_general.country]:
                unit.attack += 1
            if "strategy_of_defense" in self.main_game.acquired_tech[self.enemy_general.country]:
                unit.defense += 1
            if "improved_armor" in self.main_game.acquired_tech[self.enemy_general.country]:
                unit.health = int(unit.health*1.1)
                unit.health_upper_limit = unit.health
            if "superior_armor" in self.main_game.acquired_tech[self.enemy_general.country]:
                unit.health = int(unit.health*1.2)
                unit.health_upper_limit = unit.health
            if unit.unit_type == "light_dagger_infantry" and self.terrain == "water":
                unit.attack += 2
                unit.walking_acc += 2

            for effect, start_time in self.enemy_general_effects:
                if effect == "caution":
                    unit.defense += 2
                    if unit.walking_acc >= 2:
                        unit.walking_acc = unit.walking_acc//2
                    else:
                        unit.walking_acc = 1
                elif effect == "focus_attack":
                    unit.fatal_strike_rate *= 2
                elif effect == "iron_chain_formation":
                    if unit.walking_acc >= 5:
                        unit.walking_acc -= 3
                    else:
                        unit.walking_acc = 1
                    unit.attack += 2
                    unit.defense += 2
                    unit.unit_effects["chained"] = 1
                    unit.unit_effects["immunity"] = 1


            if self.weather == "poison_mist" and "poison_mist" not in self.enemy_general.skills:
                unit.attack = max([1, int(unit.attack * .5)])
                unit.defense = max([1, int(unit.defense * .5)])
                unit.walking_acc = max([1, int(unit.walking_acc * .5)])
                if "poison" in unit.unit_effects:
                    unit.unit_effects["poison"] += 1
                else:
                    unit.unit_effects["poison"] = 1
                    

        if self.weather == "fog" and unit.unit_class == "ranged":
            unit.attacking_range = int(unit.attacking_range*.7)
            unit.firing_accuracy *= .7
        elif self.weather == "rain" and unit.unit_class == "ranged":
            unit.attacking_range = int(unit.attacking_range*.85)
            unit.firing_accuracy *= .85

        #regenerate health bars in case upper limit was increased
        unit.health_bar_back.kill()
        unit.health_bar_front.kill()
        unit.generate_health_bar()


    def switch_selected_unit(self, delta=0, direct=-1, side="left"):
        
        if side == "left":
            self.player_unit_icons_list[self.selected_unit_index].selected = False
    
            if direct == -1:
                self.selected_unit_index += delta
                if self.selected_unit_index >= len(self.player_army_unit_types):
                    self.selected_unit_index = 0
                elif self.selected_unit_index < 0:
                    self.selected_unit_index = len(self.player_army_unit_types) - 1
    
            else:
                if direct >= len(self.player_army_unit_types):
                    self.selected_unit_index = 0
                else:
                    self.selected_unit_index = direct
    
            self.player_unit_icons_list[self.selected_unit_index].selected = True
            
        else:
            self.enemy_unit_icons_list[self.enemy_selected_unit_index].selected = False

            if direct == -1:
                self.enemy_selected_unit_index += delta
                if self.enemy_selected_unit_index >= len(self.enemy_army_unit_types):
                    self.enemy_selected_unit_index = 0
                elif self.enemy_selected_unit_index < 0:
                    self.enemy_selected_unit_index = len(self.enemy_army_unit_types) - 1

            else:
                if direct >= len(self.enemy_army_unit_types):
                    self.enemy_selected_unit_index = 0
                else:
                    self.enemy_selected_unit_index = direct

            self.enemy_unit_icons_list[self.enemy_selected_unit_index].selected = True


    def special_attack_animation(self, side):
        if side == "left":
            sp_atk_animation = FullScreenAnimation(self.player_general.simple_name)
        else:
            sp_atk_animation = FullScreenAnimation(self.enemy_general.simple_name)

        self.fixed_sprites.add(sp_atk_animation)
        self.all_sprites.add(sp_atk_animation)


    def generate_chained_fire_attack(self, target, attacking_side):
        if attacking_side == "left":
            sprite_master_list = self.player_chained_fire_attack_sprites
        else:
            sprite_master_list = self.enemy_chained_fire_attack_sprites

        if random() <= .5:
            self.fire_sound.play()

        damage_dealt = 40
        if self.terrain == "forest":
            damage_dealt *= 2
        if self.weather == "rain":
            damage_dealt = damage_dealt // 2
        if ("rattan" in target.unit_type or target.unit_type == "catapult" or target.unit_type == "barricade"):
            damage_dealt *= 2
        target.got_hit(damage_dealt, damage_type="fire")

        chained_fire_attack_sprite = Static(target.rect.centerx, target.rect.centery,
                                            "sprite_animation_chained_fire_attack_1.png",
                                            animations=[pg.image.load("sprite_animation_chained_fire_attack_{}.png".format(i)).convert_alpha() for i in range(2, 11)],
                                            frame_delta=180, kill_after_animation=True,
                                            master_list=sprite_master_list, anchor="center")
        self.all_sprites.add(chained_fire_attack_sprite)

        if attacking_side == "left":
            self.player_chained_fire_attack_sprites.append(chained_fire_attack_sprite)
            self.player_chained_fire_attack_targets.append(target)
        else:
            self.enemy_chained_fire_attack_sprites.append(chained_fire_attack_sprite)
            self.enemy_chained_fire_attack_targets.append(target)


    def use_interactive_special(self, interactive_special_atk, side, target=None):
        if side == "left":
            self.interactive_special_atk = ""
            self.interactive_special_atk_region.kill()
            self.interactive_special_atk_region = None
            target_x, target_y = target
            if interactive_special_atk == "fire_attack":
                for i in range(20):
                    projectile = SmokeBomb(self, randrange(target_x-200, target_x+175), randrange(target_y-LANDSCAPE_HEIGHT-200, target_y-LANDSCAPE_HEIGHT), side="left")
                    self.all_sprites.add(projectile)

            elif interactive_special_atk == "chained_fire_attack":
                self.player_chained_fire_attack_sprites = []
                self.enemy_chained_fire_attack_targets = []
                for unit in self.enemy_unit_sprites:
                    if unit.rect.centerx in range(target_x - 200, target_x + 200) and unit.rect.centery in range(target_y - 200, target_y + 200):
                        self.generate_chained_fire_attack(target=unit, attacking_side="left")

            elif interactive_special_atk == "second_wind":
                self.play_sound_effect(self.morale_boost_sound)
                second_wind_sprite = Static(target_x, target_y, "sprite_second_wind_1.png",
                                            animations = [pg.image.load("sprite_second_wind_{}.png".format(i)).convert_alpha() for i in [2,3,4,5,6,7,8,7,6,5,4,3,2,1]],
                                            frame_delta=50, kill_after_animation=True, anchor="center")
                self.all_sprites.add(second_wind_sprite)
                for unit in self.player_unit_sprites:
                    if unit.rect.centerx in range(target_x - 125, target_x + 125) and unit.rect.centery in range(target_y - 125, target_y + 125):
                        unit.health = unit.health_upper_limit

            elif interactive_special_atk == "inspire":
                self.play_sound_effect(self.morale_boost_sound)
                inspire_sprite = Static(target_x, target_y, "sprite_inspire_1.png",
                                            animations = [pg.image.load("sprite_inspire_{}.png".format(i)).convert_alpha() for i in range(2,9)],
                                            frame_delta=50, kill_after_animation=True, anchor="center")
                self.all_sprites.add(inspire_sprite)
                for unit in self.player_unit_sprites:
                    if unit.rect.centerx in range(target_x - 200, target_x + 200) and unit.rect.centery in range(target_y - 200, target_y + 200):
                        unit.walking_acc += 2
                        unit.unit_effects["inspiration"] = 1


        else:
            if interactive_special_atk in ["fire_attack", "chained_fire_attack"]:
                area_dic_x = {}
                area_dic_y = {}
                for unit in self.player_unit_sprites:
                    area_key_x = unit.rect.centerx//200+1
                    area_key_y = unit.rect.centery//100+1
                    if area_key_x in area_dic_x:
                        area_dic_x[area_key_x] += 1
                    else:
                        area_dic_x[area_key_x] = 1
                    if area_key_y in area_dic_y:
                        area_dic_y[area_key_y] += 1
                    else:
                        area_dic_y[area_key_y] = 1

                for unit in self.enemy_unit_sprites:
                    area_key_x = unit.rect.centerx//200+1
                    if area_key_x in area_dic_x:
                        area_dic_x[area_key_x] -= 1
                    else:
                        area_dic_x[area_key_x] = -1

                max_area_x = max(area_dic_x, key=area_dic_x.get)*200
                max_area_y = max(area_dic_y, key=area_dic_y.get)*100
                if interactive_special_atk == "fire_attack":
                    for i in range(20):
                        projectile = SmokeBomb(self, randrange(max_area_x-200, max_area_x+175),
                                               max_area_y-LANDSCAPE_HEIGHT-randrange(200), side="right")
                        self.all_sprites.add(projectile)
                elif interactive_special_atk == "chained_fire_attack":
                    print("chained fire attack", max_area_x, max_area_y)
                    for player_unit in self.player_unit_sprites:
                        if player_unit.rect.centerx in range(max_area_x-200, max_area_x+200) and player_unit.rect.centery in range(max_area_y-200, max_area_y+200):
                            self.generate_chained_fire_attack(target=player_unit, attacking_side="right")

            elif interactive_special_atk in ["second_wind", "inspire"]:
                area_dic_x = {}
                area_dic_y = {}
                for unit in self.enemy_unit_sprites:
                    area_key_x = unit.rect.centerx // 200 + 1
                    area_key_y = unit.rect.centery // 200 + 1
                    if area_key_x in area_dic_x:
                        area_dic_x[area_key_x] += 1
                    else:
                        area_dic_x[area_key_x] = 1

                    if area_key_y in area_dic_y:
                        area_dic_y[area_key_y] += 1
                    else:
                        area_dic_y[area_key_y] = 1
                max_area_x = max(area_dic_x, key=area_dic_x.get) * 200
                max_area_y = max(area_dic_y, key=area_dic_y.get) * 200

                if interactive_special_atk == "second_wind":
                    self.play_sound_effect(self.morale_boost_sound)
                    second_wind_sprite = Static(max_area_x, max_area_y, "sprite_second_wind_1.png",
                                                animations = [pg.image.load("sprite_second_wind_{}.png".format(i)).convert_alpha() for i in [2,3,4,5,6,7,8,7,6,5,4,3,2,1]],
                                                frame_delta=50, kill_after_animation=True, anchor="center")
                    self.all_sprites.add(second_wind_sprite)
                    for unit in self.enemy_unit_sprites:
                        if unit.rect.centerx in range(max_area_x - 125, max_area_x + 125) and unit.rect.centery in range(max_area_y - 125, max_area_y + 125):
                            unit.health = unit.health_upper_limit
                elif interactive_special_atk == "inspire":
                    self.play_sound_effect(self.morale_boost_sound)
                    inspire_sprite = Static(max_area_x, max_area_y, "sprite_inspire_1.png",
                                                animations = [pg.image.load("sprite_inspire_{}.png".format(i)).convert_alpha() for i in range(2,9)],
                                                frame_delta=40, kill_after_animation=True, anchor="center")
                    self.all_sprites.add(inspire_sprite)
                    for unit in self.enemy_unit_sprites:
                        if unit.rect.centerx in range(max_area_x - 200, max_area_x + 200) and unit.rect.centery in range(max_area_y - 200, max_area_y + 200):
                            unit.walking_acc += 2
                            unit.unit_effects["inspiration"] = 1




    def use_special(self, side, sp_atk_icon):
        name = sp_atk_icon.special_attack_name

        if side == "left":
            if name == "mighty_roar":
                self.morale_change("right", -int(.20*self.enemy_morale_max))
                #self.morale_change("right", -int(.9 * self.enemy_morale_max)) #for quick testing
            elif name == "taunt":
                self.morale_change("right", -int(.10*self.enemy_morale_max))
            elif name == "rally":
                self.morale_change("left", int(.10*self.player_morale_max))
                #self.morale_change("right", -int(1 * self.enemy_morale_max)) #for quick testing
            elif name == "expert_marksmanship":
                for unit_sprite in self.player_unit_sprites:
                    if unit_sprite.unit_class == "ranged":
                        unit_sprite.firing_accuracy *= 1.3
                        unit_sprite.firing_rate *= 1.3
                        if unit_sprite.firing_accuracy >= 1:
                            unit_sprite.firing_accuracy = 1
            elif name == "penetrating_shot":
                for unit_sprite in self.player_unit_sprites:
                    if unit_sprite.unit_class == "ranged":
                        unit_sprite.firing_damage *= 2
                        unit_sprite.fatal_strike_rate *= 2
            elif name == "cavalry_charge":
                for unit_sprite in self.player_unit_sprites:
                    if unit_sprite.unit_class == "cavalry":
                        unit_sprite.charge_effects["damage"] *= 2
            elif name == "fierce_assault":
                for unit_sprite in self.player_unit_sprites:
                    unit_sprite.attack += 3
            elif name == "rapid_advance":
                for unit_sprite in self.player_unit_sprites:
                    unit_sprite.walking_acc += 2
            elif name == "relentless_charge":
                for unit_sprite in self.player_unit_sprites:
                    if unit_sprite.unit_class != "ranged":
                        unit_sprite.attack += 3
                        unit_sprite.walking_acc += 3
                        unit_sprite.defense = ceil(unit_sprite.defense/2)
            elif name == "deadly_blade":
                for unit_sprite in self.player_unit_sprites:
                    if unit_sprite.unit_class != "ranged":
                        unit_sprite.fatal_strike_rate *= 2
            elif name == "piercing_blade":
                for unit_sprite in self.player_unit_sprites:
                    if unit_sprite.unit_class not in ["ranged", "cavalry"]:
                        unit_sprite.unit_effects["piercing_blade"] = 1
            elif name == "breakthrough":
                for unit_sprite in self.player_unit_sprites:
                    unit_sprite.unit_effects["repel"] = 1.0
                    unit_sprite.unit_effects["stun"] = 7
            elif name == "eye_for_an_eye":
                for unit_sprite in self.player_unit_sprites:
                    unit_sprite.unit_effects["vengeance"] = .5
            elif name == "anticipation":
                for unit_sprite in self.player_unit_sprites:
                    unit_sprite.unit_effects["immunity"] = 1
            elif name == "qinggang_sword":
                for unit_sprite in self.enemy_unit_sprites:
                    if unit_sprite.defense >= 2:
                        unit_sprite.defense = unit_sprite.defense//2
            elif name == "supply_raid":
                for unit_sprite in self.enemy_unit_sprites:
                    unit_sprite.walking_acc = unit_sprite.walking_acc/2
            elif name == "disarrayment":
                self.player_general_effects.append([name, pg.time.get_ticks()])
                for unit_sprite in self.enemy_unit_sprites:
                    unit_sprite.walking_acc = randrange(1,5)
            elif name == "sow_discord":
                number_to_destroy = len(self.enemy_unit_sprites)//4+1
                counter = 0
                while counter < number_to_destroy:
                    for unit_sprite in self.enemy_unit_sprites:
                        if random() <= .25:
                            #unit_sprite.got_hit(unit_sprite.health, stun=0, repel=0)
                            #convert enemy unit to player unit
                            unit_sprite.side = "left"
                            unit_sprite.attacking = False
                            self.enemy_unit_sprites.remove(unit_sprite)
                            self.player_unit_sprites.add(unit_sprite)
                            unit_sprite.enemy_sprites = self.enemy_unit_sprites
                            unit_sprite.load_animations()
                            counter += 1
                        if counter >= number_to_destroy:
                            break
            elif name == "drowning_the_seven_armies":
                for unit_sprite in self.enemy_unit_sprites:
                    damage = unit_sprite.health//2
                    unit_sprite.got_hit(damage, stun=0, repel=10)
            elif name == "feigned_surrender":
                self.player_morale -= self.player_morale_max//5
                if self.player_morale <= 0:
                    self.player_morale = 0
                for unit_sprite in self.enemy_unit_sprites:
                    damage = unit_sprite.health//2
                    unit_sprite.got_hit(damage, stun=0, repel=0)
            elif name in ["caution", "bloodthirst", "last_stand", "focus_attack", "iron_chain_formation"]:
                self.player_general_effects.append([name, pg.time.get_ticks()])
                if name == "last_stand":
                    self.player_command_recovery_rate *= 3
            elif name == "bagua_formation":
                if self.terrain == "water":
                    num_slots = 2
                else:
                    num_slots = 8

                for s in range(num_slots):
                    unit_type = self.player_army_unit_types[self.selected_unit_index]
                    if self.player_army[unit_type] > 0:
                        self.deploy_unit(side="left", unit_type=unit_type, slot=s, free=True)
                    else:
                        break

            elif name == "fortification":
                if self.siege_defense != "left":
                    self.game_message = "Cannot use special attack. Requirements not met."
                    self.game_message_color = RED
                    self.play_sound_effect(self.deployment_fail_sound)
                    self.last_action = pg.time.get_ticks()
                    return
                else:
                    self.wall.generate_defenders()
                    self.player_general_effects.append([name, pg.time.get_ticks()])

            elif name == "barricade":
                if self.terrain  == "water":
                    self.game_message = "Cannot use special attack. Requirements not met."
                    self.game_message_color = RED
                    self.play_sound_effect(self.deployment_fail_sound)
                    self.last_action = pg.time.get_ticks()
                    return
                else:
                    #kill any existing barricades before placing more
                    for obstacle in self.obstacle_sprites:
                        if obstacle.unit_type == "barricade":
                            obstacle.got_hit(obstacle.health, 0, 0)
                    for i in range(0,8,2):
                        barricade = Barricade(self, 600 - self.camera_location, i*60+160, "sprite_barricade.png",
                                              "left", [i,i+1])
                        self.all_sprites.add(barricade)
                        self.obstacle_sprites.add(barricade)

            elif name == "fire_attack":
                if self.interactive_special_atk:
                    self.game_message_color = RED
                    self.game_message = "Cannot use this special attack at the moment."
                    self.last_action = pg.time.get_ticks()
                    return
                else:
                    self.interactive_special_atk = sp_atk_icon.special_attack_name
                    self.game_message_color = WHITE
                    self.game_message = "Right Click to launch fire bombs at highlighted region."
                    self.last_action = pg.time.get_ticks()
                    pos = pg.mouse.get_pos()
                    self.interactive_special_atk_region = Static(pos[0], pos[1], "sprite_interactive_effect_area_400.png")
                    self.all_sprites.add(self.interactive_special_atk_region)

            elif name == "chained_fire_attack":
                if self.interactive_special_atk:
                    self.game_message_color = RED
                    self.game_message = "Cannot use this special attack at the moment."
                    self.last_action = pg.time.get_ticks()
                    return
                else:
                    self.interactive_special_atk = sp_atk_icon.special_attack_name
                    self.game_message_color = WHITE
                    self.game_message = "Right Click to launch a fire attack in the highlighted region."
                    self.last_action = pg.time.get_ticks()
                    pos = pg.mouse.get_pos()
                    self.interactive_special_atk_region = Static(pos[0], pos[1], "sprite_interactive_effect_area_400.png")
                    self.all_sprites.add(self.interactive_special_atk_region)

            elif name == "second_wind":
                if self.interactive_special_atk:
                    self.game_message_color = RED
                    self.game_message = "Cannot use this special attack at the moment."
                    self.last_action = pg.time.get_ticks()
                    return
                else:
                    self.interactive_special_atk = sp_atk_icon.special_attack_name
                    self.game_message_color = WHITE
                    self.game_message = "Right Click to heal friendly units at highlighted region."
                    self.last_action = pg.time.get_ticks()
                    pos = pg.mouse.get_pos()
                    self.interactive_special_atk_region = Static(pos[0], pos[1], "sprite_interactive_effect_area_green_250.png")
                    self.all_sprites.add(self.interactive_special_atk_region)

            elif name == "inspire":
                if self.interactive_special_atk:
                    self.game_message_color = RED
                    self.game_message = "Cannot use this special attack at the moment."
                    self.last_action = pg.time.get_ticks()
                    return
                else:
                    self.interactive_special_atk = sp_atk_icon.special_attack_name
                    self.game_message_color = WHITE
                    self.game_message = "Right Click to inspire friendly units at highlighted region."
                    self.last_action = pg.time.get_ticks()
                    pos = pg.mouse.get_pos()
                    self.interactive_special_atk_region = Static(pos[0], pos[1], "sprite_interactive_effect_area_green_400.png")
                    self.all_sprites.add(self.interactive_special_atk_region)

            elif name == "psychological_warfare":
                avg = int((self.player_morale + self.enemy_morale)/2)
                self.morale_change("left",avg-self.player_morale)
                self.morale_change("right", avg-self.enemy_morale)


        else:
            if name == "mighty_roar":
                self.morale_change("left", -int(.20 * self.player_morale_max))
            elif name == "taunt":
                self.morale_change("left", -int(.10*self.player_morale_max))
            elif name == "rally":
                self.morale_change("right", int(.10*self.enemy_morale_max))
            elif name == "expert_marksmanship":
                for unit_sprite in self.enemy_unit_sprites:
                    if unit_sprite.unit_class == "ranged":
                        unit_sprite.firing_accuracy *= 1.3
                        unit_sprite.firing_rate *= 1.3
                        if unit_sprite.firing_accuracy >= 1:
                            unit_sprite.firing_accuracy = 1
            elif name == "penetrating_shot":
                for unit_sprite in self.enemy_unit_sprites:
                    if unit_sprite.unit_class == "ranged":
                        unit_sprite.firing_damage *= 2
                        unit_sprite.fatal_strike_rate *= 2
            elif name == "cavalry_charge":
                for unit_sprite in self.enemy_unit_sprites:
                    if unit_sprite.unit_class == "cavalry":
                        unit_sprite.charge_effects["damage"] *= 2
            elif name == "supply_raid":
                for unit_sprite in self.player_unit_sprites:
                    unit_sprite.walking_acc = unit_sprite.walking_acc/2
            elif name == "disarrayment":
                self.enemy_general_effects.append([name, pg.time.get_ticks()])
                for unit_sprite in self.player_unit_sprites:
                    unit_sprite.walking_acc = randrange(1,5)
            elif name == "fierce_assault":
                for unit_sprite in self.enemy_unit_sprites:
                    unit_sprite.attack += 3
            elif name == "relentless_charge":
                for unit_sprite in self.enemy_unit_sprites:
                    if unit_sprite.unit_class != "ranged":
                        unit_sprite.attack += 3
                        unit_sprite.walking_acc += 3
                        unit_sprite.defense = ceil(unit_sprite.defense/2)
            elif name == "piercing_blade":
                for unit_sprite in self.enemy_unit_sprites:
                    if unit_sprite.unit_class not in ["ranged", "cavalry"]:
                        unit_sprite.unit_effects["piercing_blade"] = 1
            elif name == "deadly_blade":
                for unit_sprite in self.enemy_unit_sprites:
                    if unit_sprite.unit_class != "ranged":
                        unit_sprite.fatal_strike_rate *= 2
            elif name == "breakthrough":
                for unit_sprite in self.enemy_unit_sprites:
                    unit_sprite.unit_effects["repel"] = 1.0
                    unit_sprite.unit_effects["stun"] = 7
                for unit_sprite in self.player_unit_sprites:
                    if unit_sprite.defense >= 3:
                        unit_sprite.defense -= 2
                    else:
                        unit_sprite.defense = 1
            elif name == "eye_for_an_eye":
                for unit_sprite in self.enemy_unit_sprites:
                    unit_sprite.unit_effects["vengeance"] = .5
            elif name == "anticipation":
                for unit_sprite in self.enemy_unit_sprites:
                    unit_sprite.unit_effects["immunity"] = 1
            elif name == "qinggang_sword":
                for unit_sprite in self.player_unit_sprites:
                    if unit_sprite.defense >= 2:
                        unit_sprite.defense = unit_sprite.defense//2
            elif name == "rapid_advance":
                for unit_sprite in self.enemy_unit_sprites:
                    unit_sprite.walking_acc += 2
            elif name == "sow_discord":
                number_to_destroy = len(self.player_unit_sprites)//4+1
                counter = 0
                while counter < number_to_destroy:
                    for unit_sprite in self.player_unit_sprites:
                        if random() <= .25:
                            #unit_sprite.got_hit(unit_sprite.health, stun=0, repel=0)
                            #convert player unit to enemy unit
                            unit_sprite.side = "right"
                            unit_sprite.attacking = False
                            self.player_unit_sprites.remove(unit_sprite)
                            self.enemy_unit_sprites.add(unit_sprite)
                            unit_sprite.enemy_sprites = self.player_unit_sprites
                            unit_sprite.load_animations()
                            counter += 1
                        if counter >= number_to_destroy:
                            break
            elif name == "drowning_the_seven_armies":
                for unit_sprite in self.player_unit_sprites:
                    damage = unit_sprite.health//2
                    unit_sprite.got_hit(damage, stun=0, repel=10)
            elif name == "feigned_surrender":
                self.enemy_morale -= self.enemy_morale_max//5
                if self.enemy_morale <= 0:
                    self.enemy_morale = 0
                for unit_sprite in self.player_unit_sprites:
                    damage = unit_sprite.health//2
                    unit_sprite.got_hit(damage, stun=0, repel=0)
            elif name in ["caution", "bloodthirst", "last_stand", "focus_attack", "iron_chain_formation"]:
                self.enemy_general_effects.append([name, pg.time.get_ticks()])
                if name == "last_stand":
                    self.enemy_command_recovery_rate *= 3
            elif name == "bagua_formation":
                if self.terrain == "water":
                    num_slots = 2
                else:
                    num_slots = 8

                for s in range(num_slots):
                    unit_type = self.enemy_army_unit_types[self.enemy_selected_unit_index]
                    if self.enemy_army[unit_type] > 0:
                        self.deploy_unit(side="right", unit_type=unit_type, slot=s, free=True)
                    else:
                        break

            elif name == "fortification":
                self.enemy_general_effects.append([name, pg.time.get_ticks()])


            elif name == "barricade":
                # kill any existing barricades before placing more
                for obstacle in self.obstacle_sprites:
                    if obstacle.unit_type == "barricade":
                        obstacle.got_hit(obstacle.health, 0, 0)
                for i in range(0,8,2):
                    barricade = Barricade(self, self.max_camera_location-800, i*60+160, "sprite_barricade.png",
                                          "right", [i,i+1])
                    self.all_sprites.add(barricade)
                    self.obstacle_sprites.add(barricade)

            elif name in "fire_attack":
                self.use_interactive_special(sp_atk_icon.special_attack_name, "right")

            elif name in "chained_fire_attack":
                self.use_interactive_special(sp_atk_icon.special_attack_name, "right")

            elif name in "second_wind":
                self.use_interactive_special(sp_atk_icon.special_attack_name, "right")

            elif name == "psychological_warfare":
                avg = int((self.player_morale + self.enemy_morale) / 2)
                self.morale_change("left",avg-self.player_morale)
                self.morale_change("right", avg-self.enemy_morale)


        sp_atk_icon.disabled = True
        sp_atk_icon.generate_transparent_cover()
        try:
            sp_atk_sound = pg.mixer.Sound("sp_atk_{}_{}.ogg".format(name, LANGUAGE))
        except:
            sp_atk_sound = pg.mixer.Sound("sfx_default_sp_atk.ogg")
        self.play_sound_effect(sp_atk_sound)

        self.special_attack_animation(side=side)


    def play_sound_effect(self, sfx, prob=1.0):
        if prob <= .1:
            prob = .1
        if random() <= prob:
            if not self.custom:
                if not self.main_game.is_mute:
                    sfx.play()
            else:
                sfx.play()


    def events(self):
        now = pg.time.get_ticks()
        pos = pg.mouse.get_pos()

        for event in pg.event.get():
            # quit
            if event.type == pg.QUIT:
                choice = pg.display.message_box("Quit",
                                                "Are you sure you wish to quit? You will lose any unsaved progress.",
                                                buttons=("Yes", "No"), return_button=1, escape_button=None
                                                )

                if choice == 0:
                    pg.quit()
                    sys.exit()

            # Key release
            if event.type == pg.KEYUP:

                if event.key == pg.K_UP:
                    self.change_selected_slot(-1)
                if  event.key == pg.K_w and self.two_player:
                    self.change_selected_slot(-1, side="right")

                if event.key == pg.K_DOWN:
                    self.change_selected_slot(1)
                if event.key == pg.K_s and self.two_player:
                    self.change_selected_slot(1, side="right")


                #sp atk
                if event.key == pg.K_1:
                    try:
                        if self.player_general_special_attack_icons_list[0].disabled:
                            self.game_message = "Cannot use ability at this time."
                            self.game_message_color = RED
                            self.last_action = now
                        else:
                            self.use_special("left", self.player_general_special_attack_icons_list[0])
                    except:
                        pass

                elif event.key == pg.K_2:
                    try:
                        if self.player_general_special_attack_icons_list[1].disabled:
                            self.game_message = "Cannot use ability at this time."
                            self.game_message_color = RED
                            self.last_action = now
                        else:
                            self.use_special("left", self.player_general_special_attack_icons_list[1])
                    except:
                        pass

                #direct select
                if event.key == pg.K_q:
                    self.switch_selected_unit(direct=0)

                elif event.key == pg.K_w:
                    self.switch_selected_unit(direct=1)
                    
                elif event.key == pg.K_e:
                    self.switch_selected_unit(direct=2)

                elif event.key == pg.K_r:
                    self.switch_selected_unit(direct=3)

                elif event.key == pg.K_a:
                    self.switch_selected_unit(direct=4)

                elif event.key == pg.K_s:
                    self.switch_selected_unit(direct=5)

                elif event.key == pg.K_d:
                    self.switch_selected_unit(direct=6)

                elif event.key == pg.K_f:
                    self.switch_selected_unit(direct=7)


                elif event.key == pg.K_m and self.terrain not in ["water"]:
                    self.toggle_minimap(self.showing_minimap)

                elif event.key == pg.K_SPACE:
                    unit_type = self.player_army_unit_types[self.selected_unit_index]
                    self.deploy_unit(side="left", unit_type=unit_type, slot=self.selected_slot)

                elif event.key == pg.K_RETURN:
                    unit_type = self.enemy_army_unit_types[self.enemy_selected_unit_index]
                    self.deploy_unit(side="right", unit_type=unit_type, slot=self.enemy_selected_slot)

                elif event.key == pg.K_v:
                    if pg.FULLSCREEN and self.screen.get_flags():
                        pg.display.set_mode((self.game_width, self.game_height))
                    else:
                        pg.display.set_mode((self.game_width, self.game_height), pg.FULLSCREEN|pg.DOUBLEBUF)


                if event.key == pg.K_p and self.weather_resets <= 0 and self.ai_difficulty < 4: #pause
                    if self.running:
                        self.paused_time = now
                        self.running = False
                        #self.listen_for_key(False)
                    else:
                        self.running = True
                        self.unpause()

                if event.key == pg.K_ESCAPE:
                    if self.running:
                        self.paused_time = now
                        self.running = False
                        self.surrender()


            if event.type == pg.MOUSEBUTTONUP:
                self.play_sound_effect(self.click_sound)
                if event.button == 1:
                    for button in self.buttons:
                        if button.rect.collidepoint(pos):
                            if button.command:
                                if not button.disabled:
                                    button.command()
                            break

                    for sp_atk_icon in self.player_general_special_attack_icons_list:
                        if sp_atk_icon.rect.collidepoint(pos):
                            sp_atk_icon.hovering = False
                            sp_atk_icon.kill_tooltip()
                            if sp_atk_icon.static:
                                self.game_message = "Static skill. Cannot be activated."
                                self.game_message_color = RED
                                self.last_action = now
                            elif sp_atk_icon.disabled:
                                self.game_message = "Cannot use ability at this time."
                                self.game_message_color = RED
                                self.last_action = now
                            else:
                                self.use_special("left", sp_atk_icon)

                    if self.terrain != "water":
                        if self.collapsed_minimap.rect.collidepoint(pos):
                            self.toggle_minimap(hide=False)

                        elif self.close_minimap_button.rect.collidepoint(pos):
                            self.toggle_minimap()

                elif event.button == 3: #right click
                    if self.interactive_special_atk:
                        self.use_interactive_special(self.interactive_special_atk, "left", pos)


        self.cursor.rect.center = pos
        if pos[1] <= 150:
            for unit_icon in self.player_unit_icons_list + self.enemy_unit_icons_list:
                if unit_icon.rect.collidepoint(pos):
                    unit_icon.hovering = True
                else:
                    unit_icon.hovering = False

            hovering_over_sp_atk_icons = False
            for sp_atk_icon in self.player_general_special_attack_icons_list + self.enemy_general_special_attack_icons_list:
                if sp_atk_icon.rect.collidepoint(pos):
                    sp_atk_icon.hovering = True
                    hovering_over_sp_atk_icons = True
                    if sp_atk_icon.tooltip_description:
                        self.special_attack_tooltip_description = sp_atk_icon.tooltip_description
                        self.special_attack_tooltip_x = sp_atk_icon.tooltip.rect.left
                        self.special_attack_tooltip_y = sp_atk_icon.tooltip.rect.top
                else:
                    sp_atk_icon.hovering = False

            if not hovering_over_sp_atk_icons:
                self.special_attack_tooltip_description = ""

        elif pos[0] >= self.game_width*3//4 and self.camera_location < self.max_camera_location - self.game_width - 20:
            scroll_speed = int(pos[0]/self.game_width*20)
            self.scroll_screen(scroll_speed, direction="right")

        elif pos[0] <= self.game_width//4 and self.camera_location > 20:
            scroll_speed = int((self.game_width-pos[0])/self.game_width*20)
            self.scroll_screen(scroll_speed, direction="left")

        #key held down, L/R arrow key scrolling
        keys_pressed = pg.key.get_pressed()
        if keys_pressed[pg.K_LEFT] and self.camera_location > 20:
            scroll_speed = 10
            self.scroll_screen(scroll_speed, "left")
            # self.switch_selected_unit(delta=-1)

        if keys_pressed[pg.K_RIGHT] and self.camera_location <= self.max_camera_location - self.game_width - 20:
            # self.switch_selected_unit(delta=1)
            scroll_speed = 10
            self.scroll_screen(scroll_speed, "right")


    def unpause(self):
        for sp_atk_icon in self.player_general_special_attack_icons_list:
            if self.paused_time >= sp_atk_icon.special_attack_activated_time:
                sp_atk_icon.special_attack_activated_time += pg.time.get_ticks() - self.paused_time

        for sp_atk_icon in self.enemy_general_special_attack_icons_list:
            if self.paused_time >= sp_atk_icon.special_attack_activated_time:
                sp_atk_icon.special_attack_activated_time += pg.time.get_ticks() - self.paused_time


    def scroll_screen(self, scroll_speed, direction):
        if self.decision_text:
            return

        if direction == "right":
            self.camera_location += scroll_speed
            for s in self.all_sprites:
                if s in self.player_unit_sprites or s in self.enemy_unit_sprites:
                    s.position_x -= scroll_speed
                    if not self.running:
                        s.rect.centerx -= scroll_speed
                elif s not in self.fixed_sprites:
                    s.rect.centerx -= scroll_speed
        else:
            self.camera_location -= scroll_speed
            for s in self.all_sprites:
                if s in self.player_unit_sprites or s in self.enemy_unit_sprites:
                    s.position_x += scroll_speed
                    if not self.running:
                        s.rect.centerx += scroll_speed
                elif s not in self.fixed_sprites:
                    s.rect.centerx += scroll_speed


    def toggle_minimap(self, hide=True):
        if hide:
            self.collapsed_minimap.rect.bottomright = (LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT)
            self.minimap.rect.bottomright = (-3000, -3000)
            self.minimap_slot_indicator.rect.center = (-3000, -3000)
            self.close_minimap_button.rect.topright = (-3000, -3000)
            self.minimap_camera.rect.bottomright = (-3000, -3000)
            self.showing_minimap = False
            self.game_message = "Minimap minimized. Press <m> to reopen."
            self.game_message_color = WHITE
            self.last_action = pg.time.get_ticks()
        else:
            self.collapsed_minimap.rect.bottomright = (-3000, -3000)
            self.minimap.rect.bottomright = (LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT)
            self.close_minimap_button.rect.topright = (self.minimap.rect.right, self.minimap.rect.top)
            self.showing_minimap = True


    def draw(self):
        if self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)
            self.draw_text("{}".format(self.game_message), self.game_message_size, self.game_message_color,
                           self.game_width // 2, self.game_height // 2)

            for i in range(len(self.player_army_unit_types)):
                unit_type = self.player_army_unit_types[i]
                text_col = i%4
                text_row = i//4
                text_x, text_y =  text_col * 85 + 40 + 65 - 5, text_row * 62 + 11 + 62 - 5
                self.draw_text("{}".format(self.player_army[unit_type]), 11, WHITE, text_x, text_y)

            for i in range(len(self.enemy_army_unit_types)):
                unit_type = self.enemy_army_unit_types[i]
                text_col = i%4
                text_row = i//4
                text_x, text_y =  text_col * 85 + 840 + 65 - 5, text_row * 62 + 11 + 62 - 5
                self.draw_text("{}".format(self.enemy_army[unit_type]), 11, WHITE, text_x, text_y)


            #special attack recovery countdown
            for sp_atk_icon in self.player_general_special_attack_icons_list:
                if sp_atk_icon.remaining_recovery_time > 0:
                    countdown_string = time_conversion_to_string(sp_atk_icon.remaining_recovery_time//1000)
                    self.draw_text(countdown_string, 14, WHITE, sp_atk_icon.rect.centerx, sp_atk_icon.rect.centery,
                                   anchor="center")

            for sp_atk_icon in self.enemy_general_special_attack_icons_list:
                if sp_atk_icon.remaining_recovery_time > 0:
                    countdown_string = time_conversion_to_string(sp_atk_icon.remaining_recovery_time//1000)
                    self.draw_text(countdown_string, 14, WHITE, sp_atk_icon.rect.centerx, sp_atk_icon.rect.centery,
                                   anchor="center")


            if not self.running and self.weather_resets == 0:
                #self.screen.blit(self.sprite_quick_guide.image, pg.Rect((0, 0, LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT)))
                self.draw_text("Game paused. Press <p> to resume.", 24, RED, LANDSCAPE_WIDTH//2, LANDSCAPE_HEIGHT//2,
                               anchor="center")

            if self.showing_minimap:
                color_dic = {"shu": RED, "wei": BLUE, "wu": GREEN, "liu biao": ORANGE, "liu zhang":TURQUOISE,
                             "ma teng": GRAY, "yuan shao": YELLOW, "zhang lu": PURPLE}
                for unit in self.player_unit_sprites:
                    x = int(LANDSCAPE_WIDTH - 302.5 + (unit.rect.centerx + self.camera_location)//8)
                    y = int(LANDSCAPE_HEIGHT - 82.5 + unit.slot*10 + 5)
                    if self.player_country in color_dic:
                        pg.draw.circle(self.screen, color_dic[self.player_country], (x,y), 5, 3)
                    else:
                        pg.draw.circle(self.screen, WHITE, (x, y), 5, 3)
                for unit in self.enemy_unit_sprites:
                    x = int(LANDSCAPE_WIDTH - 302.5 + (unit.rect.centerx + self.camera_location)//8)
                    y = int(LANDSCAPE_HEIGHT - 82.5 + unit.slot*10 + 5)
                    if self.enemy_country in color_dic:
                        pg.draw.circle(self.screen, color_dic[self.enemy_country], (x,y), 5, 3)
                    else:
                        pg.draw.circle(self.screen, WHITE, (x, y), 5, 3)

                self.minimap_camera.rect.right = LANDSCAPE_WIDTH - 300 + (self.camera_location + LANDSCAPE_WIDTH) // 8
                self.minimap_camera.rect.bottom = LANDSCAPE_HEIGHT

            self.weather_sprites.draw(self.screen)
            self.decision_sprites.draw(self.screen)
            self.priority_sprites.draw(self.screen)
            if self.decision_text:
                if len(self.decision_text) >= 80:
                    self.drawWrappedText(self.decision_text, BLACK, pg.Rect(LANDSCAPE_WIDTH//2-275, LANDSCAPE_HEIGHT//2-140, 550, 250),
                                         FONT_FUNCTION(DEFAULT_FONT, 22))
                else:
                    self.draw_text(self.decision_text, 24, BLACK,
                                   LANDSCAPE_WIDTH // 2, LANDSCAPE_HEIGHT//2-100 , anchor="center")

            if self.special_attack_tooltip_description:
                #self.drawWrappedText(self.special_attack_tooltip_description, WHITE,
                #                     pg.Rect(self.special_attack_tooltip_x+16, self.special_attack_tooltip_y+20,
                #                             268, 360), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE))
                self.draw_multiline_text(self.special_attack_tooltip_description, FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                               WHITE, self.special_attack_tooltip_x + 16, self.special_attack_tooltip_y + 20,
                               align="topleft", max_width=268)

            pg.display.flip()


    def change_selected_slot(self, delta, side="left"):
        if side == "left":
            for effect in self.player_general_effects:
                effect_name, effect_start_time = effect
                if effect_name == "focus_attack":
                    return

            self.selected_slot += delta

            if self.terrain == "water":
                if self.selected_slot < 0:
                    self.selected_slot = 1
                elif self.selected_slot > 1:
                    self.selected_slot = 0
                if self.siege_defense == "left":
                    self.slot_indicator.rect.x, self.slot_indicator.rect.y = 470 - self.selected_slot*55, self.selected_slot*200 + 320
                else:
                    self.slot_indicator.rect.x, self.slot_indicator.rect.y = 30, self.selected_slot*200+320

            else:
                if self.selected_slot < 0:
                    self.selected_slot = 7
                elif self.selected_slot > 7:
                    self.selected_slot = 0

                if self.siege_defense == "left":
                    self.slot_indicator.rect.x, self.slot_indicator.rect.y = 470 - self.selected_slot*55, self.selected_slot * 60 + 160
                else:
                    self.slot_indicator.rect.x, self.slot_indicator.rect.y = 30, self.selected_slot*60+160


            if self.consecutive_deployments > 0:
                self.consecutive_deployments = 0

            if self.showing_minimap:
                self.minimap_slot_indicator.rect.center = (LANDSCAPE_WIDTH - 300,
                                                 LANDSCAPE_HEIGHT - 80 + self.selected_slot*10)


        else:
            for effect in self.enemy_general_effects:
                effect_name, effect_start_time = effect
                if effect_name == "focus_attack":
                    return

            self.enemy_selected_slot += delta

            if self.terrain == "water":
                if self.enemy_selected_slot < 0:
                    self.enemy_selected_slot = 1
                elif self.enemy_selected_slot > 1:
                    self.enemy_selected_slot = 0
                if self.siege_defense == "right":
                    self.enemy_slot_indicator.rect.x, self.enemy_slot_indicator.rect.y = LANDSCAPE_WIDTH - self.enemy_selected_slot*55, self.enemy_selected_slot*200 + 320
                else:
                    self.enemy_slot_indicator.rect.x, self.enemy_slot_indicator.rect.y = LANDSCAPE_WIDTH - 30, self.enemy_selected_slot*200+320

            else:
                if self.enemy_selected_slot < 0:
                    self.enemy_selected_slot = 7
                elif self.enemy_selected_slot > 7:
                    self.enemy_selected_slot = 0

                if self.siege_defense == "left":
                    self.enemy_slot_indicator.rect.x, self.enemy_slot_indicator.rect.y = LANDSCAPE_WIDTH - self.enemy_selected_slot*55, self.enemy_selected_slot * 60 + 160
                else:
                    self.enemy_slot_indicator.rect.x, self.enemy_slot_indicator.rect.y = LANDSCAPE_WIDTH - 30, self.enemy_selected_slot*60+160


            if self.enemy_consecutive_deployments > 0:
                self.enemy_consecutive_deployments = 0

            if self.showing_minimap:
                self.enemy_minimap_slot_indicator.rect.center = (LANDSCAPE_WIDTH - 15,
                                                 LANDSCAPE_HEIGHT - 80 + self.enemy_selected_slot*10)



    def show_start_screen(self):
        #self.draw_text("Use the arrow keys to move/jump.", 16, WHITE, self.game_width // 2, self.game_height * 2 // 10)
        #self.draw_text("Click and hold to shoot, using the cursor to aim.", 16,
        #               WHITE, self.game_width // 2, self.game_height * 3 // 10)
        #self.draw_text("Press <p> to pause/resume the game.", 16,
        #               WHITE, self.game_width // 2, self.game_height * 4 // 10)
        self.screen.blit(self.sprite_quick_guide.image, pg.Rect((0, 0, LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT)))
        random_hint_text = choice(RANDOM_HINTS).split("\n")
        for i in range(len(random_hint_text)):
            line = random_hint_text[i]
            self.draw_text(line, 15, WHITE, self.game_width // 2, 550+22*i, anchor="midleft")
        self.draw_text("Press <Spacebar> to start battle.", 16, WHITE, self.game_width // 2, self.game_height-30)
        pg.display.flip()
        self.listen_for_key()


    def surrender(self):

        self.decision_bg = Static(LANDSCAPE_WIDTH // 2, LANDSCAPE_HEIGHT // 2, "decision_interface_bg_small.png",
                                  anchor="center")
        self.all_sprites.add(self.decision_bg)
        self.decision_sprites.add(self.decision_bg)

        self.yes_button = Button(self.decision_bg.rect.left + 200, self.decision_bg.rect.bottom - 100,
                                 "menu_button_yes.png", command=self.confirm_surrender)
        self.all_sprites.add(self.yes_button)
        self.decision_sprites.add(self.yes_button)
        self.buttons.append(self.yes_button)

        self.cancel_button = Button(self.decision_bg.rect.right - 200, self.decision_bg.rect.bottom - 100,
                                    "menu_button_cancel.png", command=partial(self.confirm_surrender, False))
        self.all_sprites.add(self.cancel_button)
        self.decision_sprites.add(self.cancel_button)
        self.buttons.append(self.cancel_button)

        self.decision_text = "Are you sure you want to surrender?"


    def confirm_surrender(self, proceed=True):
        for sp in self.decision_sprites:
            sp.kill()
        self.running = True
        self.decision_text = ""
        if proceed:
            self.show_game_over_screen(won=False)


    def show_game_over_screen(self, won=False):
        self.dim_screen = pg.Surface(self.screen.get_size()).convert_alpha()
        self.dim_screen.fill((0, 0, 0, 200))
        self.screen.blit(self.dim_screen, (0, 0))

        captured_enemy_commander = 0
        post_battle_player_army_size = sum(self.player_army.values())
        post_battle_enemy_army_size = sum(self.enemy_army.values())

        if won:
            post_battle_army_size_ratio = post_battle_player_army_size / (post_battle_enemy_army_size+1)
            post_battle_kill_bonus = (.2 + self.player_morale/self.player_morale_max)*post_battle_army_size_ratio**.5
            self.player_exp_gained = int(self.player_morale/self.player_morale_max*300+self.enemy_general.level*50) + int(300*self.battle_experience_bonus)
            self.player_general.gain_exp(self.player_exp_gained)
            self.enemy_general.gain_exp(50*self.main_game.difficulty)
            if self.enemy_general.simple_name != "none":
                if self.enemy_general.loyalty >= 5:
                    self.enemy_general.loyalty -= 5
                if self.enemy_general.loyalty >= 15 and "straightforward" in self.player_general.skills:
                    self.enemy_general.loyalty -= 15
            if self.player_general.simple_name != "none":
                if self.player_general.loyalty <= 97:
                    self.player_general.loyalty += 3
            if "orderly_retreat" in self.enemy_general.skills:
                post_battle_kill_bonus *= .5

            #keep a portion of the units currently deployed on battlefield but not yet dead
            for sp in self.player_unit_sprites:
                if random() <= .75 and sp.unit_type in self.player_army:
                    self.player_army[sp.unit_type] += 1

            for unit_type, number in self.enemy_army.items():
                self.enemy_army[unit_type] = int(max([0,number*(1-post_battle_kill_bonus)]))

            if self.siege_defense == "right" and self.enemy_general.simple_name == "none":
                self.enemy_army = {}
            elif self.siege_defense == "left":
                walls = [building for building in self.siege_city_object.buildings if building.name == "Walls"][0]
                retainers = FORTRESS_WALL_RETAINERS[walls.level-1]
                for retainer, amount in retainers.items():
                    if retainer in self.player_army:
                        if self.player_army[retainer] >= amount:
                            self.player_army[retainer] -= amount//2

            capture_threshold = self.enemy_general.combat_prowess/self.player_general.combat_prowess - (sum(self.enemy_army.values()) == 0)*.25
            if "dragon_heart" in self.player_general.skills:
                capture_threshold *= .75
            if "dragon_spirit" in self.enemy_general.skills:
                capture_threshold *= 1.25
            if "fortune_general" in self.enemy_general.skills:
                capture_threshold *= 1.5
            if "colossal_stature" in self.enemy_general.skills:
                capture_threshold *= 1.25
            if "flying_dagger" in self.player_general.skills:
                capture_threshold *= .8
            if "meteor_hammer" in self.player_general.skills:
                capture_threshold *= .25
            if random() >= capture_threshold and self.enemy_general.simple_name != "none" and self.siege_defense != "left":
                captured_enemy_commander = 1
                if "excellent_bodyguard" in self.enemy_general.skills and random() <= .25:
                    captured_enemy_commander = 0

            if self.player_general.simple_name != "none":
                self.post_battle_sprites.add(Static(self.game_width // 2, 200,
                                                    "general_avatar_{}.png".format(self.player_general.simple_name),
                                                    anchor="center"))
            if self.custom:
                self.draw_text("Victory!", 40, YELLOW, self.game_width // 2, self.game_height*3//5)
            else:
                self.draw_text("Victory! +{} experience!".format(self.player_exp_gained),
                               40, YELLOW, self.game_width // 2, self.game_height*3//5)


        else:
            post_battle_army_size_ratio = post_battle_enemy_army_size / (post_battle_player_army_size+1)
            post_battle_kill_bonus = (.2+self.enemy_morale/self.enemy_morale_max)*post_battle_army_size_ratio**.5
            self.enemy_general.gain_exp(int(self.enemy_morale/self.enemy_morale_max*300+self.player_general.level*50)*self.main_game.difficulty + int(300*self.battle_experience_bonus))
            self.player_general.gain_exp(50)
            self.player_exp_gained = 50
            if self.player_general.simple_name != "none":
                if self.player_general.loyalty >= 5:
                    self.player_general.loyalty -= 5
                if self.player_general.loyalty >= 15 and "straightforward" in self.enemy_general.skills:
                    self.player_general.loyalty -= 15
            if self.enemy_general.simple_name != "none":
                if self.enemy_general.loyalty <= 97:
                    self.enemy_general.loyalty += 3
            if "orderly_retreat" in self.player_general.skills:
                post_battle_kill_bonus *= .5

            # keep a portion of the units currently deployed on battlefield but not yet dead
            for sp in self.enemy_unit_sprites:
                if random() <= .75:
                    self.enemy_army[sp.unit_type] += 1
            for unit_type, number in self.player_army.items():
                self.player_army[unit_type] = int(max([0, number * (1 - post_battle_kill_bonus)]))

            if self.siege_defense == "left" and self.player_general.simple_name == "none":
                self.player_army = {}
            elif self.siege_defense == "right":
                walls = [building for building in self.siege_city_object.buildings if building.name == "Walls"][0]
                retainers = FORTRESS_WALL_RETAINERS[walls.level-1]
                for retainer, amount in retainers.items():
                    if retainer in self.enemy_army:
                        if self.enemy_army[retainer] >= amount:
                            self.enemy_army[retainer] -= amount//2

            # did player general get captured?
            capture_threshold = self.player_general.combat_prowess/self.enemy_general.combat_prowess - (sum(self.player_army.values()) == 0)*.25
            if "dragon_heart" in self.enemy_general.skills:
                capture_threshold *= .75
            if "dragon_spirit" in self.player_general.skills:
                capture_threshold *= 1.25
            if "fortune_general" in self.player_general.skills:
                capture_threshold *= 1.5
            if "colossal_stature" in self.player_general.skills:
                capture_threshold *= 1.25
            if "flying_dagger" in self.enemy_general.skills:
                capture_threshold *= .8
            if "meteor_hammer" in self.enemy_general.skills:
                capture_threshold *= .25
            if random() >= capture_threshold and self.player_general.simple_name != "none" and self.siege_defense != "right":
                captured_enemy_commander = -1
                if "excellent_bodyguard" in self.player_general.skills and random() <= .25:
                    captured_enemy_commander = 0

            if self.enemy_general.simple_name != "none":
                self.post_battle_sprites.add(Static(self.game_width // 2, 200,
                                                    "general_avatar_{}.png".format(self.enemy_general.simple_name),
                                                    anchor="center"))

            if self.custom:
                self.draw_text("Defeat!", 40, RED, self.game_width // 2, self.game_height * 3 // 5)
            else:
                self.draw_text("Defeat! +{} experience!".format(self.player_exp_gained),
                               40, RED, self.game_width // 2, self.game_height*3//5)

        if not self.custom:
            #load prebattle savestate and update; save before re-loading campaign map
            campaign_data = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "prebattle_autosave.p"), "rb"))

            #update game metrics
            campaign_data["player_troops_killed"] += sum(self.initial_player_army_object.values()) - sum(self.player_army.values())
            campaign_data["enemy_troops_killed"] += sum(self.initial_enemy_army_object.values()) - sum(self.enemy_army.values())
            if won:
                campaign_data["battles_won"] += 1
            else:
                campaign_data["battles_lost"] += 1
                
            #clean up the army composition dictionay first
            self.player_army = {key:val for key, val in self.player_army.items() if val != 0}
            self.enemy_army = {key: val for key, val in self.enemy_army.items() if val != 0}

            #replace references in loaded save state
            for army in campaign_data["army"]:
                if army.commander.name == self.player_general.name:
                    army.army_composition = self.player_army
                    army.commander = self.player_general
                elif army.commander.name == self.enemy_general.name:
                    army.army_composition = self.enemy_army
                    army.commander = self.enemy_general

            #if siege defense, remove additional units
            if self.siege_defense == "left":
                for city in campaign_data["city"]:
                    if city.name == self.siege_city_object.name:
                        city.garrison = self.player_army
            elif self.siege_defense == "right":
                for city in campaign_data["city"]:
                    if city.name == self.siege_city_object.name:
                        city.garrison = self.enemy_army

            pickle.dump(campaign_data, open(os.path.join(SAVE_SLOTS_DIR, "postbattle_autosave.p"), "wb"))
            self.post_battle_cmd = partial(self.main_game.post_battle_summary, won, captured_enemy_commander,
                                           self.siege_defense, self.siege_city_object) #perform post battle summary; pass whether won/lost and whether captured enemy general

        self.post_battle_sprites.draw(self.screen)
        if self.custom:
            self.draw_text("Press <Esc> to exit. Press <r> to play again.", 16, WHITE, self.game_width // 2, self.game_height - 50)
        else:
            self.draw_text("Press <Esc> to return to campaign.", 16, WHITE, self.game_width // 2, self.game_height-50)
        pg.display.flip()
        self.listen_for_key(False, won)


    def listen_for_key(self, start=True, won=False):  # if not start, then apply game over logic
        waiting = True
        while waiting:
            if self.running:
                self.clock.tick(self.fps)

            for event in pg.event.get():
                if event.type == pg.KEYUP:
                    if start:
                        if event.key == pg.K_SPACE:
                            waiting = False
                            self.running = True

                    else:
                        if event.key == pg.K_r and self.custom:
                            waiting = False
                            self.running = True
                            self.playing = True
                            self.retry = True
                            self.new()
                        elif event.key == pg.K_ESCAPE:
                            for sp in self.all_sprites:
                                sp.kill()
                            if self.custom:
                                self.main_game.parent_scene.parent_scene.new()
                            else:
                                self.main_game.new(load_from="postbattle_autosave.p", post_battle_cmd=self.post_battle_cmd)


    def draw_text(self, text, size, color, x, y, anchor="midtop"):
        font = pg.font.Font(FONT_NAME, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        if anchor == "center":
            text_rect.center = (x, y)
        else:
            text_rect.midtop = (x, y)
        self.screen.blit(text_surface, text_rect)


    def draw_multiline_text(self, text, font, color, x, y, align="midtop", max_width=None):
        if max_width:
            text_surface = font.render(text, True, color, None, max_width)
        else:
            text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        if align == "midleft":
            text_rect.midleft = (x, y)
        elif align == "topleft":
            text_rect.topleft = (x, y)
        else:
            text_rect.midtop = (x, y)
        self.screen.blit(text_surface, text_rect)


    def drawWrappedText(self, text, color, rect, font, aa=False, bkg=None):
        rect = pg.Rect(rect)
        y = rect.top
        lineSpacing = 10

        # get the height of the font
        fontHeight = font.size("Tg")[1]

        while text:
            i = 1

            # determine if the row of text will be outside our area
            if y + fontHeight > rect.bottom:
                break

            # determine maximum width of line
            while font.size(text[:i])[0] < rect.width and i < len(text):
                i += 1

            # if we've wrapped the text, then adjust the wrap to the last word
            if i < len(text):
                i = text.rfind(" ", 0, i) + 1

            # render the line and blit it to the surface
            if bkg:
                image = font.render(text[:i], 1, color, bkg)
                image.set_colorkey(bkg)
            else:
                image = font.render(text[:i], aa, color)

            self.screen.blit(image, (rect.left, y))
            y += fontHeight + lineSpacing

            # remove the text we just blitted
            text = text[i:]

        return text