from collections import deque

#Converts seconds into time display
def time_conversion_to_string(t):
    minutes = t//60
    seconds = t%60
    if seconds < 10:
        return "{}:0{}".format(minutes, seconds)
    else:
        return "{}:{}".format(minutes, seconds)


def avg(aList):
    return sum(aList)/len(aList)


def calculate_grid_distance(x1, y1, x2, y2):
    return abs(x1-x2) + abs(y1-y2)


def concat_dicts(d1, d2):
    result_dic = d2.copy()
    for k, v in d1.items():
        if k in result_dic:
            result_dic[k] += v
        else:
            result_dic[k] = v
    return result_dic


# To store matrix cell cordinates
class Point:
    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y

# A data structure for queue used in BFS
class queueNode:
    def __init__(self, pt: Point, dist: int):
        self.pt = pt  # The cordinates of the cell
        self.dist = dist  # Cell's distance from the source


# Check whether given cell(row,col)
# is a valid cell or not
def isValid(grid_width, grid_height, row: int, col: int):
    return (row >= 0) and (row < grid_height) and (col >= 0) and (col < grid_width)


# These arrays are used to get row and column
# numbers of 4 neighbours of a given cell
rowNum = [-1, 0, 0, 1]
colNum = [0, -1, 1, 0]


# Function to find the shortest path between
# a given source cell to a destination cell.
def BFS(mat, src: Point, dest: Point):
    # check source and destination cell
    # of the matrix have value 1
    ROW = len(mat)
    COL = len(mat[0])
    if mat[src.x][src.y] != 1 or mat[dest.x][dest.y] != 1:
        return -1

    visited = [[False for i in range(COL)]
               for j in range(ROW)]

    # Mark the source cell as visited
    visited[src.x][src.y] = True

    # Create a queue for BFS
    q = deque()

    # Distance of source cell is 0
    s = queueNode(src, 0)
    q.append(s)  # Enqueue source cell

    # Do a BFS starting from source cell
    while q:
        curr = q.popleft()  # Dequeue the front cell

        # If we have reached the destination cell,
        # we are done
        pt = curr.pt
        if pt.x == dest.x and pt.y == dest.y:
            return curr.dist

        # Otherwise enqueue its adjacent cells
        for i in range(4):
            row = pt.x + rowNum[i]
            col = pt.y + colNum[i]

            # if adjacent cell is valid, has path
            # and not visited yet, enqueue it.
            if (isValid(COL, ROW, row, col) and mat[row][col] > 0 and not visited[row][col]):
                visited[row][col] = True
                Adjcell = queueNode(Point(row, col),
                                    curr.dist + mat[row][col])
                q.append(Adjcell)

    # Return -1 if destination cannot be reached
    return -1


def simplify_matrix(matrix, start_x, start_y):
    width = len(matrix)
    height = len(matrix[0])
    for i in range(height):
        for j in range(width):
            if abs(start_y-i) + abs(start_x-j) >= max([width, height]):
                matrix[i][j] = 0

    return matrix


# Driver code
def calculate_map_distance(mat, start_x, start_y, end_x, end_y):
    source = Point(start_x, start_y)
    dest = Point(end_x, end_y)

    #mat = simplify_matrix(mat, start_x, start_y)
    dist = BFS(mat, source, dest)

    if dist != -1:
        return dist
        #print("Shortest Path is", dist)
    else:
        return None
        #print("Shortest Path doesn't exist")

# This code is contributed by stutipathak31jan