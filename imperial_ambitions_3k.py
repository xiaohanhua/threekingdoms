from custom_battle_commander_select import*
from country_selection import*
from load_game_scene import*
from attribution import Attribution_Scene
#from PIL import Image
from sprites import*
#import ctypes
import datetime
import logging
import subprocess
import sys

os.chdir(RESOURCES_DIR)

class Game:
    def __init__(self):
        self.game_width, self.game_height = LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT
        if DEMO:
            self.screen = pg.display.set_mode((self.game_width, self.game_height), pg.DOUBLEBUF)
            os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (50, 50)
        else:
            self.screen = pg.display.set_mode((self.game_width, self.game_height), pg.FULLSCREEN|pg.DOUBLEBUF)

        #os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (ctypes.windll.user32.GetSystemMetrics(0)//2,
        #                                                ctypes.windll.user32.GetSystemMetrics(1)//2)
        #os.environ['SDL_VIDEO_CENTERED'] = '1'
        pg.display.set_caption(TITLE)
        pg.display.set_icon(pg.image.load("sprite_main_icon.png"))
        self.clock = pg.time.Clock()

        pg.mixer.init()
        pg.font.init()
        self.hover_sound = pg.mixer.Sound("sfx_hover.ogg")
        self.click_sound = pg.mixer.Sound("sfx_click.ogg")
        self.campaign_map_image_partitions = [pg.image.load("3k_map-{}-{}.png".format(i,j)).convert_alpha() for j in range(2) for i in range(4)]


    def new(self, start_music=True):
        self.running = True
        self.playing = True
        pg.mouse.set_visible(True)
        if start_music:
            pg.mixer.music.load("battleThemeA.mp3")
            pg.mixer.music.set_volume(.5)
            pg.mixer.music.play(20)

        # transitions
        self.transition_sprites = pg.sprite.Group()
        self.transition = None
        self.post_transition_function = None
        self.initiate_transition("in")


        self.all_sprites = pg.sprite.Group()
        self.buttons = pg.sprite.Group()

        self.background = Static(0, self.game_height - 650, "sprite_menu_bg.png")
        self.all_sprites.add(self.background)

        self.campaign_button = Button(LANDSCAPE_WIDTH//2, LANDSCAPE_HEIGHT//2 - 150, "menu_button_campaign.png",
                                  command=self.campaign)
        self.buttons.add(self.campaign_button)
        self.all_sprites.add(self.campaign_button)

        self.load_game_button = Button(LANDSCAPE_WIDTH//2, LANDSCAPE_HEIGHT//2 - 50, "menu_button_load_game.png",
                                  command=self.load_game)
        self.buttons.add(self.load_game_button)
        self.all_sprites.add(self.load_game_button)

        self.custom_battle_button = Button(LANDSCAPE_WIDTH//2, LANDSCAPE_HEIGHT//2+50, "menu_button_custom_battle.png",
                                  command=self.custom_battle)
        self.buttons.add(self.custom_battle_button)
        self.all_sprites.add(self.custom_battle_button)

        self.attribution_button = Button(LANDSCAPE_WIDTH // 2, LANDSCAPE_HEIGHT // 2 + 150,
                                           "menu_button_attribution.png",
                                           command=self.attribution)
        self.buttons.add(self.attribution_button)
        self.all_sprites.add(self.attribution_button)

        self.join_discord_button = Button(0, LANDSCAPE_HEIGHT-100, "sprite_scrub_button.png",
                                           command=self.join_discord, anchor="topleft", scale=(100, 100))
        self.buttons.add(self.join_discord_button)
        self.all_sprites.add(self.join_discord_button)

        self.game_guide_button = Button(100, LANDSCAPE_HEIGHT-100, "sprite_game_guide_button.png",
                                           command=self.game_guide, anchor="topleft", scale=(100, 100))
        self.buttons.add(self.game_guide_button)
        self.all_sprites.add(self.game_guide_button)

        self.exit_game_button = Button(LANDSCAPE_WIDTH-100, LANDSCAPE_HEIGHT-100, "sprite_exit_game_button.png",
                                           command=self.exit_game, anchor="topleft", scale=(100, 100))
        self.buttons.add(self.exit_game_button)
        self.all_sprites.add(self.exit_game_button)

        self.run()


    def exit_game(self):
        self.running = False
        self.playing = False
        pg.quit()
        sys.exit()


    def initiate_transition(self, direction):
        if direction == "in":
            self.transition = FadeBlackTransition(45, "in")
            self.transition_sprites.add(self.transition)
        else:
            self.transition = FadeBlackTransition(45, "out")
            self.transition_sprites.add(self.transition)


    def transition_clean_up(self):
        self.transition.kill()
        self.transition = None
        self.post_transition_function = None
        self.playing = False
        self.running = False
        for s in self.all_sprites:
            s.kill()


    def join_discord(self):
        if os.name == "nt":
            res = 1
            try:
                res = subprocess.call(
                    "start microsoft-edge:https://discord.gg/at6CgQBCyu",
                    shell=True, timeout=4)
            except:
                res = subprocess.call(
                    "start chrome.exe https://discord.gg/at6CgQBCyu",
                    shell=True, timeout=4)
            else:
                if res == 1:
                    print("Default browser not found. Join Discord at: https://discord.gg/at6CgQBCyu")


    def game_guide(self):
        try:
            os.system(os.path.join(RESOURCES_DIR, "guide.pdf"))
        except:
            print("Default browser not found. Find the guide.pdf file in the Resources folder.")


    def campaign(self):
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = self.campaign
        elif not self.transition.fading:
            self.transition_clean_up()
            scene = Country_Select_Scene(self)
            scene.new()


    def load_game(self):
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = self.load_game
        elif not self.transition.fading:
            self.transition_clean_up()
            scene = Load_Game_Scene(self)
            scene.new()


    def custom_battle(self):
        #pg.display.quit()
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = self.custom_battle
        elif not self.transition.fading:
            self.transition_clean_up()
            scene = Commander_Select_Scene(self)
            scene.new()

    def attribution(self):
        #pg.display.quit()
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = self.attribution
        elif not self.transition.fading:
            self.transition_clean_up()
            scene = Attribution_Scene(self)
            scene.new()


    def run(self):
        while self.playing:
            if self.running:
                self.clock.tick(FPS+10)
                self.events()
                self.update()
                self.draw()


    def events(self):
        pos = pg.mouse.get_pos()
        for button in self.buttons:
            if button.rect.collidepoint(pos):
                if pg.mouse.get_pressed()[0]:
                    button.pressed = True
                else:
                    button.pressed = False
                if not button.hovering:
                    button.hovering = True
                    self.hover_sound.play()
            else:
                button.pressed = False
                button.hovering = False

        for event in pg.event.get():
            if event.type == pg.MOUSEBUTTONUP:
                self.click_sound.play()
                for button in self.buttons:
                    if button.rect.collidepoint(pos):
                        button.pressed = False
                        button.command()
                        break

            if event.type == pg.QUIT:
                choice = pg.display.message_box("Quit", "Are you sure you wish to quit?",
                         buttons=("Yes", "No"), return_button=1, escape_button=None
                )

                if choice == 0:
                    self.exit_game()


    def update(self):
        self.all_sprites.update()
        if self.transition:
            if self.transition.fading:
                self.transition.update()
            else:
                if self.post_transition_function:
                    self.post_transition_function()
                else:
                    self.transition = None


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)
            self.transition_sprites.draw(self.screen)
            pg.display.flip()


    def drawWrappedText(self, text, color, rect, font, aa=False, bkg=None):
        rect = pg.Rect(rect)
        y = rect.top
        lineSpacing = 10

        # get the height of the font
        fontHeight = font.size("Tg")[1]

        while text:
            i = 1

            # determine if the row of text will be outside our area
            if y + fontHeight > rect.bottom:
                break

            # determine maximum width of line
            while font.size(text[:i])[0] < rect.width and i < len(text):
                i += 1

            # if we've wrapped the text, then adjust the wrap to the last word
            if i < len(text):
                i = text.rfind(" ", 0, i) + 1

            # render the line and blit it to the surface
            if bkg:
                image = font.render(text[:i], 1, color, bkg)
                image.set_colorkey(bkg)
            else:
                image = font.render(text[:i], aa, color)

            self.screen.blit(image, (rect.left, y))
            y += fontHeight + lineSpacing

            # remove the text we just blitted
            text = text[i:]

        return text


logging.basicConfig(filename=os.path.join(BASE_DIR, 'debug.log'), level=logging.INFO)

try:
    logging.info('\n\n------------------------------------\nGame launched at: ' + str(datetime.datetime.now()))
    sys.setrecursionlimit(1500)
    g = Game()
    g.new()
except Exception as e:
    print(e)
    logging.info('\nGame crashed at: ' + str(datetime.datetime.now()))
    logging.exception('Error msg: ' + str(e))
