"""
Starting Year: 200, after Battle of Guandu


77, 18 for Morale bar
77, 27 for Command bar


16, 16, for tooltip avatar
16 + 11x, 115 + 40y for tooltip stat cells


Grab 280 x 390 avatar and scale down to 180 x 250 for commander tooltips


Maps for info:
https://www.gamewikia.com/uploads/2018/07/24/cover.1280.720.1.1-0e07574866cd227f5ea597de55a0688b.jpg
https://upload.wikimedia.org/wikipedia/commons/0/07/%E4%B8%89%E5%9B%BD%E8%A1%8C%E6%94%BF%E5%8C%BA%E5%88%92%28%E7%AE%80%29.png
http://images.17173.com/so/images/map/map.jpg
http://king.yeyou.com/news/05152015/133736515.shtml


Alpha 0.7
- View All Alerts (with click to zoom on city if applicable)
- View All Armies
- View All Cities (rank by happiness or population)
- Quick guide (campaign)
- Attribution screen (scrolling)
- Winning/Losing condition
- Quick guide (battle)
- Add more pre-battle game hints
- Multipage unit recruitment interface (fix for both custom battle and campaign)
- Downgrade Luoyang/Upgrade Xuchang
- Verify starting generals/advisors
- Introduction scene
- Added basic logging
- PDF Guide
- Discord Button

Alpha 0.8
- Smarter AI
- Army commander level indicator (stars)
- Balancing


Alpha 0.9
- Bug fixes [DONE]
- Make "close" button more visible post-battle [DONE]
- WSAD to move on campaign map [DONE]
- Light Dagger Infantry [DONE]
- Camera snap back to own unit after AI turn [DONE]
- Construction notifications [DONE]
- Diplomacy rebalance (fix issue - can form alliance w/ enemy of ally right now) [DONE]
- Hide other text when viewing guide [DONE]
- Significant performance improvements on campaign map/world map [DONE]
- Hide controls guide when pausing in battle [DONE]
- Update "poison" effect (poison_mist) [DONE]
- Improve AI think time (check within 10x10 radius for "free" targets first) [DONE]



Alpha 1.0
- Bug fixes [DONE]
- Xuchang capital city bonuses [DONE]
- Improve projectile (arrow) collision detection w/ walls [DONE]
- Enhanced espionage visibility calculations (distance-dependent) [DONE]
- Enhanced battle controls (QWERASDF for slot selection; 1/2 for special atk) [DONE]
- Update battle control guide [DONE]
- Army merging/unit transfers [DONE]
- Added option to view Military Report [DONE]
- More generals/skills [DONE]
- Smarter AI on hard/very hard difficulties [DONE]


Alpha 1.01
- Bug hot fix


Alpha 1.1
- Image compression
- Fixed memory leak
- Bug fix with End Turn quick key
- Fix food production calculation bug
- Add quick key (CTRL+L) for loading previous auto_save
- Poison Mist bug fix
- Reduced in-battle lag when too many archers on battlefield
- More aggressive AI on harder difficulties


Alpha 1.2
- Merge army bug fix (when merging into embedded army)
- AI calculation speed-up (up to 38% improvement on turn 1 and 69% in subsequent turns)
- Characters gain a small amount of static exp each year while in the recruitment pool at the Academy
- Other bug fixes/adjustments for balance


Beta 1.0
- Enable quitting the game by closing the window using Red X with pop up confirmation
- Fixed bug that might cause crash when recruiting barbarian units after recruiting Southern Barbarian characters when not using Shu faction
- Increased impact of natural disaster events
- Fixed bug with health bar display
- Other minor bug fixes/adjustments for balance


Beta 1.01
- Fixed bug where characters who are sitting in the Academy are not using their upgrade points (from leveling up over time) upon recruitment, resulting in their stats being lower than they should be
- Minor adjustments for balance
- Added new skills
- Espionage adjustments
- Unit icons are greyed out if deployment conditions are not met


Beta 1.1
- More characters (Xun You, Kong Rong, Liu Ye)
- Characters gain small amount of experience when overseeing city
- Character loyalty decreases if idle (not overseeing city, not in battle) for at least 5 consecutive turns
- Rebellion if character loyalty is too low
- Bug fixes
- Adjustments for balance



------------------------------------------------------------------------------------------


Beta 1.2
- New units (halberd)
- Updated units (for more diversification)
- Unit descriptions [TODO]
- New characters
- New buildings
- More SFX for spcl atks
- Bug fixes


Beta 1.3
- Map updates
- Achievements
  1) win a campaign under certain conditions: recruit ranged units only; cannot recruit ranged units;
                                              recruit militia only; win under X turns; no generals dying;
                                              complete tech tree
  2) win a campaign with every faction on Hard/Very Hard difficulty
  3) no war with any faction for 25 turns
  4) win 5 battles in a turn using only Barbarian units
- Events requiring decisions
- Special campaign modes: open tech tree (no pre-reqs), play as Southern Barbarians (?)
- Improved logging
- Fix window drag bug abuse (see Discord for details & solution)



Yuan Shao: Shen Pei(1.2), Xin Ping, Yuan Tan, Yuan Xi
Cao Cao: Cao Xiu, Cao Chun, Xiahou Mao, Zang Ba, Zhang Yan, Liu Ye, Zhang Ji, Zhang Xiu, Huche'er and Man Chong
Liu Bei: Chen Dao(famous historically, most achievements were given to Zhao Yun)
Liu Bei: Liu Pi and Gong Du(historically until they were executed by Cao Cao)
Yuan Shao: also Zhen Ji
Cao Cao: Cao Pi, Cao Zhi
Ma Teng: Fu Gan, Ma Tie, Ma Xiu, Pang Hui(obscure dude, massacred Guan Yu's relatives)
Sun Quan: Zhuge Ke, He Qi, Lu Kang, Lu Ji, Jiang Qin, Lv Fan, Zhang Hong, Zhu Zhi, Wu Jing, Sun Ben


<<<<<<<Quotes>>>>>>>

inspire: Onwards, my men!

vanity: Impossible! I can't be losing!

bloodthirst: Your blood shall nourish my blade...

taunt: Is that all you've got? / Hmph, I expected more of a challenge...

breakthrough: An opening in the enemy lines! Chaaaarge!

second_wind: It's not over yet!

relentless_charge:

rapid_advance:

expert_marksmanship: Ready... aim... fire!

psychological_warfare: Attacking the heart is far better than attacking the city...


"""