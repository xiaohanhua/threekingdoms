from random import*
from settings import*

class Commander:
    def __init__(self, name, simple_name, age, loyalty, country, combat_prowess, intellect, command, charisma,
                 action_points, level=1, experience=0, upgrade_points=0, earliest_start_year=200, hometown="",
                 idle_turns=0):
        self.name = name
        self.simple_name = simple_name
        self.age = age
        self.age_in_months = self.age*12
        self.loyalty = loyalty
        self.country = country
        self.combat_prowess = combat_prowess
        self.intellect = intellect
        self.command = command
        self.charisma = charisma
        self.action_points = action_points

        self.level = level
        self.experience = experience
        self.upgrade_points = upgrade_points
        self.earliest_start_year = earliest_start_year
        self.hometown = hometown
        self.idle_turns = idle_turns
        self.max_army_size = int(500+self.command*10)

        self.level_thresholds = [0, 150, 350, 755, 1310, 1990, 2775, 3665, 4650, 5750, 7000, 999999999]

        #Shu
        if self.name == "Guan Yu":
            self.learnable_attacks = {"deadly_blade":1, "drowning_the_seven_armies":8}
            self.learnable_skills = {"renowned_commander":1, "red_hare":2, "advanced_formation":6}
        elif self.name == "Zhang Fei":
            self.learnable_attacks = {"fierce_assault":1, "mighty_roar":4}
            self.learnable_skills = {"iron_fist":1, "renowned_commander":2, "burning_bridges":4, "master_spearmen":6, "oath_of_vengeance":10}
        elif self.name == "Zhang Bao":
            self.learnable_attacks = {"rally":1, "fierce_assault":4}
            self.learnable_skills = {"basic_formation":1, "oath_of_vengeance": 4, "master_spearmen":6}
        elif self.name == "Guan Xing":
            self.learnable_attacks = {"inspire": 1, "deadly_blade":7}
            self.learnable_skills = {"basic_formation":1, "oath_of_vengeance": 4, "brilliant_commander":6}
        elif self.name == "Zhao Yun":
            self.learnable_attacks = {"second_wind":2, "qinggang_sword":5}
            self.learnable_skills = {"dragon_heart": 1, "dragon_spirit": 1, "master_spearmen": 3, "daredevil_commander":6}
        elif self.name == "Zhu Rong":
            self.learnable_attacks = {}
            self.learnable_skills = {"barbarian_queen":1, "flying_dagger":3, "elephant_stampede":5}
        elif self.name == "Meng Huo":
            self.learnable_attacks = {}
            self.learnable_skills = {"barbarian_king":1, "elephant_stampede":5}
        elif self.name == "Wutugu":
            self.learnable_attacks = {}
            self.learnable_skills = {"barbarian_king":1, "colossal_stature":1, "rattan_armor_infantry":5}
        elif self.name == "King Duosi":
            self.learnable_attacks = {}
            self.learnable_skills = {"barbarian_king":1, "poison_mist":5}
        elif self.name == "Zhou Cang":
            self.learnable_attacks = {}
            self.learnable_skills = {"naval_commander":1, "basic_formation":4}
        elif self.name == "Mi Zhu":
            self.learnable_attacks = {}
            self.learnable_skills = {"wealthy_merchant":1, "diplomat":1, "commissioner_of_development":1, "intellectual":3, "scholar":5}
        elif self.name == "Jian Yong":
            self.learnable_attacks = {}
            self.learnable_skills = {"skilled_persuader":1, "diplomat":1, "intellectual":3}
        elif self.name == "Sun Qian":
            self.learnable_attacks = {}
            self.learnable_skills = {"diplomat":1, "intellectual":1, "commissioner_of_internal_affairs":1}
        elif self.name == "Zhuge Liang":
            self.learnable_attacks = {"fire_attack":2, "bagua_formation":4}
            self.learnable_skills = {"foresight":1, "master_of_ambush":1, "master_inventor":1, "renowned_commander":3,  "empty_fortress":7}
        elif self.name == "Ma Su":
            self.learnable_attacks = {"psychological_warfare": 3}
            self.learnable_skills = {"intellectual":1, "barbarian_subduer":5}
        elif self.name == "Ma Liang":
            self.learnable_attacks = {}
            self.learnable_skills = {"intellectual":1, "incorruptible":1, "diplomat":3, "scholar":4}
        elif self.name == "Ma Zhong":
            self.learnable_attacks = {"rally":3}
            self.learnable_skills = {"incorruptible":1, "benevolent_policies":3, "barbarian_subduer":4, "ambush_tactics":7}
        elif self.name == "Zhang Ni":
            self.learnable_attacks = {"rally":1}
            self.learnable_skills = {"humble_upbringing":1, "barbarian_subduer":3, "benevolent_policies":6}
        elif self.name == "Deng Zhi":
            self.learnable_attacks = {"rally":3}
            self.learnable_skills = {"straightforward":1, "incorruptible":1, "diplomat":3, "skilled_persuader":4}
        elif self.name == "Xu Shu":
            self.learnable_attacks = {"breakthrough":1}
            self.learnable_skills = {"referral":1, "military_advisor":1, "intellectual":1}
        elif self.name == "Fei Yi":
            self.learnable_attacks = {}
            self.learnable_skills = {"incorruptible":1, "diplomat":1, "scholar":1, "military_advisor":3, "master_of_writing":7}
        elif self.name == "Liao Hua":
            self.learnable_attacks = {"rally":1, "taunt":3}
            self.learnable_skills = {"emergency_messenger":1, "cavalry_commander":5}
        elif self.name == "Liu Feng":
            self.learnable_attacks = {}
            self.learnable_skills = {"basic_formation":3}


        #Liu Zhang
        elif self.name == "Liu Ba":
            self.learnable_attacks = {}
            self.learnable_skills = {"intellectual":1, "logistician":1, "commissioner_of_supply":3, "commissioner_of_internal_affairs":5, "master_of_writing":8}
        elif self.name == "Li Yan":
            self.learnable_attacks = {"rally": 1}
            self.learnable_skills = {"logistician":1, "commissioner_of_development":4, "master_of_writing":6, "commissioner_of_supply":8}
        elif self.name == "Zhang Song":
            self.learnable_attacks = {}
            self.learnable_skills = {"intellectual":1, "military_advisor":1, "topographer":3}
        elif self.name == "Wu Ban":
            self.learnable_attacks = {}
            self.learnable_skills = {"naval_commander":3}
        elif self.name == "Wu Yi":
            self.learnable_attacks = {"inspire":1}
            self.learnable_skills = {"basic_formation":1}
        elif self.name == "Yan Yan":
            self.learnable_attacks = {"rally":1}
            self.learnable_skills = {"basic_formation":3, "unwavering_grit":7}
        elif self.name == "Meng Da":
            self.learnable_attacks = {}
            self.learnable_skills = {}
        elif self.name == "Fa Zheng":
            self.learnable_attacks = {"fierce_assault":5}
            self.learnable_skills = {"foresight":1, "brilliant_commander":1, "military_strategist":1, "orderly_retreat":4, "chief_advisor":5}
        elif self.name == "Huang Quan":
            self.learnable_attacks = {}
            self.learnable_skills = {"commissioner_of_internal_affairs":1, "commissioner_of_foreign_affairs":1}
        elif self.name == "Zhang Ren":
            self.learnable_attacks = {"breakthrough":1}
            self.learnable_skills = {"ambush_tactics":3}
        elif self.name == "Leng Bao":
            self.learnable_attacks = {"rally":1}
            self.learnable_skills = {"brilliant_commander":4}
        elif self.name == "Li Hui":
            self.learnable_attacks = {"inspire":1}
            self.learnable_skills = {"basic_formation":1, "barbarian_subduer":5, "daredevil_commander":5}

        #Wei
        elif self.name == "Xu Chu":
            self.learnable_attacks = {"fierce_assault":1, "focus_attack":3}
            self.learnable_skills = {"colossal_stature":1, "humble_upbringing":1, "militia_commander":4}
        elif self.name == "Xu Huang":
            self.learnable_attacks = {"expert_marksmanship":1, "supply_raid":4}
            self.learnable_skills = {"basic_formation":1, "great_axe":6}
        elif self.name == "Xiahou Yuan":
            self.learnable_attacks = {"rapid_advance":1, "expert_marksmanship":3}
            self.learnable_skills = {"emergency_messenger":1, "quick_march":5}
        elif self.name == "Xiahou Dun":
            self.learnable_attacks = {"eye_for_an_eye":1}
            self.learnable_skills = {"incorruptible":1, "messenger_pigeon":3}
        elif self.name == "Cao Ren":
            self.learnable_attacks = {"fortification":1, "barricade":3}
            self.learnable_skills = {"defensive_formation":1, "basic_formation":1, "advanced_formation":6}
        elif self.name == "Zhang Liao":
            self.learnable_attacks = {"disarrayment":3, "breakthrough":4}
            self.learnable_skills = {"valiant_defender":1, "advanced_formation":1, "daredevil_commander":5, "renowned_commander":5}
        elif self.name == "Yu Jin":
            self.learnable_attacks = {"caution":4}
            self.learnable_skills = {"basic_formation":1, "orderly_retreat":1, "landlubber":1, "advanced_formation":6}
        elif self.name == "Yue Jin":
            self.learnable_attacks = {"inspire":1, "relentless_charge":4}
            self.learnable_skills = {"siege_master":1, "messenger_pigeon":1}
        elif self.name == "Cheng Yu":
            self.learnable_attacks = {}
            self.learnable_skills = {"military_advisor":1, "intellectual":1, "logistician":1, "forgery":1, "master_of_ambush":3}
        elif self.name == "Jia Xu":
            self.learnable_attacks = {}
            self.learnable_skills = {"military_advisor":1, "intellectual":1, "advanced_formation":3, "ambush_tactics":4}
        elif self.name == "Xun Yu":
            self.learnable_attacks = {}
            self.learnable_skills = {"military_advisor":1, "scholar":1, "commissioner_of_development":3, "commissioner_of_internal_affairs":5, "master_of_writing":7}
        elif self.name == "Guo Jia":
            self.learnable_attacks = {"anticipation":1, "rally":3}
            self.learnable_skills = {"foresight":1, "chief_advisor":1, "intellectual":1, "military_strategist":3, "bequeathed_strategy":6}
        elif self.name == "Deng Ai":
            self.learnable_attacks = {}
            self.learnable_skills = {"humble_upbringing":1, "tuntian":1, "topographer":4, "military_strategist":5, "daredevil_commander":8}
        elif self.name == "Jiang Wei":
            self.learnable_attacks = {"bagua_formation":6, "last_stand":8}
            self.learnable_skills = {"foresight":1,  "basic_formation":1, "advanced_formation":4, "master_of_ambush":6, "unwavering_grit":8}
        elif self.name == "Cao Hong":
            self.learnable_attacks = {}
            self.learnable_skills = {"fortune_general":1, "emergency_messenger":3, "basic_formation":3}
        elif self.name == "Cao Zhen":
            self.learnable_attacks = {"anticipation":1, "barricade":5}
            self.learnable_skills = {"basic_formation":1, "foresight":7}
        elif self.name == "Cao Zhang":
            self.learnable_attacks = {"rally":1}
            self.learnable_skills = {"basic_formation":3}
        elif self.name == "Li Dian":
            self.learnable_attacks = {}
            self.learnable_skills = {"logistician":1, "intellectual":1, "basic_formation":3, "clansman_relations":5}
        elif self.name == "Sima Yi":
            self.learnable_attacks = {}
            self.learnable_skills = {"patience":1, "cease_fire":4, "fortune_general":6, "hidden_ambition":7}
        elif self.name == "Wang Shuang":
            self.learnable_attacks = {}
            self.learnable_skills = {"colossal_stature":1, "meteor_hammer":1}
        elif self.name == "Hao Zhao":
            self.learnable_attacks = {"fortification":3, "barricade":5}
            self.learnable_skills = {"defensive_formation":3, "valiant_defender":5, "daredevil_commander":5}
        elif self.name == "Yang Xiu":
            self.learnable_attacks = {}
            self.learnable_skills = {"scholar":1}
        elif self.name == "Zhong Yao":
            self.learnable_attacks = {}
            self.learnable_skills = {"scholar":1, "forgery":1, "master_of_writing":3}
        elif self.name == "Liu Ye":
            self.learnable_attacks = {}
            self.learnable_skills = {"military_advisor":1, "foresight":1}
        elif self.name == "Xun You":
            self.learnable_attacks = {"supply_raid":1}
            self.learnable_skills = {"chief_advisor":1, "master_of_writing":1, "intellectual":1, "military_strategist":1, "man_of_virtue":5}
        elif self.name == "Kong Rong":
            self.learnable_attacks = {}
            self.learnable_skills = {"scholar":1, "straightforward":1, "confucian_schools":1}

        #Wu
        elif self.name == "Zhou Yu":
            self.learnable_attacks = {"sow_discord":4, "fire_attack":5}
            self.learnable_skills = {"renowned_commander":1, "advanced_formation":1, "naval_commander":3, "military_strategist":6,}
        elif self.name == "Pang Tong":
            self.learnable_attacks = {"iron_chain_formation":4}
            self.learnable_skills = {"military_advisor":1, "brilliant_commander":1, "scholar":1, "military_strategist":4, "commissioner_of_foreign_affairs":5}
        elif self.name == "Gu Yong":
            self.learnable_attacks = {}
            self.learnable_skills = {"benevolent_policies":1, "incorruptible":1, "master_of_writing":5, "commissioner_of_internal_affairs":6}
        elif self.name == "Lu Su":
            self.learnable_attacks = {"rally":1}
            self.learnable_skills = {"diplomat":1, "peacemaker":3, "military_advisor":3, "commissioner_of_foreign_affairs":3, "benevolent_policies":6}
        elif self.name == "Han Dang":
            self.learnable_attacks = {"expert_marksmanship":1}
            self.learnable_skills = {"basic_formation":1, "naval_commander":4}
        elif self.name == "Taishi Ci":
            self.learnable_attacks = {"breakthrough":1, "expert_marksmanship":1}
            self.learnable_skills = {"emergency_messenger":1, "advanced_formation":5}
        elif self.name == "Huang Gai":
            self.learnable_attacks = {"feigned_surrender":4, "relentless_charge":4}
            self.learnable_skills = {"naval_commander":1, "messenger_pigeon":2}
        elif self.name == "Zhang Zhao":
            self.learnable_attacks = {}
            self.learnable_skills = {"scholar":1, "commissioner_of_internal_affairs":2, "commissioner_of_development":3}
        elif self.name == "Yu Fan":
            self.learnable_attacks = {}
            self.learnable_skills = {"straightforward":1, "scholar":1, "commissioner_of_supply":3}
        elif self.name == "Cheng Pu":
            self.learnable_attacks = {"rally":1}
            self.learnable_skills = {"brilliant_commander":1, "basic_formation":1}
        elif self.name == "Zhou Tai":
            self.learnable_attacks = {"last_stand":1}
            self.learnable_skills = {"unwavering_grit":1, "humble_upbringing":1, "militia_commander":4}
        elif self.name == "Ling Tong":
            self.learnable_attacks = {"relentless_charge":1}
            self.learnable_skills = {"basic_formation": 3, "daredevil_commander":5}
        elif self.name == "Xu Sheng":
            self.learnable_attacks = {"disarrayment":3}
            self.learnable_skills = {"basic_formation":1, "defensive_formation":5}
        elif self.name == "Lv Meng":
            self.learnable_attacks = {}
            self.learnable_skills = {"patience":1, "self_cultivation":1}
        elif self.name == "Lu Xun":
            self.learnable_attacks = {"rally":1, "chained_fire_attack":3}
            self.learnable_skills = {"incorruptible":1, "renowned_commander":4, "orderly_retreat": 6, "benevolent_policies":7}
        elif self.name == "Ding Feng":
            self.learnable_attacks = {"rapid_advance":1, "rally":1}
            self.learnable_skills = {"light_dagger_infantry":1, "quick_march":1}
        elif self.name == "Zhuge Jin":
            self.learnable_attacks = {}
            self.learnable_skills = {"diplomat":1, "peacemaker":1}
        elif self.name == "Zhu Huan":
            self.learnable_attacks = {"inspire":1, "fortification":5}
            self.learnable_skills = {"incorruptible":1, "benevolent_policies":3, "valiant_defender":5}

        #Zhang Lu
        elif self.name == "Zhang Wei":
            self.learnable_attacks = {"rally":1}
            self.learnable_skills = {"basic_formation":3}
        elif self.name == "Yan Pu":
            self.learnable_attacks = {}
            self.learnable_skills = {"military_advisor":1, "diplomat":1}

        #Yuan Shao
        elif self.name == "Zhang He":
            self.learnable_attacks = {"rally":1}
            self.learnable_skills = {"advanced_formation":1, "topographer":1, "military_strategist":6, "renowned_commander":7}

        elif self.name == "Yan Liang":
            self.learnable_attacks = {"rally":1, "piercing_blade":1}
            self.learnable_skills = {"renowned_commander":1, "swordsman_commander":1}

        elif self.name == "Wen Chou":
            self.learnable_attacks = {"rally":1, "piercing_blade":1}
            self.learnable_skills = {"renowned_commander":1, "spearman_commander":1}

        elif self.name == "Gao Lan":
            self.learnable_attacks = {"rapid_advance":1,}
            self.learnable_skills = {"brilliant_commander":1}

        elif self.name == "Guo Tu":
            self.learnable_attacks = {"rapid_advance":1, "relentless_charge":4}
            self.learnable_skills = {"military_advisor":1, "quick_march":3}

        elif self.name == "Ju Shou":
            self.learnable_attacks = {"caution":1}
            self.learnable_skills = {"military_advisor":1, "intellectual":1, "basic_formation":1, "diplomat":3, "logistician":4}

        elif self.name == "Shen Pei":
            self.learnable_attacks = {"rally":1, "fortification":4}
            self.learnable_skills = {"military_advisor":1, "advanced_formation":1, "unwavering_grit":4}

        elif self.name == "Tian Feng":
            self.learnable_attacks = {}
            self.learnable_skills = {"military_advisor":1, "straightforward":1, "commissioner_of_development":4}


        # Ma Teng
        elif self.name == "Ma Chao":
            self.learnable_attacks = {"rally":1, "cavalry_charge":3}
            self.learnable_skills = {"cavalry_commander":1, "spearman_commander":1, "advanced_formation":3, "meteor_hammer":4, "renowned_commander":7}

        elif self.name == "Ma Dai":
            self.learnable_attacks = {"cavalry_charge":6}
            self.learnable_skills = {"messenger_pigeon":1, "basic_formation": 3, "cavalry_commander":5, "barbarian_subduer":5}

        elif self.name == "Pang De":
            self.learnable_attacks = {"second_wind":3, "last_stand":7}
            self.learnable_skills = {"cavalry_commander":1, "swordsman_commander":1, "landlubber":1, "advanced_formation":5}

        elif self.name == "Chenggong Ying":
            self.learnable_attacks = {"expert_marksmanship":1}
            self.learnable_skills = {"basic_formation":3, "cavalry_commander": 5}

        elif self.name == "Cheng Yi":
            self.learnable_attacks = {}
            self.learnable_skills = {"cavalry_commander": 5}

        elif self.name == "Yang Qiu":
            self.learnable_attacks = {}
            self.learnable_skills = {"cavalry_commander": 5}

        # Liu Biao
        elif self.name == "Gan Ning":
            self.learnable_attacks = {"expert_marksmanship": 1}
            self.learnable_skills = {"naval_commander": 1, "hawk_eye": 4, "penetrating_shot":6, "daredevil_commander":7, "legendary_archer":8}

        elif self.name == "Huo Jun":
            self.learnable_attacks = {}
            self.learnable_skills = {"militia_commander":1, "basic_formation":3, "valiant_defender":6}

        elif self.name == "Huang Zu":
            self.learnable_attacks = {"rally":1}
            self.learnable_skills = {"ambush_tactics":1}

        elif self.name == "Cai Mao":
            self.learnable_attacks = {}
            self.learnable_skills = {"naval_commander":1}

        elif self.name == "Zhang Yun":
            self.learnable_attacks = {}
            self.learnable_skills = {"naval_commander":1}

        elif self.name == "Huang Zhong":
            self.learnable_attacks = {"expert_marksmanship":1, "deadly_blade":8}
            self.learnable_skills = {"hawk_eye":3, "penetrating_shot":5, "legendary_archer":6}

        elif self.name == "Wen Pin":
            self.learnable_attacks = {"fortification":2, "barricade":5}
            self.learnable_skills = {"defensive_formation":1, "basic_formation":1}

        elif self.name == "Wei Yan":
            self.learnable_attacks = {"bloodthirst":5}
            self.learnable_skills = {"basic_formation":1, "vanity":3, "advanced_formation":6}

        elif self.name == "Kuai Liang":
            self.learnable_attacks = {}
            self.learnable_skills = {"diplomat":1, "military_advisor":1, "commissioner_of_foreign_affairs":1, "ambush_tactics":3}

        elif self.name == "Kuai Yue":
            self.learnable_attacks = {}
            self.learnable_skills = {"diplomat":1, "military_advisor":2, "militia_commander":3, "commissioner_of_internal_affairs":4}

        else:
            self.learnable_attacks = {}
            self.learnable_skills = {}

        self.update_sp_atks_and_skills()
        self.update_state()


    def update_sp_atks_and_skills(self):
        self.sp_atks = []
        self.skills = []
        for sp_atk, unlock_level in self.learnable_attacks.items():
            if unlock_level <= self.level:
                self.sp_atks.append(sp_atk)
        for skill, unlock_level in self.learnable_skills.items():
            if unlock_level <= self.level:
                self.skills.append(skill)

    def auto_use_upgrade_points(self):
        while self.upgrade_points > 0:
            if self.upgrade_points >= 3:
                r = randrange(4)
            else:
                r= randrange(3)

            if r == 0:
                self.combat_prowess += 1
                self.upgrade_points -= 1
            elif r == 1:
                self.intellect += 1
                self.upgrade_points -= 1
            if r == 2:
                self.command += 1
                self.upgrade_points -= 1
            if r == 3:
                self.action_points += 1
                self.upgrade_points -= 3


    def gain_exp(self, amount):
        if self.level >= 10:
            return
        gained_amount = 0
        leveled_up = False
        while gained_amount < amount and self.level < 10:
            self.experience += 1
            gained_amount += 1
            if self.experience == self.level_thresholds[self.level]:
                self.level += 1
                self.intellect_multiplier = 1 + (self.intellect-50)/1000
                if "self_cultivation" in self.skills:
                    self.upgrade_points += int(self.intellect_multiplier*1.4*3*self.level**.75)
                else:
                    self.upgrade_points += int(self.intellect_multiplier*3*self.level**.75)
                self.max_army_size = int(1000 + self.level ** 2 * 50)
                self.update_sp_atks_and_skills()
                leveled_up = True
        return leveled_up

    def update_state(self, embedded=""):
        self.age_in_months += 6
        self.age = self.age_in_months//12
        if embedded:
            self.idle_turns = 0
            if "intellectual" in self.skills:
                self.gain_exp(100)
            if "scholar" in self.skills:
                self.gain_exp(200)
            self.gain_exp(10*self.level)
        else:
            self.idle_turns += 1

        if self.idle_turns >= 10:
            self.loyalty -= 5
            print(f"{self.name}'s loyalty decreased to {self.loyalty} due to being idle for {self.idle_turns}turns")
        elif self.idle_turns >= 5:
            self.loyalty -= 3
            print(f"{self.name}'s loyalty decreased to {self.loyalty} due to being idle for {self.idle_turns}turns")


class Army:
    def __init__(self, grid_position_x, grid_position_y, commander, army_composition,
                 action_points, retreat_penalty, embedded_in_city=None, attack_target=None, defend_target=None,
                 anchor="midleft"):
        self.grid_position_x = grid_position_x
        self.grid_position_y = grid_position_y
        self.commander = commander
        self.army_composition = army_composition
        self.max_action_points = self.commander.action_points
        self.action_points = action_points
        if self.action_points == -1:
            self.action_points = self.max_action_points
        self.retreat_penalty = retreat_penalty
        self.embedded_in_city = embedded_in_city
        self.anchor = anchor
        self.attack_target = attack_target
        self.defend_target = defend_target



class City:
    def __init__(self, grid_position_x, grid_position_y, name, fortress_level, country,
                 population, population_growth, happiness, defender=None, anchor="topleft", raw_buildings={},
                 buildings = [], garrison={}, units_recruited_this_turn=0, effects={}):
        self.grid_position_x = grid_position_x
        self.grid_position_y = grid_position_y
        self.name = name
        self.fortress_level = fortress_level
        self.country = country
        self.population = population
        self.population_growth = population_growth
        self.happiness = happiness
        self.defender = defender
        self.anchor = anchor

        self.garrison = garrison.copy()
        self.units_recruited_this_turn = units_recruited_this_turn
        self.raw_buildings = raw_buildings
        self.buildings = buildings
        self.effects = effects.copy()
        if not self.buildings:
            self.generate_buildings()

    def generate_buildings(self):
        self.buildings = []
        for building, level in self.raw_buildings.items():
            self.buildings.append(Building(building, level))
        for building, level in SPECIAL_RESOURCE_BUILDINGS[self.name].items():
            self.buildings.append(Building(building, level))



class Building:
    def __init__(self, name, level):
        self.name = name
        self.level = level
        self.upgrade_progress = -1
        self.description = ""
        self.max_level = 1

        if self.name == "Farm":
            self.upgrade_cost = {1:300, 2:800, 3:1500, 4:2500}
            self.upgrade_turns = {1:1, 2:3, 3:5, 4:8}
            self.max_level = 5
        elif self.name == "Market":
            self.upgrade_cost = {1:500, 2:1500, 3:3000, 4:6000}
            self.upgrade_turns = {1:2, 2:4, 3:6, 4:8}
            self.max_level = 5
        elif self.name == "Barracks":
            self.upgrade_cost = {1: 1600, 2: 4000, 3: 7000}
            self.upgrade_turns = {1:3, 2:6, 3:9}
            self.max_level = 4
        elif self.name == "Walls":
            self.upgrade_cost = {1: 2000, 2: 7000, 3: 15000}
            self.upgrade_turns = {1:3, 2:6, 3:10}
            self.max_level = 4
        elif self.name == "Academy":
            self.upgrade_cost = {1: 2000, 2: 5000}
            self.upgrade_turns = {1: 3, 2: 8}
            self.max_level = 3
        elif self.name == "Forest":
            self.upgrade_cost = {1: 400, 2: 600}
            self.upgrade_turns = {1: 1, 2: 2}
            self.max_level = 3
        elif self.name == "Quarry":
            self.upgrade_cost = {1: 550, 2: 800}
            self.upgrade_turns = {1: 2, 2: 5}
            self.max_level = 3
        elif self.name == "Iron Mine":
            self.upgrade_cost = {1: 600, 2: 1000}
            self.upgrade_turns = {1: 3, 2: 6}
            self.max_level = 3
        elif self.name == "Gold Mine":
            self.upgrade_cost = {1: 2000, 2: 7200}
            self.upgrade_turns = {1: 4, 2: 10}
            self.max_level = 3
        elif self.name == "Trade Port":
            self.upgrade_cost = {1: 1500, 2: 3200}
            self.upgrade_turns = {1: 2, 2: 5}
            self.max_level = 3
        else:
            self.upgrade_cost = {}
            self.upgrade_turns = {}
            self.max_level = 1


    def get_description(self):
        if self.level < self.max_level:
            if self.name == "Farm":
                self.description = "Provides food x {} each turn\nCost to upgrade: {}\nTurns to upgrade: {}\n".format(int(5*self.level**1.2), self.upgrade_cost[self.level], self.upgrade_turns[self.level])
            elif self.name == "Market":
                self.description = "+{} to income generated each turn\n+{} happiness to city population\nCost to upgrade: {}\nTurns to upgrade: {}\n".format(
                    int(self.level**1.5*100), self.level, self.upgrade_cost[self.level], self.upgrade_turns[self.level])
            elif self.name == "Barracks":
                if self.level == 1:
                    bonus_description = "Next upgrade enables recruitment\nof basic infantry units"
                elif self.level == 2:
                    bonus_description = "Next upgrade enables recruitment\nof a wider variety of infantry units"
                elif self.level == 3:
                    bonus_description = "Next upgrade enables recruitment of elite\ninfantry units and cavalry"
                else:
                    bonus_description = ""

                self.description = "Per-turn unit recruitment limit: {}\nMax garrison size: {}\nCost to upgrade: {}\nTurns to upgrade: {}\n\n{}".format(
                    int(self.level*50), int(self.level*500), self.upgrade_cost[self.level], self.upgrade_turns[self.level], bonus_description)
            elif self.name == "Walls":
                if self.level == 1:
                    bonus_description = "Wall Strength: 3000\nDefending units:\n        8 Wall Archers, 30 Spear Militia\n        30 Sword Militia"
                elif self.level == 2:
                    bonus_description = "Wall Strength: 6000\nDefending units:\n        16 Wall Archers, 30 Spear Infantry\n        30 Sword Infantry"
                elif self.level == 3:
                    bonus_description = "Wall Strength: 9000\nDefending units:\n        24 Wall Archers, 8 Rock Throwers\n        60 Spear Infantry, 60 Sword Infantry"
                elif self.level == 4:
                    bonus_description = "Wall Strength: 12000\nDefending units:\n        32 Wall Archers, 16 Rock Throwers\n        100 Spear Infantry, 100 Sword Infantry"
                elif self.level == 5:
                    bonus_description = "Wall Strength: 15000\nDefending units:\n        32 Wall Archers, 16 Rock Throwers\n        100 Spear Infantry, 100 Sword Infantry\n        100 Bow Infantry, 30 Light Cavalry"
                else:
                    bonus_description = ""

                self.description = "{}\nCost to upgrade: {}\nTurns to upgrade: {}\n".format(bonus_description, self.upgrade_cost[self.level], self.upgrade_turns[self.level])

            elif self.name == "Academy":
                self.description = "+{}% chance of attracting new talent\n+{}% bonus when learning new technology\n\nCost to upgrade: {}\nTurns to upgrade: {}".format(5*self.level, 10*self.level, self.upgrade_cost[self.level], self.upgrade_turns[self.level])
            elif self.name == "Forest":
                self.description = "-{}% recruitment cost for ranged units\nacross all provinces\n\nCost to upgrade: {}\nTurns to upgrade: {}".format(self.level, self.upgrade_cost[self.level], self.upgrade_turns[self.level])
            elif self.name == "Quarry":
                self.description = "+{}% wall strength across all provinces\n\nCost to upgrade: {}\nTurns to upgrade: {}".format(self.level, self.upgrade_cost[self.level], self.upgrade_turns[self.level])
            elif self.name == "Iron Mine":
                self.description = "-{}% recruitment cost for melee units\nacross all provinces\n\nCost to upgrade: {}\nTurns to upgrade: {}".format(self.level, self.upgrade_cost[self.level], self.upgrade_turns[self.level])
            elif self.name == "Gold Mine":
                self.description = "+{} gold revenue per turn\n\nCost to upgrade: {}\nTurns to upgrade: {}".format(200*self.level**2, self.upgrade_cost[self.level], self.upgrade_turns[self.level])
            elif self.name == "Trade Port":
                self.description = "+{}% to trade revenue\n+{} population growth per turn\n\nCost to upgrade: {}\nTurns to upgrade: {}\nCan recruit Light Dagger Infantry\n(requires lv 2 barracks)".format(self.level*3, 10*self.level**2, self.upgrade_cost[self.level], self.upgrade_turns[self.level])


        else:
            if self.name == "Farm":
                self.description = "Provides food x {} each turn\n\nCannot be upgraded further.".format(int(5*self.level**1.2))
            elif self.name == "Market":
                self.description = "+{} to income generated each turn\n+{} happiness to city population\n\nCannot be upgraded further.".format(
                    int(self.level**1.5 * 100), self.level)
            elif self.name == "Barracks":
                self.description = "Per-turn unit recruitment limit: {}\nMax garrison size: {}".format(int(self.level*50),int(self.level*500))
            elif self.name == "Walls":
                if self.level == 1:
                    bonus_description = "Wall Strength: 3000\nDefending units:\n        8 Wall Archers, 30 Spear Militia\n        30 Sword Militia"
                elif self.level == 2:
                    bonus_description = "Wall Strength: 6000\nDefending units:\n        16 Wall Archers, 30 Spear Infantry\n        30 Sword Infantry"
                elif self.level == 3:
                    bonus_description = "Wall Strength: 9000\nDefending units:\n        24 Wall Archers, 8 Rock Throwers\n        60 Spear Infantry, 60 Sword Infantry"
                elif self.level == 4:
                    bonus_description = "Wall Strength: 12000\nDefending units:\n        32 Wall Archers, 16 Rock Throwers\n        100 Spear Infantry, 100 Sword Infantry"
                elif self.level == 5:
                    bonus_description = "Wall Strength: 15000\nDefending units:\n        32 Wall Archers, 16 Rock Throwers\n        100 Spear Infantry, 100 Sword Infantry\n        100 Bow Infantry, 30 Light Cavalry"
                else:
                    bonus_description = ""

                self.description = "{}".format(bonus_description)

            elif self.name == "Academy":
                self.description = "+{}% chance of attracting new talent\n+{}% bonus when learning new technology\n\nCannot be upgraded further.".format(5*self.level, 10*self.level)
            elif self.name == "Forest":
                self.description = "-{}% recruitment cost for ranged units\nacross all provinces\n\nCannot be upgraded further.".format(self.level)
            elif self.name == "Quarry":
                self.description = "+{}% wall strength across all provinces\n\nCannot be upgraded further.".format(self.level)
            elif self.name == "Iron Mine":
                self.description = "-{}% recruitment cost for melee units\nacross all provinces\n\nCannot be upgraded further.".format(self.level)
            elif self.name == "Gold Mine":
                self.description = "+{} gold revenue per turn\n\nCannot be upgraded further.".format(200*self.level**2)
            elif self.name == "Trade Port":
                self.description = "+{}% to trade revenue\n+{} population growth per turn\n\nCannot be upgraded further.".format(self.level*3, 10*self.level**2)


        return self.name + " (Level {})\n------------------------------\n".format(self.level) + self.description


    def update(self):
        if self.upgrade_progress > 1:
            self.upgrade_progress -= 1
        elif self.upgrade_progress == 1:
            self.upgrade_progress = -1
            self.level += 1
            return True



SPECIAL_RESOURCE_BUILDINGS = {"Ping Yuan": {"Forest":2}, #Yuan Shao Start
                              "Yanmen Pass": {"Quarry":1},
                              "Jin Yang": {"Iron Mine":2, "Academy":2},
                              "Yu She": {"Forest":1, "Quarry":1, "Iron Mine":2},
                              "Cang Zhou": {"Forest":2, "Trade Port":2},
                              "Bei Ping": {"Gold Mine":1, "Quarry":2, "Iron Mine":1, "Trade Port":3},
                              "Shang Dang": {"Quarry": 1, "Trade Port":2},
                              "Tong Pass": {"Quarry": 2},

                              #Wei Start
                              "An Ding": {"Quarry": 2, "Iron Mine": 2},
                              "Jie Ting": {},
                              "Tian Shui": {"Quarry":1, "Forest":1},
                              "Chang An": {"Academy":3, "Forest":3, "Trade Port":3},
                              "Hulao Pass": {},
                              "Hu Pass": {"Quarry": 1, "Iron Mine": 1},
                              "Xu Chang": {"Forest":3, "Academy":3, "Trade Port":3},
                              "Xu Zhou": {"Forest":1, "Quarry":1, "Academy":2, "Trade Port":3},
                              "Ru Nan": {"Forest":1, "Trade Port":2},
                              "Pu Yang": {"Forest":1, "Trade Port":1},
                              "Bei Hai": {"Trade Port":3},
                              "Chen Liu": {"Forest":2},
                              "Luo Yang": {"Quarry":1, "Iron Mine":3, "Academy":1},
                              "Xin Ye": {"Forest":3},
                              "Wan Cheng": {"Forest": 2, "Trade Port": 1},
                              "Chen Cang": {"Quarry":3, "Forest":1},

                              #Wu Start
                              "Jian Ye": {"Gold Mine":1, "Academy":3, "Trade Port":3},
                              "Lu Jiang": {"Trade Port":1},
                              "Shou Chun": {"Forest":2, "Trade Port":3},
                              "Jiang Xia": {"Quarry":1, "Forest":1, "Trade Port":2},
                              "Chai Sang": {"Forest":3},
                              "Yu Zhang": {"Quarry":1, "Trade Port":1},
                              "Kuai Ji": {"Forest":2},


                              #Liu Zhang Start
                              "Cheng Du": {"Quarry":3, "Iron Mine":3, "Academy":3, "Trade Port":3},
                              "Jian Ning": {"Quarry":2, "Iron Mine":1},
                              "Zi Tong": {"Forest":2},
                              "Long Nan": {},
                              "He Pu": {"Quarry":1},
                              "Hu Nan": {"Forest":1, "Trade Port":2},

                              #Zhang Lu Start
                              "Han Zhong": {"Quarry": 2, "Iron Mine": 2, "Forest": 1, "Academy": 3},
                              "Jin Cheng": {"Quarry": 2},
                              "Yangping Pass": {},
                              "Qi Shan": {"Quarry": 2, "Iron Mine": 2},


                              #Ma Teng start
                              "Liang Zhou": {"Gold Mine":1, "Iron Mine":2, "Academy":2},

                              # Liu Biao start
                              "Shang Yong": {"Gold Mine":1, "Forest":2, "Trade Port":2},
                              "Xiang Yang": {"Academy":3, "Trade Port":2},
                              "Jiang Zhou": {"Forest":2, "Trade Port":1},
                              "Chang Sha": {"Academy":2, "Trade Port":2},
                              "Wu Ling": {"Forest":2},
                              "Ling Ling": {"Forest":2},
                              "Gui Lin": {"Quarry":1, "Trade Port":2},
                              "Fan Cheng": {"Forest": 2, "Quarry": 2, "Iron Mine":1},
                              }