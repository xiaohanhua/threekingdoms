from game_classes import*
from sprites import*
from functools import partial
import pickle

class Diplomacy_Scene:
    def __init__(self, campaign_map_scene):
        self.campaign_map_scene = campaign_map_scene
        self.game_width, self.game_height = LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT
        if DEMO:
            self.screen = pg.display.set_mode((self.game_width, self.game_height))
        else:
            self.screen = pg.display.set_mode((self.game_width, self.game_height), pg.FULLSCREEN|pg.DOUBLEBUF)
        os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (50, 50)
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()

        pg.mixer.init()
        pg.font.init()
        self.hover_sound = pg.mixer.Sound("sfx_hover.ogg")
        self.click_sound = pg.mixer.Sound("sfx_click.ogg")
        self.morale_boost_sound = pg.mixer.Sound("sfx_restore_status.ogg")


    def new(self):
        self.running = True
        self.playing = True
        pg.mouse.set_visible(True)

        # transitions
        self.transition_sprites = pg.sprite.Group()
        self.transition = None
        self.post_transition_function = None
        self.initiate_transition("in")

        self.tooltip_description = ""
        self.tooltip_description_x, self.tooltip_description_y = -3000, -3000

        self.campaign_map_scene.set_diplomacy_variables()
        self.diplomacy_countries = [self.campaign_map_scene.player_country]
        for country in self.campaign_map_scene.available_countries:
            if country not in self.diplomacy_countries:
                self.diplomacy_countries.append(country)
        self.player_owned_cities = 0
        for city_sprite in self.campaign_map_scene.city_sprites:
            if city_sprite.country == self.campaign_map_scene.player_country:
                self.player_owned_cities += 1
        self.starting_index = 0

        self.all_sprites = pg.sprite.Group()
        self.icons = pg.sprite.Group()
        self.priority_sprites = pg.sprite.Group()
        self.diplomacy_sprites = pg.sprite.Group()
        self.decision_sprites = pg.sprite.Group()
        self.country_buttons = pg.sprite.Group()
        self.diplomacy_option_buttons = []

        self.background = Static(0, self.game_height - 650, "mountain_lake_bg.png")
        self.all_sprites.add(self.background)

        self.gold_icon = CampaignMapSprite(self, 0, 0, "campaign_icon_gold.png", anchor="topleft")
        self.all_sprites.add(self.gold_icon)
        self.icons.add(self.gold_icon)

        self.food_icon = CampaignMapSprite(self, 7, 0, "campaign_icon_food.png", anchor="topleft")
        self.all_sprites.add(self.food_icon)
        self.icons.add(self.food_icon)

        self.back_button = Static(LANDSCAPE_WIDTH-80, 20, "campaign_button_back.png", anchor="topleft")
        self.all_sprites.add(self.back_button)

        self.close_diplomacy_button = Static(-3000, -3000, "sprite_close_button_large.png", anchor="topleft")
        self.all_sprites.add(self.close_diplomacy_button)


        self.game_message = ""
        self.last_displayed_message = 0

        self.decision_text = ""
        self.game_tooltip_description = ""
        self.enemy_demand_description = ""
        self.enemy_gold_demand = 0
        self.enemy_food_demand = 0
        self.viewing_decision_interface = False
        self.viewing_diplomacy_interface = False

        self.selected_country = None
        self.option_status = {"declare_war":0, "request_peace":0, "request_trade":0, "form_alliance":0,
                              "alternative_payment":0, "cancel_trade":0}

        self.load_country_buttons()
        self.run()


    def load_country_buttons(self):
        counter = self.starting_index
        for country in self.diplomacy_countries[self.starting_index::]:
            country_button = Button(75+counter//4*550, counter%4*120+100, "diplomacy_country_buttons_{}.png".format(country.lower()),
                                         command=partial(self.enter_diplomacy, country), anchor="topleft",
                                    tooltip="DIPLOMACY_{}".format(country), master=self)
            self.all_sprites.add(country_button)
            self.country_buttons.add(country_button)
            counter += 1


    def enter_diplomacy(self, target_country):
        if target_country == self.campaign_map_scene.player_country:
            return

        self.selected_country = target_country
        self.viewing_diplomacy_interface = True
        self.back_button.rect.topleft = -3000, -3000
        for sp in self.country_buttons:
            if sp.tooltip_bg:
                sp.tooltip_bg.kill()
            sp.kill()


        self.diplomacy_bg = Static(LANDSCAPE_WIDTH // 2, LANDSCAPE_HEIGHT // 2 + 30, "sprite_battle_preview_bg.png",
                                     anchor="center")
        self.all_sprites.add(self.diplomacy_bg)
        self.diplomacy_sprites.add(self.diplomacy_bg)
        self.close_diplomacy_button.rect.topleft = self.diplomacy_bg.rect.right, self.diplomacy_bg.rect.top,

        country_banner = Static(self.diplomacy_bg.rect.centerx, self.diplomacy_bg.rect.top,
                                "diplomacy_country_buttons_{}.png".format(target_country.lower()), anchor="midtop")
        self.all_sprites.add(country_banner)
        self.diplomacy_sprites.add(country_banner)


        #diplomacy option check buttons/buttons
        counter = 0
        for option in ["declare_war", "request_peace", "request_trade", "form_alliance"]:
            if option == "request_trade" and self.selected_country in self.campaign_map_scene.trade_relationships[self.campaign_map_scene.player_country]:
                option = "cancel_trade"
            option_button = Button(self.diplomacy_bg.rect.left + counter%2*300+200, self.diplomacy_bg.rect.top + counter//2*100+150,
                                   "campaign_button_diplomacy_{}.png".format(option),
                                   command=partial(self.toggle_option, option), anchor="topleft",
                                   tooltip=DIPLOMACY_OPTION_DESCRIPTION[option], master=self)
            self.all_sprites.add(option_button)
            self.diplomacy_sprites.add(option_button)
            self.diplomacy_option_buttons.append(option_button)
            counter += 1

        alternative_payment_button = Button(self.diplomacy_bg.rect.left + 200, self.diplomacy_bg.rect.top + 350,
                                   "sprite_check_button.png", command=partial(self.toggle_option, "alternative_payment"), anchor="topleft",
                                   tooltip=DIPLOMACY_OPTION_DESCRIPTION["alternative_payment"], master=self)
        self.all_sprites.add(alternative_payment_button)
        self.diplomacy_sprites.add(alternative_payment_button)
        self.diplomacy_option_buttons.append(alternative_payment_button)


        self.make_proposal_button = Button(LANDSCAPE_WIDTH//2, self.diplomacy_bg.rect.top + 500,
                                        "campaign_button_diplomacy_make_proposal.png", command=self.make_proposal,
                                        anchor="center", master=self)
        self.all_sprites.add(self.make_proposal_button)
        self.diplomacy_sprites.add(self.make_proposal_button)
        self.diplomacy_option_buttons.append(self.make_proposal_button)

        self.update_diplomacy_option_buttons()


    def toggle_option(self, option):
        print(self.option_status)
        self.option_status[option] = (self.option_status[option]+1)%2
        if option == "cancel_trade":
            index = list(self.option_status.keys()).index("request_trade")
        else:
            index = list(self.option_status.keys()).index(option)

        self.diplomacy_option_buttons[index].pressed = not self.diplomacy_option_buttons[index].pressed

        if option in ["declare_war"] and self.option_status[option] == 1:
            self.option_status["request_trade"] = -1
            self.option_status["form_alliance"] = -1
            self.option_status["request_peace"] = -1
            self.option_status["cancel_trade"] = -1
        elif option in ["declare_war"] and self.option_status[option] == 0:
            if self.option_status["request_trade"] == -1:
                self.option_status["request_trade"] = 0
            if self.option_status["cancel_trade"] == -1:
                self.option_status["cancel_trade"] = 0
            if self.option_status["form_alliance"] == -1:
                self.option_status["form_alliance"] = 0
            if self.option_status["request_peace"] == -1:
                self.option_status["request_peace"] = 0
        elif option not in ["declare_war", "alternative_payment"] and self.option_status[option] == 1:
            self.option_status["declare_war"] = -1
        elif self.option_status["request_trade"] < 1 and self.option_status["form_alliance"] < 1 and self.option_status["request_peace"] < 1 and self.option_status["cancel_trade"] < 1:
            if self.option_status["declare_war"] < 0:
                self.option_status["declare_war"] = 0

        self.update_diplomacy_option_buttons()


    def update_diplomacy_option_buttons(self):
        if self.selected_country in self.campaign_map_scene.hostile_relationships[self.campaign_map_scene.player_country]:
            self.option_status["declare_war"] = -1
            if self.option_status["request_peace"] != 1:
                self.option_status["request_trade"] = -1
                self.option_status["form_alliance"] = -1
            elif self.option_status["request_peace"] == 1:
                if self.option_status["request_trade"] < 0:
                    self.option_status["request_trade"] = 0
                if self.option_status["form_alliance"] < 0:
                    self.option_status["form_alliance"] = 0

        if self.selected_country in self.campaign_map_scene.alliance_relationships[self.campaign_map_scene.player_country]:
            self.option_status["form_alliance"] = -1
            self.option_status["request_peace"] = -1

        #do not allow alliance w/ enemy of allies
        for selected_country_enemy in self.campaign_map_scene.hostile_relationships[self.selected_country]:
            if selected_country_enemy in self.campaign_map_scene.alliance_relationships[self.campaign_map_scene.player_country]:
                self.option_status["form_alliance"] = -1

        if self.selected_country in self.campaign_map_scene.trade_relationships[self.campaign_map_scene.player_country]:
            self.option_status["request_trade"] = -1
            if self.option_status["cancel_trade"] == -1 and self.option_status["declare_war"] != 1:
                self.option_status["cancel_trade"] = 0

        if len(self.campaign_map_scene.trade_relationships[self.campaign_map_scene.player_country]) >= self.player_owned_cities//3:
            self.option_status["request_trade"] = -1

        if self.campaign_map_scene.peace_relationships[self.selected_country] > 0:
            self.option_status["request_peace"] = -1

        for option, value in self.option_status.items():
            if option == "cancel_trade" and self.selected_country in self.campaign_map_scene.trade_relationships[self.campaign_map_scene.player_country]:
                index = list(self.option_status.keys()).index("request_trade")
            else:
                index = list(self.option_status.keys()).index(option)
            if not self.diplomacy_option_buttons[index].disabled and value == -1:
                self.diplomacy_option_buttons[index].disabled = True
                print(option, value)
            elif self.diplomacy_option_buttons[index].disabled and value != -1:
                self.diplomacy_option_buttons[index].disabled = False

        at_least_one_option_selected = False
        for option in ["declare_war", "request_peace", "request_trade", "form_alliance", "cancel_trade"]:
            if self.option_status[option] >= 1:
                at_least_one_option_selected = True
                break

        if at_least_one_option_selected:
            self.make_proposal_button.disabled = False
        else:
            self.make_proposal_button.disabled = True

        self.calculate_enemy_demand()


    def calculate_enemy_demand(self):
        self.enemy_gold_demand = 0
        self.enemy_food_demand = 0
        relationship = self.campaign_map_scene.calculate_diplomatic_relationship(self.campaign_map_scene.player_country, self.selected_country)["Total"]
        if self.option_status["request_peace"] == 1:
            player_military_strength = self.campaign_map_scene.calculate_military_strength(self.campaign_map_scene.player_country)
            enemy_military_strength = self.campaign_map_scene.calculate_military_strength(self.selected_country)
            demand = (enemy_military_strength - player_military_strength)//10

            if self.selected_country in self.campaign_map_scene.hostile_relationships[self.campaign_map_scene.player_country]:
                demand *= 2 #double demand if countries at war

            #commander effects to reduce demand
            for army_sprite in self.campaign_map_scene.army_sprites:
                if "peacemaker" in army_sprite.commander.skills:
                    demand *= .5

            if demand > 0:
                demand = demand*(1-relationship/300)
            else:
                if self.campaign_map_scene.realm_divide:
                    demand = randrange(20000, 25001)
                elif relationship <= -200:
                    demand = 2000
                else:
                    demand = 1000

            if self.option_status["alternative_payment"] == 1:
                gold_component, food_component = self.balance_gold_food(demand)
                self.enemy_gold_demand += gold_component
                self.enemy_food_demand += food_component
            else:
                self.enemy_gold_demand += demand

        if self.option_status["form_alliance"] == 1:
            player_military_strength = self.campaign_map_scene.calculate_military_strength(self.campaign_map_scene.player_country)
            enemy_military_strength = self.campaign_map_scene.calculate_military_strength(self.selected_country)
            demand = (enemy_military_strength - player_military_strength)//(3+self.campaign_map_scene.difficulty*2)
            if relationship > 300:
                relationship = 300
            if relationship < -300:
                relationship = -300
            if demand > 0:
                demand = demand*(1-relationship/1000)
            else:
                if self.campaign_map_scene.realm_divide or self.selected_country == "Southern Barbarians":
                    demand = randrange(20000, 25001)
                else:
                    demand = demand/(1-relationship/1000)

            if self.option_status["alternative_payment"] == 1:
                gold_component, food_component = self.balance_gold_food(demand)
                self.enemy_gold_demand += gold_component
                self.enemy_food_demand += food_component
            else:
                self.enemy_gold_demand += demand

        if self.option_status["request_trade"] == 1:
            player_trade_revenue = self.campaign_map_scene.calculate_trade_revenue(self.campaign_map_scene.player_country, self.selected_country)[0]
            demand = player_trade_revenue*5*(max([1-relationship/100, 0.1]))
            if self.option_status["alternative_payment"] == 1:
                gold_component, food_component = self.balance_gold_food(demand)
                self.enemy_gold_demand += gold_component
                self.enemy_food_demand += food_component
            else:
                self.enemy_gold_demand += demand


        #make sure opponent can't offer more than he has
        if self.enemy_gold_demand < 0:
            if -self.enemy_gold_demand > self.campaign_map_scene.loaded_game_data["gold"][self.selected_country]:
                self.enemy_gold_demand = -self.campaign_map_scene.loaded_game_data["gold"][self.selected_country]
            if -self.enemy_food_demand > self.campaign_map_scene.loaded_game_data["food"][self.selected_country]:
                self.enemy_food_demand = -self.campaign_map_scene.loaded_game_data["food"][self.selected_country]

        self.enemy_gold_demand = int(self.enemy_gold_demand)
        self.enemy_food_demand = int(self.enemy_food_demand)


    def balance_gold_food(self, demand):
        gold_ratio = randrange(60,81)/100
        food_ratio = 1 - gold_ratio
        print(demand)
        return (int(gold_ratio*demand), food_ratio*demand//10)


    def make_proposal(self):
        self.viewing_decision_interface = True
        self.decision_bg = Static(LANDSCAPE_WIDTH // 2, LANDSCAPE_HEIGHT // 2, "decision_interface_bg_small.png",
                                  anchor="center")
        self.all_sprites.add(self.decision_bg)
        self.decision_sprites.add(self.decision_bg)

        self.yes_button = Button(self.decision_bg.rect.left + 200, self.decision_bg.rect.bottom - 100,
                                 "menu_button_yes.png",
                                 command=partial(self.confirm_proposal, True))
        self.all_sprites.add(self.yes_button)
        self.decision_sprites.add(self.yes_button)

        if self.campaign_map_scene.loaded_game_data["gold"][self.campaign_map_scene.player_country] < self.enemy_gold_demand:
            self.yes_button.disabled = True
        if self.campaign_map_scene.loaded_game_data["food"][self.campaign_map_scene.player_country] < self.enemy_food_demand:
            self.yes_button.disabled = True

        self.cancel_button = Button(self.decision_bg.rect.right - 200, self.decision_bg.rect.bottom - 100,
                                    "menu_button_cancel.png", command=partial(self.confirm_proposal, False))
        self.all_sprites.add(self.cancel_button)
        self.decision_sprites.add(self.cancel_button)

        if self.option_status["declare_war"] == 1:
            if self.campaign_map_scene.peace_relationships[self.selected_country] > 0 or self.selected_country in self.campaign_map_scene.alliance_relationships[self.campaign_map_scene.player_country]:
                self.decision_text = "Are you sure you wish to declare war on {}?\nDoing so will result in a diplomatic penalty due\nto outstanding treaties.".format(self.selected_country)
            else:
                self.decision_text = "Are you sure you wish to declare war on {}?".format(self.selected_country)

        else:
            self.decision_text = "Enter into the following agreements w/ {}:\n".format(self.selected_country)
            agreements = []
            if self.option_status["request_trade"] == 1:
                agreements.append("Trade")
            if self.option_status["request_peace"] == 1:
                agreements.append("Peace")
            if self.option_status["form_alliance"] == 1:
                agreements.append("Alliance")
            if self.option_status["cancel_trade"] == 1:
                agreements.append("Cancel Trade")
            self.decision_text += ", ".join(agreements)

            if self.enemy_gold_demand > 0 or self.enemy_food_demand > 0:
                self.decision_text += "\nYou will pay {} gold and {} food.".format(self.enemy_gold_demand, self.enemy_food_demand)
            elif self.enemy_gold_demand < 0 or self.enemy_food_demand < 0:
                self.decision_text += "\n{} will pay you {} gold and {} food.".format(self.selected_country, abs(self.enemy_gold_demand), abs(self.enemy_food_demand))


    def confirm_proposal(self, confirm):
        if confirm:
            if self.option_status["declare_war"] == 1:
                if self.campaign_map_scene.peace_relationships[self.selected_country] > 0 or self.selected_country in self.campaign_map_scene.alliance_relationships[self.campaign_map_scene.player_country]:
                    if self.campaign_map_scene.player_country == "Liu Zhang":
                        self.campaign_map_scene.diplomacy_penalty["All"] += 25
                    else:
                        self.campaign_map_scene.diplomacy_penalty["All"] += 50
                self.campaign_map_scene.declare_war(self.campaign_map_scene.player_country, self.selected_country)
                self.option_status["declare_war"] = -1
                self.option_status["request_peace"] = 0

            if self.option_status["request_peace"] == 1:
                self.campaign_map_scene.terminate_relationship("hostile", self.campaign_map_scene.player_country, self.selected_country)
                self.campaign_map_scene.peace_relationships[self.selected_country] = 10
                self.option_status["request_peace"] = -1
                if self.option_status["declare_war"] == -1:
                    self.option_status["declare_war"] = 0

            if self.option_status["form_alliance"] == 1:
                self.campaign_map_scene.begin_relationship("alliance", self.campaign_map_scene.player_country,
                                                               self.selected_country)
                self.option_status["request_peace"] = -1
                self.option_status["form_alliance"] = -1
                if self.option_status["declare_war"] == -1:
                    self.option_status["declare_war"] = 0

            self.campaign_map_scene.loaded_game_data["gold"][self.campaign_map_scene.player_country] -= self.enemy_gold_demand
            self.campaign_map_scene.loaded_game_data["gold"][self.selected_country] += self.enemy_gold_demand
            self.campaign_map_scene.loaded_game_data["food"][self.campaign_map_scene.player_country] -= self.enemy_food_demand
            self.campaign_map_scene.loaded_game_data["food"][self.selected_country] += self.enemy_food_demand

            self.update_diplomacy_option_buttons()

            if self.option_status["cancel_trade"] == 1:
                self.campaign_map_scene.terminate_relationship("trade", self.campaign_map_scene.player_country,
                                                           self.selected_country)
                if self.campaign_map_scene.player_country == "Liu Zhang":
                    self.campaign_map_scene.diplomacy_penalty["All"] += 10
                else:
                    self.campaign_map_scene.diplomacy_penalty["All"] += 20
                selected_country = self.selected_country
                self.exit_diplomacy()
                self.enter_diplomacy(selected_country)
                return


            if self.option_status["request_trade"] == 1:
                self.campaign_map_scene.begin_relationship("trade", self.campaign_map_scene.player_country,
                                                           self.selected_country)
                self.option_status["request_trade"] = -1
                if self.option_status["declare_war"] == -1:
                    self.option_status["declare_war"] = 0
                selected_country = self.selected_country
                self.exit_diplomacy()
                self.enter_diplomacy(selected_country)


        for sp in self.decision_sprites:
            sp.kill()
        self.viewing_decision_interface = False
        self.decision_text = ""


    def exit_diplomacy(self):
        for sp in self.diplomacy_sprites:
            sp.kill()
        for sp in self.decision_sprites:
            sp.kill()

        self.load_country_buttons()

        self.viewing_diplomacy_interface = False
        self.viewing_decision_interface = False
        self.decision_text = ""
        self.enemy_demand_description = ""
        self.enemy_gold_demand = 0
        self.enemy_food_demand = 0
        self.selected_country = None
        self.option_status = {"declare_war":0, "request_peace":0, "request_trade":0, "form_alliance":0,
                              "alternative_payment":0, "cancel_trade":0}
        self.diplomacy_option_buttons = []
        self.close_diplomacy_button.rect.topleft = -3000, -3000
        self.back_button.rect.topleft = LANDSCAPE_WIDTH - 80, 20


    def initiate_transition(self, direction):
        if direction == "in":
            self.transition = FadeBlackTransition(80, "in")
            self.transition_sprites.add(self.transition)
        else:
            self.transition = FadeBlackTransition(80, "out")
            self.transition_sprites.add(self.transition)


    def transition_clean_up(self):
        self.transition.kill()
        self.transition = None
        self.post_transition_function = None
        self.playing = False
        self.running = False


    def run(self):
        while self.playing:
            if self.running:
                self.clock.tick(FPS)
                self.events()
                self.update()
                self.draw()


    def events(self):
        pos = pg.mouse.get_pos()

        # gold icon or food icon hover
        if self.gold_icon.rect.collidepoint(pos):
            self.gold_icon.hovering = True
            if self.gold_icon.tooltip_background:
                self.game_tooltip_description = "Projected Tax/Building Income: {}\nProjected Trade Revenue: {}\nProjected Upkeep Cost: {}\nProjected Net Income: {}"
        else:
            self.gold_icon.hovering = False
            if self.food_icon.rect.collidepoint(pos):
                self.food_icon.hovering = True
                if self.food_icon.tooltip_background:
                    self.game_tooltip_description = "Projected Food Production: {}\nProjected Food Consumption: {}\nProjected Food Surplus: {}"
            else:
                self.food_icon.hovering = False
                self.game_tooltip_description = ""

        for event in pg.event.get():
            if event.type == pg.KEYUP:
                if event.key == pg.K_ESCAPE:
                    self.return_to_campaign_map()

            # quit
            if event.type == pg.QUIT:
                choice = pg.display.message_box("Quit",
                                                "Are you sure you wish to quit? You will lose any unsaved progress.",
                                                buttons=("Yes", "No"), return_button=1, escape_button=None
                                                )

                if choice == 0:
                    pg.quit()
                    sys.exit()

            # Left clicks
            if event.type == pg.MOUSEBUTTONUP:
                self.campaign_map_scene.play_sound_effect(self.click_sound)

                for sp in self.decision_sprites:
                    if type(sp) == Button:
                        if sp.rect.collidepoint(pos):
                            if sp.command:
                                if not sp.disabled:
                                    sp.command()
                            break
                for button in self.country_buttons:
                    if button.rect.collidepoint(pos) and not self.viewing_diplomacy_interface and not self.viewing_decision_interface:
                        if button.command:
                            if not button.disabled:
                                button.command()
                        break
                for button in self.diplomacy_option_buttons:
                    if button.rect.collidepoint(pos):
                        if button.command:
                            if not button.disabled:
                                button.command()
                        break
                if self.back_button.rect.collidepoint(pos):
                    self.return_to_campaign_map()
                if self.close_diplomacy_button.rect.collidepoint(pos):
                    self.exit_diplomacy()


        for sp in self.decision_sprites:
            if type(sp) == Button:
                if sp.rect.collidepoint(pos):
                    sp.hovering = True
                else:
                    sp.hovering = False


        hovering_over_button = False
        for button in self.country_buttons:
            if button.rect.collidepoint(pos):
                button.hovering = True
                hovering_over_button = True
                if button.tooltip_bg:
                    self.tooltip_description = button.tooltip
                    self.tooltip_description_x, self.tooltip_description_y = button.tooltip_bg.rect.left, button.tooltip_bg.rect.top
            else:
                button.hovering = False
                if button.tooltip_bg:
                    button.kill_tooltip()

        for button in self.diplomacy_option_buttons:
            if button.rect.collidepoint(pos):
                button.hovering = True
                hovering_over_button = True
                if button.tooltip_bg:
                    self.tooltip_description = button.tooltip
                    self.tooltip_description_x, self.tooltip_description_y = button.tooltip_bg.rect.left, button.tooltip_bg.rect.top
            else:
                button.hovering = False
                if button.tooltip_bg:
                    button.kill_tooltip()

        if not hovering_over_button:
            self.tooltip_description = ""
            self.tooltip_description_x = -3000
            self.tooltip_description_y = -3000


    def return_to_campaign_map(self):
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = self.return_to_campaign_map
        elif not self.transition.fading:
            self.transition_clean_up()
            campaign_data = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "diplomacy_autosave.p"), "rb"))
            campaign_data["gold"] = self.campaign_map_scene.gold
            campaign_data["food"] = self.campaign_map_scene.food
            campaign_data["hostile_relationships"] = self.campaign_map_scene.hostile_relationships
            campaign_data["peace_relationships"] = self.campaign_map_scene.peace_relationships
            campaign_data["trade_relationships"] = self.campaign_map_scene.trade_relationships
            campaign_data["alliance_relationships"] = self.campaign_map_scene.alliance_relationships
            campaign_data["diplomatic_relationships"] = self.campaign_map_scene.diplomatic_relationships
            campaign_data["past_conflicts"] = self.campaign_map_scene.past_conflicts
            campaign_data["diplomacy_penalty"] = self.campaign_map_scene.diplomacy_penalty
            pickle.dump(campaign_data, open(os.path.join(SAVE_SLOTS_DIR, "diplomacy_autosave.p"), "wb"))

            for sp in self.all_sprites:
                sp.kill()
            self.campaign_map_scene.new(load_from="diplomacy_autosave.p")


    def update(self):
        now = pg.time.get_ticks()
        self.all_sprites.update()
        if now - self.last_displayed_message > 2000:
            self.recruitment_message = ""
        if self.transition:
            if self.transition.fading:
                self.transition.update()
            else:
                if self.post_transition_function:
                    self.post_transition_function()
                else:
                    self.transition = None


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)

            estimated_gold_delta = self.campaign_map_scene.calculate_projected_gold_delta(self.campaign_map_scene.player_country)
            estimated_food_delta = self.campaign_map_scene.calculate_projected_food_delta(self.campaign_map_scene.player_country)
            gold_label = "{} ({})".format(self.campaign_map_scene.gold[self.campaign_map_scene.player_country], estimated_gold_delta)
            food_label = "{} ({})".format(self.campaign_map_scene.food[self.campaign_map_scene.player_country], estimated_food_delta)
            gold_label_color = WHITE
            food_label_color = WHITE
            if estimated_gold_delta < 0:
                gold_label_color = DARK_RED
            if estimated_food_delta < 0:
                food_label_color = DARK_RED

            self.draw_text(gold_label, FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE - len(gold_label) // 6), gold_label_color,
                           55, 28, align="midleft")
            self.draw_text(food_label, FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE - len(food_label) // 6), food_label_color,
                           270, 27, align="midleft")


            if not self.viewing_diplomacy_interface:
                for button in self.country_buttons:
                    target_country = button.tooltip.split("DIPLOMACY_")[-1]
                    cities_owned = len([city_sprite for city_sprite in self.campaign_map_scene.city_sprites if city_sprite.country == target_country])
                    text_lines = []
                    text_lines.append("Faction: {}        Population: {}".format(target_country, self.campaign_map_scene.calculate_population(target_country)))
                    text_lines.append("Military Strength: {}        Cities: {}".format(self.campaign_map_scene.calculate_military_strength(target_country, True), cities_owned))
                    text_lines.append("Economic Growth: {}".format(self.campaign_map_scene.calculate_tax_and_additional_income(target_country, True)))
                    if len(self.campaign_map_scene.alliance_relationships[target_country]) > 0:
                        allies_str = ", ".join(self.campaign_map_scene.alliance_relationships[target_country])
                    else:
                        allies_str = "None"

                    if len(self.campaign_map_scene.hostile_relationships[target_country]) > 0:
                        enemies_str = ", ".join(self.campaign_map_scene.hostile_relationships[target_country])
                    else:
                        enemies_str = "None"

                    if len(allies_str) >= 30:
                        allies_str = allies_str[:30] + "..."
                    if len(enemies_str) >= 30:
                        enemies_str = enemies_str[:30] + "..."

                    text_lines.append("Allies: {}".format(allies_str))
                    text_lines.append("Enemies: {}".format(enemies_str))

                    counter = 0
                    for line in text_lines:
                        self.draw_text(line, FONT_FUNCTION(DEFAULT_FONT, 14), BLACK,
                                       button.rect.left + 125, button.rect.top+25+16*counter, align="midleft")
                        counter += 1

            else:
                text_lines = []
                text_lines.append("Faction: {}        Population: {}".format(self.selected_country, self.campaign_map_scene.calculate_population(self.selected_country)))
                text_lines.append("Military Strength: {}".format(
                    self.campaign_map_scene.calculate_military_strength(self.selected_country, True)))
                text_lines.append("Economic Growth: {}".format(
                    self.campaign_map_scene.calculate_tax_and_additional_income(self.selected_country, True)))
                if len(self.campaign_map_scene.alliance_relationships[self.selected_country]) > 0:
                    allies_str = ", ".join(self.campaign_map_scene.alliance_relationships[self.selected_country])
                else:
                    allies_str = "None"

                if len(self.campaign_map_scene.hostile_relationships[self.selected_country]) > 0:
                    enemies_str = ", ".join(self.campaign_map_scene.hostile_relationships[self.selected_country])
                else:
                    enemies_str = "None"

                if len(allies_str) >= 30:
                    allies_str = allies_str[:30] + "..."
                if len(enemies_str) >= 30:
                    enemies_str = enemies_str[:30] + "..."

                text_lines.append("Allies: {}".format(allies_str))
                text_lines.append("Enemies: {}".format(enemies_str))

                counter = 0
                for line in text_lines:
                    self.draw_text(line, FONT_FUNCTION(DEFAULT_FONT, 14), BLACK,
                                   self.diplomacy_bg.rect.left+375, self.diplomacy_bg.rect.top + 25 + 16 * counter, align="midleft")
                    counter += 1


            if self.viewing_diplomacy_interface:
                self.draw_text("Accept/offer food as alternative form of payment", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                               WHITE, self.diplomacy_bg.rect.left + 250, self.diplomacy_bg.rect.top + 355,
                               align="topleft")

                if self.enemy_gold_demand > 0 or self.enemy_food_demand > 0:
                    self.draw_text("{} demands {} gold and {} food for this proposal.".format(self.selected_country, abs(self.enemy_gold_demand), abs(self.enemy_food_demand)),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), RED, LANDSCAPE_WIDTH//2,
                                   self.diplomacy_bg.rect.top + 425 , align="center")
                elif self.enemy_gold_demand < 0 or self.enemy_food_demand < 0 :
                    self.draw_text("{} is willing to pay {} gold and {} food for this proposal.".format(self.selected_country, abs(self.enemy_gold_demand), abs(self.enemy_food_demand)),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), GREEN, LANDSCAPE_WIDTH//2,
                                   self.diplomacy_bg.rect.top + 425 , align="center")



            if self.tooltip_description:
                self.priority_sprites.draw(self.screen)
                if "DIPLOMACY" in self.tooltip_description:
                    target_country = self.tooltip_description.split("DIPLOMACY_")[-1]

                    if target_country == self.campaign_map_scene.player_country:
                        faction_bonus_text = COUNTRY_DESCRIPTIONS[self.campaign_map_scene.player_country].split("Faction Bonus:\n")[1].split("\n\nAllies")[0]
                        faction_bonus_text_split = faction_bonus_text.split("\n")
                        self.draw_text("Faction Bonus:", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), YELLOW,
                                       self.tooltip_description_x + 20, self.tooltip_description_y + 30,
                                       align="midleft")
                        j = 1
                        for bonus_str in faction_bonus_text_split:
                            self.drawWrappedText("*" + bonus_str, GREEN,
                                                 pg.Rect(self.tooltip_description_x + 20, self.tooltip_description_y+30+j*30,
                                                         268, 300), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE))
                            if len(bonus_str) <= 28:
                                j += 1
                            else:
                                j += 2

                        for dilemma_effect in self.campaign_map_scene.loaded_game_data["dilemma_effects"]:
                            if self.campaign_map_scene.loaded_game_data["dilemma_effects"][dilemma_effect] > 0:
                                effect_str = DECISION_TEXT[dilemma_effect].split("\n")[1].split(" for")[0] + " ({} turns)"
                                self.drawWrappedText("*" + effect_str.format(self.campaign_map_scene.loaded_game_data["dilemma_effects"][dilemma_effect]),
                                                     BRIGHT_ORANGE, pg.Rect(self.tooltip_description_x + 20,
                                                     self.tooltip_description_y+30+j*30, 268, 300),
                                                     FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE))
                                if len(effect_str) <= 28:
                                    j += 1
                                else:
                                    j += 2

                    else:
                        relationship = self.campaign_map_scene.calculate_diplomatic_relationship(
                            self.campaign_map_scene.player_country, target_country)

                        counter = 0
                        for k,v in relationship.items():
                            if v < 0:
                                color = RED
                            else:
                                color = GREEN

                            if k == "Total":
                                if v >= 100:
                                    relationship_rating = "Friendly"
                                elif v >= 50:
                                    relationship_rating = "Peaceful"
                                elif v >= 0:
                                    relationship_rating = "Indifferent"
                                elif v >= -50:
                                    relationship_rating = "Unfriendly"
                                else:
                                    relationship_rating = "Hostile"
                                text = "Overall: {} ({})".format(relationship_rating, v)
                                self.draw_text(text,  FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), color,
                                                self.tooltip_description_x+20, self.tooltip_description_y+30+22*counter,
                                               align = "midleft")

                                counter += 1
                                self.draw_text("----------"*4,  FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), color,
                                                self.tooltip_description_x+20, self.tooltip_description_y+30+22*counter,
                                               align = "midleft")

                                counter += 1
                                self.draw_text("",  FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), color,
                                                self.tooltip_description_x+20, self.tooltip_description_y+30+22*counter,
                                               align = "midleft")

                            else:
                                if v > 0:
                                    value_sign = "+"
                                else:
                                    value_sign = ""
                                text = "{}: ({}{})".format(k, value_sign, v)
                                self.draw_text(text,  FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), color,
                                                self.tooltip_description_x+20, self.tooltip_description_y+30+22*counter,
                                               align = "midleft")

                            counter += 1

                else:
                    self.drawWrappedText(self.tooltip_description, WHITE,
                                         pg.Rect(self.tooltip_description_x + 16, self.tooltip_description_y + 20,
                                                 268, 360), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE))


            if self.game_tooltip_description:
                self.priority_sprites.draw(self.screen)
                if "Income" in self.game_tooltip_description:
                    projected_tax_and_additional_income = self.campaign_map_scene.calculate_tax_and_additional_income(self.campaign_map_scene.player_country)
                    projected_trade_revenue = self.campaign_map_scene.calculate_total_trade_revenue(self.campaign_map_scene.player_country)
                    projected_upkeep_cost = self.campaign_map_scene.calculate_upkeep_cost(self.campaign_map_scene.player_country)
                    projected_total_income = self.campaign_map_scene.calculate_projected_gold_delta(self.campaign_map_scene.player_country)
                    tooltip_description_split = self.game_tooltip_description.split("\n")

                    self.draw_text(tooltip_description_split[0].format(projected_tax_and_additional_income),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                   self.gold_icon.tooltip_background.rect.left + 20,
                                   self.gold_icon.tooltip_background.rect.top + 30, align="midleft")

                    self.draw_text(tooltip_description_split[1].format(projected_trade_revenue),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                   self.gold_icon.tooltip_background.rect.left + 20,
                                   self.gold_icon.tooltip_background.rect.top + 60, align="midleft")

                    self.draw_text(tooltip_description_split[2].format(projected_upkeep_cost),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), RED,
                                   self.gold_icon.tooltip_background.rect.left + 20,
                                   self.gold_icon.tooltip_background.rect.top + 90, align="midleft")

                    if projected_total_income >= 0:
                        self.draw_text(tooltip_description_split[3].format(projected_total_income),
                                       FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                       self.gold_icon.tooltip_background.rect.left + 20,
                                       self.gold_icon.tooltip_background.rect.top + 120, align="midleft")
                    else:
                        self.draw_text(tooltip_description_split[3].format(projected_total_income),
                                       FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), RED,
                                       self.gold_icon.tooltip_background.rect.left + 20,
                                       self.gold_icon.tooltip_background.rect.top + 120, align="midleft")

                else:
                    projected_food_production = self.campaign_map_scene.calculate_food_production(self.campaign_map_scene.player_country)
                    projected_food_consumption = self.campaign_map_scene.calculate_food_consumption(self.campaign_map_scene.player_country)
                    projected_food_surplus = self.campaign_map_scene.calculate_projected_food_delta(self.campaign_map_scene.player_country)
                    tooltip_description_split = self.game_tooltip_description.split("\n")

                    self.draw_text(tooltip_description_split[0].format(projected_food_production),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                   self.food_icon.tooltip_background.rect.left + 20,
                                   self.food_icon.tooltip_background.rect.top + 30, align="midleft")

                    self.draw_text(tooltip_description_split[1].format(projected_food_consumption),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), RED,
                                   self.food_icon.tooltip_background.rect.left + 20,
                                   self.food_icon.tooltip_background.rect.top + 60, align="midleft")

                    if projected_food_surplus >= 0:
                        self.draw_text(tooltip_description_split[2].format(projected_food_surplus),
                                       FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                       self.food_icon.tooltip_background.rect.left + 20,
                                       self.food_icon.tooltip_background.rect.top + 90, align="midleft")
                    else:
                        self.draw_text(tooltip_description_split[2].format(projected_food_surplus),
                                       FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), RED,
                                       self.food_icon.tooltip_background.rect.left + 20,
                                       self.food_icon.tooltip_background.rect.top + 90, align="midleft")


            self.decision_sprites.draw(self.screen)
            # DECISIONS CAPTION
            if self.decision_text:
                text_size = 600 // len(self.decision_text) + 15
                line_count = 0
                for line in self.decision_text.split("\n"):
                    self.draw_text(line, FONT_FUNCTION(DEFAULT_FONT, text_size), BLACK, self.game_width // 2,
                                   self.game_height//3 + line_count * (text_size+5))
                    line_count += 1

            self.transition_sprites.draw(self.screen)
            pg.display.flip()


    def draw_text(self, text, font, color, x, y, align="midtop"):
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        if align == "midleft":
            text_rect.midleft = (x, y)
        elif align == "topleft":
            text_rect.topleft = (x, y)
        elif align == "center":
            text_rect.center = (x, y)
        else:
            text_rect.midtop = (x, y)
        self.screen.blit(text_surface, text_rect)


    def drawWrappedText(self, text, color, rect, font, aa=False, bkg=None):
        rect = pg.Rect(rect)
        y = rect.top
        lineSpacing = 10

        # get the height of the font
        fontHeight = font.size("Tg")[1]

        while text:
            i = 1

            # determine if the row of text will be outside our area
            if y + fontHeight > rect.bottom:
                break

            # determine maximum width of line
            while font.size(text[:i])[0] < rect.width and i < len(text):
                i += 1

            # if we've wrapped the text, then adjust the wrap to the last word
            if i < len(text):
                i = text.rfind(" ", 0, i) + 1

            # render the line and blit it to the surface
            if bkg:
                image = font.render(text[:i], 1, color, bkg)
                image.set_colorkey(bkg)
            else:
                image = font.render(text[:i], aa, color)

            self.screen.blit(image, (rect.left, y))
            y += fontHeight + lineSpacing

            # remove the text we just blitted
            text = text[i:]

        return text