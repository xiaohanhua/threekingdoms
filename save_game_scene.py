from functools import partial
from settings import*
from sprites import*
import datetime as dt
import pygame as pg
import os
import pickle

class Save_Game_Scene:
    def __init__(self, parent_scene):
        self.campaign_map_scene = parent_scene
        self.game_width, self.game_height = LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT
        if DEMO:
            self.screen = pg.display.set_mode((self.game_width, self.game_height))
        else:
            self.screen = pg.display.set_mode((self.game_width, self.game_height), pg.FULLSCREEN|pg.DOUBLEBUF)
        os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (50, 50)
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()

        pg.mixer.init()
        pg.font.init()
        self.hover_sound = pg.mixer.Sound("sfx_hover.ogg")
        self.click_sound = pg.mixer.Sound("sfx_click.ogg")
        self.deployment_fail_sound = pg.mixer.Sound("sfx_deployment_failed.ogg")


    def new(self):

        self.running = True
        self.playing = True
        pg.mouse.set_visible(True)

        # transitions
        self.transition_sprites = pg.sprite.Group()
        self.transition = None
        self.post_transition_function = None
        self.initiate_transition("in")
        self.selected_save_slot_index = 0
        self.selected_save_slot_index_offset = 0
        self.save_files = self.fetch_save_files()
        self.save_filename = self.generate_generic_filename()
        self.game_message = ""
        self.game_message_color = GREEN
        self.game_message_size = 24
        self.last_action = 0

        self.viewing_decision_interface = False
        self.decision_text = ""

        self.all_sprites = pg.sprite.Group()
        self.buttons = pg.sprite.Group()
        self.decision_sprites = pg.sprite.Group()
        self.save_slot_buttons = []

        self.background = Static(0, self.game_height - 650, "sprite_menu_bg.png")
        self.all_sprites.add(self.background)

        self.black_bg = Static(LANDSCAPE_WIDTH // 2, LANDSCAPE_HEIGHT // 2 - 15, "sprite_battle_preview_bg.png",
                               anchor="center")
        self.all_sprites.add(self.black_bg)

        self.ui_bg = Static(LANDSCAPE_WIDTH // 2, LANDSCAPE_HEIGHT // 2, "decision_interface_bg_small.png", anchor="center")
        self.all_sprites.add(self.ui_bg)

        self.savefile_entry = Static(self.ui_bg.rect.left + 11, self.ui_bg.rect.top - 33, "sprite_save_slot_filename_entry.png")
        self.all_sprites.add(self.savefile_entry)

        for i in range(10):
            save_slot_button = Button(self.ui_bg.rect.left+11, self.ui_bg.rect.top+11+33*i,
                                  "sprite_save_slot_{}.png".format("dark" if i%2 == 0 else "light"),
                                  command=partial(self.update_selected_save_file, i), anchor="topleft")
            self.all_sprites.add(save_slot_button)
            self.buttons.add(save_slot_button)
            self.save_slot_buttons.append(save_slot_button)


        self.save_slot_highlight = Static(-3000, -3000, "sprite_save_slot_highlight.png", anchor="topleft")
        self.all_sprites.add(self.save_slot_highlight)

        self.toggle_country_left_button = Button(175, LANDSCAPE_HEIGHT//2, "menu_button_left.png",
                                                 command=partial(self.toggle_savefile_page, -1))
        self.buttons.add(self.toggle_country_left_button)
        self.all_sprites.add(self.toggle_country_left_button)

        self.toggle_country_right_button = Button(1025, LANDSCAPE_HEIGHT//2, "menu_button_right.png",
                                                  command=partial(self.toggle_savefile_page, 1))
        self.buttons.add(self.toggle_country_right_button)
        self.all_sprites.add(self.toggle_country_right_button)

        self.back_button = Button(LANDSCAPE_WIDTH//2-400, LANDSCAPE_HEIGHT-30, "menu_button_back.png",
                                  command=self.return_to_campaign_map, scale=(210,50))
        self.buttons.add(self.back_button)
        self.all_sprites.add(self.back_button)

        self.save_button = Button(self.ui_bg.rect.right-50, self.ui_bg.rect.top-30,
                                  "sprite_save_icon.png", command=self.save_game_as, scale=(25, 25), anchor="topleft")
        self.buttons.add(self.save_button)
        self.all_sprites.add(self.save_button)

        #self.delete_button = Button(self.save_slot_buttons[0].rect.right-20, self.save_slot_buttons[0].rect.top+4,
        #                            "sprite_close_button_large.png", command=self.delete_selected_savefile,
        #                            scale=(25, 25))
        #self.buttons.add(self.delete_button)
        #self.all_sprites.add(self.delete_button)

        self.run()


    def generate_generic_filename(self):
        d = self.campaign_map_scene.player_country + "_" + ["Easy", "Normal", "Hard", "Expert"][self.campaign_map_scene.difficulty-1] + "_" + str(dt.date.today()) + "_turn_" + str(self.campaign_map_scene.turn_number)
        return d.replace(" ", "_").replace(":","").replace(".","").lower()


    def initiate_transition(self, direction):
        if direction == "in":
            self.transition = FadeBlackTransition(45, "in")
            self.transition_sprites.add(self.transition)
        else:
            self.transition = FadeBlackTransition(45, "out")
            self.transition_sprites.add(self.transition)


    def transition_clean_up(self):
        self.transition.kill()
        self.transition = None
        self.post_transition_function = None
        self.playing = False
        self.running = False
        for s in self.all_sprites:
            s.kill()


    def save_game_as(self, overwrite=False):
        if len(self.save_filename) < 3:
            self.game_message_color = DARK_RED
            self.game_message = "Please enter a filename that is at least 3 characters long."
            self.last_action = pg.time.get_ticks()
            self.campaign_map_scene.play_sound_effect(self.deployment_fail_sound)
            return
        elif self.save_filename[-2:] == ".p":
            pass
        else:
            self.save_filename += ".p"

        if os.path.exists(os.path.join(SAVE_SLOTS_DIR, self.save_filename)):
            print(self.save_filename)
            if not overwrite:
                self.overwrite_selected_savefile() #pops up dialogue box with question
                return
            else:
                try:
                    os.remove(os.path.join(SAVE_SLOTS_DIR, self.save_filename))
                except Exception as e:
                    print("Error overwriting save file...")
                    print(e)

        self.campaign_map_scene.save_game(self.save_filename)
        self.save_files = self.fetch_save_files()
        self.game_message = "Game saved."
        self.game_message_color = GREEN


    def overwrite_selected_savefile(self):
        self.campaign_map_scene.play_sound_effect(self.click_sound)
        self.viewing_decision_interface = True
        self.decision_bg = Static(LANDSCAPE_WIDTH // 2, LANDSCAPE_HEIGHT // 2, "decision_interface_bg_small.png",
                                  anchor="center")
        self.all_sprites.add(self.decision_bg)
        self.decision_sprites.add(self.decision_bg)

        self.yes_button = Button(self.decision_bg.rect.left + 200, self.decision_bg.rect.bottom - 100,
                                 "menu_button_yes.png",
                                 command=partial(self.confirm_overwrite_selected_savefile, True))
        self.all_sprites.add(self.yes_button)
        self.buttons.add(self.yes_button)
        self.decision_sprites.add(self.yes_button)

        self.cancel_button = Button(self.decision_bg.rect.right - 200, self.decision_bg.rect.bottom - 100,
                                    "menu_button_cancel.png", command=partial(self.confirm_overwrite_selected_savefile, False))
        self.all_sprites.add(self.cancel_button)
        self.buttons.add(self.cancel_button)
        self.decision_sprites.add(self.cancel_button)

        self.decision_text = "A savefile with the same name already exists. Overwrite?"


    def confirm_overwrite_selected_savefile(self, confirm=False):
        for sp in self.decision_sprites:
            sp.kill()
            if sp in self.buttons:
                self.buttons.remove(sp)
        self.viewing_decision_interface = False
        self.decision_text = ""
        if confirm:
            self.click_sound.play()
            self.save_game_as(overwrite=True)


    def update_selected_save_file(self, i):
        self.save_files = self.fetch_save_files()
        if self.selected_save_slot_index_offset*10 + i >= len(self.save_files):
            self.campaign_map_scene.play_sound_effect(self.deployment_fail_sound)
            print("No save file selected...")
        else:
            self.click_sound.play()
            self.selected_save_slot_index = i
            save_filenames = list(self.save_files.keys())
            self.save_filename = save_filenames[i+10*self.selected_save_slot_index_offset][:-2]
            #self.save_slot_highlight.rect.left = self.save_slot_buttons[self.selected_save_slot_index].rect.left
            #self.save_slot_highlight.rect.top = self.save_slot_buttons[self.selected_save_slot_index].rect.top
            #self.load_button.rect.top = self.save_slot_buttons[self.selected_save_slot_index].rect.top + 4
            #self.delete_button.rect.top = self.save_slot_buttons[self.selected_save_slot_index].rect.top + 4
            #print(list(self.save_files.keys())[i+self.selected_save_slot_index_offset])


    def fetch_save_files(self, reverse=True, filter="autosave"):
        file_dicts = {}
        for filename in os.listdir(SAVE_SLOTS_DIR):
            if filter and filter not in filename.split(".p")[0]:
                if filename[-2::] == ".p":
                    file_dicts[filename] = os.path.getmtime(os.path.join(SAVE_SLOTS_DIR, filename))
        return dict(sorted(file_dicts.items(), key=lambda item: item[1], reverse=reverse))


    def toggle_savefile_page(self, direction):
        self.update_selected_save_file(0)
        self.selected_save_slot_index_offset += direction
        if self.selected_save_slot_index_offset > len(self.save_files)//10:
            self.selected_save_slot_index_offset = 0
        elif self.selected_save_slot_index_offset < 0:
            self.selected_save_slot_index_offset = len(self.save_files)//10


    def run(self):
        while self.playing:
            if self.running:
                self.clock.tick(FPS)
                self.events()
                self.update()
                self.draw()


    def events(self):
        pos = pg.mouse.get_pos()
        now = pg.time.get_ticks()
        keys = pg.key.get_pressed()
        if keys[pg.K_BACKSPACE] and now - self.last_action >= 80:
            self.save_filename = self.save_filename[:-1]
            self.last_action = now

        for button in self.buttons:
            if button.rect.collidepoint(pos):
                if pg.mouse.get_pressed()[0]:
                    button.pressed = True
                else:
                    button.pressed = False
                if not button.hovering and button not in self.save_slot_buttons:
                    button.hovering = True
            else:
                button.pressed = False
                button.hovering = False

        for event in pg.event.get():
            if event.type == pg.QUIT:
                choice = pg.display.message_box("Quit",
                                                "Are you sure you wish to quit? You will lose any unsaved progress.",
                                                buttons=("Yes", "No"), return_button=1, escape_button=None
                                                )

                if choice == 0:
                    pg.quit()
                    sys.exit()

            if event.type == pg.MOUSEBUTTONUP:
                self.last_action = pg.time.get_ticks()
                for button in self.buttons:
                    if button.rect.collidepoint(pos) and (not self.viewing_decision_interface or button in self.decision_sprites):
                        button.pressed = False
                        button.command()
                        break
            if event.type == pg.KEYUP:
                if event.key == pg.K_ESCAPE:
                    self.return_to_campaign_map()
            if event.type == pg.KEYDOWN and len(self.save_filename) < 50:
                #if event.unicode.isalpha():
                if event.key in list(range(65, 123)):
                    self.save_filename += chr(event.key)
                elif event.key == pg.K_MINUS:
                    if keys[pg.K_LSHIFT] or keys[pg.K_RSHIFT]:
                        self.save_filename += "_"
                    else:
                        self.save_filename += "-"
                elif event.key == pg.K_0:
                    self.save_filename += "0"
                elif event.key == pg.K_1:
                    self.save_filename += "1"
                elif event.key == pg.K_2:
                    self.save_filename += "2"
                elif event.key == pg.K_3:
                    self.save_filename += "3"
                elif event.key == pg.K_4:
                    self.save_filename += "4"
                elif event.key == pg.K_5:
                    self.save_filename += "5"
                elif event.key == pg.K_6:
                    self.save_filename += "6"
                elif event.key == pg.K_7:
                    self.save_filename += "7"
                elif event.key == pg.K_8:
                    self.save_filename += "8"
                elif event.key == pg.K_9:
                    self.save_filename += "9"


    def return_to_campaign_map(self):
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = self.return_to_campaign_map
        elif not self.transition.fading:
            self.transition_clean_up()
            campaign_data = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "custom_save_autosave.p"), "rb"))
            campaign_data["gold"] = self.campaign_map_scene.gold
            campaign_data["food"] = self.campaign_map_scene.food
            campaign_data["hostile_relationships"] = self.campaign_map_scene.hostile_relationships
            campaign_data["peace_relationships"] = self.campaign_map_scene.peace_relationships
            campaign_data["trade_relationships"] = self.campaign_map_scene.trade_relationships
            campaign_data["alliance_relationships"] = self.campaign_map_scene.alliance_relationships
            campaign_data["diplomatic_relationships"] = self.campaign_map_scene.diplomatic_relationships
            campaign_data["past_conflicts"] = self.campaign_map_scene.past_conflicts
            campaign_data["diplomacy_penalty"] = self.campaign_map_scene.diplomacy_penalty
            pickle.dump(campaign_data, open(os.path.join(SAVE_SLOTS_DIR, "custom_save_autosave.p"), "wb"))

            for sp in self.all_sprites:
                sp.kill()
            self.campaign_map_scene.new(load_from="custom_save_autosave.p")


    def update(self):
        now = pg.time.get_ticks()
        self.all_sprites.update()
        if self.transition:
            if self.transition.fading:
                self.transition.update()
            else:
                if self.post_transition_function:
                    self.post_transition_function()
                else:
                    self.transition = None
        if now - self.last_action > 2500:
            self.game_message = ""



    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)

            if self.save_filename[-2:] == ".p":
                display_filename = self.save_filename[:-2]
            else:
                display_filename = self.save_filename

            self.draw_text("Enter a file name and click the save button to save:", FONT_FUNCTION(DEFAULT_FONT, 24), WHITE, LANDSCAPE_WIDTH//2,
                           50, align="center")
            self.draw_text("*Only alphanumeric characters, dash, and underscore accepted", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                           YELLOW, LANDSCAPE_WIDTH // 2, 80, align="center")
            self.draw_text(display_filename, FONT_FUNCTION(DEFAULT_FONT, 20),
                           BLACK, self.savefile_entry.rect.left+5, self.savefile_entry.rect.top+5, align="topleft")

            save_filenames = list(self.save_files.keys())
            for i in range(self.selected_save_slot_index_offset*10, self.selected_save_slot_index_offset*10+10):
                try:
                    filename = save_filenames[i].replace(".p", "")
                    corresponding_slot_button = self.save_slot_buttons[i%10]
                    self.draw_text(filename, FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                           corresponding_slot_button.rect.left+10, corresponding_slot_button.rect.top+5, align="topleft")
                    self.draw_text("{}".format(dt.datetime.fromtimestamp(self.save_files[filename+".p"]).strftime('%Y-%m-%d %H:%M:%S')),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                   corresponding_slot_button.rect.right-250, corresponding_slot_button.rect.top+5,
                                   align="topleft")
                except Exception as e:
                    pass

            self.draw_text("{}".format(self.game_message), FONT_FUNCTION(DEFAULT_FONT, self.game_message_size),
                           self.game_message_color, self.game_width//2, self.game_height//2, align="center")


            self.decision_sprites.draw(self.screen)
            # DECISIONS CAPTION
            if self.decision_text:
                text_size = 600 // len(self.decision_text) + 15
                line_count = 0
                for line in self.decision_text.split("\n"):
                    self.draw_text(line, FONT_FUNCTION(DEFAULT_FONT, text_size), BLACK, self.game_width // 2,
                                   self.game_height // 3 + line_count * (text_size + 5))
                    line_count += 1

            self.transition_sprites.draw(self.screen)
            pg.display.flip()


    def draw_text(self, text, font, color, x, y, align="midtop"):
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        if align == "midleft":
            text_rect.midleft = (x, y)
        elif align == "topleft":
            text_rect.topleft = (x, y)
        else:
            text_rect.midtop = (x, y)
        self.screen.blit(text_surface, text_rect)


    def drawWrappedText(self, text, color, rect, font, aa=False, bkg=None):
        rect = pg.Rect(rect)
        y = rect.top
        lineSpacing = 10

        # get the height of the font
        fontHeight = font.size("Tg")[1]

        while text:
            i = 1

            # determine if the row of text will be outside our area
            if y + fontHeight > rect.bottom:
                break

            # determine maximum width of line
            while font.size(text[:i])[0] < rect.width and i < len(text):
                i += 1

            # if we've wrapped the text, then adjust the wrap to the last word
            if i < len(text):
                i = text.rfind(" ", 0, i) + 1

            # render the line and blit it to the surface
            if bkg:
                image = font.render(text[:i], 1, color, bkg)
                image.set_colorkey(bkg)
            else:
                image = font.render(text[:i], aa, color)

            self.screen.blit(image, (rect.left, y))
            y += fontHeight + lineSpacing

            # remove the text we just blitted
            text = text[i:]

        return text