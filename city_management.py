from game_classes import*
from sprites import*
from functools import partial
import pickle
#import tkinter as tk
#import tkinter.messagebox as messagebox

class Dummy:
    def __init__(self):
        self.enemy_unit_sprites = pg.sprite.Group()
        self.all_sprites = pg.sprite.Group()
        self.terrain = ""


class City_Management_Scene:
    def __init__(self, campaign_map_scene, city_sprite):
        self.campaign_map_scene = campaign_map_scene
        self.city_sprite = city_sprite
        self.game_width, self.game_height = LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT
        if DEMO:
            self.screen = pg.display.set_mode((self.game_width, self.game_height))
        else:
            self.screen = pg.display.set_mode((self.game_width, self.game_height), pg.FULLSCREEN|pg.DOUBLEBUF)
        os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (50, 50)
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()

        pg.mixer.init()
        pg.font.init()
        self.hover_sound = pg.mixer.Sound("sfx_hover.ogg")
        self.click_sound = pg.mixer.Sound("sfx_click.ogg")
        self.morale_boost_sound = pg.mixer.Sound("sfx_restore_status.ogg")


    def new(self):
        self.running = True
        self.playing = True
        pg.mouse.set_visible(True)

        # transitions
        self.transition_sprites = pg.sprite.Group()
        self.transition = None
        self.post_transition_function = None
        self.initiate_transition("in")

        if random() <= .1 and not self.campaign_map_scene.is_mute:
            pg.mixer.music.load("wly.ogg")
            pg.mixer.music.set_volume(.6)
            pg.mixer.music.play(10)

        self.tooltip_description = ""
        self.tooltip_description_x, self.tooltip_description_y = -3000, -3000
        self.viewing_city_info = None


        barracks = [building for building in self.city_sprite.buildings if building.name == "Barracks"][0]
        try:
            trade_port_level = [building for building in self.city_sprite.buildings if building.name == "Trade Port"][0].level
        except:
            trade_port_level = 0
        self.recruitable_units = RECRUITABLE_UNIT_DICT[barracks.level].copy()

        if (self.campaign_map_scene.task_progress["southern_barbarians"] >= 4 or self.campaign_map_scene.turn_number >= 31) and self.campaign_map_scene.player_country == "Shu":
            self.recruitable_units += ["barbarian_spear_woman", "barbarian_assassin", "barbarian_archer"]

        if barracks.level >= 4 and self.campaign_map_scene.player_country == "Wei":
            self.recruitable_units += ["tiger_knight_cavalry"]

        if trade_port_level >= 1 and barracks.level >= 2:
            self.recruitable_units += ["light_dagger_infantry"]

        if self.city_sprite.defender in ["Wutugu"]:
            defending_army = [army_sprite for army_sprite in self.campaign_map_scene.army_sprites if army_sprite.commander.name == self.city_sprite.defender][0]
            if defending_army.commander.level >= 5:
                self.recruitable_units.append("rattan_armor_infantry")

        if self.city_sprite.defender in ["Zhu Rong", "Meng Huo"]:
            defending_army = [army_sprite for army_sprite in self.campaign_map_scene.army_sprites if army_sprite.commander.name == self.city_sprite.defender][0]
            if defending_army.commander.level >= 5:
                self.recruitable_units.append("elephant_rider")

        if self.city_sprite.defender:
            defending_army = [army_sprite for army_sprite in self.campaign_map_scene.army_sprites if army_sprite.commander.name == self.city_sprite.defender][0]
            if "barbarian_subduer" in defending_army.commander.skills:
                self.recruitable_units += ["barbarian_spear_woman", "barbarian_assassin", "barbarian_archer"]
            if defending_army.commander.name == "Ding Feng" and "light_dagger_infantry" not in self.recruitable_units:
                self.recruitable_units += ["light_dagger_infantry"]

        if "fire_arrows" in self.campaign_map_scene.acquired_tech[self.campaign_map_scene.player_country] and barracks.level >= 2:
            self.recruitable_units.append("flaming_arrow_infantry")
        if "crossbows" in self.campaign_map_scene.acquired_tech[self.campaign_map_scene.player_country] and barracks.level >= 3:
            self.recruitable_units.append("crossbow_infantry")
        if "siege_weaponry" in self.campaign_map_scene.acquired_tech[self.campaign_map_scene.player_country] and barracks.level >= 4:
            self.recruitable_units.append("catapult")


        self.recruitable_units_icons = []
        self.cost_icons = []
        self.page_number = 0
        self.recruitable_characters = self.campaign_map_scene.loaded_game_data["recruitable_characters"][self.city_sprite.country]
        self.character_pool = self.campaign_map_scene.loaded_game_data["character_pool"]
        self.selected_unit_index = 0
        self.selected_char_index = 0


        self.all_sprites = pg.sprite.Group()
        self.icons = pg.sprite.Group()
        self.info_sprites = pg.sprite.Group()
        self.alert_sprites = pg.sprite.Group()
        self.priority_sprites = pg.sprite.Group()
        self.recruitment_sprites = pg.sprite.Group()
        self.character_recruitment_sprites = pg.sprite.Group()
        self.unit_stats_sprites = pg.sprite.Group()
        self.decision_sprites = pg.sprite.Group()
        self.buttons = []
        self.sp_atk_icons_list = []
        self.skill_icons_list = []

        self.background = Static(0, self.game_height - 650, "city_management_day_bg.png")
        self.all_sprites.add(self.background)

        self.gold_icon = CampaignMapSprite(self, 0, 0, "campaign_icon_gold.png", anchor="topleft")
        self.all_sprites.add(self.gold_icon)
        self.icons.add(self.gold_icon)

        self.food_icon = CampaignMapSprite(self, 7, 0, "campaign_icon_food.png", anchor="topleft")
        self.all_sprites.add(self.food_icon)
        self.icons.add(self.food_icon)

        self.question_icon = Static(LANDSCAPE_WIDTH-75, 20, "campaign_button_question.png", anchor="topleft")
        self.all_sprites.add(self.question_icon)

        self.back_button = Static(LANDSCAPE_WIDTH-150, 20, "campaign_button_back.png", anchor="topleft")
        self.all_sprites.add(self.back_button)

        counter = 0
        for building in self.city_sprite.buildings:
            building_button = CityManagementButton(self, building, 200+225*(counter%4), 175+counter//4*250, "city_management_{}.png".format(building.name.lower()),
                                                   command=partial(self.clicked_on_building, building, counter),
                                                   tooltip=building.get_description(), scale=(125,125))
            if building.upgrade_progress > 0:
                building_button.upgrading = True
            self.all_sprites.add(building_button)
            self.buttons.append(building_button)
            counter += 1


            if building.name in self.campaign_map_scene.game_alerts["building"]:
                alert = Static(building_button.rect.centerx, building_button.rect.centery, "sprite_exclamation_mark_1.png",
                               [pg.image.load("sprite_exclamation_mark_{}.png".format(i)).convert_alpha() for i in
                                [2, 3, 4, 5, 4, 3, 2, 1]], frame_delta=85, anchor="center")
                self.all_sprites.add(alert)
                self.alert_sprites.add(alert)


        self.upgrade_building_button = None
        self.additional_option_button = None
        self.close_recruitment_button = None
        self.recruit_unit_button = None
        self.unit_recruitment_cost = UNIT_RECRUITMENT_COST.copy()

        # Recruitment cost bonus
        ranged_discount = 0
        melee_discount = 0
        for city_sprite in self.campaign_map_scene.city_sprites:
            if city_sprite.country == self.campaign_map_scene.player_country:
                for building in city_sprite.buildings:
                    if building.name == "Forest":
                        ranged_discount += building.level
                    elif building.name == "Iron Mine":
                        melee_discount += building.level

        for unit_type in self.recruitable_units:
            if unit_type in RANGED_UNITS:
                self.unit_recruitment_cost[unit_type] = int(self.unit_recruitment_cost[unit_type]*(100-ranged_discount)/100)
            if unit_type not in RANGED_UNITS:
                self.unit_recruitment_cost[unit_type] = int(self.unit_recruitment_cost[unit_type]*(100-melee_discount)/100)
            if unit_type in CAVALRY_UNITS:
                if self.city_sprite.country == "Ma Teng":
                    self.unit_recruitment_cost[unit_type] = int(self.unit_recruitment_cost[unit_type]*.8)
            if "militia" in unit_type:
                if self.city_sprite.country == "Liu Biao":
                    self.unit_recruitment_cost[unit_type] = int(self.unit_recruitment_cost[unit_type]*.7)

            if self.unit_recruitment_cost[unit_type] <= 1:
                self.unit_recruitment_cost[unit_type] = 1


        self.recruitment_quantity = 10
        self.recruitment_limit = barracks.level*50
        if self.city_sprite.defender:
            defending_army = [army_sprite for army_sprite in self.campaign_map_scene.army_sprites if army_sprite.commander.name == self.city_sprite.defender][0]
            if "iron_fist" in defending_army.commander.skills:
                self.recruitment_limit *= 2
            elif "clansman_relations" in defending_army.commander.skills:
                self.recruitment_limit = int(self.recruitment_limit*1.5)
        if self.campaign_map_scene.loaded_game_data["dilemma_effects"]["nationalism"] > 0:
            self.recruitment_limit *= 2

        self.recruitment_message = ""
        self.game_message_color = GREEN
        self.last_displayed_message = 0


        self.decision_text = ""
        self.game_tooltip_description = ""
        self.viewing_decision_interface = False
        self.viewing_recruitment_interface = False
        self.viewing_character_recruitment_interface = False
        self.cost_to_recruit_character = 999999999
        self.load_city_info_sprites()
        self.toggle_view_city_info(False)
        self.run()


    def initiate_transition(self, direction):
        if direction == "in":
            self.transition = FadeBlackTransition(80, "in")
            self.transition_sprites.add(self.transition)
        else:
            self.transition = FadeBlackTransition(80, "out")
            self.transition_sprites.add(self.transition)


    def transition_clean_up(self):
        self.transition.kill()
        self.transition = None
        self.post_transition_function = None
        self.playing = False
        self.running = False


    def upgrade_building(self, building, master_button_index):
        self.upgrade_building_button.kill()
        self.upgrade_building_button = None
        if self.additional_option_button:
            self.additional_option_button.kill()
            self.additional_option_button = None

        self.viewing_decision_interface = True
        self.decision_bg = Static(LANDSCAPE_WIDTH//2, LANDSCAPE_HEIGHT//2, "decision_interface_bg_small.png",
                                        anchor="center")
        self.all_sprites.add(self.decision_bg)
        self.decision_sprites.add(self.decision_bg)

        self.yes_button = Button(self.decision_bg.rect.left+200, self.decision_bg.rect.bottom-100, "menu_button_yes.png",
                                  command=partial(self.confirm_upgrade, building, master_button_index, True))
        self.all_sprites.add(self.yes_button)
        self.decision_sprites.add(self.yes_button)

        self.cancel_button = Button(self.decision_bg.rect.right-200, self.decision_bg.rect.bottom-100, "menu_button_cancel.png",
                                  command=partial(self.confirm_upgrade, building, master_button_index, False))
        self.all_sprites.add(self.cancel_button)
        self.decision_sprites.add(self.cancel_button)

        self.decision_text = "Spend {} gold to upgrade {}?".format(building.upgrade_cost[building.level],
                                                                            building.name)
        #root = tk.Tk()
        #root.overrideredirect(1)
        #root.withdraw()
        #response = messagebox.askquestion("Upgrade", "Spend {} gold to upgrade {}? This action cannot be undone.".format(building.upgrade_cost[building.level], building.name))


    def confirm_upgrade(self, building, master_button_index, decision):
        self.viewing_decision_interface = False
        self.decision_text = ""
        for sp in self.decision_sprites:
            sp.kill()
            if sp in self.buttons:
                self.buttons.remove(sp)
        if decision:
            upgrade_turns_multiplier = 1
            for army_sprite in self.campaign_map_scene.player_army_sprites:
                if "commissioner_of_development" in army_sprite.commander.skills:
                    upgrade_turns_multiplier -= .3
                    break

            self.campaign_map_scene.gold[self.campaign_map_scene.player_country] -= building.upgrade_cost[building.level]
            building.upgrade_progress = math.ceil(building.upgrade_turns[building.level]*upgrade_turns_multiplier)
            self.buttons[master_button_index].upgrading = True



    def clicked_on_building(self, building, master_button_index):
        if self.upgrade_building_button:
            self.upgrade_building_button.kill()
        if self.additional_option_button:
            self.additional_option_button.kill()

        self.upgrade_building_button = Button((master_button_index%4)*225+200, 150+125+master_button_index//4*250, "menu_button_upgrade.png",
                        command=partial(self.upgrade_building, building, master_button_index), scale=(120,30))
        self.all_sprites.add(self.upgrade_building_button)

        if building.level >= building.max_level:
            self.upgrade_building_button.disabled = True
        elif self.campaign_map_scene.gold[self.campaign_map_scene.player_country] < building.upgrade_cost[building.level]:
            self.upgrade_building_button.disabled = True
        elif building.upgrade_progress > 0:
            self.upgrade_building_button.disabled = True

        if building.name == "Barracks":
            self.additional_option_button = Button((master_button_index%4)*225+200, 150+165+master_button_index//4*250,
                                                   "campaign_button_battle_preview_recruit.png",
                                                    command=self.recruit, scale=(120,30))
            self.all_sprites.add(self.additional_option_button)
            self.buttons.append(self.additional_option_button)

        if building.name == "Academy":
            self.additional_option_button = Button((master_button_index%4)*225+200, 150+165+master_button_index//4*250,
                                                   "campaign_button_battle_preview_recruit.png",
                                                    command=self.recruit_character, scale=(120,30))
            self.all_sprites.add(self.additional_option_button)
            self.buttons.append(self.additional_option_button)

        else:
            if building.name in self.campaign_map_scene.game_alerts["building"]:
                self.campaign_map_scene.game_alerts["building"].remove(building.name)



    def exit_recruit(self):
        for recruited_char in self.campaign_map_scene.loaded_game_data["recruited_characters"]:
            if recruited_char in self.campaign_map_scene.loaded_game_data["recruitable_characters"][self.city_sprite.country]:
                self.campaign_map_scene.loaded_game_data["recruitable_characters"][self.city_sprite.country].remove(recruited_char)
        self.viewing_recruitment_interface = False
        self.viewing_character_recruitment_interface = False
        self.recruitable_units_icons = []
        self.question_icon.rect.topleft = LANDSCAPE_WIDTH-75, 20
        self.back_button.rect.topleft = LANDSCAPE_WIDTH-150, 20

        for sp in self.recruitment_sprites:
            sp.kill()
        self.recruit_unit_button = None
        self.selected_char_index = 0


    def recruit_character(self):
        self.upgrade_building_button.kill()
        self.upgrade_building_button = None
        self.additional_option_button.kill()
        self.additional_option_button = None
        self.viewing_character_recruitment_interface = True
        self.question_icon.rect.topleft = -3000, -3000
        self.back_button.rect.topleft = -3000, -3000
        for building_name in self.campaign_map_scene.game_alerts["building"]:
            self.campaign_map_scene.game_alerts["building"].remove(building_name)
        for sp in self.alert_sprites:
            sp.kill()

        self.recruitment_bg = Static(LANDSCAPE_WIDTH // 2, LANDSCAPE_HEIGHT // 2, "sprite_battle_preview_bg.png", anchor="center")
        self.all_sprites.add(self.recruitment_bg)
        self.recruitment_sprites.add(self.recruitment_bg)

        self.close_recruitment_button = Static(self.recruitment_bg.rect.right, self.recruitment_bg.rect.top,
                                               "sprite_close_button_large.png", anchor="center")
        self.all_sprites.add(self.close_recruitment_button)
        self.recruitment_sprites.add(self.close_recruitment_button)

        if self.recruitable_characters:
            self.recruit_character_button = Button(self.recruitment_bg.rect.right - 200, self.recruitment_bg.rect.bottom - 50,
                                              "campaign_button_battle_preview_recruit.png",
                                              command=self.recruit_selected_character, scale=(200, 50))
            self.all_sprites.add(self.recruit_character_button)
            self.recruitment_sprites.add(self.recruit_character_button)

            self.toggle_character_left_button = Button(100, LANDSCAPE_HEIGHT // 2, "menu_button_left.png",
                                                            command=partial(self.toggle_character, -1))
            self.recruitment_sprites.add(self.toggle_character_left_button)
            self.all_sprites.add(self.toggle_character_left_button)

            self.toggle_character_right_button = Button(LANDSCAPE_WIDTH-100, LANDSCAPE_HEIGHT // 2, "menu_button_right.png",
                                                             command=partial(self.toggle_character, 1))
            self.recruitment_sprites.add(self.toggle_character_right_button)
            self.all_sprites.add(self.toggle_character_right_button)

            self.load_character_info()
            self.update_recruit_character_button_status()

        else:
            static_text_sprite = Static(LANDSCAPE_WIDTH//2, LANDSCAPE_HEIGHT//2,
                                        "sprite_no_characters_available_for_recruitment.png", anchor="center")
            self.recruitment_sprites.add(static_text_sprite)
            self.character_recruitment_sprites.add(static_text_sprite)


    def toggle_character(self, direction):
        self.selected_char_index += direction
        if self.selected_char_index >= len(self.recruitable_characters):
            self.selected_char_index = 0
        elif self.selected_char_index < 0:
            self.selected_char_index = len(self.recruitable_characters) - 1
        self.load_character_info()
        self.update_recruit_character_button_status()


    def load_character_info(self):

        for sp in self.character_recruitment_sprites:
            sp.kill()

        commander_simple_name = self.recruitable_characters[self.selected_char_index]
        self.commander = self.character_pool[commander_simple_name]

        #calibrate exp/level
        self.commander.experience = 0
        base_exp = 100*(200 + self.campaign_map_scene.turn_number//2 - self.commander.earliest_start_year)
        self.commander.gain_exp(base_exp)

        self.character_preview = Static(150, 80, "general_avatar_{}.png".format(self.commander.simple_name),
                                             anchor="topleft")
        self.recruitment_sprites.add(self.character_preview)
        self.character_recruitment_sprites.add(self.character_preview)

        self.lock_icon_values = {}
        self.sp_atk_icons_list = []
        self.skill_icons_list = []
        counter = 0
        for sp_atk, level_req in self.commander.learnable_attacks.items():
            sp_atk_icon = SpecialAttackIcon(self, 350 + 70 * counter, 110, sp_atk, anchor="topleft")
            self.all_sprites.add(sp_atk_icon)
            self.recruitment_sprites.add(sp_atk_icon)
            self.character_recruitment_sprites.add(sp_atk_icon)
            self.sp_atk_icons_list.append(sp_atk_icon)
            if level_req > self.commander.level:
                lock_icon = Static(350 + 70 * counter, 110, "sprite_lock_icon.png", anchor="topleft")
                self.recruitment_sprites.add(lock_icon)
                self.character_recruitment_sprites.add(lock_icon)
                self.lock_icon_values[lock_icon] = level_req
            counter += 1

        counter = 0
        for skill, level_req in self.commander.learnable_skills.items():
            sp_atk_icon = SpecialAttackIcon(self, 350 + 70 * counter, 230, skill, anchor="topleft", type="skill")
            self.all_sprites.add(sp_atk_icon)
            self.recruitment_sprites.add(sp_atk_icon)
            self.character_recruitment_sprites.add(sp_atk_icon)
            self.skill_icons_list.append(sp_atk_icon)
            if level_req > self.commander.level:
                lock_icon = Static(350 + 70 * counter, 230, "sprite_lock_icon.png", anchor="topleft")
                self.recruitment_sprites.add(lock_icon)
                self.character_recruitment_sprites.add(lock_icon)
                self.lock_icon_values[lock_icon] = level_req
            counter += 1

        if self.city_sprite.country.lower() == "wei":
            self.cost_to_recruit_character = 0
        else:
            self.cost_to_recruit_character = 2*(self.commander.combat_prowess + self.commander.intellect + self.commander.command + self.commander.charisma + len(self.commander.learnable_skills)*250 + len(self.commander.learnable_attacks)*250)



    def recruit_selected_character(self):
        self.campaign_map_scene.loaded_game_data["gold"][self.city_sprite.country] -= self.cost_to_recruit_character
        self.campaign_map_scene.loaded_game_data["recruited_characters"].append(self.commander.simple_name)

        # update age and level upon recruitment
        self.commander.age += self.campaign_map_scene.turn_number//2
        self.commander.experience = 0
        base_exp = 100*(200 + self.campaign_map_scene.turn_number//2 - self.commander.earliest_start_year)
        self.commander.gain_exp(base_exp)
        self.commander.auto_use_upgrade_points()

        #self.campaign_map_scene.loaded_game_data["recruitable_characters"][self.city_sprite.country].remove(self.commander.simple_name)
        new_army = Army(self.city_sprite.grid_position_x+randrange(-2,3), self.city_sprite.grid_position_y+randrange(-1,2),
                        commander=self.commander, army_composition={}, action_points=-1, retreat_penalty=0, anchor="midleft")
        self.campaign_map_scene.loaded_game_data["army"].append(new_army)

        self.campaign_map_scene.play_sound_effect(self.morale_boost_sound)
        self.recruitment_message = "Recruited {}!".format(self.commander.name)
        self.game_message_color = GREEN
        self.last_displayed_message = pg.time.get_ticks()

        self.update_recruit_character_button_status()


    def update_recruit_character_button_status(self):
        cond_1 = self.campaign_map_scene.loaded_game_data["gold"][self.city_sprite.country] < self.cost_to_recruit_character
        cond_2 = self.commander.simple_name in self.campaign_map_scene.loaded_game_data["recruited_characters"]
        cond_3 = len(self.recruitable_characters) == 0
        if cond_1 or cond_2 or cond_3:
            self.recruit_character_button.disabled = True
        else:
            self.recruit_character_button.disabled = False


    def recruit(self):
        self.upgrade_building_button.kill()
        self.upgrade_building_button = None
        self.additional_option_button.kill()
        self.additional_option_button = None
        self.viewing_recruitment_interface = True
        self.question_icon.rect.topleft = -3000, -3000
        self.back_button.rect.topleft = -3000, -3000

        for building_name in self.campaign_map_scene.game_alerts["building"]:
            if building_name != "Academy":
                self.campaign_map_scene.game_alerts["building"].remove(building_name)
        for sp in self.alert_sprites:
            sp.kill()

        self.recruitment_bg = Static(LANDSCAPE_WIDTH//2, LANDSCAPE_HEIGHT//2+30, "sprite_battle_preview_bg.png",
                                        anchor="center")
        self.all_sprites.add(self.recruitment_bg)
        self.recruitment_sprites.add(self.recruitment_bg)

        self.close_recruitment_button = Static(self.recruitment_bg.rect.right, self.recruitment_bg.rect.top,
                                      "sprite_close_button_large.png", anchor="center")
        self.all_sprites.add(self.close_recruitment_button)
        self.recruitment_sprites.add(self.close_recruitment_button)


        self.recruit_unit_button = Button(self.recruitment_bg.rect.right - 350, self.recruitment_bg.rect.bottom - 50,
                                          "campaign_button_battle_preview_recruit.png", command=self.recruit_selected_unit,
                                          scale=(200,50))
        self.all_sprites.add(self.recruit_unit_button)
        self.recruitment_sprites.add(self.recruit_unit_button)


        self.disband_button = Button(LANDSCAPE_WIDTH- 225, self.recruitment_bg.rect.bottom - 50,
                                     "menu_button_disband.png", command=self.disband_selected_unit,
                                     scale=(200,50))
        self.recruitment_sprites.add(self.disband_button)
        self.all_sprites.add(self.disband_button)


        self.recruit_quantity_label = Static(self.recruitment_bg.rect.right - 600, self.recruitment_bg.rect.bottom - 53,
                                             "sprite_unit_quantity_label.png", anchor="center")
        self.all_sprites.add(self.recruit_quantity_label)
        self.recruitment_sprites.add(self.recruit_quantity_label)

        self.plus_button = Button(self.recruit_quantity_label.rect.left+120, self.recruit_quantity_label.rect.top+25,
                                          "menu_button_plus.png",
                                          command=partial(self.toggle_recruitment_quantity,10))
        self.all_sprites.add(self.plus_button)
        self.recruitment_sprites.add(self.plus_button)

        self.minus_button = Button(self.recruit_quantity_label.rect.left + 17,
                                  self.recruit_quantity_label.rect.top + 25,
                                  "menu_button_minus.png",
                                  command=partial(self.toggle_recruitment_quantity, -10))

        if len(self.recruitable_units) > 20:
            self.toggle_page_left_button = Button(150, LANDSCAPE_HEIGHT//2, "menu_button_left.png",
                                                  command=partial(self.toggle_page, -1))
            self.buttons.append(self.toggle_page_left_button)
            self.recruitment_sprites.add(self.toggle_page_left_button)
            self.all_sprites.add(self.toggle_page_left_button)

            self.toggle_page_right_button = Button(700, LANDSCAPE_HEIGHT//2, "menu_button_right.png",
                                                   command=partial(self.toggle_page, 1))
            self.buttons.append(self.toggle_page_right_button)
            self.recruitment_sprites.add(self.toggle_page_right_button)
            self.all_sprites.add(self.toggle_page_right_button)

        self.all_sprites.add(self.minus_button)
        self.recruitment_sprites.add(self.minus_button)

        self.update_recruit_button_status()

        self.load_recruitable_units()
        self.load_unit_stats()
        self.campaign_map_scene.give_advice("help_upkeep_food_cost")


    def toggle_page(self, direction):
        self.page_number -= direction
        if self.page_number < 0:
            self.page_number = len(self.recruitable_units)//20
        elif self.page_number > len(self.recruitable_units)//20:
            self.page_number = 0
        self.selected_unit_index = 20*self.page_number
        self.load_recruitable_units()
        self.load_unit_stats()
        self.update_recruit_button_status()


    def load_recruitable_units(self):
        for sp in self.recruitable_units_icons:
            sp.kill()
        for sp in self.cost_icons:
            sp.kill()
        self.recruitable_units_icons = []
        self.cost_icons = []
        max_col = 5
        current_col = 0
        for i in range(self.page_number * 20, self.page_number * 20 + 20):
            if i < len(self.recruitable_units):
                row = i % 20 // max_col
                unit_type = self.recruitable_units[i]
                if "barbarian" in unit_type.lower() or "elephant" in unit_type.lower() or "rattan" in unit_type.lower():
                    unit_country_suffix = "shu"
                else:
                    unit_country_suffix = self.campaign_map_scene.player_country.lower()
                unit_icon = UnitIcon(self, "left", current_col * 100 + 200, row * 120 + 100, unit_type,
                                     unit_country_suffix)
                if i == self.selected_unit_index:
                    unit_icon.selected = True
                self.recruitable_units_icons.append(unit_icon)
                self.all_sprites.add(unit_icon)
                self.recruitment_sprites.add(unit_icon)

                cost_icon = Static(current_col * 100 + 200, row * 120 + 175, "sprite_gold_tiny.png")
                self.all_sprites.add(cost_icon)
                self.cost_icons.append(cost_icon)
                self.recruitment_sprites.add(cost_icon)

                current_col += 1
                if current_col >= max_col:
                    current_col = 0


    def disband_selected_unit(self):
        self.viewing_decision_interface = True
        self.decision_bg = Static(LANDSCAPE_WIDTH // 2, LANDSCAPE_HEIGHT // 2, "decision_interface_bg_small.png",
                                  anchor="center")
        self.all_sprites.add(self.decision_bg)
        self.decision_sprites.add(self.decision_bg)

        self.yes_button = Button(self.decision_bg.rect.left + 200, self.decision_bg.rect.bottom - 100,
                                 "menu_button_yes.png", command=self.confirm_disband_selected_unit)
        self.all_sprites.add(self.yes_button)
        self.decision_sprites.add(self.yes_button)
        self.buttons.append(self.yes_button)

        self.cancel_button = Button(self.decision_bg.rect.right - 200, self.decision_bg.rect.bottom - 100,
                                    "menu_button_cancel.png", command=partial(self.confirm_disband_selected_unit, False))
        self.all_sprites.add(self.cancel_button)
        self.decision_sprites.add(self.cancel_button)
        self.buttons.append(self.cancel_button)

        selected_unit_name = UNIT_FRIENDLY_NAMES[self.recruitable_units[self.selected_unit_index]]
        self.decision_text = "Disband this unit: <{}>?".format(selected_unit_name)


    def confirm_disband_selected_unit(self, proceed=True):
        for sp in self.decision_sprites:
            sp.kill()
            if sp in self.buttons:
                self.buttons.remove(sp)
        self.viewing_decision_interface = False
        self.decision_text = ""
        if proceed:
            del self.city_sprite.garrison[self.recruitable_units[self.selected_unit_index]]
            self.update_recruit_button_status()


    def update_recruit_button_status(self):
        if self.recruitment_quantity > self.recruitment_limit - self.city_sprite.units_recruited_this_turn:
            self.recruit_unit_button.disabled = True
        else:
            if self.recruitment_quantity*self.unit_recruitment_cost[self.recruitable_units[self.selected_unit_index]] > self.campaign_map_scene.gold[self.campaign_map_scene.player_country]:
                self.recruit_unit_button.disabled = True
            else:
                self.recruit_unit_button.disabled = False

        selected_unit = self.recruitable_units[self.selected_unit_index]
        self.disband_button.disabled = True
        if selected_unit in self.city_sprite.garrison:
            if self.city_sprite.garrison[selected_unit]:
                self.disband_button.disabled = False


    def toggle_recruitment_quantity(self, delta):
        self.recruitment_quantity += delta
        if self.recruitment_quantity <= 10:
            self.recruitment_quantity = 10
        elif self.recruitment_quantity >= 999:
            self.recruitment_quantity = 999

        self.update_recruit_button_status()


    def recruit_selected_unit(self):
        barracks = [building for building in self.city_sprite.buildings if building.name == "Barracks"][0]
        selected_unit = self.recruitable_units[self.selected_unit_index]
        if selected_unit not in self.city_sprite.garrison and len(self.city_sprite.garrison) >= 8:
            self.recruitment_message = "Cannot have more than 8 different unit types."
            self.game_message_color = BRIGHT_ORANGE
            self.last_displayed_message = pg.time.get_ticks()
            return
        elif sum(self.city_sprite.garrison.values()) >= barracks.level*500:
            self.recruitment_message = "Max garrison size limit reached."
            self.game_message_color = BRIGHT_ORANGE
            self.last_displayed_message = pg.time.get_ticks()
            return

        self.city_sprite.units_recruited_this_turn += self.recruitment_quantity
        if selected_unit in self.city_sprite.garrison:
            self.city_sprite.garrison[selected_unit] += self.recruitment_quantity
        else:
            self.city_sprite.garrison[selected_unit] = self.recruitment_quantity
        self.campaign_map_scene.gold[self.campaign_map_scene.player_country] -= self.recruitment_quantity * self.unit_recruitment_cost[selected_unit]
        self.update_recruit_button_status()

        self.campaign_map_scene.play_sound_effect(self.morale_boost_sound)
        self.recruitment_message = "Recruited {} x {}!".format(UNIT_FRIENDLY_NAMES[selected_unit], self.recruitment_quantity)
        self.game_message_color = GREEN
        self.last_displayed_message = pg.time.get_ticks()

        #check if any ongoing achievements have been invalidated
        if selected_unit not in RANGED_UNITS:
            if self.campaign_map_scene.ongoing_achievements["ranged_units_only"]:
                self.campaign_map_scene.ongoing_achievements["ranged_units_only"] = False
        else:
            if self.campaign_map_scene.ongoing_achievements["no_ranged_units"]:
                self.campaign_map_scene.ongoing_achievements["no_ranged_units"] = False

        if selected_unit not in CAVALRY_UNITS and self.campaign_map_scene.ongoing_achievements["cavalry_units_only"]:
            self.campaign_map_scene.ongoing_achievements["cavalry_units_only"] = False



    def load_unit_stats(self):
        for sp in self.unit_stats_sprites:
            sp.kill()

        selected_unit = self.recruitable_units[self.selected_unit_index]
        sample_unit = self.generate_sample_unit(selected_unit)
        if str(self.campaign_map_scene.player_country.lower()) not in ["shu", "wei", "wu"]:
            unit_country = "shu"
        elif "barbarian" in selected_unit.lower() or "elephant" in selected_unit.lower() or "rattan" in selected_unit.lower():
            unit_country = "shu"
        else:
            unit_country = self.campaign_map_scene.player_country.lower()
        avatar_filename = "sprite_{}_icon_{}.png".format(selected_unit, unit_country)
        tooltip_filename = "sprite_{}_tooltip.png".format(selected_unit)

        tooltip = Static(725, 100, tooltip_filename)
        self.unit_stats_sprites.add(tooltip)
        self.recruitment_sprites.add(tooltip)

        tooltip_avatar = Static(tooltip.rect.left + 16, tooltip.rect.top + 16, avatar_filename, anchor="topleft")
        self.unit_stats_sprites.add(tooltip_avatar)
        self.recruitment_sprites.add(tooltip_avatar)

        cell_filename = "sprite_stat_single_cell.png"
        if sample_unit.unit_class == "ranged":

            health_cell_length = int(sample_unit.health/20)
            for i in range(health_cell_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 110, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            attacking_range_cell_length = sample_unit.attacking_range // 100
            for i in range(attacking_range_cell_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 146, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            firing_rate_cell_length = int((sample_unit.firing_rate - .5) * 10) + 1
            for i in range(firing_rate_cell_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 182, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            firing_accuracy_cell_length = int(sample_unit.firing_accuracy * 10)
            for i in range(firing_accuracy_cell_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 218, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            damage_cell_length = int(sample_unit.firing_damage)
            for i in range(damage_cell_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 254, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            fatal_strike_rate_length = int(sample_unit.fatal_strike_rate * 100)
            for i in range(fatal_strike_rate_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 290, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            defense_length = int(sample_unit.defense)
            for i in range(defense_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 326, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            speed_length = int(sample_unit.walking_acc)
            for i in range(speed_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 362, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            unit_cost_length = int(UNIT_COMMAND_COST[selected_unit] // 5)
            for i in range(unit_cost_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 398, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

        elif sample_unit.unit_class == "cavalry":
            health_cell_length = int(sample_unit.health/20)
            for i in range(health_cell_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 110, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            attacking_range_cell_length = int(sample_unit.attacking_range / 10)
            for i in range(attacking_range_cell_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 146, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            damage_cell_length = int(sample_unit.attack)
            for i in range(damage_cell_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 182, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            fatal_strike_rate_length = int(sample_unit.fatal_strike_rate * 100)
            for i in range(fatal_strike_rate_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 218, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            defense_length = int(sample_unit.defense)
            for i in range(defense_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 254, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            speed_length = int(sample_unit.walking_acc)
            for i in range(speed_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 290, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            charge_range_length = int(sample_unit.max_charge_range)//50
            for i in range(charge_range_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 326, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            unit_cost_length = int(UNIT_COMMAND_COST[selected_unit] // 5)
            for i in range(unit_cost_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 362, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

        else:
            health_cell_length = int(sample_unit.health/20)
            for i in range(health_cell_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 110, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            attacking_range_cell_length = int(sample_unit.attacking_range/10)
            for i in range(attacking_range_cell_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 146, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            damage_cell_length = int(sample_unit.attack)
            for i in range(damage_cell_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 182, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            fatal_strike_rate_length = int(sample_unit.fatal_strike_rate * 100)
            for i in range(fatal_strike_rate_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 218, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            defense_length = int(sample_unit.defense)
            for i in range(defense_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 254, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            speed_length = int(sample_unit.walking_acc)
            for i in range(speed_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 290, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)

            unit_cost_length = int(UNIT_COMMAND_COST[selected_unit] // 5)
            for i in range(unit_cost_length):
                cell = Static(tooltip.rect.left + 20 + 11 * i, tooltip.rect.top + 326, cell_filename, anchor="topleft")
                self.unit_stats_sprites.add(cell)
                self.recruitment_sprites.add(cell)


    def generate_sample_unit(self, unit_type):
        dummy = Dummy()
        if unit_type == "spear_militia":
            return Spear_Militia(dummy, 0, 0, unit_type,
                                 self.campaign_map_scene.player_country.lower(), "left", 0)

        elif unit_type == "sword_militia":
            return Sword_Militia(dummy, 0, 0, unit_type,
                                 self.campaign_map_scene.player_country.lower(), "left", 0)

        elif unit_type == "axe_militia":
            return Axe_Militia(dummy, 0, 0, unit_type,
                                 self.campaign_map_scene.player_country.lower(), "left", 0)

        elif unit_type == "bow_militia":
            return Bow_Militia(dummy, 0, 0, unit_type,
                               self.campaign_map_scene.player_country.lower(), "left", 0)

        elif unit_type == "barbarian_spear_woman":
            return Barbarian_Spear_Woman(dummy, 0, 0, unit_type, "shu", "left", 0)

        elif unit_type == "barbarian_assassin":
            return Barbarian_Assassin(dummy, 0, 0, unit_type, "shu", "left", 0)

        elif unit_type == "barbarian_archer":
            return Barbarian_Archer(dummy, 0, 0, unit_type, "shu", "left", 0)

        elif unit_type == "rattan_armor_infantry":
            return Rattan_Armor_Infantry(dummy, 0, 0, unit_type, "shu", "left", 0)

        elif unit_type == "spear_infantry":
            return Spear_Infantry(dummy, 0, 0, unit_type,
                                 self.campaign_map_scene.player_country.lower(), "left", 0)

        elif unit_type == "longspear_infantry":
            return Longspear_Infantry(dummy, 0, 0, unit_type,
                                 self.campaign_map_scene.player_country.lower(), "left", 0)

        elif unit_type == "elite_spear_infantry":
            return Elite_Spear_Infantry(dummy, 0, 0, unit_type,
                                 self.campaign_map_scene.player_country.lower(), "left", 0)

        elif unit_type == "light_dagger_infantry":
            return Light_Dagger_Infantry(dummy, 0, 0, unit_type,
                                 self.campaign_map_scene.player_country.lower(), "left", 0)

        elif unit_type == "sword_infantry":
            return Sword_Infantry(dummy, 0, 0, unit_type,
                                 self.campaign_map_scene.player_country.lower(), "left", 0)

        elif unit_type == "bow_infantry":
            return Bow_Infantry(dummy, 0, 0, unit_type,
                                 self.campaign_map_scene.player_country.lower(), "left", 0)

        elif unit_type == "flaming_arrow_infantry":
            return Flaming_Arrow_Infantry(dummy, 0, 0, unit_type,
                                 self.campaign_map_scene.player_country.lower(), "left", 0)

        elif unit_type == "crossbow_infantry":
            return Crossbow_Infantry(dummy, 0, 0, unit_type,
                                 self.campaign_map_scene.player_country.lower(), "left", 0)

        elif unit_type == "catapult":
            return Catapult(dummy, 0, 0, unit_type,
                                 self.campaign_map_scene.player_country.lower(), "left", 0)

        elif unit_type == "shielded_sword_infantry":
            return Shielded_Sword_Infantry(dummy, 0, 0, unit_type,
                                 self.campaign_map_scene.player_country.lower(), "left", 0)

        elif unit_type == "elite_sword_infantry":
            return Elite_Sword_Infantry(dummy, 0, 0, unit_type,
                                 self.campaign_map_scene.player_country.lower(), "left", 0)

        elif unit_type == "mace_infantry":
            return Mace_Infantry(dummy, 0, 0, unit_type,
                                 self.campaign_map_scene.player_country.lower(), "left", 0)

        elif unit_type == "battleaxe_infantry":
            return Battleaxe_Infantry(dummy, 0, 0, unit_type,
                                 self.campaign_map_scene.player_country.lower(), "left", 0)

        elif unit_type == "elephant_rider":
            return Elephant_Rider(dummy, 0, 0, unit_type,
                                 "shu", "left", 0)

        elif unit_type == "halberd_cavalry":
            return Halberd_Cavalry(dummy, 0, 0, unit_type,
                                  self.campaign_map_scene.player_country.lower(), "left", 0)

        elif unit_type == "light_cavalry":
            return Light_Cavalry(dummy, 0, 0, unit_type,
                                  self.campaign_map_scene.player_country.lower(), "left", 0)

        elif unit_type == "tiger_knight_cavalry":
            return Tiger_Knight_Cavalry(dummy, 0, 0, unit_type,
                                  self.campaign_map_scene.player_country.lower(), "left", 0)


    def load_city_info_sprites(self):
        info_bg = Static(LANDSCAPE_WIDTH - 350, LANDSCAPE_HEIGHT//2, "sprite_army_info_bg.png", anchor="midleft")
        self.all_sprites.add(info_bg)
        self.info_sprites.add(info_bg)

        if self.city_sprite.country.lower() not in ["shu", "wei", "wu", "liu biao", "ma teng", "zhang lu"]:
            city_country = "rogue"
        else:
            city_country = self.city_sprite.country.lower()
        info_city_avatar = Static(LANDSCAPE_WIDTH-325, 50, "sprite_fortress_{}_icon_{}.png".format(self.city_sprite.fortress_level, city_country),
                                                           anchor="topleft", scale=(70, 60))
        self.all_sprites.add(info_city_avatar)
        self.info_sprites.add(info_city_avatar)

        self.info_army_unit_types = list(self.city_sprite.garrison.keys())
        count = 0
        for row in range(2):
            for column in range(4):
                if count < len(self.info_army_unit_types):
                    unit_type = self.info_army_unit_types[count]
                    unit_icon = UnitIcon(self, "left", column * 65 + LANDSCAPE_WIDTH - 300, row * 62 + 200,
                                         unit_type, self.city_sprite.country)

                else:
                    unit_icon = UnitIcon(self, "left", column * 65 + LANDSCAPE_WIDTH - 300, row * 62 + 200, None,
                                         None)
                self.info_sprites.add(unit_icon)
                self.all_sprites.add(unit_icon)
                count += 1


    def toggle_view_city_info(self, show):
        if show:
            self.viewing_city_info = self.city_sprite.name
            self.load_city_info_sprites()
        else:
            self.viewing_city_info = None
            for sp in self.info_sprites:
                sp.kill()


    def run(self):
        while self.playing:
            if self.running:
                self.clock.tick(FPS)
                self.events()
                self.update()
                self.draw()


    def events(self):

        pos = pg.mouse.get_pos()
        # gold icon or food icon hover
        if self.gold_icon.rect.collidepoint(pos):
            self.gold_icon.hovering = True
            if self.gold_icon.tooltip_background:
                self.game_tooltip_description = "Projected Tax/Building Income: {}\nProjected Trade Revenue: {}\nProjected Upkeep Cost: {}\nProjected Net Income: {}"
        else:
            self.gold_icon.hovering = False
            if self.food_icon.rect.collidepoint(pos):
                self.food_icon.hovering = True
                if self.food_icon.tooltip_background:
                    self.game_tooltip_description = "Projected Food Production: {}\nProjected Food Consumption: {}\nProjected Food Surplus: {}"
            else:
                self.food_icon.hovering = False
                self.game_tooltip_description = ""

        if self.viewing_decision_interface:
            for event in pg.event.get():
                for sp in self.decision_sprites:
                    if type(sp) == Button:
                        if sp.rect.collidepoint(pos):
                            if event.type == pg.MOUSEBUTTONUP:
                                self.campaign_map_scene.play_sound_effect(self.click_sound)
                                sp.command()
                            if not sp.hovering:
                                sp.hovering = True
                        else:
                            sp.hovering = False

            return

        if self.question_icon.rect.collidepoint(pos):
            if not self.viewing_city_info:
                self.toggle_view_city_info(True)
        else:
            if self.viewing_city_info:
                self.toggle_view_city_info(False)


        if not self.viewing_recruitment_interface and not self.viewing_character_recruitment_interface: #not recruiting
            for event in pg.event.get():
                # quit
                if event.type == pg.QUIT:
                    choice = pg.display.message_box("Quit",
                                                    "Are you sure you wish to quit? You will lose any unsaved progress.",
                                                    buttons=("Yes", "No"), return_button=1, escape_button=None
                                                    )

                    if choice == 0:
                        pg.quit()
                        sys.exit()
                #Left clicks
                if event.type == pg.MOUSEBUTTONUP:
                    self.campaign_map_scene.play_sound_effect(self.click_sound)
                    for button in self.buttons:
                        if button.rect.collidepoint(pos):
                            if button.command:
                                if not button.disabled:
                                    button.command()
                            break
                    if self.back_button.rect.collidepoint(pos):
                        self.return_to_campaign_map()
                    if self.upgrade_building_button:
                        if self.upgrade_building_button.rect.collidepoint(pos) and not self.upgrade_building_button.disabled:
                            self.upgrade_building_button.command()

        else: #recruiting
            #hovers
            for sp in [sp for sp in self.recruitment_sprites if type(sp)==Button]:
                if sp.rect.collidepoint(pos):
                    sp.hovering = True
                else:
                    sp.hovering = False

            for event in pg.event.get():
                if event.type == pg.KEYUP:
                    if event.key == pg.K_ESCAPE:
                        self.return_to_campaign_map()
                #Left clicks
                if event.type == pg.MOUSEBUTTONUP:
                    self.campaign_map_scene.play_sound_effect(self.click_sound)
                    for sp in self.recruitment_sprites:
                        if sp in self.recruitable_units_icons:
                            if sp.rect.collidepoint(pos):
                                self.recruitable_units_icons[self.selected_unit_index%20].selected = False
                                self.selected_unit_index = self.recruitable_units_icons.index(sp) + self.page_number*20
                                sp.selected = True
                                self.load_unit_stats()
                                self.update_recruit_button_status()
                        elif type(sp) == Button:
                            if sp.rect.collidepoint(pos) and not sp.disabled and sp.command:
                                    sp.command()

                    if self.close_recruitment_button.rect.collidepoint(pos):
                        self.exit_recruit()


        if self.upgrade_building_button:
            if self.upgrade_building_button.rect.collidepoint(pos):
                self.upgrade_building_button.hovering = True
            else:
                self.upgrade_building_button.hovering = False
        if self.additional_option_button:
            if self.additional_option_button.rect.collidepoint(pos):
                self.additional_option_button.hovering = True
            else:
                self.additional_option_button.hovering = False

        if not self.viewing_recruitment_interface and not self.viewing_character_recruitment_interface:
            hovering_over_button = False
            for button in self.buttons:
                if button.rect.collidepoint(pos):
                    button.hovering = True
                    hovering_over_button = True
                    if button.tooltip:
                        self.tooltip_description = button.tooltip_description
                        self.tooltip_description_x = button.tooltip.rect.left
                        self.tooltip_description_y = button.tooltip.rect.top
                else:
                    button.hovering = False
            if not hovering_over_button:
                self.tooltip_description = ""

        if self.viewing_character_recruitment_interface:
            # sp_atk buttons with tooltips
            hovering_over_button = False
            for icon in self.sp_atk_icons_list + self.skill_icons_list:
                if icon.rect.collidepoint(pos):
                    icon.hovering = True
                    hovering_over_button = True
                    if icon.tooltip:
                        self.tooltip_description = icon.tooltip_description
                        self.tooltip_description_x = icon.tooltip.rect.left
                        self.tooltip_description_y = icon.tooltip.rect.top
                else:
                    icon.hovering = False
            if not hovering_over_button:
                self.tooltip_description = ""


    def return_to_campaign_map(self):
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = self.return_to_campaign_map
        elif not self.transition.fading:
            self.transition_clean_up()
            campaign_data = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "city_management_autosave.p"), "rb"))
            campaign_data["gold"] = self.campaign_map_scene.gold
            campaign_data["army"] = self.campaign_map_scene.loaded_game_data["army"]
            campaign_data["recruitable_characters"] = self.campaign_map_scene.loaded_game_data["recruitable_characters"]
            campaign_data["recruited_characters"] = self.campaign_map_scene.loaded_game_data["recruited_characters"]
            campaign_data["game_alerts"] = self.campaign_map_scene.game_alerts
            campaign_data["advice_given"] = self.campaign_map_scene.advice_given
            campaign_data["show_advice"] = self.campaign_map_scene.show_advice
            campaign_data["ongoing_achievements"] = self.campaign_map_scene.ongoing_achievements
            for city in campaign_data["city"]:
                if city.name == self.city_sprite.name:
                    city.buildings = self.city_sprite.buildings
                    city.garrison = self.city_sprite.garrison
                    city.units_recruited_this_turn = self.city_sprite.units_recruited_this_turn

                    if city.defender:
                        for army in campaign_data["army"]:
                            if army.commander.name == city.defender:
                                army.army_composition = city.garrison
                    break

            pickle.dump(campaign_data, open(os.path.join(SAVE_SLOTS_DIR, "city_management_autosave.p"), "wb"))
            for sp in self.all_sprites:
                sp.kill()
            self.campaign_map_scene.new(load_from="city_management_autosave.p")


    def update(self):
        now = pg.time.get_ticks()
        self.all_sprites.update()
        if now - self.last_displayed_message > 2000:
            self.recruitment_message = ""
            self.game_message_color = GREEN
        if self.transition:
            if self.transition.fading:
                self.transition.update()
            else:
                if self.post_transition_function:
                    self.post_transition_function()
                else:
                    self.transition = None


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)

            estimated_gold_delta = self.campaign_map_scene.calculate_projected_gold_delta(self.campaign_map_scene.player_country)
            estimated_food_delta = self.campaign_map_scene.calculate_projected_food_delta(self.campaign_map_scene.player_country)
            gold_label = "{} ({})".format(self.campaign_map_scene.gold[self.campaign_map_scene.player_country], estimated_gold_delta)
            food_label = "{} ({})".format(self.campaign_map_scene.food[self.campaign_map_scene.player_country], estimated_food_delta)
            gold_label_color = WHITE
            food_label_color = WHITE
            if estimated_gold_delta < 0:
                gold_label_color = DARK_RED
            if estimated_food_delta < 0:
                food_label_color = DARK_RED

            self.draw_text(gold_label, FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE - len(gold_label) // 6), gold_label_color,
                           55, 28, align="midleft")
            self.draw_text(food_label, FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE - len(food_label) // 6), food_label_color,
                           270, 27, align="midleft")

            if not self.viewing_city_info:
                for button in self.buttons:
                    if type(button) == CityManagementButton:
                        if button.upgrading:
                            self.draw_text("{}".format(button.master.upgrade_progress), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                                           WHITE, button.rect.centerx, button.rect.centery, align="center")

            if self.viewing_city_info:
                self.draw_text(self.city_sprite.name, FONT_FUNCTION(DEFAULT_FONT, 24), WHITE,
                               LANDSCAPE_WIDTH - 225, 60, align="midleft")

                self.draw_text("Population: {}".format(self.city_sprite.population),
                               FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, LANDSCAPE_WIDTH - 225, 90,
                               align="midleft")
                if self.city_sprite.happiness >= 0:
                    self.draw_text("Happiness: {}".format(int(self.city_sprite.happiness)),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, LANDSCAPE_WIDTH - 225, 110,
                                   align="midleft")
                else:
                    self.draw_text("Happiness: {}".format(int(self.city_sprite.happiness)),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), RED, LANDSCAPE_WIDTH - 225,
                                   110, align="midleft")

                self.draw_text("Faction: {}".format(self.city_sprite.country),
                               FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, LANDSCAPE_WIDTH - 225, 130,
                               align="midleft")

                self.draw_text("Garrisoned Troops:", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                               LANDSCAPE_WIDTH - 325, 180, align="midleft")

                self.info_army_unit_types = list(self.city_sprite.garrison.keys())
                for i in range(len(self.info_army_unit_types)):
                    unit_type = self.info_army_unit_types[i]
                    text_col = i % 4
                    text_row = i // 4
                    text_x, text_y = text_col * 65 + LANDSCAPE_WIDTH - 300 + 65 - 10, text_row * 62 + 62 - 15 + 200
                    self.draw_text("{}".format(self.city_sprite.garrison[unit_type]),
                                   FONT_FUNCTION(DEFAULT_FONT, 14), WHITE, text_x, text_y)


            self.recruitment_sprites.draw(self.screen)
            self.character_recruitment_sprites.draw(self.screen)
            self.unit_stats_sprites.draw(self.screen)

            if self.viewing_character_recruitment_interface and self.recruitable_characters:
                # draw commander stats text
                self.draw_text(self.commander.name, FONT_FUNCTION(DEFAULT_FONT, 24), WHITE, 150, 60,
                               align="midleft")
                self.draw_text("Level: {}".format(self.commander.level), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                               WHITE, 150, 340 + 20, align="midleft")
                if self.commander.level < 10:
                    self.draw_text("Exp: {}/{}".format(self.commander.experience,
                                                       self.commander.level_thresholds[self.commander.level]),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 150, 340 + 60, align="midleft")
                else:
                    self.draw_text("Exp: --/--", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 150, 340 + 60,
                                   align="midleft")

                if self.commander.simple_name in self.campaign_map_scene.loaded_game_data["recruited_characters"]:
                    self.draw_text("Age: {}".format(self.commander.age), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 150,
                                   340 + 100, align="midleft")
                else:
                    self.draw_text("Age: {}".format(self.commander.age + self.campaign_map_scene.turn_number//2), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 150,
                                   340 + 100, align="midleft")

                self.draw_text("Faction: {}".format(self.commander.country), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                               150, 340 + 140, align="midleft")
                self.draw_text("Loyalty: {}".format(self.commander.loyalty), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                               150, 340 + 180, align="midleft")
                self.draw_text("Combat Prowess: {}".format(self.commander.combat_prowess),
                               FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), YELLOW, 380, 340 + 20, align="midleft")
                self.draw_text("Intellect: {}".format(self.commander.intellect), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                               YELLOW, 380, 340 + 60, align="midleft")
                self.draw_text("Command: {}".format(self.commander.command), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                               YELLOW, 380, 340 + 100, align="midleft")
                self.draw_text("Charisma: {}".format(self.commander.charisma), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                               YELLOW, 380, 340 + 140, align="midleft")
                self.draw_text("Action Pts: {}".format(self.commander.action_points),
                               FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                               YELLOW, 380, 340 + 180, align="midleft")

                self.draw_text("Special Attacks:", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 350, 90, align="midleft")
                self.draw_text("Skills:", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 350, 210, align="midleft")

                self.draw_text("Cost to recruit: {}".format(self.cost_to_recruit_character),
                               FONT_FUNCTION(DEFAULT_FONT, 24), WHITE,
                               self.recruitment_bg.rect.right-200, self.recruitment_bg.rect.bottom-100, align="center")

                for lock_icon, level_req in self.lock_icon_values.items():
                    self.draw_text("Lv. {}".format(level_req),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), RED, lock_icon.rect.centerx,
                                   lock_icon.rect.centery - 5)


                if pg.time.get_ticks() - self.last_displayed_message <= 2000:
                    self.draw_text(self.recruitment_message, FONT_FUNCTION(DEFAULT_FONT, 24), self.game_message_color,
                                   LANDSCAPE_WIDTH//2, LANDSCAPE_HEIGHT//2)


            if self.viewing_recruitment_interface:
                max_col = 5
                current_col = 0
                for i in range(self.page_number * 20, self.page_number * 20 + 20):
                    if i < len(self.recruitable_units):
                        row = i % 20 // max_col
                        unit_type = self.recruitable_units[i]
                        self.draw_text("{}".format(self.unit_recruitment_cost[unit_type]), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                                       YELLOW, current_col * 100 + 240, row * 120 + 175)

                        # display how much you have of this unit in garrison already
                        garrisoned_quantity = 0
                        if unit_type in self.city_sprite.garrison:
                            garrisoned_quantity = self.city_sprite.garrison[unit_type]
                        self.draw_text("{}".format(garrisoned_quantity), FONT_FUNCTION(DEFAULT_FONT, 14),
                                       WHITE, current_col * 100 + 255, row * 120 + 145)

                        current_col += 1
                        if current_col >= max_col:
                            current_col = 0


                self.draw_text("Limit: {}".format(self.recruitment_limit - self.city_sprite.units_recruited_this_turn),
                               FONT_FUNCTION(DEFAULT_FONT, 22), WHITE, self.recruitment_bg.rect.left+50,
                               self.recruitment_bg.rect.bottom - 55, align="midleft")

                self.draw_text("Quantity:", FONT_FUNCTION(DEFAULT_FONT, 22), WHITE,
                               self.recruitment_bg.rect.left+200, self.recruitment_bg.rect.bottom - 55,
                               align="midleft")

                self.draw_text("{}".format(self.recruitment_quantity),
                               FONT_FUNCTION(DEFAULT_FONT, 20), WHITE, self.recruit_quantity_label.rect.centerx,
                               self.recruit_quantity_label.rect.centery-15, align="center")

                if pg.time.get_ticks() - self.last_displayed_message <= 2000:
                    self.draw_text(self.recruitment_message, FONT_FUNCTION(DEFAULT_FONT, 24), self.game_message_color,
                                   LANDSCAPE_WIDTH//2, LANDSCAPE_HEIGHT//2)

            self.decision_sprites.draw(self.screen)
            if self.decision_text:
                self.draw_text(self.decision_text, FONT_FUNCTION(DEFAULT_FONT, 26), BLACK, LANDSCAPE_WIDTH//2,
                               LANDSCAPE_HEIGHT//2-100, align="center")

            if self.tooltip_description:
                if self.viewing_character_recruitment_interface:
                    self.priority_sprites.draw(self.screen)
                    #self.drawWrappedText(self.tooltip_description, WHITE,
                    #                     pg.Rect(self.tooltip_description_x + 16, self.tooltip_description_y + 20,
                    #                             268, 360), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE))
                    self.draw_text(self.tooltip_description, FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                                   WHITE, self.tooltip_description_x + 16, self.tooltip_description_y + 20,
                                   align="topleft", max_width=268)
                else:
                    description_split = self.tooltip_description.split("\n")
                    counter = 1
                    for line in description_split:
                        self.draw_text(line, FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, self.tooltip_description_x+15,
                                       self.tooltip_description_y+25*counter, align="midleft")
                        counter += 1


            if self.game_tooltip_description:
                self.priority_sprites.draw(self.screen)
                if "Income" in self.game_tooltip_description:
                    projected_tax_and_additional_income = self.campaign_map_scene.calculate_tax_and_additional_income(self.campaign_map_scene.player_country)
                    projected_trade_revenue = self.campaign_map_scene.calculate_total_trade_revenue(self.campaign_map_scene.player_country)
                    projected_upkeep_cost = self.campaign_map_scene.calculate_upkeep_cost(self.campaign_map_scene.player_country)
                    projected_total_income = self.campaign_map_scene.calculate_projected_gold_delta(self.campaign_map_scene.player_country)
                    tooltip_description_split = self.game_tooltip_description.split("\n")

                    self.draw_text(tooltip_description_split[0].format(projected_tax_and_additional_income),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                   self.gold_icon.tooltip_background.rect.left + 20,
                                   self.gold_icon.tooltip_background.rect.top + 30, align="midleft")

                    self.draw_text(tooltip_description_split[1].format(projected_trade_revenue),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                   self.gold_icon.tooltip_background.rect.left + 20,
                                   self.gold_icon.tooltip_background.rect.top + 60, align="midleft")

                    self.draw_text(tooltip_description_split[2].format(projected_upkeep_cost),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), RED,
                                   self.gold_icon.tooltip_background.rect.left + 20,
                                   self.gold_icon.tooltip_background.rect.top + 90, align="midleft")

                    if projected_total_income >= 0:
                        self.draw_text(tooltip_description_split[3].format(projected_total_income),
                                       FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                       self.gold_icon.tooltip_background.rect.left + 20,
                                       self.gold_icon.tooltip_background.rect.top + 120, align="midleft")
                    else:
                        self.draw_text(tooltip_description_split[3].format(projected_total_income),
                                       FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), RED,
                                       self.gold_icon.tooltip_background.rect.left + 20,
                                       self.gold_icon.tooltip_background.rect.top + 120, align="midleft")

                else:
                    projected_food_production = self.campaign_map_scene.calculate_food_production(self.campaign_map_scene.player_country)
                    projected_food_consumption = self.campaign_map_scene.calculate_food_consumption(self.campaign_map_scene.player_country)
                    projected_food_surplus = self.campaign_map_scene.calculate_projected_food_delta(self.campaign_map_scene.player_country)
                    tooltip_description_split = self.game_tooltip_description.split("\n")

                    self.draw_text(tooltip_description_split[0].format(projected_food_production),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                   self.food_icon.tooltip_background.rect.left + 20,
                                   self.food_icon.tooltip_background.rect.top + 30, align="midleft")

                    self.draw_text(tooltip_description_split[1].format(projected_food_consumption),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), RED,
                                   self.food_icon.tooltip_background.rect.left + 20,
                                   self.food_icon.tooltip_background.rect.top + 60, align="midleft")

                    if projected_food_surplus >= 0:
                        self.draw_text(tooltip_description_split[2].format(projected_food_surplus),
                                       FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                       self.food_icon.tooltip_background.rect.left + 20,
                                       self.food_icon.tooltip_background.rect.top + 90, align="midleft")
                    else:
                        self.draw_text(tooltip_description_split[2].format(projected_food_surplus),
                                       FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), RED,
                                       self.food_icon.tooltip_background.rect.left + 20,
                                       self.food_icon.tooltip_background.rect.top + 90, align="midleft")

            self.transition_sprites.draw(self.screen)
            pg.display.flip()

    def draw_text(self, text, font, color, x, y, align="midtop", max_width=None):
        if max_width:
            text_surface = font.render(text, True, color, None, max_width)
        else:
            text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        if align == "midleft":
            text_rect.midleft = (x, y)
        elif align == "topleft":
            text_rect.topleft = (x, y)
        else:
            text_rect.midtop = (x, y)
        self.screen.blit(text_surface, text_rect)


    def drawWrappedText(self, text, color, rect, font, aa=False, bkg=None):
        rect = pg.Rect(rect)
        y = rect.top
        lineSpacing = 10

        # get the height of the font
        fontHeight = font.size("Tg")[1]

        while text:
            i = 1

            # determine if the row of text will be outside our area
            if y + fontHeight > rect.bottom:
                break

            # determine maximum width of line
            while font.size(text[:i])[0] < rect.width and i < len(text):
                i += 1

            # if we've wrapped the text, then adjust the wrap to the last word
            if i < len(text):
                i = text.rfind(" ", 0, i) + 1

            # render the line and blit it to the surface
            if bkg:
                image = font.render(text[:i], 1, color, bkg)
                image.set_colorkey(bkg)
            else:
                image = font.render(text[:i], aa, color)

            self.screen.blit(image, (rect.left, y))
            y += fontHeight + lineSpacing

            # remove the text we just blitted
            text = text[i:]

        return text