from game_classes import*
from sprites import*
from functools import partial
import pickle
#import tkinter as tk
#import tkinter.messagebox as messagebox

class Army_Management_Scene:
    def __init__(self, campaign_map_scene, army_sprite):
        self.campaign_map_scene = campaign_map_scene
        self.army_sprite = army_sprite
        self.commander = self.army_sprite.commander
        self.game_width, self.game_height = LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT
        if DEMO:
            self.screen = pg.display.set_mode((self.game_width, self.game_height))
        else:
            self.screen = pg.display.set_mode((self.game_width, self.game_height), pg.FULLSCREEN|pg.DOUBLEBUF)
        os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (50, 50)
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()

        pg.mixer.init()
        pg.font.init()
        self.hover_sound = pg.mixer.Sound("sfx_hover.ogg")
        self.click_sound = pg.mixer.Sound("sfx_click.ogg")
        self.morale_boost_sound = pg.mixer.Sound("sfx_restore_status.ogg")


    def new(self):
        self.running = True
        self.playing = True
        pg.mouse.set_visible(True)

        # transitions
        self.transition_sprites = pg.sprite.Group()
        self.transition = None
        self.post_transition_function = None
        self.initiate_transition("in")

        self.tooltip_description = ""
        self.tooltip_description_x, self.tooltip_description_y = -3000, -3000
        self.viewing_city_info = None

        self.selected_unit_index = 0


        self.all_sprites = pg.sprite.Group()
        self.icons = pg.sprite.Group()
        self.priority_sprites = pg.sprite.Group()
        self.decision_sprites = pg.sprite.Group()
        self.unit_icons = []
        self.buttons = []
        self.sp_atk_icons_list = []
        self.skill_icons_list = []

        self.background = Static(0, self.game_height - 650, "forest_day_bg.png")
        self.all_sprites.add(self.background)

        self.gold_icon = CampaignMapSprite(self, 0, 0, "campaign_icon_gold.png", anchor="topleft")
        self.all_sprites.add(self.gold_icon)
        self.icons.add(self.gold_icon)

        self.food_icon = CampaignMapSprite(self, 7, 0, "campaign_icon_food.png", anchor="topleft")
        self.all_sprites.add(self.food_icon)
        self.icons.add(self.food_icon)

        self.back_button = Static(LANDSCAPE_WIDTH-60, 10, "campaign_button_back.png", anchor="topleft")
        self.all_sprites.add(self.back_button)

        self.army_management_bg = Static(LANDSCAPE_WIDTH // 2, LANDSCAPE_HEIGHT // 2, "sprite_battle_preview_bg.png",
                                     anchor="center")
        self.all_sprites.add(self.army_management_bg)

        self.player_general_preview = Static(150, 80, "general_avatar_{}.png".format(self.commander.simple_name),
                                             anchor="topleft")
        self.all_sprites.add(self.player_general_preview)


        self.combat_prowess_button = Button(350, 340+20, "menu_button_plus_vertical.png",
                                            command=partial(self.upgrade_attribute, "combat_prowess", 1),
                                            tooltip="Determines your army's maximum morale as well as the chance of capturing an enemy general after a victory/escaping capture following a loss. Costs 1 upgrade point to increase.",
                                            master=self)
        self.buttons.append(self.combat_prowess_button)
        self.all_sprites.add(self.combat_prowess_button)

        self.intellect_button = Button(350, 340+60, "menu_button_plus_vertical.png",
                                       command=partial(self.upgrade_attribute, "intellect", 1),
                                       tooltip="Affects the rate at which your command points are restored in battle as well as the number of upgrade points received for leveling up. Costs 1 upgrade point to increase.",
                                       master=self)
        self.buttons.append(self.intellect_button)
        self.all_sprites.add(self.intellect_button)

        self.command_button = Button(350, 340+100, "menu_button_plus_vertical.png",
                                     command=partial(self.upgrade_attribute, "command", 1),
                                     tooltip="Determines your general's maximum command in battle. Costs 1 upgrade point to increase.",
                                     master=self)
        self.buttons.append(self.command_button)
        self.all_sprites.add(self.command_button)

        self.charisma_button = Button(350, 340+140, "menu_button_plus_vertical.png",
                                      command=partial(self.upgrade_attribute, "charisma", 1),
                                      tooltip="Impacts the population happiness of the city in which the general is embedded. Generals with high charisma also have a higher chance of recruiting a captured enemy commander. Costs 1 upgrade point to increase.",
                                      master=self)
        self.buttons.append(self.charisma_button)
        self.all_sprites.add(self.charisma_button)

        self.action_point_button = Button(350, 340+180, "menu_button_plus_vertical.png", command=partial(self.upgrade_attribute, "action_points", 3),
                                            tooltip="Action points are needed for movement on the campaign map and/or special actions. Costs 3 upgrade points to increase.",
                                            master=self)
        self.buttons.append(self.action_point_button)
        self.all_sprites.add(self.action_point_button)

        self.disband_button = Button(LANDSCAPE_WIDTH-325, 575, "menu_button_disband.png", command=self.disband_selected_unit,
                                     scale=(200,50))
        self.buttons.append(self.disband_button)
        self.all_sprites.add(self.disband_button)


        self.lock_icon_values = {}
        counter = 0
        for sp_atk, level_req in self.commander.learnable_attacks.items():
            sp_atk_icon = SpecialAttackIcon(self, 350 + 70 * counter, 110, sp_atk, anchor="topleft")
            self.all_sprites.add(sp_atk_icon)
            self.sp_atk_icons_list.append(sp_atk_icon)
            if level_req > self.commander.level:
                lock_icon = Static(350 + 70*counter, 110, "sprite_lock_icon.png", anchor="topleft")
                self.all_sprites.add(lock_icon)
                self.lock_icon_values[lock_icon] = level_req
            counter += 1

        counter = 0
        for skill, level_req in self.commander.learnable_skills.items():
            sp_atk_icon = SpecialAttackIcon(self, 350 + 70 * counter, 230, skill, anchor="topleft", type="skill")
            self.all_sprites.add(sp_atk_icon)
            self.skill_icons_list.append(sp_atk_icon)
            if level_req > self.commander.level:
                lock_icon = Static(350 + 70*counter, 230, "sprite_lock_icon.png", anchor="topleft")
                self.all_sprites.add(lock_icon)
                self.lock_icon_values[lock_icon] = level_req
            counter += 1


        self.reload_unit_icons()

        self.game_message = ""
        self.last_displayed_message = 0


        self.decision_text = ""
        self.game_tooltip_description = ""
        self.viewing_decision_interface = False
        self.update_upgrade_buttons()
        self.run()


    def initiate_transition(self, direction):
        if direction == "in":
            self.transition = FadeBlackTransition(80, "in")
            self.transition_sprites.add(self.transition)
        else:
            self.transition = FadeBlackTransition(80, "out")
            self.transition_sprites.add(self.transition)


    def transition_clean_up(self):
        self.transition.kill()
        self.transition = None
        self.post_transition_function = None
        self.playing = False
        self.running = False


    def reload_unit_icons(self):
        self.selected_unit_index = 0
        for icon in self.unit_icons:
            icon.kill()
        self.unit_icons = []
        self.info_army_unit_types = list(self.army_sprite.army_composition.keys())
        count = 0
        for row in range(2):
            for column in range(4):
                if count < len(self.info_army_unit_types):
                    unit_type = self.info_army_unit_types[count]
                    unit_icon = UnitIcon(self, "left", column * 65 + LANDSCAPE_WIDTH - 450, row * 62 + 400,
                                         unit_type, self.army_sprite.commander.country)
                    if count == self.selected_unit_index:
                        unit_icon.selected = True
                else:
                    unit_icon = UnitIcon(self, "left", column * 65 + LANDSCAPE_WIDTH - 450, row * 62 + 400, None,
                                         None)
                self.all_sprites.add(unit_icon)
                self.unit_icons.append(unit_icon)
                count += 1


    def disband_selected_unit(self):
        if self.selected_unit_index < len(self.army_sprite.army_composition):
            self.viewing_decision_interface = True
            self.decision_bg = Static(LANDSCAPE_WIDTH // 2, LANDSCAPE_HEIGHT // 2, "decision_interface_bg_small.png",
                                      anchor="center")
            self.all_sprites.add(self.decision_bg)
            self.decision_sprites.add(self.decision_bg)

            self.yes_button = Button(self.decision_bg.rect.left + 200, self.decision_bg.rect.bottom - 100,
                                     "menu_button_yes.png", command=self.confirm_disband_selected_unit)
            self.all_sprites.add(self.yes_button)
            self.decision_sprites.add(self.yes_button)
            self.buttons.append(self.yes_button)

            self.cancel_button = Button(self.decision_bg.rect.right - 200, self.decision_bg.rect.bottom - 100,
                                        "menu_button_cancel.png", command=partial(self.confirm_disband_selected_unit, False))
            self.all_sprites.add(self.cancel_button)
            self.decision_sprites.add(self.cancel_button)
            self.buttons.append(self.cancel_button)

            selected_unit_name = UNIT_FRIENDLY_NAMES[self.info_army_unit_types[self.selected_unit_index]]
            self.decision_text = "Disband this unit: <{}>?".format(selected_unit_name)


    def confirm_disband_selected_unit(self, proceed=True):
        for sp in self.decision_sprites:
            sp.kill()
            if sp in self.buttons:
                self.buttons.remove(sp)
        self.viewing_decision_interface = False
        self.decision_text = ""
        if proceed:
            del self.army_sprite.army_composition[self.info_army_unit_types[self.selected_unit_index]]
            self.reload_unit_icons()


    def upgrade_attribute(self, attribute, cost):
        if attribute == "combat_prowess":
            self.commander.combat_prowess += 1
        if attribute == "intellect":
            self.commander.intellect += 1
        if attribute == "command":
            self.commander.command += 1
        if attribute == "charisma":
            self.commander.charisma += 1
        if attribute == "action_points":
            self.commander.action_points += 1
        self.commander.upgrade_points -= cost
        self.update_upgrade_buttons()
        self.campaign_map_scene.play_sound_effect(self.morale_boost_sound)
        self.recruitment_message = "Spent {} upgrade points to increase {} by 1!".format(cost, attribute)
        self.last_displayed_message = pg.time.get_ticks()


    def update_upgrade_buttons(self):
        if self.commander.upgrade_points < 3:
            self.action_point_button.disabled = True
        if self.commander.upgrade_points < 1:
            self.combat_prowess_button.disabled = True
            self.intellect_button.disabled = True
            self.command_button.disabled = True
            self.charisma_button.disabled = True


    def run(self):
        while self.playing:
            if self.running:
                self.clock.tick(FPS)
                self.events()
                self.update()
                self.draw()


    def events(self):
        pos = pg.mouse.get_pos()

        # gold icon or food icon hover
        if self.gold_icon.rect.collidepoint(pos):
            self.gold_icon.hovering = True
            if self.gold_icon.tooltip_background:
                self.game_tooltip_description = "Projected Tax/Building Income: {}\nProjected Trade Revenue: {}\nProjected Upkeep Cost: {}\nProjected Net Income: {}"
        else:
            self.gold_icon.hovering = False
            if self.food_icon.rect.collidepoint(pos):
                self.food_icon.hovering = True
                if self.food_icon.tooltip_background:
                    self.game_tooltip_description = "Projected Food Production: {}\nProjected Food Consumption: {}\nProjected Food Surplus: {}"
            else:
                self.food_icon.hovering = False
                self.game_tooltip_description = ""

        for event in pg.event.get():
            if event.type == pg.KEYUP:
                if event.key == pg.K_ESCAPE:
                    self.return_to_campaign_map()

            # quit
            if event.type == pg.QUIT:
                choice = pg.display.message_box("Quit",
                                                "Are you sure you wish to quit? You will lose any unsaved progress.",
                                                buttons=("Yes", "No"), return_button=1, escape_button=None
                                                )

                if choice == 0:
                    pg.quit()
                    sys.exit()

            # Left clicks
            if event.type == pg.MOUSEBUTTONDOWN:
                self.campaign_map_scene.play_sound_effect(self.click_sound)
                for button in self.buttons:
                    if button.rect.collidepoint(pos):
                        if button.command:
                            if not button.disabled:
                                button.command()
                                #print("Executing commmand...")
                        break
                for icon in self.unit_icons:
                    if icon.rect.collidepoint(pos):
                        self.unit_icons[self.selected_unit_index].selected = False
                        self.selected_unit_index = self.unit_icons.index(icon)
                        icon.selected = True
                if self.back_button.rect.collidepoint(pos):
                    self.return_to_campaign_map()


        hovering_over_button = False
        for button in self.buttons:
            if button.rect.collidepoint(pos):
                button.hovering = True
                hovering_over_button = True
            else:
                button.hovering = False

        #sp_atk buttons with tooltips
        for icon in self.sp_atk_icons_list+self.skill_icons_list:
            if icon.rect.collidepoint(pos):
                icon.hovering = True
                hovering_over_button = True
                if icon.tooltip:
                    self.tooltip_description = icon.tooltip_description
                    self.tooltip_description_x = icon.tooltip.rect.left
                    self.tooltip_description_y = icon.tooltip.rect.top
            else:
                icon.hovering = False
        if not hovering_over_button:
            self.tooltip_description = ""
            self.tooltip_description_x = -3000
            self.tooltip_description_y = -3000


    def return_to_campaign_map(self):
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = self.return_to_campaign_map
        elif not self.transition.fading:
            self.transition_clean_up()
            campaign_data = pickle.load(open(os.path.join(SAVE_SLOTS_DIR, "army_management_autosave.p"), "rb"))
            campaign_data["gold"] = self.campaign_map_scene.gold
            campaign_data["advice_given"] = self.campaign_map_scene.advice_given
            campaign_data["show_advice"] = self.campaign_map_scene.show_advice
            for army_sprite in campaign_data["army"]:
                if army_sprite.commander.name == self.commander.name:
                    army_sprite.commander = self.commander
                    army_sprite.army_composition = self.army_sprite.army_composition
                if self.army_sprite.embedded_in_city:
                    for city_sprite in campaign_data["city"]:
                        if city_sprite.name == self.army_sprite.embedded_in_city:
                            city_sprite.garrison = self.army_sprite.army_composition
            pickle.dump(campaign_data, open(os.path.join(SAVE_SLOTS_DIR, "army_management_autosave.p"), "wb"))

            for sp in self.all_sprites:
                sp.kill()
            self.campaign_map_scene.new(load_from="army_management_autosave.p")


    def update(self):
        now = pg.time.get_ticks()
        self.all_sprites.update()
        if now - self.last_displayed_message > 2000:
            self.recruitment_message = ""
        if self.transition:
            if self.transition.fading:
                self.transition.update()
            else:
                if self.post_transition_function:
                    self.post_transition_function()
                else:
                    self.transition = None


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)

            estimated_gold_delta = self.campaign_map_scene.calculate_projected_gold_delta(self.campaign_map_scene.player_country)
            estimated_food_delta = self.campaign_map_scene.calculate_projected_food_delta(self.campaign_map_scene.player_country)
            gold_label = "{} ({})".format(self.campaign_map_scene.gold[self.campaign_map_scene.player_country], estimated_gold_delta)
            food_label = "{} ({})".format(self.campaign_map_scene.food[self.campaign_map_scene.player_country], estimated_food_delta)
            gold_label_color = WHITE
            food_label_color = WHITE
            if estimated_gold_delta < 0:
                gold_label_color = DARK_RED
            if estimated_food_delta < 0:
                food_label_color = DARK_RED

            self.draw_text(gold_label, FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE - len(gold_label) // 6), gold_label_color,
                           55, 28, align="midleft")
            self.draw_text(food_label, FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE - len(food_label) // 6), food_label_color,
                           270, 27, align="midleft")


            # draw commander stats text
            self.draw_text(self.commander.name, FONT_FUNCTION(DEFAULT_FONT, 24), WHITE, 150, 60, align="midleft")
            self.draw_text("Level: {}".format(self.commander.level), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 150,
                           340 + 20, align="midleft")
            if self.commander.level < 10:
                self.draw_text("Exp: {}/{}".format(self.commander.experience, self.commander.level_thresholds[self.commander.level]),
                    FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 150, 340 + 60, align="midleft")
            else:
                self.draw_text("Exp: --/--", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 150, 340 + 60, align="midleft")
            self.draw_text("Age: {}".format(self.commander.age), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 150,
                           340 + 100, align="midleft")
            self.draw_text("Faction: {}".format(self.commander.country), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                           150, 340 + 140, align="midleft")
            self.draw_text("Loyalty: {}".format(self.commander.loyalty), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                           150, 340 + 180, align="midleft")
            self.draw_text("Combat Prowess: {}".format(self.commander.combat_prowess),
                           FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), YELLOW, 380, 340 + 20, align="midleft")
            self.draw_text("Intellect: {}".format(self.commander.intellect), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                           YELLOW, 380, 340 + 60, align="midleft")
            self.draw_text("Command: {}".format(self.commander.command), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                           YELLOW, 380, 340 + 100, align="midleft")
            self.draw_text("Charisma: {}".format(self.commander.charisma), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                           YELLOW, 380, 340 + 140, align="midleft")
            self.draw_text("Action Pts: {}".format(self.commander.action_points), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                           YELLOW, 380, 340 + 180, align="midleft")

            if self.commander.upgrade_points > 0:
                color = GREEN
            else:
                color = RED
            self.draw_text("Upgrade Pts: {}".format(self.commander.upgrade_points), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                           color, 380, 340 + 220, align="midleft")

            self.draw_text("Special Attacks:", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 350, 90, align="midleft")
            self.draw_text("Skills:", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, 350, 210, align="midleft")
            self.draw_text("Army:", FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, LANDSCAPE_WIDTH-450, 375, align="midleft")


            for i in range(len(self.info_army_unit_types)):
                unit_type = self.info_army_unit_types[i]
                text_col = i % 4
                text_row = i // 4
                text_x, text_y = text_col * 65 + LANDSCAPE_WIDTH-450 + 65 - 10, text_row * 62 + 62 - 15 + 400
                self.draw_text("{}".format(self.army_sprite.army_composition[unit_type]),
                               FONT_FUNCTION(DEFAULT_FONT, 14),
                               WHITE, text_x, text_y)


            for lock_icon, level_req in self.lock_icon_values.items():
                self.draw_text("Lv. {}".format(level_req),
                               FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), RED, lock_icon.rect.centerx, lock_icon.rect.centery-5)


            if self.tooltip_description:
                self.priority_sprites.draw(self.screen)
                #self.drawWrappedText(self.tooltip_description, WHITE,
                #                     pg.Rect(self.tooltip_description_x + 16, self.tooltip_description_y + 20,
                #                             268, 360), FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE))
                self.draw_text(self.tooltip_description, FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE),
                               WHITE, self.tooltip_description_x + 16, self.tooltip_description_y + 20,
                               align="topleft", max_width=268)
                #description_split = self.tooltip_description.split("\n")
                #counter = 1
                #for line in description_split:
                #    self.draw_text(line, FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE, self.tooltip_description_x+15,
                #                   self.tooltip_description_y+25*counter, align="midleft")
                #    counter += 1


            if self.game_tooltip_description:
                self.priority_sprites.draw(self.screen)
                if "Income" in self.game_tooltip_description:
                    projected_tax_and_additional_income = self.campaign_map_scene.calculate_tax_and_additional_income(self.campaign_map_scene.player_country)
                    projected_trade_revenue = self.campaign_map_scene.calculate_total_trade_revenue(self.campaign_map_scene.player_country)
                    projected_upkeep_cost = self.campaign_map_scene.calculate_upkeep_cost(self.campaign_map_scene.player_country)
                    projected_total_income = self.campaign_map_scene.calculate_projected_gold_delta(self.campaign_map_scene.player_country)
                    tooltip_description_split = self.game_tooltip_description.split("\n")

                    self.draw_text(tooltip_description_split[0].format(projected_tax_and_additional_income),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                   self.gold_icon.tooltip_background.rect.left + 20,
                                   self.gold_icon.tooltip_background.rect.top + 30, align="midleft")

                    self.draw_text(tooltip_description_split[1].format(projected_trade_revenue),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                   self.gold_icon.tooltip_background.rect.left + 20,
                                   self.gold_icon.tooltip_background.rect.top + 60, align="midleft")

                    self.draw_text(tooltip_description_split[2].format(projected_upkeep_cost),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), RED,
                                   self.gold_icon.tooltip_background.rect.left + 20,
                                   self.gold_icon.tooltip_background.rect.top + 90, align="midleft")

                    if projected_total_income >= 0:
                        self.draw_text(tooltip_description_split[3].format(projected_total_income),
                                       FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                       self.gold_icon.tooltip_background.rect.left + 20,
                                       self.gold_icon.tooltip_background.rect.top + 120, align="midleft")
                    else:
                        self.draw_text(tooltip_description_split[3].format(projected_total_income),
                                       FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), RED,
                                       self.gold_icon.tooltip_background.rect.left + 20,
                                       self.gold_icon.tooltip_background.rect.top + 120, align="midleft")

                else:
                    projected_food_production = self.campaign_map_scene.calculate_food_production(self.campaign_map_scene.player_country)
                    projected_food_consumption = self.campaign_map_scene.calculate_food_consumption(self.campaign_map_scene.player_country)
                    projected_food_surplus = self.campaign_map_scene.calculate_projected_food_delta(self.campaign_map_scene.player_country)
                    tooltip_description_split = self.game_tooltip_description.split("\n")

                    self.draw_text(tooltip_description_split[0].format(projected_food_production),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                   self.food_icon.tooltip_background.rect.left + 20,
                                   self.food_icon.tooltip_background.rect.top + 30, align="midleft")

                    self.draw_text(tooltip_description_split[1].format(projected_food_consumption),
                                   FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), RED,
                                   self.food_icon.tooltip_background.rect.left + 20,
                                   self.food_icon.tooltip_background.rect.top + 60, align="midleft")

                    if projected_food_surplus >= 0:
                        self.draw_text(tooltip_description_split[2].format(projected_food_surplus),
                                       FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                                       self.food_icon.tooltip_background.rect.left + 20,
                                       self.food_icon.tooltip_background.rect.top + 90, align="midleft")
                    else:
                        self.draw_text(tooltip_description_split[2].format(projected_food_surplus),
                                       FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), RED,
                                       self.food_icon.tooltip_background.rect.left + 20,
                                       self.food_icon.tooltip_background.rect.top + 90, align="midleft")


            self.decision_sprites.draw(self.screen)
            if self.decision_text:
                self.draw_text(self.decision_text, FONT_FUNCTION(DEFAULT_FONT, 26), BLACK, LANDSCAPE_WIDTH//2,
                               LANDSCAPE_HEIGHT//2-100, align="center")

            self.transition_sprites.draw(self.screen)
            pg.display.flip()


    def draw_text(self, text, font, color, x, y, align="midtop", max_width=None):
        if max_width:
            text_surface = font.render(text, True, color, None, max_width)
        else:
            text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        if align == "midleft":
            text_rect.midleft = (x, y)
        elif align == "topleft":
            text_rect.topleft = (x, y)
        else:
            text_rect.midtop = (x, y)
        self.screen.blit(text_surface, text_rect)

    #def draw_text(self, text, font, color, x, y, align="midtop"):
    #    text_surface = font.render(text, True, color)
    #    text_rect = text_surface.get_rect()
    #    if align == "midleft":
    #        text_rect.midleft = (x, y)
    #    elif align == "topleft":
    #        text_rect.topleft = (x, y)
    #    elif align == "center":
    #        text_rect.center = (x, y)
    #    else:
    #        text_rect.midtop = (x, y)
    #    self.screen.blit(text_surface, text_rect)


    def drawWrappedText(self, text, color, rect, font, aa=False, bkg=None):
        rect = pg.Rect(rect)
        y = rect.top
        lineSpacing = 10

        # get the height of the font
        fontHeight = font.size("Tg")[1]

        while text:
            i = 1

            # determine if the row of text will be outside our area
            if y + fontHeight > rect.bottom:
                break

            # determine maximum width of line
            while font.size(text[:i])[0] < rect.width and i < len(text):
                i += 1

            # if we've wrapped the text, then adjust the wrap to the last word
            if i < len(text):
                i = text.rfind(" ", 0, i) + 1

            # render the line and blit it to the surface
            if bkg:
                image = font.render(text[:i], 1, color, bkg)
                image.set_colorkey(bkg)
            else:
                image = font.render(text[:i], aa, color)

            self.screen.blit(image, (rect.left, y))
            y += fontHeight + lineSpacing

            # remove the text we just blitted
            text = text[i:]

        return text