from math import atan, pi, log
from settings import*
from random import*
from helper_functions import*
import pygame as pg


vec = pg.math.Vector2


class FadeBlackTransition(pg.sprite.Sprite):
    def __init__(self, alpha_delta, direction="out"):
        pg.sprite.Sprite.__init__(self)
        self.direction = direction
        if self.direction == "out":
            self.alpha = 0
        else:
            self.alpha = 255
        self.alpha_delta = alpha_delta
        self.fading = True

        if DEMO:
            self.image = pg.Surface((LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT)).convert_alpha()
        else:
            info = pg.display.Info()
            self.image = pg.Surface((info.current_w, info.current_h)).convert_alpha()
        self.image.fill((0, 0, 0, self.alpha))
        self.rect = self.image.get_rect()
        self.rect.topleft = (0,0)


    def update(self):
        if self.fading:
            if self.direction == "out":
                self.alpha += self.alpha_delta
            else:
                self.alpha -= self.alpha_delta

            if self.alpha > 255:
                self.alpha = 255
                self.fading = False
            elif self.alpha < 0:
                self.alpha = 0
                self.fading = False

            self.image.fill((0, 0, 0, self.alpha))


class Unit(pg.sprite.Sprite):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot, wal, aal,
                 health, attack, defense, walking_acc, attacking_range, damage_frame,
                 fatal_strike_rate=0.0, unit_effects = {},
                 unit_class="", firing_rate=0.0, firing_accuracy=0.0, firing_damage=0.0,
                 min_charge_range=0, max_charge_range=0, charge_effects={}):

        pg.sprite.Sprite.__init__(self)
        self.game = game
        self.unit_type = unit_type
        self.unit_country = unit_country
        if self.unit_country not in ["shu", "wei", "wu"]:
            self.unit_country = "shu"
        self.side = side
        self.slot = slot
        self.walking_animation_length = wal
        self.attacking_animation_length = aal
        if self.side == "left":
            self.enemy_sprites = self.game.enemy_unit_sprites
        else:
            self.enemy_sprites = self.game.player_unit_sprites

        self.attacking = False
        self.attacking_range = attacking_range
        self.charging = False
        self.min_charge_range = min_charge_range
        self.max_charge_range = max_charge_range
        if self.game.terrain == "rock":
            self.max_charge_range = int(self.max_charge_range*.75)
        self.charge_effects = charge_effects

        self.damage_frame = damage_frame
        self.unit_effects = unit_effects
        self.fatal_strike_rate = fatal_strike_rate

        self.last_update = 0
        self.last_special_effect_activation = 0
        self.position_x = x
        self.position_y = y
        self.current_frame = 0
        self.stun_count = 0
        self.friction = PLAYER_FRICTION

        self.health = health
        self.health_upper_limit = health
        self.attack = attack
        self.defense = defense
        self.base_walking_acc = walking_acc
        self.walking_acc = walking_acc
        self.walking_speed = 0
        if unit_class in RANGED_UNITS:
            self.unit_class = "ranged"
        else:
            self.unit_class = unit_class

        self.firing_rate = firing_rate
        self.firing_accuracy = firing_accuracy
        self.firing_damage = firing_damage
        self.last_fired = 0

        self.load_animations()
        self.image = self.walking_animations[0]
        self.rect = self.image.get_rect()
        if self.side == "left":
            self.rect.left, self.rect.centery = x, y
        else:
            self.rect.right, self.rect.centery = x, y

        if self.game:
            self.generate_health_bar()

    def generate_health_bar(self):
        self.health_bar_back = UnitHealthBar(self.rect.centerx, self.rect.top, "generic_health_bar_back.png",
                                             self.game,
                                             master=self, dynamic=False)
        self.health_bar_front = UnitHealthBar(self.rect.centerx, self.rect.top, "generic_health_bar_front.png",
                                              self.game, master=self, dynamic=True)
        self.game.all_sprites.add(self.health_bar_back)
        self.game.all_sprites.add(self.health_bar_front)


    def load_animations(self):
        if self.unit_type == "flaming_arrow_infantry":
            image_unit_type = "bow_infantry"
        else:
            image_unit_type = self.unit_type
        self.walking_animations = [pg.image.load("sprite_{}_walking_{}_{}.png".format(image_unit_type, i, self.unit_country)).convert_alpha() for i in range(1,self.walking_animation_length+1)]
        self.attacking_animations = [pg.image.load("sprite_{}_attacking_{}_{}.png".format(image_unit_type, i, self.unit_country)).convert_alpha() for i in range(1,self.attacking_animation_length+1)]
        if (self.side == "right" and self.unit_type != "catapult") or (self.side == "left" and self.unit_type == "catapult"):
            for i in range(len(self.walking_animations)):
                frame = self.walking_animations[i]
                self.walking_animations[i] = pg.transform.flip(frame, True, False)
            for i in range(len(self.attacking_animations)):
                frame = self.attacking_animations[i]
                self.attacking_animations[i] = pg.transform.flip(frame, True, False)



    def update(self):

        if self.stun_count > 0:
            self.attacking = False
            self.walking_speed = 0
            self.stun_count -= 1

        if self.charging:
            self.walking_acc = 3*self.base_walking_acc

        self.enemy_sprites_same_slot = pg.sprite.Group()
        for enemy_sprite in self.enemy_sprites:
            if self.slot == enemy_sprite.slot:
                if self.side == "left" and self.rect.left < enemy_sprite.rect.right:
                    self.enemy_sprites_same_slot.add(enemy_sprite)
                if self.side == "right" and self.rect.right > enemy_sprite.rect.left:
                        self.enemy_sprites_same_slot.add(enemy_sprite)


        if not self.attacking:
            if self.side == "left":
                self.walking_speed += self.walking_acc/5
                if self.walking_speed >= self.walking_acc:
                    self.walking_speed = self.walking_acc
                self.position_x += self.walking_speed
                self.rect.left = self.position_x

            else:
                self.walking_speed += self.walking_acc/5
                if self.walking_speed >= self.walking_acc:
                    self.walking_speed = self.walking_acc
                self.position_x -= self.walking_speed
                self.rect.right = self.position_x

            if self.unit_class == "cavalry":
                closest_enemy_distance = 999999999
                for enemy_unit in self.enemy_sprites_same_slot:
                    if abs(self.rect.centerx - enemy_unit.rect.centerx) <= closest_enemy_distance:
                        closest_enemy_distance = abs(self.rect.centerx - enemy_unit.rect.centerx)

                if self.unit_class == "cavalry" and closest_enemy_distance <= self.max_charge_range and closest_enemy_distance >= self.min_charge_range and not self.charging:
                    self.charging = True
                    if self.charge_effects["sfx"]:
                        charging_sfx = pg.mixer.Sound(self.charge_effects["sfx"])
                        charging_sfx.play()

            for enemy_unit in self.enemy_sprites_same_slot:
                if abs(self.rect.centerx - enemy_unit.rect.centerx) <= self.attacking_range:
                    self.attacking = True
                    self.current_frame = 0
                    break

            #check for wall collision
            if self.game.siege_defense == "left" and self.side == "right":
                if self.rect.left - self.game.wall.rect.right <= self.attacking_range - (50 + self.slot*60):
                    self.attacking = True
                    self.current_frame = 0
            elif self.game.siege_defense == "right" and self.side == "left":
                if self.game.wall.rect.left - self.rect.right <= self.attacking_range - (50 + self.slot*60):
                    self.attacking = True
                    self.current_frame = 0

            #check for barricade collision
            for obstacle in self.game.obstacle_sprites:
                if obstacle.unit_type == "barricade":
                    if self.side != obstacle.side and abs(self.rect.centerx-obstacle.rect.centerx) <= self.attacking_range and self.slot in obstacle.slots:
                        self.attacking = True
                        self.current_frame = 0
                        break


        else:
            enemy_collision = pg.sprite.spritecollide(self, self.enemy_sprites_same_slot, False)
            if self.game.siege_defense == "left" and self.side == "right":
                if self.rect.left - self.game.wall.rect.right <= self.attacking_range - (50 + self.slot*60):
                    enemy_collision = [self.game.wall]
            elif self.game.siege_defense == "right" and self.side == "left":
                if self.game.wall.rect.left - self.rect.right <= self.attacking_range - (50 + self.slot*60):
                    enemy_collision = [self.game.wall]

            for obstacle in self.game.obstacle_sprites:
                if obstacle.unit_type == "barricade":
                    if self.side != obstacle.side and abs(self.rect.centerx-obstacle.rect.centerx) <= self.attacking_range and self.slot in obstacle.slots:
                        enemy_collision = [obstacle]

            if self.unit_class == "ranged":
                enemies_within_range = []
                for enemy_unit in self.enemy_sprites_same_slot:
                    if abs(self.rect.centerx - enemy_unit.rect.centerx) <= self.attacking_range:
                        enemies_within_range.append(enemy_unit)

                for obstacle in self.game.obstacle_sprites:
                    if obstacle.unit_type == "barricade":
                        if self.side != obstacle.side and abs(self.rect.centerx - obstacle.rect.centerx) <= self.attacking_range and self.slot in obstacle.slots:
                            enemies_within_range.append(obstacle)

                if self.game.siege_defense == "left" and self.side == "right":
                    if abs(self.rect.left - self.game.wall.rect.right) <= self.attacking_range and not enemies_within_range: #only fire at wall if no other enemies in range
                        enemies_within_range.append(self.game.wall)
                elif self.game.siege_defense == "right" and self.side == "left":
                    if abs(self.rect.right - self.game.wall.rect.left) <= self.attacking_range and not enemies_within_range: #only fire at wall if no other enemies in range
                        enemies_within_range.append(self.game.wall)


                if enemies_within_range:
                    if self.current_frame == self.damage_frame:
                        self.current_frame += 1
                        self.fire_projectile(enemies_within_range)
                else:
                    self.attacking = False

            else:
                if not enemy_collision:
                    self.attacking = False
                else:
                    if self.charging:
                        self.charging = False
                        self.walking_acc = self.base_walking_acc
                        closest_enemy = enemy_collision[0]
                        if random() <= self.fatal_strike_rate and closest_enemy not in self.game.obstacle_sprites:
                            damage_dealt = closest_enemy.health
                        else:
                            damage_dealt = ((self.charge_effects["damage"] + 2) / (closest_enemy.defense//2+1)) ** 1.5 * randrange(8, 11)

                        if self.game.terrain in ["rock", "forest"]:
                            damage_dealt = damage_dealt//2
                        closest_enemy.got_hit(damage_dealt, self.charge_effects["stun"], self.charge_effects["repel"])

                        if closest_enemy.unit_type not in ["wall", "barricade"]:
                            if "vengeance" in closest_enemy.unit_effects:
                                self.got_hit(damage_dealt//2)

                    else:
                        if self.current_frame == self.damage_frame:
                            self.current_frame += 1
                            closest_enemy = enemy_collision[0]
                            if random() <= self.fatal_strike_rate and closest_enemy not in self.game.obstacle_sprites and closest_enemy.unit_class != "cavalry":
                                damage_dealt = closest_enemy.health
                            else:
                                damage_dealt = ((self.attack+2)/(closest_enemy.defense+2))**1.5*randrange(8,11)

                            if self.unit_class == "spear" and closest_enemy.unit_class == "cavalry":
                                damage_dealt *= 3

                            stun = 0
                            repel = 0
                            if "stun" in self.unit_effects:
                                stun = self.unit_effects["stun"]
                            if "repel" in self.unit_effects:
                                repel = self.unit_effects["repel"]
                            closest_enemy.got_hit(damage_dealt, stun, repel)

                            if closest_enemy.unit_type not in ["wall", "barricade"]:
                                if "vengeance" in closest_enemy.unit_effects:
                                    self.got_hit(damage_dealt // 2)
                                if "piercing_blade" in self.unit_effects and "immunity" not in closest_enemy.unit_effects:
                                    if "bleeding" in closest_enemy.unit_effects:
                                        closest_enemy.unit_effects["bleeding"] += self.unit_effects["piercing_blade"]
                                    else:
                                        closest_enemy.unit_effects["bleeding"] = self.unit_effects["piercing_blade"]

        # check for passive effects
        if "heal" in self.unit_effects:
            now = pg.time.get_ticks()
            if now - self.last_special_effect_activation >= 1000 and self.health < self.health_upper_limit:
                self.health += self.unit_effects["heal"]
                self.last_special_effect_activation = now

        if "bleeding" in self.unit_effects:
            self.got_hit(self.unit_effects["bleeding"])

        #if "poison" in self.unit_effects and random() <= .1:
        #    self.got_hit(int(self.unit_effects["poison"]/20*self.health_upper_limit))

        self.animate()

        if self.rect.centerx <= -50 - self.game.camera_location and self.side == "right":
            if self.health <= 100:
                morale_dmg = self.health/20
            else:
                morale_dmg = (self.health-100)/30 + 5
            if "morale_damage" in self.unit_effects:
                morale_dmg *= self.unit_effects["morale_damage"]
            self.game.morale_change(side="left", amount=-morale_dmg)
            if "inspiration" in self.unit_effects:
                for unit in self.game.enemy_unit_sprites:
                    unit.attack += 1
                    unit.walking_acc += 1
            self.health_bar_back.kill()
            self.health_bar_front.kill()
            self.kill()

        elif self.rect.centerx >= self.game.max_camera_location-self.game.camera_location + 50 and self.side == "left":
            if self.health <= 100:
                morale_dmg = self.health/20
            else:
                morale_dmg = (self.health-100)/30 + 5
            if "morale_damage" in self.unit_effects:
                morale_dmg *= self.unit_effects["morale_damage"]
            self.game.morale_change(side="right", amount=-morale_dmg)
            if "inspiration" in self.unit_effects:
                for unit in self.game.player_unit_sprites:
                    unit.attack += 1
                    unit.walking_acc += 1
            self.health_bar_back.kill()
            self.health_bar_front.kill()
            self.kill()



    def fire_projectile(self, enemies_within_range):
        closest_enemy_distance = 999999999
        closest_enemy = None
        for enemy_unit in enemies_within_range:
            dist = abs(self.rect.centerx - enemy_unit.rect.centerx)
            if dist < closest_enemy_distance:
                closest_enemy_distance = dist
                closest_enemy = enemy_unit

        if self.unit_type == "flaming_arrow_infantry":
            projectile = "flaming_arrow"
        elif self.unit_type == "crossbow_infantry":
            projectile = "crossbow_bolt"
        elif self.unit_type == "catapult":
            projectile = "catapult_rock"
        else:
            projectile = "arrow"
        self.game.generate_projectile(self.side, projectile, self, closest_enemy)



    def animate(self, attacking_frame_delta=50):
        walking_frame_delta = int(350/self.walking_acc)
        if walking_frame_delta >= 100:
            walking_frame_delta = 100
        elif walking_frame_delta <= 30:
            walking_frame_delta = 30


        now = pg.time.get_ticks()
        if not self.attacking:
            if now - self.last_update > walking_frame_delta:
                self.last_update = now
                self.current_frame = (self.current_frame + 1)%len(self.walking_animations)
                self.image = self.walking_animations[self.current_frame]

        else:
            if now - self.last_update > attacking_frame_delta:
                self.last_update = now
                if self.unit_class == "ranged" and self.current_frame == 0:
                    if now - self.last_fired >= 3000/self.firing_rate:
                        self.current_frame += 1
                        self.last_fired = now
                else:
                    self.current_frame = (self.current_frame + 1) % len(self.attacking_animations)

                self.image = self.attacking_animations[self.current_frame]
                self.rect = self.image.get_rect()
                if self.side == "right":
                    self.rect.right = self.position_x
                    self.rect.centery = self.position_y
                else:
                    self.rect.left = self.position_x
                    self.rect.centery = self.position_y


    def got_hit(self, damage, stun=0, repel=0, damage_type=None):
        if damage == 0:
            damage = 1
        if "blocking" in self.unit_effects:
            if random() <= self.unit_effects["blocking"]: #if attack is blocked, don't do anything
                return
        self.health -= damage
        if repel > 0 and self.unit_class != "cavalry" and "immunity" not in self.unit_effects:
            if self.side == "left":
                self.position_x -= repel*self.image.get_width()
            else:
                self.position_x += repel*self.image.get_width()

        if stun > 0 and self.unit_class != "cavalry" and "immunity" not in self.unit_effects:
            self.stun_count += stun

        #check if unit is chained with other chained units
        if self.side == "left" and damage_type=="fire" and "chained" in self.unit_effects:
            for unit in self.game.player_unit_sprites:
                if "chained" in unit.unit_effects:
                    unit.got_hit(int(damage*.3+1), 0, 0, damage_type=None) #setting type to None to prevent endless chaining

        elif self.side == "right" and damage_type=="fire" and "chained" in self.unit_effects:
            for unit in self.game.enemy_unit_sprites:
                if "chained" in unit.unit_effects:
                    unit.got_hit((damage*.3+1), 0, 0, damage_type=None)

        if self.health <= 0:
            self.health = 0
            self.health_bar_back.kill()
            self.health_bar_front.kill()
            self.kill()
            if self.unit_class == "ranged":
                self.game.number_of_ranged_units -= 1
            if self.side == "left":
                if self.game.kill_streak <= 0:
                    self.game.kill_streak -= 1
                else:
                    self.game.kill_streak = 0

                if "bloodthirst" in [effect for effect, effect_timestamp in self.game.enemy_general_effects]:
                    self.game.morale_change("right", 3)

            else:
                if self.game.kill_streak >= 0:
                    self.game.kill_streak += 1
                else:
                    self.game.kill_streak = 0

                if "bloodthirst" in [effect for effect, effect_timestamp in self.game.player_general_effects]:
                    self.game.morale_change("left", 3)
                    print("bloodthirst activated", self.game.player_morale)


class UnitHealthBar(pg.sprite.Sprite):
    def __init__(self, x, y, filename, game, master, dynamic=True):
        pg.sprite.Sprite.__init__(self)
        self.game = game
        self.filename = filename
        self.master = master
        self.max = self.master.health
        self.dynamic = dynamic

        self.image = pg.image.load(self.filename)
        self.image = pg.transform.smoothscale(self.image, (20, 3))
        self.rect = self.image.get_rect()
        self.rect.left = x-10
        self.rect.centery = y

    def update(self):
        if self.dynamic:
            self.image = pg.transform.smoothscale(self.image, (int(self.master.health / self.max * 20), 3))
        self.rect = self.image.get_rect()
        self.rect.left, self.rect.centery = self.master.rect.centerx-10, self.master.rect.top
        self.rect.left, self.rect.centery = self.master.rect.centerx-10, self.master.rect.top



class Spear_Militia(Unit):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot):
        Unit.__init__(self, game, x, y, unit_type, unit_country, side, slot, wal=8, aal=8,
                      health=100, attack=4, defense=4, walking_acc=3,
                      attacking_range=50, damage_frame=6, fatal_strike_rate=.01, unit_effects={},
                      unit_class="spear")


class Spear_Infantry(Unit):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot):
        Unit.__init__(self, game, x, y, unit_type, unit_country, side, slot, wal=8, aal=8,
                      health=160, attack=6, defense=7, walking_acc=3,
                      attacking_range=50, damage_frame=6, fatal_strike_rate=.02, unit_effects={},
                      unit_class="spear")


class Longspear_Infantry(Unit):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot):
        Unit.__init__(self, game, x, y, unit_type, unit_country, side, slot, wal=4, aal=8,
                      health=160, attack=9, defense=5, walking_acc=2,
                      attacking_range=110, damage_frame=6, fatal_strike_rate=.02, unit_effects={"repel":1, "stun":5},
                      unit_class="spear")


class Elite_Spear_Infantry(Unit):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot):
        Unit.__init__(self, game, x, y, unit_type, unit_country, side, slot, wal=9, aal=6,
                      health=200, attack=11, defense=8, walking_acc=4,
                      attacking_range=110, damage_frame=5, fatal_strike_rate=.05, unit_effects={"repel":1.5, "stun":10},
                      unit_class="spear")


class Barbarian_Spear_Woman(Unit):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot):
        Unit.__init__(self, game, x, y, unit_type, unit_country, side, slot, wal=8, aal=8,
                      health=120, attack=9, defense=1, walking_acc=5,
                      attacking_range=50, damage_frame=6, fatal_strike_rate=.12, unit_effects={"repel":1.5, "stun":7},
                      unit_class="spear")


class Rattan_Armor_Infantry(Unit):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot):
        Unit.__init__(self, game, x, y, unit_type, unit_country, side, slot, wal=8, aal=7,
                      health=150, attack=4, defense=6, walking_acc=5, attacking_range=50, damage_frame=6,
                      fatal_strike_rate=.05, unit_effects = {},  unit_class="spear")


class Barbarian_Assassin(Unit):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot):
        Unit.__init__(self, game, x, y, unit_type, unit_country, side, slot, wal=8, aal=6,
                      health=100, attack=5, defense=1, walking_acc=7,
                      attacking_range=10, damage_frame=5, fatal_strike_rate=.2, unit_effects={},
                      unit_class="blade")


class Barbarian_Archer(Unit):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot):
        Unit.__init__(self, game, x, y, unit_type, unit_country, side, slot, wal=4, aal=14,
                      health=100, attack=0, defense=1, walking_acc=3,
                      attacking_range=1300, damage_frame=9, fatal_strike_rate=.05, unit_effects={},
                      unit_class="ranged", firing_rate=1.2, firing_accuracy=.65, firing_damage=6)


class Bow_Militia(Unit):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot):
        Unit.__init__(self, game, x, y, unit_type, unit_country, side, slot, wal=4, aal=14,
                      health=80, attack=0, defense=1, walking_acc=2,
                      attacking_range=1100, damage_frame=9, fatal_strike_rate=.03, unit_effects={},
                      unit_class="ranged", firing_rate=1.0, firing_accuracy=.5, firing_damage=5)


class Bow_Infantry(Unit):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot):
        Unit.__init__(self, game, x, y, unit_type, unit_country, side, slot, wal=4, aal=14,
                      health=120, attack=0, defense=2, walking_acc=2,
                      attacking_range=1400, damage_frame=9, fatal_strike_rate=.04, unit_effects={},
                      unit_class="ranged", firing_rate=1.3, firing_accuracy=.75, firing_damage=6)


class Flaming_Arrow_Infantry(Unit):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot):
        Unit.__init__(self, game, x, y, unit_type, unit_country, side, slot, wal=4, aal=14,
                      health=120, attack=0, defense=2, walking_acc=2,
                      attacking_range=1400, damage_frame=9, fatal_strike_rate=.05, unit_effects={},
                      unit_class="ranged", firing_rate=1.2, firing_accuracy=.75, firing_damage=9)


class Crossbow_Infantry(Unit):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot):
        Unit.__init__(self, game, x, y, unit_type, unit_country, side, slot, wal=8, aal=8,
                      health=120, attack=0, defense=2, walking_acc=1,
                      attacking_range=1000, damage_frame=6, fatal_strike_rate=.2, unit_effects={},
                      unit_class="ranged", firing_rate=1.0, firing_accuracy=.9, firing_damage=12)


class Catapult(Unit):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot):
        Unit.__init__(self, game, x, y, unit_type, unit_country, side, slot, wal=6, aal=6,
                      health=500, attack=0, defense=5, walking_acc=1,
                      attacking_range=2000, damage_frame=4, fatal_strike_rate=.25, unit_effects={"immunity":1, "stun":5, "repel":2},
                      unit_class="ranged", firing_rate=.5, firing_accuracy=.25, firing_damage=25)


class Axe_Militia(Unit):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot):
        Unit.__init__(self, game, x, y, unit_type, unit_country, side, slot, wal=9, aal=5,
                      health=90, attack=2, defense=2, walking_acc=6,
                      attacking_range=20, damage_frame=4, fatal_strike_rate=.04, unit_effects={},
                      unit_class="axe")


class Sword_Militia(Unit):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot):
        Unit.__init__(self, game, x, y, unit_type, unit_country, side, slot, wal=8, aal=8,
                      health=100, attack=3, defense=3, walking_acc=5,
                      attacking_range=30, damage_frame=5, fatal_strike_rate=.02, unit_effects={},
                      unit_class="blade")


class Light_Dagger_Infantry(Unit):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot):
        Unit.__init__(self, game, x, y, unit_type, unit_country, side, slot, wal=9, aal=6,
                      health=120, attack=4, defense=3, walking_acc=6,
                      attacking_range=10, damage_frame=5, fatal_strike_rate=.08, unit_effects={"piercing_blade":1},
                      unit_class="blade")


class Sword_Infantry(Unit):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot):
        Unit.__init__(self, game, x, y, unit_type, unit_country, side, slot, wal=8, aal=8,
                      health=160, attack=5, defense=5, walking_acc=5,
                      attacking_range=30, damage_frame=5, fatal_strike_rate=.04, unit_effects={},
                      unit_class="blade")

class Shielded_Sword_Infantry(Unit):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot):
        Unit.__init__(self, game, x, y, unit_type, unit_country, side, slot, wal=9, aal=8,
                      health=200, attack=4, defense=9, walking_acc=2,
                      attacking_range=30, damage_frame=5, fatal_strike_rate=.04, unit_effects={"immunity":1},
                      unit_class="blade")

class Elite_Sword_Infantry(Unit):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot):
        Unit.__init__(self, game, x, y, unit_type, unit_country, side, slot, wal=9, aal=6,
                      health=220, attack=7, defense=10, walking_acc=3,
                      attacking_range=30, damage_frame=5, fatal_strike_rate=.08, unit_effects={"immunity":1, "blocking":.05},
                      unit_class="blade")


class Mace_Infantry(Unit):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot):
        Unit.__init__(self, game, x, y, unit_type, unit_country, side, slot, wal=9, aal=10,
                      health=160, attack=10, defense=4, walking_acc=5,
                      attacking_range=30, damage_frame=9, fatal_strike_rate=.05, unit_effects={"morale_damage":2},
                      unit_class="other")


class Battleaxe_Infantry(Unit):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot):
        Unit.__init__(self, game, x, y, unit_type, unit_country, side, slot, wal=9, aal=8,
                      health=160, attack=7, defense=5, walking_acc=4,
                      attacking_range=30, damage_frame=7, fatal_strike_rate=.15,
                      unit_effects = {}, unit_class="axe")



class Elephant_Rider(Unit):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot):
        Unit.__init__(self, game, x, y, unit_type, unit_country, side, slot, wal=3, aal=8,
                      health=400, attack=9, defense=8, walking_acc=2,
                      attacking_range=50, damage_frame=6, fatal_strike_rate=.1, unit_effects={},
                      unit_class="cavalry", firing_rate=0, firing_accuracy=0, firing_damage=0,
                      min_charge_range=300, max_charge_range=800, charge_effects={"damage":200, "repel":5, "stun":10, "sfx":"sfx_elephant.ogg"})


class Halberd_Cavalry(Unit):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot):
        Unit.__init__(self, game, x, y, unit_type, unit_country, side, slot, wal=8, aal=6,
                      health=250, attack=6, defense=5, walking_acc=4,
                      attacking_range=50, damage_frame=5, fatal_strike_rate=.05, unit_effects={},
                      unit_class="cavalry", firing_rate=0, firing_accuracy=0, firing_damage=0,
                      min_charge_range=300, max_charge_range=1000, charge_effects={"damage":35, "repel":3, "stun":10, "sfx":"sfx_horse_neigh_gallop.ogg"})



class Light_Cavalry(Unit):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot):
        Unit.__init__(self, game, x, y, unit_type, unit_country, side, slot, wal=8, aal=6,
                      health=220, attack=4, defense=4, walking_acc=5,
                      attacking_range=50, damage_frame=5, fatal_strike_rate=.02, unit_effects={},
                      unit_class="cavalry", firing_rate=0, firing_accuracy=0, firing_damage=0,
                      min_charge_range=300, max_charge_range=1200, charge_effects={"damage":40, "repel":3, "stun":10, "sfx":"sfx_horse_neigh_gallop.ogg"})


class Tiger_Knight_Cavalry(Unit):
    def __init__(self, game, x, y, unit_type, unit_country, side, slot):
        Unit.__init__(self, game, x, y, unit_type, unit_country, side, slot, wal=8, aal=7,
                      health=250, attack=7, defense=7, walking_acc=4,
                      attacking_range=70, damage_frame=6, fatal_strike_rate=.05, unit_effects={},
                      unit_class="cavalry", firing_rate=0, firing_accuracy=0, firing_damage=0,
                      min_charge_range=300, max_charge_range=1200, charge_effects={"damage":50, "repel":3, "stun":10, "sfx":"sfx_horse_neigh_gallop.ogg"})



class TransparentCover(pg.sprite.Sprite):
    def __init__(self, x, y, filename, anchor="topleft"):
        pg.sprite.Sprite.__init__(self)
        self.filename = filename
        self.image = pg.image.load(self.filename).convert()
        self.rect = self.image.get_rect()
        self.alpha = 0
        if anchor == "topleft":
            self.rect.topleft = (x,y)
        elif anchor == "topright":
            self.rect.topright = (x,y)
        else:
            self.rect.x = x
            self.rect.y = y

    def update(self):
        self.image.set_alpha(self.alpha)


class Fog(pg.sprite.Sprite):
    def __init__(self, filename, x, y, vel_x, vel_y, anchor="topleft"):
        pg.sprite.Sprite.__init__(self)
        self.vel_x, self.vel_y = vel_x, vel_y
        self.image = pg.image.load(filename).convert_alpha()
        self.rect = self.image.get_rect()
        if anchor == "topleft":
            self.rect.topleft = (x,y)
        elif anchor == "topright":
            self.rect.topright = (x,y)
        elif anchor == "bottomleft":
            self.rect.bottomleft = (x,y)
        elif anchor == "bottomright":
            self.rect.bottomright = (x,y)
        elif anchor == "midleft":
            self.rect.midleft = (x,y)
        elif anchor == "midright":
            self.rect.midright = (x,y)
        elif anchor == "midtop":
            self.rect.midtop = (x,y)
        elif anchor == "midbottom":
            self.rect.midbottom = (x,y)
        elif anchor == "center":
            self.rect.center = (x,y)
        else:
            self.rect.x = x
            self.rect.y = y

    def update(self):
        self.rect.x += self.vel_x
        self.rect.y += self.vel_y
        if self.rect.x <= -(self.image.get_width()-LANDSCAPE_WIDTH) and self.vel_x < 0:
            self.rect.x = 0
            self.vel_x = -randrange(4,6)
        elif self.rect.x >= 0 and self.vel_x > 0:
            self.rect.x = -(self.image.get_width()-LANDSCAPE_WIDTH)
            self.vel_x = randrange(4, 6)


class Rain(pg.sprite.Sprite):
    def __init__(self, x, y, falling_speed):
        pg.sprite.Sprite.__init__(self)
        self.falling_speed = falling_speed
        self.image = pg.Surface((1,randrange(3,6))).convert_alpha()
        self.image.fill(GRAY)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

    def update(self):
        self.rect.y += self.falling_speed
        if self.rect.y >= LANDSCAPE_HEIGHT*1.1:
            self.rect.y = randrange(-1000,-50)
            self.rect.x = randrange(0, LANDSCAPE_WIDTH)


class Static(pg.sprite.Sprite):
    def __init__(self, x, y, filename, animations=[], frame_delta=100, kill_after_animation=False, master_list=None,
                 rotation=0, anchor="default", reverse=False, scale=None):
        pg.sprite.Sprite.__init__(self)
        self.filename = filename
        if reverse:
            self.image = pg.transform.flip(pg.image.load(self.filename).convert_alpha(), True, False)
        else:
            self.image = pg.image.load(self.filename).convert_alpha()
        if scale:
            self.image = pg.transform.smoothscale(self.image, scale)

        self.rect = self.image.get_rect()
        if anchor == "topleft":
            self.rect.topleft = (x,y)
        elif anchor == "topright":
            self.rect.topright = (x,y)
        elif anchor == "bottomleft":
            self.rect.bottomleft = (x,y)
        elif anchor == "bottomright":
            self.rect.bottomright = (x,y)
        elif anchor == "midleft":
            self.rect.midleft = (x,y)
        elif anchor == "midright":
            self.rect.midright = (x,y)
        elif anchor == "midtop":
            self.rect.midtop = (x,y)
        elif anchor == "midbottom":
            self.rect.midbottom = (x,y)
        elif anchor == "center":
            self.rect.center = (x,y)
        else:
            self.rect.x = x
            self.rect.y = y
        self.animations = animations
        self.current_frame = 0
        self.last_update = 0
        self.frame_delta = frame_delta
        self.animation_frames = animations
        self.kill_after_animation = kill_after_animation
        self.master_list = master_list
        self.rotation = rotation

        if self.rotation != 0:
            self.image = pg.transform.rotate(self.image, self.rotation)

    def animate(self):
        now = pg.time.get_ticks()
        if now - self.last_update > self.frame_delta:
            self.last_update = now
            self.current_frame = (self.current_frame + 1)%len(self.animation_frames)
            self.image = self.animation_frames[self.current_frame]
            if self.rotation != 0:
                self.image = pg.transform.rotate(self.image, self.rotation)

    def update(self):
        if self.animations:
            self.animate()
        if self.kill_after_animation and self.current_frame == len(self.animation_frames) - 1:
            if self.master_list:
                self.master_list.remove(self)
            self.kill()


class UnitIcon(pg.sprite.Sprite):
    def __init__(self, game, side, x, y, unit_type, unit_country, selected=False):
        pg.sprite.Sprite.__init__(self)
        self.game = game
        self.side = side
        self.unit_type = unit_type
        self.unit_country = unit_country
        if str(self.unit_country).lower() not in ["shu", "wei", "wu"]:
            self.unit_country = "shu"
        #if self.unit_type == "unknown_unit":
        #    self.filename = "sprite_unknown_unit_icon_{}.png"
        if self.unit_type:
            self.filename = "sprite_{}_icon_{}.png".format(self.unit_type, self.unit_country)
        else:
            self.filename = "sprite_unit_bg.png"

        self.default_image = pg.image.load(self.filename).convert_alpha()
        if self.filename != "sprite_unit_bg.png":
            self.selected_image = pg.image.load(self.filename.replace(".png", "_selected.png")).convert_alpha()
            self.disabled_image = pg.image.load("sprite_{}_icon_disabled.png".format(self.unit_type)).convert_alpha()
        else:
            self.selected_image = pg.image.load(self.filename).convert_alpha()
            self.disabled_image = pg.image.load(self.filename).convert_alpha()

        self.image = self.default_image
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.selected = selected
        self.disabled = False
        self.hovering = False
        self.hover_time = 0
        self.tooltip = None
        self.tooltip_sprites = []

    def update(self):
        if self.disabled:
            self.image = self.disabled_image
        elif self.selected:
            self.image = self.selected_image
        else:
            self.image = self.default_image

        if self.hovering and self.unit_type:
            self.hover_time += 1
        else:
            self.hover_time = 0
            if self.tooltip:
                self.kill_tooltip()

        if self.hover_time >= 6 and not self.tooltip:
            self.generate_tooltip()


    def generate_sample_unit(self):
        if self.side == "left":
            if self.unit_type == "spear_militia":
                return Spear_Militia(self.game, 0, 0, self.unit_type,
                                     self.game.player_country, self.side, 0)

            elif self.unit_type == "sword_militia":
                return Sword_Militia(self.game, 0, 0, self.unit_type,
                                     self.game.player_country, self.side, 0)

            elif self.unit_type == "axe_militia":
                return Axe_Militia(self.game, 0, 0, self.unit_type,
                                     self.game.player_country, self.side, 0)

            elif self.unit_type == "bow_militia":
                return Bow_Militia(self.game, 0, 0, self.unit_type,
                                   self.game.player_country, self.side, 0)

            elif self.unit_type == "barbarian_spear_woman":
                return Barbarian_Spear_Woman(self.game, 0, 0, self.unit_type,
                                     self.game.player_country, self.side, 0)

            elif self.unit_type == "barbarian_assassin":
                return Barbarian_Assassin(self.game, 0, 0, self.unit_type,
                                     self.game.player_country, self.side, 0)

            elif self.unit_type == "barbarian_archer":
                return Barbarian_Archer(self.game, 0, 0, self.unit_type,
                                     self.game.player_country, self.side, 0)

            elif self.unit_type == "rattan_armor_infantry":
                return Rattan_Armor_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.player_country, self.side, 0)

            elif self.unit_type == "spear_infantry":
                return Spear_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.player_country, self.side, 0)

            elif self.unit_type == "longspear_infantry":
                return Longspear_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.player_country, self.side, 0)

            elif self.unit_type == "elite_spear_infantry":
                return Elite_Spear_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.player_country, self.side, 0)

            elif self.unit_type == "sword_infantry":
                return Sword_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.player_country, self.side, 0)

            elif self.unit_type == "light_dagger_infantry":
                return Light_Dagger_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.player_country, self.side, 0)

            elif self.unit_type == "bow_infantry":
                return Bow_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.player_country, self.side, 0)

            elif self.unit_type == "flaming_arrow_infantry":
                return Flaming_Arrow_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.player_country, self.side, 0)

            elif self.unit_type == "crossbow_infantry":
                return Crossbow_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.player_country, self.side, 0)

            elif self.unit_type == "catapult":
                return Catapult(self.game, 0, 0, self.unit_type,
                                     self.game.player_country, self.side, 0)

            elif self.unit_type == "shielded_sword_infantry":
                return Shielded_Sword_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.player_country, self.side, 0)

            elif self.unit_type == "elite_sword_infantry":
                return Elite_Sword_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.player_country, self.side, 0)

            elif self.unit_type == "mace_infantry":
                return Mace_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.player_country, self.side, 0)

            elif self.unit_type == "battleaxe_infantry":
                return Battleaxe_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.player_country, self.side, 0)

            elif self.unit_type == "elephant_rider":
                return Elephant_Rider(self.game, 0, 0, self.unit_type,
                                     self.game.player_country, self.side, 0)

            elif self.unit_type == "halberd_cavalry":
                return Halberd_Cavalry(self.game, 0, 0, self.unit_type,
                                     self.game.player_country, self.side, 0)

            elif self.unit_type == "light_cavalry":
                return Light_Cavalry(self.game, 0, 0, self.unit_type,
                                     self.game.player_country, self.side, 0)

            elif self.unit_type == "tiger_knight_cavalry":
                return Tiger_Knight_Cavalry(self.game, 0, 0, self.unit_type,
                                     self.game.player_country, self.side, 0)

        else:
            if self.unit_type == "spear_militia":
                return Spear_Militia(self.game, 0, 0, self.unit_type,
                                     self.game.enemy_country, self.side, 0)

            elif self.unit_type == "sword_militia":
                return Sword_Militia(self.game, 0, 0, self.unit_type,
                                     self.game.enemy_country, self.side, 0)

            elif self.unit_type == "axe_militia":
                return Axe_Militia(self.game, 0, 0, self.unit_type,
                                     self.game.enemy_country, self.side, 0)

            elif self.unit_type == "bow_militia":
                return Bow_Militia(self.game, 0, 0, self.unit_type,
                                   self.game.enemy_country, self.side, 0)

            elif self.unit_type == "barbarian_spear_woman":
                return Barbarian_Spear_Woman(self.game, 0, 0, self.unit_type,
                                     self.game.enemy_country, self.side, 0)

            elif self.unit_type == "barbarian_assassin":
                return Barbarian_Assassin(self.game, 0, 0, self.unit_type,
                                     self.game.enemy_country, self.side, 0)

            elif self.unit_type == "barbarian_archer":
                return Barbarian_Archer(self.game, 0, 0, self.unit_type,
                                     self.game.enemy_country, self.side, 0)

            elif self.unit_type == "rattan_armor_infantry":
                return Rattan_Armor_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.enemy_country, self.side, 0)

            elif self.unit_type == "spear_infantry":
                return Spear_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.enemy_country, self.side, 0)

            elif self.unit_type == "longspear_infantry":
                return Longspear_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.enemy_country, self.side, 0)

            elif self.unit_type == "elite_spear_infantry":
                return Elite_Spear_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.enemy_country, self.side, 0)

            elif self.unit_type == "sword_infantry":
                return Sword_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.enemy_country, self.side, 0)

            elif self.unit_type == "light_dagger_infantry":
                return Light_Dagger_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.enemy_country, self.side, 0)

            elif self.unit_type == "bow_infantry":
                return Bow_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.enemy_country, self.side, 0)

            elif self.unit_type == "flaming_arrow_infantry":
                return Flaming_Arrow_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.enemy_country, self.side, 0)

            elif self.unit_type == "crossbow_infantry":
                return Crossbow_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.enemy_country, self.side, 0)

            elif self.unit_type == "catapult":
                return Catapult(self.game, 0, 0, self.unit_type,
                                     self.game.enemy_country, self.side, 0)

            elif self.unit_type == "shielded_sword_infantry":
                return Shielded_Sword_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.enemy_country, self.side, 0)

            elif self.unit_type == "elite_sword_infantry":
                return Elite_Sword_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.enemy_country, self.side, 0)

            elif self.unit_type == "mace_infantry":
                return Mace_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.enemy_country, self.side, 0)

            elif self.unit_type == "battleaxe_infantry":
                return Battleaxe_Infantry(self.game, 0, 0, self.unit_type,
                                     self.game.enemy_country, self.side, 0)

            elif self.unit_type == "elephant_rider":
                return Elephant_Rider(self.game, 0, 0, self.unit_type,
                                     self.game.enemy_country, self.side, 0)

            elif self.unit_type == "halberd_cavalry":
                return Halberd_Cavalry(self.game, 0, 0, self.unit_type,
                                     self.game.enemy_country, self.side, 0)

            elif self.unit_type == "light_cavalry":
                return Light_Cavalry(self.game, 0, 0, self.unit_type,
                                     self.game.enemy_country, self.side, 0)

            elif self.unit_type == "tiger_knight_cavalry":
                return Tiger_Knight_Cavalry(self.game, 0, 0, self.unit_type,
                                     self.game.enemy_country, self.side, 0)


    def generate_tooltip(self):
        mouse_x, mouse_y = pg.mouse.get_pos()
        tooltip_filename = "sprite_{}_tooltip.png".format(self.unit_type)
        if self.side == "left":
            self.tooltip = Static(mouse_x, mouse_y, tooltip_filename)
        else:
            self.tooltip = Static(mouse_x, mouse_y, tooltip_filename, anchor="topright")
        #self.game.fixed_sprites.add(self.tooltip)
        #self.game.all_sprites.add(self.tooltip)
        self.tooltip_sprites.append(self.tooltip)

        self.tooltip_avatar = Static(self.tooltip.rect.left+16, self.tooltip.rect.top+16, self.filename, anchor="topleft")
        #self.game.fixed_sprites.add(self.tooltip_avatar)
        #self.game.all_sprites.add(self.tooltip_avatar)
        self.tooltip_sprites.append(self.tooltip_avatar)

        self.sample_unit = self.generate_sample_unit()
        cell_filename = "sprite_stat_single_cell.png"
        if self.sample_unit.unit_class == "ranged":
            health_cell_length = self.sample_unit.health//20
            for i in range(health_cell_length):
                cell = Static(self.tooltip.rect.left+20+11*i, self.tooltip.rect.top+110, cell_filename, anchor="topleft")
                self.tooltip_sprites.append(cell)

            attacking_range_cell_length = self.sample_unit.attacking_range//100
            for i in range(attacking_range_cell_length):
                cell = Static(self.tooltip.rect.left+20+11*i, self.tooltip.rect.top+146, cell_filename, anchor="topleft")
                self.tooltip_sprites.append(cell)

            firing_rate_cell_length = int((self.sample_unit.firing_rate - .5) *10) + 1
            for i in range(firing_rate_cell_length):
                cell = Static(self.tooltip.rect.left+20+11*i, self.tooltip.rect.top+182, cell_filename, anchor="topleft")
                self.tooltip_sprites.append(cell)

            firing_accuracy_cell_length = int(self.sample_unit.firing_accuracy*10)
            for i in range(firing_accuracy_cell_length):
                cell = Static(self.tooltip.rect.left+20+11*i, self.tooltip.rect.top+218, cell_filename, anchor="topleft")
                self.tooltip_sprites.append(cell)

            damage_cell_length = int(self.sample_unit.firing_damage)
            for i in range(damage_cell_length):
                cell = Static(self.tooltip.rect.left+20+11*i, self.tooltip.rect.top+254, cell_filename, anchor="topleft")
                self.tooltip_sprites.append(cell)

            fatal_strike_rate_length = int(self.sample_unit.fatal_strike_rate*100)
            for i in range(fatal_strike_rate_length):
                cell = Static(self.tooltip.rect.left + 20 + 11 * i, self.tooltip.rect.top + 290, cell_filename,
                              anchor="topleft")
                self.tooltip_sprites.append(cell)

            defense_length = int(self.sample_unit.defense)
            for i in range(defense_length):
                cell = Static(self.tooltip.rect.left + 20 + 11 * i, self.tooltip.rect.top + 326, cell_filename,
                              anchor="topleft")
                self.tooltip_sprites.append(cell)

            speed_length = int(self.sample_unit.walking_acc)
            for i in range(speed_length):
                cell = Static(self.tooltip.rect.left + 20 + 11 * i, self.tooltip.rect.top + 362, cell_filename,
                              anchor="topleft")
                self.tooltip_sprites.append(cell)

            unit_cost_length = int(UNIT_COMMAND_COST[self.unit_type]//5)
            for i in range(unit_cost_length):
                cell = Static(self.tooltip.rect.left + 20 + 11 * i, self.tooltip.rect.top + 398, cell_filename,
                              anchor="topleft")
                self.tooltip_sprites.append(cell)

        elif self.sample_unit.unit_class=="cavalry":
            health_cell_length = self.sample_unit.health//20
            for i in range(health_cell_length):
                cell = Static(self.tooltip.rect.left+20+11*i, self.tooltip.rect.top+110, cell_filename, anchor="topleft")
                self.tooltip_sprites.append(cell)

            attacking_range_cell_length = int(self.sample_unit.attacking_range/10)
            for i in range(attacking_range_cell_length):
                cell = Static(self.tooltip.rect.left + 20 + 11 * i, self.tooltip.rect.top + 146, cell_filename,
                              anchor="topleft")
                self.tooltip_sprites.append(cell)

            damage_cell_length = int(self.sample_unit.attack)
            for i in range(damage_cell_length):
                cell = Static(self.tooltip.rect.left + 20 + 11 * i, self.tooltip.rect.top + 182, cell_filename,
                              anchor="topleft")
                self.tooltip_sprites.append(cell)

            fatal_strike_rate_length = int(self.sample_unit.fatal_strike_rate * 100)
            for i in range(fatal_strike_rate_length):
                cell = Static(self.tooltip.rect.left + 20 + 11 * i, self.tooltip.rect.top + 218, cell_filename,
                              anchor="topleft")
                self.tooltip_sprites.append(cell)

            defense_length = int(self.sample_unit.defense)
            for i in range(defense_length):
                cell = Static(self.tooltip.rect.left + 20 + 11 * i, self.tooltip.rect.top + 254, cell_filename,
                              anchor="topleft")
                self.tooltip_sprites.append(cell)

            speed_length = int(self.sample_unit.walking_acc)
            for i in range(speed_length):
                cell = Static(self.tooltip.rect.left + 20 + 11 * i, self.tooltip.rect.top + 290, cell_filename,
                              anchor="topleft")
                self.tooltip_sprites.append(cell)

            charge_range_length = int(self.sample_unit.max_charge_range) // 50
            for i in range(charge_range_length):
                cell = Static(self.tooltip.rect.left + 20 + 11 * i, self.tooltip.rect.top + 326, cell_filename, anchor="topleft")
                self.tooltip_sprites.append(cell)

            unit_cost_length = int(UNIT_COMMAND_COST[self.unit_type] // 5)
            for i in range(unit_cost_length):
                cell = Static(self.tooltip.rect.left + 20 + 11 * i, self.tooltip.rect.top + 362, cell_filename,
                              anchor="topleft")
                self.tooltip_sprites.append(cell)

        else:
            health_cell_length = self.sample_unit.health//20
            for i in range(health_cell_length):
                cell = Static(self.tooltip.rect.left+20+11*i, self.tooltip.rect.top+110, cell_filename, anchor="topleft")
                self.tooltip_sprites.append(cell)

            attacking_range_cell_length = int(self.sample_unit.attacking_range/10)
            for i in range(attacking_range_cell_length):
                cell = Static(self.tooltip.rect.left + 20 + 11 * i, self.tooltip.rect.top + 146, cell_filename,
                              anchor="topleft")
                self.tooltip_sprites.append(cell)

            damage_cell_length = int(self.sample_unit.attack)
            for i in range(damage_cell_length):
                cell = Static(self.tooltip.rect.left + 20 + 11 * i, self.tooltip.rect.top + 182, cell_filename,
                              anchor="topleft")
                self.tooltip_sprites.append(cell)

            fatal_strike_rate_length = int(self.sample_unit.fatal_strike_rate * 100)
            for i in range(fatal_strike_rate_length):
                cell = Static(self.tooltip.rect.left + 20 + 11 * i, self.tooltip.rect.top + 218, cell_filename,
                              anchor="topleft")
                self.tooltip_sprites.append(cell)

            defense_length = int(self.sample_unit.defense)
            for i in range(defense_length):
                cell = Static(self.tooltip.rect.left + 20 + 11 * i, self.tooltip.rect.top + 254, cell_filename,
                              anchor="topleft")
                self.tooltip_sprites.append(cell)

            speed_length = int(self.sample_unit.walking_acc)
            for i in range(speed_length):
                cell = Static(self.tooltip.rect.left + 20 + 11 * i, self.tooltip.rect.top + 290, cell_filename,
                              anchor="topleft")
                self.tooltip_sprites.append(cell)

            unit_cost_length = int(UNIT_COMMAND_COST[self.unit_type] // 5)
            for i in range(unit_cost_length):
                cell = Static(self.tooltip.rect.left + 20 + 11 * i, self.tooltip.rect.top + 326, cell_filename,
                              anchor="topleft")
                self.tooltip_sprites.append(cell)


        for sp in self.tooltip_sprites:
            self.game.fixed_sprites.add(sp)
            self.game.all_sprites.add(sp)


    def kill_tooltip(self):
        for sp in self.tooltip_sprites:
            sp.kill()
        self.tooltip_sprites = []
        self.tooltip = None



class TerrainIcon(pg.sprite.Sprite):
    def __init__(self, game, x, y, terrain, anchor="topleft", scale=None):
        pg.sprite.Sprite.__init__(self)
        self.game = game
        self.terrain = terrain
        self.filename = "sprite_terrain_selection_{}.png".format(self.terrain)
        self.default_image = pg.image.load(self.filename).convert_alpha()
        self.hover_image = pg.image.load(self.filename.replace(".png", "_selected.png")).convert_alpha()
        if scale:
            self.default_image = pg.transform.smoothscale(self.default_image, scale)
            self.hover_image = pg.transform.smoothscale(self.hover_image, scale)

        self.terrain_descriptions = {"sand": "Terrain: Normal \n\nDefault battle terrain.",
                                    "forest": "Terrain: Forest \n\nReduced damage from cavalry charges and ranged units. Increased damage from fire attacks.",
                                    "rock": "Terrain: Rock \n\nReduced damage from cavalry charges; reduced cavalry charge range.",
                                    "water": "Terrain: Water \n\nNarrow deployment slots. Shorter map width.",}


        self.image = self.default_image
        self.rect = self.image.get_rect()
        if anchor == "topleft":
            self.rect.topleft = (x,y)
        else:
            self.rect.center = (x,y)

        self.hovering = False
        self.hover_time = 0
        self.tooltip = None
        self.tooltip_description = ""
        self.tooltip_sprites = []


    def update(self):

        if self.hovering:
            self.image = self.hover_image
            self.hover_time += 1
        else:
            self.image = self.default_image
            self.hover_time = 0
            if self.tooltip:
                self.kill_tooltip()

        if self.hover_time >= 6 and not self.tooltip:
            self.generate_tooltip()


    def generate_tooltip(self):
        mouse_x, mouse_y = pg.mouse.get_pos()
        tooltip_filename = "sprite_sp_atk_tooltip.png"

        #conditions to force change anchor criteria
        horizontal_anchor = "left"
        vertical_anchor = "top"

        if mouse_x <= 300:
            horizontal_anchor = "left"
        elif mouse_x >= LANDSCAPE_WIDTH - 300:
            horizontal_anchor = "right"

        if mouse_y <= 300:
            vertical_anchor = "top"
        elif mouse_y >= LANDSCAPE_HEIGHT - 300:
            vertical_anchor = "bottom"

        self.tooltip = Static(mouse_x, mouse_y, tooltip_filename, anchor=vertical_anchor+horizontal_anchor)
        self.tooltip_sprites.append(self.tooltip)
        self.game.all_sprites.add(self.tooltip)
        self.game.priority_sprites.add(self.tooltip)
        try:
            self.game.tooltip_sprites.add(self.tooltip)
        except:
            pass

        self.tooltip_description = self.terrain_descriptions[self.terrain]

    def kill_tooltip(self):
        for sp in self.tooltip_sprites:
            sp.kill()
        self.tooltip_sprites = []
        self.tooltip = None
        self.tooltip_description = ""
        self.game.special_attack_tooltip_description = ""
        try:
            self.game.tooltip_description = ""
        except:
            pass


class SpecialAttackIcon(pg.sprite.Sprite):
    def __init__(self, game, x, y, special_attack_name, anchor="topleft", type="sp_atk", master=None):
        pg.sprite.Sprite.__init__(self)
        self.game = game
        self.special_attack_name = special_attack_name
        self.filename = "sprite_sp_atk_icon_{}.png".format(self.special_attack_name)
        self.default_image = pg.image.load(self.filename).convert_alpha()
        self.hover_image = pg.image.load(self.filename.replace(".png", "_selected.png")).convert_alpha()
        self.type = type
        self.master = master

        if type != "sp_atk":
            self.static = True
            self.disabled = True
        else:
            self.static = False
            self.disabled = False

        self.image = self.default_image
        self.rect = self.image.get_rect()
        if anchor == "topleft":
            self.rect.topleft = (x,y)
        else:
            self.rect.x = x
            self.rect.y = y

        self.hovering = False
        self.hover_time = 0
        self.tooltip = None
        self.tooltip_description = ""
        self.tooltip_sprites = []
        self.transparent_cover = None

        self.special_attack_activated_time = -999999999
        self.remaining_recovery_time = 0


    def update(self):

        if self.hovering:
            self.image = self.hover_image
            self.hover_time += 1
        else:
            self.image = self.default_image
            self.hover_time = 0
            if self.tooltip:
                self.kill_tooltip()

        if self.hover_time >= 6 and not self.tooltip:
            self.generate_tooltip()

        if self.disabled and self.transparent_cover:
            time_since_special_activation = pg.time.get_ticks() - self.special_attack_activated_time
            recovery_time = SPECIAL_ATTACK_INFO[self.special_attack_name]["Recovery"]*1000
            if "versatile_commander" in self.game.main_game.acquired_tech[self.master.country]:
                recovery_time = int(SPECIAL_ATTACK_INFO[self.special_attack_name]["Recovery"]*1000*.8)

            if self.game.running:
                self.remaining_recovery_time = recovery_time-time_since_special_activation
                opacity = self.remaining_recovery_time/recovery_time*125+125
                if time_since_special_activation <= recovery_time:
                    self.transparent_cover.alpha = opacity
                else:
                    self.transparent_cover.alpha = 0
                    self.remaining_recovery_time = 0
                    self.disabled = False


    def generate_transparent_cover(self):
        self.transparent_cover = TransparentCover(self.rect.left, self.rect.top, "sprite_black_50_50.png",
                                                  anchor="topleft")
        self.special_attack_activated_time = pg.time.get_ticks()
        self.game.fixed_sprites.add(self.transparent_cover)
        self.game.all_sprites.add(self.transparent_cover)


    def generate_tooltip(self):
        mouse_x, mouse_y = pg.mouse.get_pos()
        tooltip_filename = "sprite_sp_atk_tooltip.png"

        #conditions to force change anchor criteria
        horizontal_anchor = "left"
        vertical_anchor = "top"

        if mouse_x <= 300:
            horizontal_anchor = "left"
        elif mouse_x >= LANDSCAPE_WIDTH - 300:
            horizontal_anchor = "right"

        if mouse_y <= 300:
            vertical_anchor = "top"
        elif mouse_y >= LANDSCAPE_HEIGHT - 300:
            vertical_anchor = "bottom"

        self.tooltip = Static(mouse_x, mouse_y, tooltip_filename, anchor=vertical_anchor+horizontal_anchor)
        self.tooltip_sprites.append(self.tooltip)
        self.game.all_sprites.add(self.tooltip)
        self.game.priority_sprites.add(self.tooltip)
        try:
            self.game.tooltip_sprites.add(self.tooltip)
        except:
            pass

        if self.type == "sp_atk":
            self.tooltip_description = SPECIAL_ATTACK_INFO[self.special_attack_name]["Description"]
        else:
            self.tooltip_description = SKILL_INFO[self.special_attack_name]["Description"]


    def kill_tooltip(self):
        for sp in self.tooltip_sprites:
            sp.kill()
        self.tooltip_sprites = []
        self.tooltip = None
        self.tooltip_description = ""
        self.game.special_attack_tooltip_description = ""
        try:
            self.game.tooltip_description = ""
        except:
            pass


class Arrow(pg.sprite.Sprite):
    def __init__(self, game, x, y, x_vel, y_vel, side, slot, fatal_strike_rate, damage, stun, repel,
                 colorkey=None, arrow_type=None):
        pg.sprite.Sprite.__init__(self)
        self.game = game
        self.name = "arrow"
        self.side = side
        self.slot = slot
        self.fatal_strike_rate = fatal_strike_rate
        self.original_image = pg.image.load("sprite_arrow.png").convert_alpha()
        self.original_image = pg.transform.smoothscale(self.original_image, (25, 4))
        self.image = self.original_image
        if colorkey:
            self.image.set_colorkey(colorkey)
        self.rect = self.image.get_rect()
        self.rect.centerx = x
        self.rect.centery = y
        self.starting_height = y
        self.x_vel = x_vel
        self.y_vel = y_vel
        self.damage = damage
        self.stun = stun
        self.repel = repel
        self.dead = False
        self.last_set_orientation = 0
        self.set_orientation()
        self.gravity = .5
        self.arrow_type = arrow_type

    def set_orientation(self):
        if self.x_vel == 0:
            self.x_vel = self.y_vel + 1
        angle = atan(-self.y_vel / self.x_vel+.0000000001) * 180 / pi
        if self.x_vel < 0:
            self.image = pg.transform.rotate(pg.transform.flip(self.original_image, True, False), angle)
        else:
            self.image = pg.transform.rotate(self.original_image, angle)
        #self.mask = pg.mask.from_surface(self.image)

    def update(self):
        if not self.dead:
            self.y_vel += self.gravity
            self.last_set_orientation += 1
            if self.arrow_type == "crossbow_bolt" and self.last_set_orientation % 3 == 0:
                self.set_orientation()
            elif self.last_set_orientation % 5 == 0:
                self.set_orientation()
            self.rect.centerx += self.x_vel
            self.rect.centery += self.y_vel
        else:
            if random() <= .005 + self.game.number_of_ranged_units/2000:
                self.kill()

        if self.rect.y >= self.starting_height+5 and self.arrow_type != "wall_arrow":
            self.dead = True
        elif self.rect.y >= LANDSCAPE_HEIGHT - (-50 - self.starting_height) and self.arrow_type == "wall_arrow":
            self.dead = True


class Flaming_Arrow(pg.sprite.Sprite):
    def __init__(self, game, x, y, x_vel, y_vel, side, slot, fatal_strike_rate, damage, stun, repel,
                 colorkey=None, wall_arrow=False):
        pg.sprite.Sprite.__init__(self)
        self.game = game
        self.name = "flaming_arrow"
        self.side = side
        self.slot = slot
        self.fatal_strike_rate = fatal_strike_rate
        self.animations = [pg.image.load("sprite_flaming_arrow_{}.png".format(i)).convert_alpha() for i in range(1,9)]
        self.animations = [pg.transform.smoothscale(frame, (25, 4)) for frame in self.animations]

        self.image = self.animations[0]
        if colorkey:
            self.image.set_colorkey(colorkey)
        self.rect = self.image.get_rect()
        self.rect.centerx = x
        self.rect.centery = y
        self.starting_height = y
        self.x_vel = x_vel
        self.y_vel = y_vel
        self.damage = damage
        self.stun = stun
        self.repel = repel
        self.dead = False
        self.gravity = .5
        self.wall_arrow = wall_arrow
        self.frame_delta = 150
        self.current_frame = 0
        self.last_update = 0

        self.set_orientation()


    def set_orientation(self):
        if self.x_vel == 0:
            self.x_vel = self.y_vel + 1
        angle = atan(-self.y_vel / self.x_vel+.0000000001) * 180 / pi
        if self.x_vel < 0:
            self.image = pg.transform.rotate(pg.transform.flip(self.animations[self.current_frame], True, False), angle)
        else:
            self.image = pg.transform.rotate(self.animations[self.current_frame], angle)

    def animate(self):
        now = pg.time.get_ticks()
        if now - self.last_update > self.frame_delta:
            self.last_update = now
            self.current_frame = (self.current_frame + 1) % len(self.animations)
            self.image = self.animations[self.current_frame]

    def update(self):
        self.animate()
        self.set_orientation()
        if not self.dead:
            self.y_vel += self.gravity
            self.rect.centerx += self.x_vel
            self.rect.centery += self.y_vel
        else:
            if random() <= .0075+self.game.number_of_ranged_units/2000:
                self.kill()

        if self.rect.y >= self.starting_height+5 and not self.wall_arrow:
            self.dead = True
        elif self.rect.y >= LANDSCAPE_HEIGHT - (-50 - self.starting_height) and self.wall_arrow:
            self.dead = True


class Catapult_Rock(pg.sprite.Sprite):
    def __init__(self, game, x, y, x_vel, y_vel, side, slot, fatal_strike_rate, damage, stun, repel, colorkey=None):
        pg.sprite.Sprite.__init__(self)
        self.game = game
        self.name = "catapult_rock"
        self.side = side
        self.slot = slot
        self.fatal_strike_rate = fatal_strike_rate
        self.original_image = pg.image.load("sprite_catapult_rock.png").convert_alpha()
        self.image = self.original_image
        if colorkey:
            self.image.set_colorkey(colorkey)
        self.rect = self.image.get_rect()
        self.rect.centerx = x
        self.rect.centery = y
        self.starting_height = y-50
        self.x_vel = x_vel
        self.y_vel = y_vel
        self.damage = damage
        self.stun = stun
        self.repel = repel
        self.dead = False
        self.set_orientation()
        self.gravity = .5

    def set_orientation(self):
        if self.x_vel == 0:
            self.x_vel = self.y_vel + 1
        angle = atan(-self.y_vel / self.x_vel) * 180 / pi
        if self.x_vel < 0:
            self.image = pg.transform.rotate(pg.transform.flip(self.original_image, True, False), angle)
        else:
            self.image = pg.transform.rotate(self.original_image, angle)
        self.mask = pg.mask.from_surface(self.image)

    def update(self):
        if not self.dead:
            self.y_vel += self.gravity
            if random() <= .4:
                self.set_orientation()
            self.rect.centerx += self.x_vel
            self.rect.centery += self.y_vel

        if self.rect.y >= self.starting_height+100:
            self.dead = True
            self.generate_broken_pieces()

    def generate_broken_pieces(self):
        for i in range(1,5):
            vel_x = random()*randrange(-10,10)
            vel_y = random()*randrange(-15,-5)
            filename = "sprite_catapult_rock_piece_{}.png".format(i)
            piece = Catapult_Rock_Piece(self.game, self.rect.centerx, self.rect.centery, vel_x, vel_y, filename)
            self.game.all_sprites.add(piece)
        self.kill()


class Catapult_Rock_Piece(pg.sprite.Sprite):
    def __init__(self, game, x, y, vel_x, vel_y, filename):
        pg.sprite.Sprite.__init__(self)
        self.game = game
        self.image = pg.image.load(filename)
        self.rect = self.image.get_rect()
        self.rect.center = (x, y)
        self.width = self.image.get_width()
        self.height = self.image.get_height()
        self.starting_y = y
        self.vel_x = vel_x
        self.vel_y = vel_y
        self.gravity = 1

    def update(self):
        self.vel_y += self.gravity
        self.rect.x += self.vel_x
        self.rect.y += self.vel_y

        condition_1 = self.rect.x < 0 or self.rect.x > self.game.game_width
        condition_2 = self.rect.y < 0 or self.rect.y > self.game.game_height
        condition_3 = self.rect.y - self.starting_y >= 50
        if condition_1 or condition_2 or condition_3:
            self.kill()


class Rock(pg.sprite.Sprite):
    def __init__(self, x, y, x_vel, y_vel, side, slot, fatal_strike_rate, damage, stun, repel, colorkey=None):
        pg.sprite.Sprite.__init__(self)
        self.name = "rock"
        self.side = side
        self.slot = slot
        self.fatal_strike_rate = fatal_strike_rate
        self.original_image = pg.image.load("sprite_rock_{}.png".format(randrange(1,4))).convert_alpha()
        self.image = self.original_image
        if colorkey:
            self.image.set_colorkey(colorkey)
        self.rect = self.image.get_rect()
        self.rect.centerx = x
        self.rect.centery = y
        self.starting_height = y
        self.x_vel = x_vel
        self.y_vel = y_vel
        self.damage = damage
        self.stun = stun
        self.repel = repel
        self.dead = False
        self.gravity = .8

    def update(self):
        if not self.dead:
            self.y_vel += self.gravity
            self.rect.centerx += self.x_vel
            self.rect.centery += self.y_vel
        else:
            if random() <= .005:
                self.kill()

        if self.rect.y >= LANDSCAPE_HEIGHT - (-50 - self.starting_height):
            self.dead = True


class Barricade(pg.sprite.Sprite):
    def __init__(self, game, x, y, filename, side, slots, anchor="topleft"):
        pg.sprite.Sprite.__init__(self)
        self.game = game
        self.filename = filename
        self.side = side
        self.slots = slots
        self.unit_type = "barricade"
        self.unit_class = "obstacle"
        self.health = 1000
        self.max_health = self.health
        self.defense = 4
        if self.side == "right":
            self.default_image = pg.transform.flip(pg.image.load(self.filename).convert_alpha(), True, False)
            self.got_hit_image = pg.transform.flip(pg.image.load(self.filename.replace(".png", "_hit.png")).convert_alpha(), True, False)
        else:
            self.default_image = pg.image.load(self.filename).convert_alpha()
            self.got_hit_image = pg.image.load(self.filename.replace(".png", "_hit.png")).convert_alpha()
        self.image = self.default_image
        self.rect = self.image.get_rect()

        if anchor == "topright":
            self.rect.topright = x, y
        else:
            self.rect.topleft = x, y
        self.last_got_hit = 0

        if self.game:
            self.generate_health_bar()

    def generate_health_bar(self):
        self.health_bar_back = UnitHealthBar(self.rect.centerx, self.rect.top, "generic_health_bar_back.png", self.game,
                                             master=self, dynamic=False)
        self.health_bar_front = UnitHealthBar(self.rect.centerx, self.rect.top, "generic_health_bar_front.png",
                                              self.game, master=self, dynamic=True)
        self.game.all_sprites.add(self.health_bar_back)
        self.game.all_sprites.add(self.health_bar_front)

    def update(self):
        now = pg.time.get_ticks()
        if now - self.last_got_hit >= 1000:
            self.image = self.default_image
        else:
            self.image = self.got_hit_image

    def got_hit(self, damage, stun, repel, damage_type=None):
        if damage == 0:
            damage = 1
        self.health -= damage
        if self.health <= 0:
            self.health = 0
            self.health_bar_back.kill()
            self.health_bar_front.kill()
            self.kill()
        now = pg.time.get_ticks()
        if now - self.last_got_hit >= 1000:
            self.last_got_hit = pg.time.get_ticks()



class FortressWall(pg.sprite.Sprite):
    def __init__(self, game, x, y, filename, side, level, anchor="topleft"):
        pg.sprite.Sprite.__init__(self)
        self.game = game
        self.filename = filename
        self.side = side
        self.level = level
        self.unit_type = "wall"
        self.unit_class = "obstacle"
        self.defense = 4+self.level
        self.health = int(3000*self.level)
        self.max_health = self.health
        if self.side == "right":
            self.default_image = pg.transform.flip(pg.image.load(self.filename).convert_alpha(), True, False)
            self.got_hit_image = pg.transform.flip(pg.image.load(self.filename.replace(".png", "_hit.png")).convert_alpha(), True, False)
        else:
            self.default_image = pg.image.load(self.filename).convert_alpha()
            self.got_hit_image = pg.image.load(self.filename.replace(".png", "_hit.png")).convert_alpha()

        self.image = self.default_image
        self.rect = self.image.get_rect()

        if anchor == "topright":
            self.rect.topright = x, y
        else:
            self.rect.topleft = x, y
        self.last_got_hit = 0

    def update(self):
        now = pg.time.get_ticks()
        if now - self.last_got_hit >= 1000:
            self.image = self.default_image
        else:
            self.image = self.got_hit_image

        self.mask = pg.mask.from_surface(self.image)

    def got_hit(self, damage, stun, repel, damage_type=None):
        if damage == 0:
            damage = 1
        self.health -= damage
        now = pg.time.get_ticks()
        if self.side == "left":
            self.game.morale_change("left", -damage)
        elif self.side == "right":
            self.game.morale_change("right", -damage)
        if now - self.last_got_hit >= 1000:
            self.last_got_hit = pg.time.get_ticks()

    def generate_defenders(self):
        if self.level >= 1:
            if self.side == "left":
                for slot in range(8*self.level):
                    defender_archer = DefenderArcher(self.game, -100, - 300 + slot*30, slot, self.side)
                    self.game.all_sprites.add(defender_archer)

            else:
                for slot in range(8*self.level):
                    defender_archer = DefenderArcher(self.game, self.game.max_camera_location-self.game.camera_location + 100, -300 + slot*30, slot, self.side)
                    self.game.all_sprites.add(defender_archer)

        if self.level >= 3:
            if self.side == "left":
                for slot in range(8*(self.level-2)):
                    defender_rock_thrower = DefenderRockThrower(self.game, -100, - 300 + slot*30, slot, self.side)
                    self.game.all_sprites.add(defender_rock_thrower)

            else:
                for slot in range(8*(self.level-2)):
                    defender_rock_thrower = DefenderRockThrower(self.game, self.game.max_camera_location-self.game.camera_location + 100, -300 + slot*30, slot, self.side)
                    self.game.all_sprites.add(defender_rock_thrower)


class DefenderRockThrower(pg.sprite.Sprite):
    def __init__(self, game, x, y, slot, side):
        pg.sprite.Sprite.__init__(self)
        self.game = game
        self.side = side
        self.slot = slot
        self.image = pg.image.load("sprite_transparent.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.center = x,y

        self.last_fired = 0
        self.firing_rate = 1.0
        self.firing_accuracy = 0.33
        self.firing_damage = 50
        self.fatal_strike_rate = .75

    def update(self):
        now = pg.time.get_ticks()
        if now - self.last_fired >= randrange(3000,3500):
            if self.side == "left":
                same_slot_target = [es for es in self.game.enemy_unit_sprites if es.slot == self.slot]
            else:
                same_slot_target = [es for es in self.game.player_unit_sprites if es.slot == self.slot]

            shuffle(same_slot_target)
            for target in same_slot_target:
                if abs(self.rect.left - target.rect.centerx) <= 800 - self.slot*30 and abs(self.rect.left - target.rect.centerx) >= 200:
                    self.last_fired = now
                    self.game.generate_projectile(self.side, "wall_rock", self, target)
                    break


class SmokeBomb(pg.sprite.Sprite):
    def __init__(self, game, x, y, side):
        pg.sprite.Sprite.__init__(self)
        self.game = game
        self.side = side
        self.image = pg.image.load("sprite_smoke_bomb.png").convert_alpha()
        self.explosion_animations = [pg.image.load("sprite_smoke_bomb_explosion_{}.png".format(i)).convert_alpha() for i in range(1,18)]
        self.rect = self.image.get_rect()
        self.rect.center = x, y
        self.distance_from_ground = randrange(LANDSCAPE_HEIGHT, LANDSCAPE_HEIGHT+100)
        if self.side == "left":
            self.x_vel = randrange(1,4)
        else:
            self.x_vel = -randrange(1,4)
        self.y_vel = randrange(5,10)
        self.dead = False
        self.damage = 5
        self.animation_frame = 0
        self.last_animated = 0

    def update(self):
        if not self.dead:
            self.y_vel += .5
            self.rect.centerx += self.x_vel
            self.rect.centery += self.y_vel
            self.distance_from_ground -= self.y_vel
            if self.distance_from_ground <= 0:
                self.explode()
        else:
            now = pg.time.get_ticks()
            if now - self.last_animated >= 80:
                self.last_animated = now
                self.image = self.explosion_animations[self.animation_frame]
                self.animation_frame += 1
                if self.animation_frame >= len(self.explosion_animations):
                    self.kill()

    def explode(self):
        self.dead = True
        if not self.game.custom:
            self.game.main_game.play_sound_effect(self.game.smoke_bomb_sound)
        else:
            self.game.smoke_bomb_sound.play()
        self.game.earthquake_count += 1



class DefenderArcher(pg.sprite.Sprite):
    def __init__(self, game, x, y, slot, side):
        pg.sprite.Sprite.__init__(self)
        self.game = game
        self.side = side
        self.slot = slot
        self.image = pg.image.load("sprite_transparent.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.center = x,y

        self.last_fired = 0
        self.firing_rate = 1.0
        self.firing_accuracy = 0.5
        self.firing_damage = 6
        self.fatal_strike_rate = .03

    def update(self):
        now = pg.time.get_ticks()
        if now - self.last_fired >= randrange(1000,1200):
            if self.side == "left":
                same_slot_target = [es for es in self.game.enemy_unit_sprites if es.slot == self.slot]
            else:
                same_slot_target = [es for es in self.game.player_unit_sprites if es.slot == self.slot]

            shuffle(same_slot_target)
            for target in same_slot_target:
                if abs(self.rect.left - target.rect.centerx) <= 1800 - self.slot*30 and abs(self.rect.left - target.rect.centerx) >= 200:
                    self.last_fired = now
                    self.game.generate_projectile(self.side, "wall_arrow", self, target)
                    break


class DynamicBar(pg.sprite.Sprite):
    def __init__(self, x, y, sprite_type, game, reverse=False):
        pg.sprite.Sprite.__init__(self)
        self.game = game
        self.sprite_type = sprite_type
        self.reverse = reverse

        if self.sprite_type == "player_morale" or self.sprite_type == "enemy_morale":
            self.default_image = pg.image.load("sprite_morale_bar.png").convert_alpha()
        elif self.sprite_type == "player_command" or self.sprite_type == "enemy_command":
            self.default_image = pg.image.load("sprite_command_bar.png").convert_alpha()

        if self.reverse:
            self.default_image = pg.transform.flip(self.default_image, True, False)

        self.image = self.default_image
        self.rect = self.image.get_rect()

        if self.reverse:
            self.rect.topright = (x,y)
        else:
            self.rect.topleft = (x, y)

    def update(self):
        if self.sprite_type == "player_morale":
            self.image = pg.transform.smoothscale(self.default_image, (int(self.game.player_morale/self.game.player_morale_max*100), 7))

        elif self.sprite_type == "player_command":
            self.image = pg.transform.smoothscale(self.default_image, (int(self.game.player_command/self.game.player_command_max*100), 7))

        elif self.sprite_type == "enemy_morale":
            self.image = pg.transform.smoothscale(self.default_image, (int(self.game.enemy_morale/self.game.enemy_morale_max*100), 7))

        elif self.sprite_type == "enemy_command":
            self.image = pg.transform.smoothscale(self.default_image, (int(self.game.enemy_command/self.game.enemy_command_max*100), 7))

        if self.reverse:
            self.image = pg.transform.flip(self.image, True, False)


class FullScreenAnimation(pg.sprite.Sprite):
    def __init__(self, commander_name, frame_delta=50):
        pg.sprite.Sprite.__init__(self)
        self.commander_name = commander_name
        self.filename = "sprite_fullscreen_animation_{}.png".format(self.commander_name)
        self.default_image = pg.image.load(self.filename).convert()
        self.white_fullscreen_image = pg.image.load("sprite_white_fullscreen.png").convert_alpha()

        self.image = self.white_fullscreen_image
        self.rect = self.image.get_rect()
        self.rect.center = (LANDSCAPE_WIDTH//2,LANDSCAPE_HEIGHT//2)
        self.current_frame = 0
        self.last_update = 0
        self.frame_delta = frame_delta
        self.alpha = 255
        self.animation_frames = [self.white_fullscreen_image, self.white_fullscreen_image, self.default_image]

    def animate(self):
        now = pg.time.get_ticks()
        if now - self.last_update > self.frame_delta:
            self.last_update = now
            if self.current_frame == len(self.animation_frames) - 1:
                self.alpha -= 20
                self.image.set_alpha(self.alpha)
            else:
                self.current_frame = (self.current_frame + 1) % len(self.animation_frames)
                self.image = self.animation_frames[self.current_frame]

            self.rect = self.image.get_rect()
            self.rect.center = (LANDSCAPE_WIDTH // 2, LANDSCAPE_HEIGHT // 2)


    def update(self):
        self.animate()
        if self.alpha <= 0:
            self.kill()



class Button(pg.sprite.Sprite):
    def __init__(self, x, y, default_image_file, command, tooltip="", anchor="center", scale=None, master=None,
                 custom_suffix={}, shortened_tooltip=False, is_checkbutton=False):
        pg.sprite.Sprite.__init__(self)
        self.default_filename = default_image_file
        self.default_image = pg.image.load(self.default_filename).convert_alpha()
        if custom_suffix:
            self.default_image = pg.image.load(self.default_filename.replace(".png", "{}.png".format(custom_suffix["default"]))).convert_alpha()
            self.hover_image = pg.image.load(self.default_filename.replace(".png", "{}.png".format(custom_suffix["selected"]))).convert_alpha()
            self.pressed_image = pg.image.load(self.default_filename.replace(".png", "{}.png".format(custom_suffix["pressed"]))).convert_alpha()
            try:
                self.disabled_image = pg.image.load(self.default_filename.replace(".png", "{}.png".format(custom_suffix["disabled"]))).convert_alpha()
            except:
                self.disabled_image = pg.image.load(self.default_filename).convert_alpha()
        else:
            self.hover_image = pg.image.load(self.default_filename.replace(".png", "_selected.png")).convert_alpha()
            self.pressed_image = pg.image.load(self.default_filename.replace(".png", "_pressed.png")).convert_alpha()
            try:
                self.disabled_image = pg.image.load(self.default_filename.replace(".png", "_disabled.png")).convert_alpha()
            except:
                self.disabled_image = pg.image.load(self.default_filename).convert_alpha()

        if scale:
            self.default_image = pg.transform.smoothscale(self.default_image, scale)
            self.hover_image = pg.transform.smoothscale(self.hover_image, scale)
            self.pressed_image = pg.transform.smoothscale(self.pressed_image, scale)
            self.disabled_image = pg.transform.smoothscale(self.disabled_image, scale)

        self.image = self.default_image
        self.rect = self.image.get_rect()
        if anchor == "topleft":
            self.rect.topleft = (x,y)
        elif anchor == "topright":
            self.rect.topright = (x,y)
        elif anchor == "midleft":
            self.rect.midleft = (x,y)
        elif anchor == "midright":
            self.rect.midright = (x, y)
        else:
            self.rect.centerx = x
            self.rect.centery = y

        self.hovering = False
        self.hover_time = 0
        self.pressed = False
        self.disabled = False
        self.command = command
        self.tooltip = tooltip
        self.tooltip_bg = None
        self.shortened_tooltip = shortened_tooltip
        self.is_checkbutton = is_checkbutton
        self.master = master


    def update(self):
        if self.disabled:
            self.image = self.disabled_image
            if self.hovering:
                self.hover_time += 1
            elif self.tooltip_bg:
                self.kill_tooltip()
        elif self.pressed:
            self.image = self.pressed_image
            if self.hovering:
                self.hover_time += 1
            elif self.tooltip_bg:
                self.kill_tooltip()
        elif self.hovering:
            self.image = self.hover_image
            self.hover_time += 1
        else:
            self.image = self.default_image
            if self.tooltip_bg:
                self.kill_tooltip()

        if self.hover_time >= 6 and self.tooltip and not self.tooltip_bg:
            self.generate_tooltip()

    def kill_tooltip(self):
        self.tooltip_bg.kill()
        self.tooltip_bg = None
        self.hover_time = 0
        try:
            self.master.tooltip_description = ""
            self.master.tooltip_description_x = -3000
            self.master.tooltip_description_y = -3000
        except:
            pass

    def generate_tooltip(self):
        mouse_x, mouse_y = pg.mouse.get_pos()
        # conditions to force change anchor criteria
        horizontal_anchor = "left"
        vertical_anchor = "top"

        if mouse_x <= 400:
            horizontal_anchor = "left"
        elif mouse_x >= LANDSCAPE_WIDTH - 400:
            horizontal_anchor = "right"

        if mouse_y <= 400:
            vertical_anchor = "top"
        elif mouse_y >= LANDSCAPE_HEIGHT - 400:
            vertical_anchor = "bottom"

        if self.shortened_tooltip:
            self.tooltip_bg = Static(mouse_x, mouse_y, "sprite_sp_atk_tooltip_shortened.png",
                                     anchor=vertical_anchor + horizontal_anchor)
        else:
            self.tooltip_bg = Static(mouse_x, mouse_y, "sprite_sp_atk_tooltip.png",
                                     anchor=vertical_anchor + horizontal_anchor)

        try:
            self.master.priority_sprites.add(self.tooltip_bg)
            self.master.tooltip_description = self.tooltip
            self.master.tooltip_description_x = self.tooltip_bg.rect.left
            self.master.tooltip_description_y = self.tooltip_bg.rect.top
        except Exception as e:
            print(e)



class CityManagementButton(pg.sprite.Sprite):
    def __init__(self, city_management_scene, master, x, y, default_image_file, command, tooltip="", scale=None):
        pg.sprite.Sprite.__init__(self)
        self.default_filename = default_image_file
        self.default_image = pg.image.load(self.default_filename).convert_alpha()
        self.hover_image = pg.image.load(self.default_filename.replace(".png", "_selected.png")).convert_alpha()
        self.upgrading_image = pg.image.load(self.default_filename.replace(".png", "_upgrading.png")).convert_alpha()

        if scale:
            self.default_image = pg.transform.smoothscale(self.default_image, scale)
            self.hover_image = pg.transform.smoothscale(self.hover_image, scale)
            self.upgrading_image = pg.transform.smoothscale(self.upgrading_image, scale)

        self.city_management_scene = city_management_scene
        self.master = master

        self.image = self.default_image
        self.rect = self.image.get_rect()
        self.rect.centerx = x
        self.rect.centery = y

        self.hover_time = 0
        self.hovering = False
        self.upgrading = False
        self.disabled = False
        self.command = command
        self.tooltip_description = tooltip
        self.tooltip = None
        self.tooltip_sprites = []


    def update(self):

        if self.upgrading:
            self.image = self.upgrading_image

        if self.hovering:
            if not self.upgrading:
                self.image = self.hover_image
            self.hover_time += 1
        else:
            if not self.upgrading:
                self.image = self.default_image
            self.hover_time = 0
            if self.tooltip:
                self.kill_tooltip()

        if self.hover_time >= 6 and not self.tooltip:
            self.generate_tooltip()


    def generate_tooltip(self):
        mouse_x, mouse_y = pg.mouse.get_pos()
        tooltip_filename = "sprite_building_tooltip.png"
        self.tooltip_description = self.master.get_description()

        #conditions to force change anchor criteria
        horizontal_anchor = "left"
        vertical_anchor = "top"

        if mouse_x <= 400:
            horizontal_anchor = "left"
        elif mouse_x >= LANDSCAPE_WIDTH - 400:
            horizontal_anchor = "right"

        if mouse_y <= 400:
            vertical_anchor = "top"
        elif mouse_y >= LANDSCAPE_HEIGHT - 400:
            vertical_anchor = "bottom"

        self.tooltip = Static(mouse_x, mouse_y, tooltip_filename, anchor=vertical_anchor+horizontal_anchor)
        self.tooltip_sprites.append(self.tooltip)

        for sp in self.tooltip_sprites:
            self.city_management_scene.all_sprites.add(sp)
            self.city_management_scene.priority_sprites.add(sp)


    def kill_tooltip(self):
        for sp in self.tooltip_sprites:
            sp.kill()
        self.tooltip_sprites = []
        self.tooltip = None
        self.city_management_scene.tooltip_description = ""



##### CAMPAIGN MAP SPRITES ######
class CampaignMap(pg.sprite.Sprite):
    def __init__(self, main_game, x, y, anchor="center", world_map=False, source_image=None):
        pg.sprite.Sprite.__init__(self)
        self.main_game = main_game
        self.anchor = anchor
        self.map_width = 11776
        self.map_height = 9216
        if source_image:
            self.image = source_image
        else:
            self.image = pg.Surface((self.map_width,self.map_height))
            try:
                #self.image = self.main_game.campaign_map_image
                self.image_list = self.main_game.campaign_map_image_partitions
            except:
                #self.image = self.main_game.main_menu.campaign_map_image
                self.image_list = self.main_game.main_menu.campaign_map_image_partitions

            self.image.blits(blit_sequence=((self.image_list[i], (i%4*self.map_width//4, i//4*self.map_height//2)) for i in range(8)))

        if world_map:
            orig_img_width, orig_img_height = self.image.get_size()
            self.image = pg.transform.smoothscale(self.image, (int(orig_img_width*.25), int(orig_img_height*.25)))

        self.rect = self.image.get_rect()
        self.position_x, self.position_y = x,y


    def update(self):
        # self.position_x, self.position_y = self.grid_position_x * 32, self.grid_position_y * 32
        if self.anchor == "topleft":
            self.rect.topleft = (self.position_x, self.position_y)
        elif self.anchor == "topright":
            self.rect.topright = (self.position_x, self.position_y)
        elif self.anchor == "bottomleft":
            self.rect.bottomleft = (self.position_x, self.position_y)
        elif self.anchor == "bottomright":
            self.rect.bottomright = (self.position_x, self.position_y)
        elif self.anchor == "center":
            self.rect.center = (self.position_x, self.position_y)
        else:
            self.rect.x, self.rect.y = (self.position_x, self.position_y)


class CampaignMapSprite(pg.sprite.Sprite):
    def __init__(self, campaign_map_scene, x, y, filename, anchor="default", scale=None,
                 scale_percentage=1):
        pg.sprite.Sprite.__init__(self)
        self.campaign_map_scene = campaign_map_scene
        self.filename = filename
        self.image_default = pg.image.load(self.filename).convert_alpha()
        try:
            self.image_hovered = pg.image.load(self.filename.replace(".png", "_selected.png")).convert_alpha()
        except:
            self.image_hovered = pg.image.load(self.filename).convert_alpha()

        if scale:
            self.image_default = pg.transform.smoothscale(self.image_default, scale)
            self.image_hovered = pg.transform.smoothscale(self.image_hovered, scale)
        if scale_percentage != 1:
            orig_img_width, orig_img_height = self.image_default.get_size()
            self.iamge_default = pg.transform.smoothscale(self.image_default, (int(orig_img_width*scale_percentage), int(orig_img_height*scale_percentage)))
            self.image_hovered = pg.transform.smoothscale(self.image_hovered, (int(orig_img_width*scale_percentage), int(orig_img_height*scale_percentage)))

        self.image = self.image_default
        self.rect = self.image.get_rect()
        self.anchor = anchor
        self.hovering = False
        self.hover_time = 0
        self.tooltip_background = None
        self.grid_position_x, self.grid_position_y = x,y
        self.position_x, self.position_y = self.grid_position_x*int(32*scale_percentage), self.grid_position_y*int(32*scale_percentage)

    def update(self):
        #self.position_x, self.position_y = self.grid_position_x * 32, self.grid_position_y * 32
        if self.anchor == "topleft":
            self.rect.topleft = (self.position_x, self.position_y)
        elif self.anchor == "topright":
            self.rect.topright = (self.position_x, self.position_y)
        elif self.anchor == "bottomleft":
            self.rect.bottomleft = (self.position_x, self.position_y)
        elif self.anchor == "bottomright":
            self.rect.bottomright = (self.position_x, self.position_y)
        elif self.anchor == "center":
            self.rect.center = (self.position_x, self.position_y)
        else:
            self.rect.x, self.rect.y = (self.position_x, self.position_y)

        if self.hovering:
            self.image = self.image_hovered
            self.hover_time += 1
        else:
            self.image = self.image_default
            self.hover_time = 0
            if self.tooltip_background:
                self.tooltip_background.kill()
                self.tooltip_background = None
                self.campaign_map_scene.game_tooltip_description = ""

        if self.hover_time >= 6 and not self.tooltip_background:
            self.generate_tooltip()


    def generate_tooltip(self):
        mouse_x, mouse_y = pg.mouse.get_pos()
        self.tooltip_background = Static(mouse_x, mouse_y, "sprite_building_tooltip.png", anchor="topleft")
        self.campaign_map_scene.all_sprites.add(self.tooltip_background)
        self.campaign_map_scene.priority_sprites.add(self.tooltip_background)



class CampaignMapArmySprite(pg.sprite.Sprite):
    def __init__(self, campaign_map_scene, x, y, commander, army_composition,
                 action_points, retreat_penalty, embedded_in_city=None,
                 attack_target=None, defend_target=None,
                 anchor="midleft", scale_percentage=1, display_stars=True):
        pg.sprite.Sprite.__init__(self)
        self.campaign_map_scene = campaign_map_scene
        self.commander = commander
        self.army_composition = army_composition
        self.max_action_points = self.commander.action_points
        self.action_points = action_points
        if self.action_points == -1:
            self.action_points = self.max_action_points
        self.retreat_penalty = retreat_penalty #if retreat, action points for next turn is reduced
        self.selected = False
        self.embedded_in_city = embedded_in_city
        self.attack_target = attack_target
        self.defend_target = defend_target
        self.anchor = anchor
        self.display_stars = display_stars

        flag_country = self.commander.country.lower()
        if flag_country not in ["shu", "wei", "wu", "liu biao", "liu zhang", "ma teng", "zhang lu", "yuan shao"]:
            flag_country = "rogue"
        filename = "sprite_flag_icon_{}.png".format(flag_country)
        self.image_default = pg.image.load(filename).convert_alpha()
        self.image_selected = pg.image.load(filename.replace(".png", "_selected.png")).convert_alpha()
        if scale_percentage != 1:
            orig_img_width, orig_img_height = self.image_default.get_size()
            self.image_default = pg.transform.smoothscale(self.image_default, (int(orig_img_width*scale_percentage),int(orig_img_height*scale_percentage)))
            self.image_selected = pg.transform.smoothscale(self.image_selected, (int(orig_img_width*scale_percentage),int(orig_img_height*scale_percentage)))

        self.grid_position_x, self.grid_position_y = x, y
        self.position_x, self.position_y = self.grid_position_x*int(32*scale_percentage), self.grid_position_y*int(32*scale_percentage)
        self.image = self.image_default
        self.rect = self.image.get_rect()

        if self.display_stars:
            self.stars = []
            self.update_stars()


    def update_stars(self):
        while len(self.stars) < self.commander.level:
            star = CampaignMapSprite(self.campaign_map_scene, self.rect.right+self.commander.level//5*8,
                                     self.rect.top+self.commander.level%5*8+10,
                                     "sprite_army_general_star.png", anchor="topleft")
            self.stars.append(star)
            self.campaign_map_scene.all_sprites.add(star)
            self.campaign_map_scene.fixed_sprites.add(star)


    def update(self):
        #self.position_x, self.position_y = self.grid_position_x * 32, self.grid_position_y * 32
        if self.anchor == "topleft":
            self.rect.topleft = (self.position_x, self.position_y)
        elif self.anchor == "topright":
            self.rect.topright = (self.position_x, self.position_y)
        elif self.anchor == "bottomleft":
            self.rect.bottomleft = (self.position_x, self.position_y)
        elif self.anchor == "bottomright":
            self.rect.bottomright = (self.position_x, self.position_y)
        elif self.anchor == "center":
            self.rect.center = (self.position_x, self.position_y)
        elif self.anchor == "midbottom":
            self.rect.midbottom = (self.position_x, self.position_y)
        elif self.anchor == "midleft":
            self.rect.midleft = (self.position_x, self.position_y)
        else:
            self.rect.x, self.rect.y = (self.position_x, self.position_y)

        if self.selected:
            self.image = self.image_selected
        else:
            self.image = self.image_default

        # update star position
        if self.display_stars:
            for i in range(len(self.stars)):
                star = self.stars[i]
                star.rect.topleft = self.rect.right+i//5*8, self.rect.top+i%5*8+10


    def reset_state(self):
        if "patience" in self.commander.skills:
            if self.action_points <= 2*self.max_action_points:
                self.action_points += self.max_action_points - self.retreat_penalty
            else:
                self.action_points = 3*self.max_action_points - self.retreat_penalty
            print(self.commander.name, self.action_points)
        else:
            self.action_points = self.max_action_points - self.retreat_penalty
        self.retreat_penalty = 0
        if "red_hare" in self.commander.skills:
            self.action_points += 5
        if "quick_march" in self.commander.skills:
            self.action_points += int(self.action_points*.15)
        if self.commander.country == "Ma Teng":
            self.action_points += 2

        if self.action_points < 0:
            self.action_points = 0

        #get rid of any units in army composition where amount is 0
        units_to_delete = []
        for unit_type, amount in self.army_composition.items():
            if amount == 0:
                units_to_delete.append(unit_type)

        for unit_type in units_to_delete:
            del self.army_composition[unit_type]

        #update commander
        self.commander.update_state(self.embedded_in_city)
        if self.display_stars:
            self.update_stars()


    def switch_country(self, country, reset_army=True):
        self.commander.country = country
        self.commander.loyalty = 70
        if reset_army:
            self.army_composition = {}
        self.action_points = 0
        filename = "sprite_flag_icon_{}.png".format(self.commander.country.lower())
        self.image_default = pg.image.load(filename).convert_alpha()
        self.image_selected = pg.image.load(filename.replace(".png", "_selected.png")).convert_alpha()


    def kill_self(self):
        for star in self.stars:
            if star in self.campaign_map_scene.all_sprites:
                self.campaign_map_scene.all_sprites.remove(star)
            if star in self.campaign_map_scene.fixed_sprites:
                self.campaign_map_scene.fixed_sprites.remove(star)
            star.kill()
        self.kill()



class CampaignMapCitySprite(pg.sprite.Sprite):
    def __init__(self, campaign_map_scene, grid_position_x, grid_position_y, name, fortress_level, country,
                 population, population_growth, happiness, defender=None, anchor="center", raw_buildings={},
                 buildings=[], garrison = {}, units_recruited_this_turn=0, scale_percentage=1, effects={}):
        pg.sprite.Sprite.__init__(self)
        self.campaign_map_scene = campaign_map_scene
        self.grid_position_x = grid_position_x
        self.grid_position_y = grid_position_y
        self.name = name
        self.fortress_level = fortress_level
        self.country = country
        self.population = population
        self.population_growth = population_growth
        self.happiness = happiness
        self.defender = defender
        self.selected = False
        self.anchor = anchor

        if self.country.lower() not in ["shu", "wei", "wu", "liu biao", "liu zhang", "ma teng", "zhang lu"]:
            filename = "sprite_fortress_{}_icon_{}.png".format(self.fortress_level, "rogue")
        else:
            filename = "sprite_fortress_{}_icon_{}.png".format(self.fortress_level, self.country.lower())
        self.image_default = pg.image.load(filename).convert_alpha()
        if scale_percentage != 1:
            orig_img_width, orig_img_height = self.image_default.get_size()
            self.image_default = pg.transform.smoothscale(self.image_default, (int(orig_img_width*scale_percentage),int(orig_img_height*scale_percentage)))
        self.grid_position_x, self.grid_position_y = grid_position_x, grid_position_y
        self.position_x, self.position_y = self.grid_position_x*int(32*scale_percentage), self.grid_position_y*int(32*scale_percentage)
        self.image = self.image_default
        self.rect = self.image.get_rect()

        self.garrison = garrison
        self.units_recruited_this_turn = units_recruited_this_turn
        self.raw_buildings = raw_buildings
        self.buildings = buildings
        self.effects = effects.copy()
        if not self.buildings:
            self.generate_buildings()


    def generate_buildings(self):
        self.buildings = []
        for building, level in self.raw_buildings.items():
            self.buildings.append(Building(building, level))


    def update(self):
        #self.position_x, self.position_y = self.grid_position_x * 32, self.grid_position_y * 32
        if self.anchor == "topleft":
            self.rect.topleft = (self.position_x, self.position_y)
        elif self.anchor == "topright":
            self.rect.topright = (self.position_x, self.position_y)
        elif self.anchor == "bottomleft":
            self.rect.bottomleft = (self.position_x, self.position_y)
        elif self.anchor == "bottomright":
            self.rect.bottomright = (self.position_x, self.position_y)
        elif self.anchor == "center":
            self.rect.center = (self.position_x, self.position_y)
        elif self.anchor == "midbottom":
            self.rect.midbottom = (self.position_x, self.position_y)
        elif self.anchor == "midleft":
            self.rect.midleft = (self.position_x, self.position_y)
        else:
            self.rect.x, self.rect.y = (self.position_x, self.position_y)


    def estimate_projected_income(self):
        additional_income = 0
        tax_bonus = 1.0

        for building in self.buildings:
            if building.name == "Market":
                additional_income += int(building.level ** 1.5 * 100)
            elif building.name == "Gold Mine":
                additional_income += building.level ** 2 * 200
        for army_sprite in self.campaign_map_scene.army_sprites:
            if army_sprite.commander.country == self.country:
                if "commissioner_of_internal_affairs" in army_sprite.commander.skills:
                    tax_bonus += .03
            if army_sprite.commander.name == self.defender:
                if "wealthy_merchant" in army_sprite.commander.skills:
                    additional_income += 1000

        if "tax_reform" in self.campaign_map_scene.acquired_tech[self.country]:
            tax_bonus += .05

        projected_population, projected_happiness = self.calculate_projected_population_and_happiness()
        projected_tax = int(projected_population*self.campaign_map_scene.tax[self.country]*(10-self.campaign_map_scene.difficulty)/10)
        projected_income = projected_tax + additional_income
        return int(projected_income*tax_bonus)


    def calculate_projected_population_and_happiness(self):
        raw_population_growth = 0
        if self.country != self.campaign_map_scene.player_country:
            projected_happiness = self.campaign_map_scene.difficulty - 1
        else:
            projected_happiness = 0
        if self.country in self.campaign_map_scene.available_countries:
            food_storage = self.campaign_map_scene.loaded_game_data["food"][self.country]
        else:
            food_storage = 100

        if food_storage > 0:
            projected_happiness += int(log(food_storage, 3) / 2)
        else:
            projected_happiness += int(food_storage / 100 - 1)

        for army_sprite in self.campaign_map_scene.army_sprites:
            if army_sprite.commander.name == self.defender:
                projected_happiness += math.ceil((army_sprite.commander.charisma - 70)/5)
                if "incorruptible" in army_sprite.commander.skills:
                    projected_happiness += 5
                if "man_of_virtue" in army_sprite.commander.skills:
                    projected_happiness += 7
                if "benevolent_policies" in army_sprite.commander.skills:
                    projected_happiness += 3
                    raw_population_growth += 100
            if "confucian_schools" in army_sprite.commander.skills:
                projected_happiness += 1


        for building in self.buildings:
            if building.name == "Market":
                projected_happiness += building.level
            elif building.name == "Trade Port":
                raw_population_growth += building.level ** 2 * 10

        #city/campaign effects
        if "war" in self.effects:
            projected_happiness -= (6 + self.effects["war"]) #war sentiment slowly reduces over time
        if "natural_disaster_fire" in self.effects:
            projected_happiness -= 8
        if "natural_disaster_storm" in self.effects:
            projected_happiness -= 5
        if "natural_disaster_drought" in self.effects:
            projected_happiness -= 10
        if self.campaign_map_scene.loaded_game_data["dilemma_effects"]["famine"] > 0:
            projected_happiness -= 10
        if self.country == "Zhang Lu":
            projected_happiness += 3
        if "pacification" in self.campaign_map_scene.acquired_tech[self.country]:
            projected_happiness += 2

        projected_population = int(self.population*(1+(projected_happiness/200)**2)) + projected_happiness*(self.fortress_level*25) + raw_population_growth
        return projected_population, projected_happiness


    def update_state(self):

        self.population, self.happiness = self.calculate_projected_population_and_happiness()
        if self.population <= 500:
            self.population = 500
        if self.country == self.campaign_map_scene.player_country or self.campaign_map_scene.difficulty <= 1:
            #print(self.estimate_projected_income())
            self.campaign_map_scene.gold[self.country] += self.estimate_projected_income()
            if self.country == self.campaign_map_scene.player_country:
                self.campaign_map_scene.loaded_game_data["gold_produced"] += self.estimate_projected_income()
        else: #give income bonus to AI on harder difficulties
            self.campaign_map_scene.gold[self.country] += int(self.estimate_projected_income()*self.campaign_map_scene.difficulty/1.5)

        self.units_recruited_this_turn = 0

        #get rid of any units in garrison where amount is 0
        units_to_delete = []
        for unit_type, amount in self.garrison.items():
            if amount == 0:
                units_to_delete.append(unit_type)

        for unit_type in units_to_delete:
            del self.garrison[unit_type]

        #get rid of any effects that have expired
        effects_to_delete = []
        for effect in self.effects.keys():
            if self.effects[effect] <= 1:
                effects_to_delete.append(effect)
            else:
                self.effects[effect] -= 1

        for effect in effects_to_delete:
            del self.effects[effect]


    def captured(self, captor):
        self.country = captor.commander.country
        if self.country != "Shu" and "incorruptible" not in captor.commander.skills:
            penalty = .05 + .2*(100-captor.commander.charisma)/50
            self.effects["war"] = 4
            self.population = int(self.population*(1-penalty))

            if self.population <= 500:
                self.population = 500


        if self.country.lower() not in ["shu", "wei", "wu", "liu biao", "liu zhang", "ma teng", "zhang lu"]:
            filename = "sprite_fortress_{}_icon_rogue.png".format(self.fortress_level)
        else:
            filename = "sprite_fortress_{}_icon_{}.png".format(self.fortress_level, self.country.lower())
        self.image_default = pg.image.load(filename).convert_alpha()
        self.image = self.image_default
        self.rect = self.image.get_rect()
        self.garrison = {}

        if self.defender:
            #Retreat current defender
            for army_sprite in self.campaign_map_scene.army_sprites:
                if army_sprite.commander.name == self.defender:
                    random_retreat_direction = choice([(3,3), (-3,3), (3,-3), (-3,-3)])
                    army_sprite.grid_position_x += random_retreat_direction[0]
                    army_sprite.grid_position_y += random_retreat_direction[1]
                    army_sprite.position_x += random_retreat_direction[0]*32
                    army_sprite.position_y += random_retreat_direction[1]*32
                    army_sprite.embedded_in_city = None
                    self.garrison = {}

        #make captor new defender
        self.embed_defender(captor)

        #update happiness
        self.update_happiness()


    def update_happiness(self):
        population, self.happiness = self.calculate_projected_population_and_happiness()


    def embed_defender(self, defender):
        for army_sprite in self.campaign_map_scene.army_sprites:
            if army_sprite.commander.name == defender.commander.name:
                delta_x = self.grid_position_x - army_sprite.grid_position_x
                delta_y = self.grid_position_y - army_sprite.grid_position_y
                army_sprite.grid_position_x += delta_x
                army_sprite.grid_position_y += delta_y
                army_sprite.position_x += delta_x * 32
                army_sprite.position_y += delta_y * 32
                army_sprite.embedded_in_city = self.name
                self.defender = army_sprite.commander.name

                merged_army = concat_dicts(self.garrison, army_sprite.army_composition)
                army_sprite.army_composition = merged_army.copy()
                self.garrison = merged_army.copy()
                break