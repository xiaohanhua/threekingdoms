from battle import*
from custom_battle_recruitment import*
from functools import partial


class Custom_Battle_Settings_Scene:
    def __init__(self, parent_scene, player_general, enemy_general):
        self.parent_scene = parent_scene
        self.player_general = player_general
        self.enemy_general = enemy_general
        self.game_width, self.game_height = LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT
        if DEMO:
            self.screen = pg.display.set_mode((self.game_width, self.game_height))
        else:
            self.screen = pg.display.set_mode((self.game_width, self.game_height), pg.FULLSCREEN|pg.DOUBLEBUF)
        os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (50, 50)
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()

        pg.mixer.init()
        pg.font.init()
        self.hover_sound = pg.mixer.Sound("sfx_hover.ogg")
        self.click_sound = pg.mixer.Sound("sfx_click.ogg")


    def new(self):

        self.running = True
        self.playing = True
        pg.mouse.set_visible(True)

        # transitions
        self.transition_sprites = pg.sprite.Group()
        self.transition = None
        self.post_transition_function = None
        self.initiate_transition("in")

        self.all_sprites = pg.sprite.Group()
        self.buttons = pg.sprite.Group()
        self.terrain_buttons = []
        self.army_size_radio_buttons = []
        self.difficulty_radio_buttons = []

        self.background = Static(0, self.game_height - 650, "sprite_menu_bg.png")
        self.all_sprites.add(self.background)


        self.ui_bg = Static(LANDSCAPE_WIDTH // 2, LANDSCAPE_HEIGHT // 2 - 30, "sprite_battle_preview_bg.png", anchor="center")
        self.all_sprites.add(self.ui_bg)


        #buttons
        self.terrain_options = ["sand", "forest", "rock", "water", "random"]
        self.terrain_description = {"sand": "Normal: default battle terrain.",
                                    "forest": "Forest: Reduced damage from cavalry charges and ranged units. Increased damage from fire attacks.",
                                    "rock": "Rock:  Reduced damage from cavalry charges; reduced cavalry charge range.",
                                    "water": "Water: Narrow deployment slots. Shorter map width.",
                                    "random": "Random: a terrain will be chosen at random."}
        counter = 0
        for terrain in ["sand", "forest", "rock", "water", "random"]:
            terrain_button = Button(counter*150+300, 150, "sprite_terrain_selection_{}.png".format(terrain),
                                    command=partial(self.choose_terrain, counter))
            self.terrain_buttons.append(terrain_button)
            self.all_sprites.add(terrain_button)
            counter += 1

        self.selected_terrain = "sand"
        self.choose_terrain(0)

        self.back_button = Button(LANDSCAPE_WIDTH//2-300, LANDSCAPE_HEIGHT-30, "menu_button_back.png",
                                  command=self.return_to_parent_scene, scale=(210,50))
        self.buttons.add(self.back_button)
        self.all_sprites.add(self.back_button)

        self.next_button = Button(LANDSCAPE_WIDTH//2+300, LANDSCAPE_HEIGHT-30, "menu_button_next.png",
                                  command=self.load_recruitment_scene, scale=(210,50))
        self.buttons.add(self.next_button)
        self.all_sprites.add(self.next_button)


        #radiobuttons
        for i in range(4):
            radio_button = Button(300, i*50+350, "sprite_radio_button.png",
                                    command=partial(self.choose_army_size, i))
            self.army_size_radio_buttons.append(radio_button)
            self.all_sprites.add(radio_button)

        self.selected_army_size = 5000
        self.choose_army_size(0)

        for i in range(3):
            radio_button = Button(550, i*50+350, "sprite_radio_button.png",
                                    command=partial(self.choose_difficulty, i))
            self.difficulty_radio_buttons.append(radio_button)
            self.all_sprites.add(radio_button)

        self.selected_difficulty = 1
        self.choose_difficulty(0)

        self.run()


    def initiate_transition(self, direction):
        if direction == "in":
            self.transition = FadeBlackTransition(45, "in")
            self.transition_sprites.add(self.transition)
        else:
            self.transition = FadeBlackTransition(45, "out")
            self.transition_sprites.add(self.transition)


    def transition_clean_up(self):
        self.transition.kill()
        self.transition = None
        self.post_transition_function = None
        self.playing = False
        self.running = False
        for s in self.all_sprites:
            s.kill()


    def choose_terrain(self, index):
        self.selected_terrain = self.terrain_options[index]
        for i in range(len(self.terrain_buttons)):
            if i == index:
                self.terrain_buttons[i].hovering = True
            else:
                self.terrain_buttons[i].hovering = False


    def choose_army_size(self, index):
        self.selected_army_size = 5000 + 5000*index**2
        for i in range(len(self.army_size_radio_buttons)):
            if i == index:
                self.army_size_radio_buttons[i].hovering = True
            else:
                self.army_size_radio_buttons[i].hovering = False


    def choose_difficulty(self, index):
        self.selected_difficulty = index+1
        for i in range(len(self.difficulty_radio_buttons)):
            if i == index:
                self.difficulty_radio_buttons[i].hovering = True
            else:
                self.difficulty_radio_buttons[i].hovering = False


    def run(self):
        while self.playing:
            if self.running:
                self.clock.tick(FPS)
                self.events()
                self.update()
                self.draw()


    def events(self):
        pos = pg.mouse.get_pos()
        for button in self.buttons:
            if button.rect.collidepoint(pos):
                if pg.mouse.get_pressed()[0]:
                    button.pressed = True
                else:
                    button.pressed = False
                if not button.hovering:
                    button.hovering = True
                    self.hover_sound.play()
            else:
                button.pressed = False
                button.hovering = False

        for event in pg.event.get():
            if event.type == pg.QUIT:
                choice = pg.display.message_box("Quit",
                                                "Are you sure you wish to quit?",
                                                buttons=("Yes", "No"), return_button=1, escape_button=None
                                                )

                if choice == 0:
                    pg.quit()
                    sys.exit()

            if event.type == pg.MOUSEBUTTONUP:
                self.click_sound.play()
                for button in self.buttons:
                    if button.rect.collidepoint(pos):
                        button.pressed = False
                        button.command()
                        break
                for button in self.terrain_buttons + self.army_size_radio_buttons + self.difficulty_radio_buttons:
                    if button.rect.collidepoint(pos):
                        button.pressed = False
                        button.command()
                        break

            if event.type == pg.KEYUP:
                if event.key == pg.K_ESCAPE:
                    self.return_to_parent_scene()


    def return_to_parent_scene(self):
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = self.return_to_parent_scene
        elif not self.transition.fading:
            self.transition_clean_up()
            self.parent_scene.new()


    def load_recruitment_scene(self):
        if not self.transition:
            self.initiate_transition("out")
            self.post_transition_function = self.load_recruitment_scene
        elif not self.transition.fading:
            self.transition_clean_up()
            if self.selected_terrain == "random":
                self.selected_terrain = choice(["sand", "forest", "rock", "water"])
            scene = Custom_Battle_Recruitment_Scene(self, self.player_general, self.enemy_general, self.selected_terrain,
                                                    self.selected_army_size, self.selected_difficulty)
            scene.new()


    def update(self):
        self.all_sprites.update()
        if self.transition:
            if self.transition.fading:
                self.transition.update()
            else:
                if self.post_transition_function:
                    self.post_transition_function()
                else:
                    self.transition = None


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)

            self.draw_text("Choose battle terrain:", FONT_FUNCTION(DEFAULT_FONT, 20), YELLOW, LANDSCAPE_WIDTH//2,
                           50, align="center")

            self.draw_text(self.terrain_description[self.selected_terrain], FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                           LANDSCAPE_WIDTH//2, 225, align="center")

            self.draw_text("Army Size:", FONT_FUNCTION(DEFAULT_FONT, 20), YELLOW,
                           350, 300, align="center")
            self.draw_text("Difficulty:", FONT_FUNCTION(DEFAULT_FONT, 20), YELLOW,
                           600, 300, align="center")

            counter = 0
            for size in ["Small", "Medium", "Large", "Huge"]:
                self.draw_text(size, FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                               350, counter*50+350, align="midleft")
                counter += 1


            counter = 0
            for difficulty in ["Easy", "Normal", "Hard"]:
                self.draw_text(difficulty, FONT_FUNCTION(DEFAULT_FONT, DEFAULT_FONT_SIZE), WHITE,
                               600, counter*50+350, align="midleft")
                counter += 1

            self.transition_sprites.draw(self.screen)
            pg.display.flip()


    def draw_text(self, text, font, color, x, y, align="midtop"):
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        if align == "midleft":
            text_rect.midleft = (x, y)
        elif align == "topleft":
            text_rect.topleft = (x, y)
        else:
            text_rect.midtop = (x, y)
        self.screen.blit(text_surface, text_rect)


    def drawWrappedText(self, text, color, rect, font, aa=False, bkg=None):
        rect = pg.Rect(rect)
        y = rect.top
        lineSpacing = 10

        # get the height of the font
        fontHeight = font.size("Tg")[1]

        while text:
            i = 1

            # determine if the row of text will be outside our area
            if y + fontHeight > rect.bottom:
                break

            # determine maximum width of line
            while font.size(text[:i])[0] < rect.width and i < len(text):
                i += 1

            # if we've wrapped the text, then adjust the wrap to the last word
            if i < len(text):
                i = text.rfind(" ", 0, i) + 1

            # render the line and blit it to the surface
            if bkg:
                image = font.render(text[:i], 1, color, bkg)
                image.set_colorkey(bkg)
            else:
                image = font.render(text[:i], aa, color)

            self.screen.blit(image, (rect.left, y))
            y += fontHeight + lineSpacing

            # remove the text we just blitted
            text = text[i:]

        return text